package com.unilab.customerregistration.utilities.constants;

public class Constants {

    /**
     * SharedPreferences tags
     */
    public static final String PREF_NAME = "ocr_pref";
    public static final String PREFKEY_EMAIL = "email";
    public static final String PREFKEY_PASSWORD = "password";
    public static final String PREFKEY_BRANDNAME = "brandName";
    public static final String PREFKEY_BRANDCODE = "brandCode";
    public static final String PREFKEY_CAMPAIGNNAME = "campaignName";
    public static final String PREFKEY_FORMATTED_CAMPAIGN_EVENT_NAME = "formattedCampaignEventName";
    public static final String PREFKEY_CAMPAIGNCODE = "campaignCode";
    public static final String PREFKEY_EVENTNAME = "eventName";
    public static final String PREFKEY_EVENTCODE = "eventCode";
    public static final String PREFKEY_HEADER_TEXT = "headerText";
    public static final String PREFKEY_FOOTER_TEXT = "footerText";
    public static final String PREFKEY_GENERATED_URL = "generatedUrl";

    public static final String PREFKEY_FOREGROUND_COLOR = "foregroundColor";
    public static final String PREFKEY_BACKGROUND_COLOR = "backgroundColor";
    public static final String PREFKEY_HEADER_COLOR = "headerColor";
    public static final String PREFKEY_FOOTER_COLOR = "footerColor";
    public static final String PREFKEY_SUBMIT_BUTTON_COLOR = "submitButton";
    public static final String PREFKEY_SUBMIT_BUTTON_RADIUS = "submitButtonRadius";
    public static final String PREFKEY_SUBMIT_BUTTON_WIDTH = "submitButtonWidth";
    public static final String PREFKEY_MOBILE_LOGO = "mobile_logo";
    public static final String PREFKEY_PRELOADED_DATA_STATUS = "hasPreLoadedData";

    /**
     * Input category tags
     */
    public static final int CATEGORY_STANDARD_FIELDS = 0;
    public static final int CATEGORY_NON_STANDARD_FIELDS = 1;
    public static final int CATEGORY_ADDITIONAL_FIELDS = 2;

    /**
     * Design tags
     */
    public static final int SET_FOREGROUND_COLOR = 0;
    public static final int SET_BACKGROUND_COLOR = 1;
    public static final int SET_HEADER_COLOR = 2;
    public static final int SET_FOOTER_COLOR = 3;
    public static final int SET_SUBMIT_BUTTON_COLOR = 4;

    /**
     * Request Code
     */
    public static final int PICK_WEB_LOGO = 1000;
    public static final int PICK_MOBILE_LOGO = 1001;

    public static final int SET_WEB_LOGO_ALIGNMENT = 2000;
    public static final int SET_MOBILE_LOGO_ALIGNMENT = 2001;

    /**
     * API
     */
    public static final String API_WEB_BASE_URL3 = "http://reg.webqa.unilab.com.ph/";
    public static final String API_WEB_TOKEN = "48c38960947075c8345b5912a14106ab";

    public static final String API_DBM_BASE_URL = "http://161.202.200.30/dbm/api/";
    public static final String API_DBM_TOKEN = "48c38960947075c8345b5912a14106ab";
    public static final String DBM_HOSTNAME = "reg.webqa.unilab.com.ph";

    public static final String CMD_EVENT_AUTH_LOGIN = "authenticateLogin";
    public static final String CMD_EVENT_GET_BRAND = "getBrand";
    public static final String CMD_EVENT_GET_CAMPAIGN = "getCampaign";
    public static final String CMD_EVENT_GET_CAMPAIGN_EVENT = "getCampaignEvent";
    public static final String CMD_EVENT_GET_COUNTRY = "getCountry";
    public static final String CMD_EVENT_GET_REGION = "getRegion";
    public static final String CMD_EVENT_GET_PROVINCE = "getProvince";
    public static final String CMD_EVENT_GET_CITY = "getCity";
    public static final String CMD_EVENT_GET_SETTING = "getSetting";
    public static final String CMD_EVENT_UPDATE_SETTING = "updateSetting";
    public static final String CMD_EVENT_GET_INPUT = "getInput";
    public static final String CMD_EVENT_POST_INPUT = "postInput";
    public static final String CMD_EVENT_UPDATE_INPUT = "updateInput";
    public static final String CMD_EVENT_POST_GROUP = "postGroup";
    public static final String CMD_EVENT_GET_GROUP = "getGroup";
    public static final String CMD_EVENT_UPDATE_GROUP = "updateGroup";
    public static final String CMD_EVENT_DELETE_GROUP = "deleteGroup";
    public static final String CMD_EVENT_DBM_POST_FIELD = "postFields";
    public static final String CMD_EVENT_DBM_POST_REGISTRATION = "postRegistration";
    public static final String CMD_EVENT_DBM_UPDATE_REGISTRATION = "updateRegistration";
    public static final String CMD_EVENT_AUTHENTICATE_EMAIL = "authenticationEmail";
    public static final String CMD_EVENT_GET_CUSTOMER_DETAILS = "getCustomerDetails";
    public static final String CMD_EVENT_GET_DATA_SOURCE = "getInitiativeId";
    public static final String CMD_SENDING_BULK_REGISTRATION_DETAILS = "bulkRegistration";

    public static final int API_RESPONSE_OK = 200;
    public static final int API_RESPONSE_FORBIDDEN = 403;

    /**
     * Designs
     */
    public static final String LEFT_ALIGNMENT = "left";
    public static final String CENTER_ALIGNMENT = "center";
    public static final String RIGHT_ALIGNMENT = "right";

    public static final String FONT_ARIAL = "fonts/Arial.ttf";
    public static final String FONT_CALIBRI = "fonts/calibri.otf";
    public static final String FONT_CONSOLAS = "fonts/Consolas.ttf";
    public static final String FONT_HELVETICA = "fonts/Helvetica.ttf";
    public static final String FONT_TAHOMA = "fonts/Tahoma.ttf";
    public static final String FONT_COMICSANS = "fonts/comicsans.ttf";
    public static final String FONT_GARAMOND = "fonts/garamond.ttf";
    public static final String FONT_FREESTYLESCRIPT = "fonts/freescript.TTF";
    public static final String FONT_LUCIDACONSOLE = "fonts/lucidaconsole.ttf";
    public static final String FONT_MYRIAD = "fonts/myriad.otf";
    public static final String COPYING_LOGO = "0";
    public static final String UPLOAD_LOGO = "1";

    public static final int SETTING_TYPE_NONE = 0;
    public static final int SETTING_TYPE_TEXTCOLOR = 1;

    /**
     * Fragment tags
     */
    public static final int CONFIGURE_FRAGMENT = 0;
    public static final int DESIGN_FRAGMENT = 1;
    public static final int FORM_FRAGMENT = 2;
    public static final int PREVIEW_FRAGMENT = 3;
    public static final int BRAND_LIST_PROMPT = 4;
    public static final int CAMPAIGN_LIST_PROMPT = 5;
    public static final int EVENT_LIST_PROMPT = 6;

    /**
     * SMTP tags
     */
    public static final String SMTP_EMAIL_ADDRESS = "ulcrsystem@gmail.com";
    public static final String SMTP_PASSWORD = "p@ssw0rd123";

    /**
     * SavedInstance and RestoreInstance state tags
     */
    public static final String STATE_FRAGMENT_PAGE = "fragment_page";
    public static final String STATE_SELECTED_BRAND_CODE = "selected_brand_code";
    public static final String STATE_SELECTED_CAMPAIGN_CODE = "selected_campaign_code";
    public static final String STATE_SELECTED_EVENT_CODE = "selected_event_code";
    public static final String STATE_SELECTED_BRAND_NAME = "selected_brand_name";
    public static final String STATE_SELECTED_CAMPAIGN_NAME = "selected_campaign_name";
    public static final String STATE_SELECTED_EVENT_NAME = "selected_event_name";
    public static final String STATE_BRAND_LIST = "brand_list";
    public static final String STATE_CAMPAIGN_LIST = "campaign_list";
    public static final String STATE_EVENT_LIST = "event_list";
    public static final String STATE_SETTING_MODEL = "setting_model";
    public static final String STATE_IS_COPYING = "is_copying";
    public static final String STATE_HAS_WEB_LOGO = "has_web_logo";
    public static final String STATE_HAS_MOBILE_LOGO = "has_mobile_logo";
    public static final String STATE_DEFAULT_FOOTER_TEXT = "default_footer_text";
    public static final String STATE_LOGIN_TYPE = "login_type";
    public static final String STATE_LOGIN_EMAIL = "login_email";
    public static final String STATE_LOGIN_PASSWORD = "login_password";
    public static final String STATE_REGISTRATION_MODEL = "registration_model";
    public static final String STATE_REGISTRATION_EMAIL = "registration_email";
    public static final String STATE_MOBILE_INITIALIZATION_TOUCH = "mobile_initialization_touch";
    public static final String STATE_SECONDARY_MOBILE_INITIALIZATION_TOUCH = "secondary_mobile_initialization_touch";

    public static final String STATE_STANDARD_GROUPS_LIST = "standard_group_list";
    public static final String STATE_NON_STANDARD_GROUPS_LIST = "non_standard_group_list";
    public static final String STATE_CUSTOM_GROUPS_LIST = "custom_group_list";

    public static final String STATE_STANDARD_FIELDS_LIST = "standard_field_list";
    public static final String STATE_NON_STANDARD_FIELDS_LIST = "non_standard_field_list";
    public static final String STATE_CUSTOM_FIELDS_LIST = "custom_field_list";

    public static final String STATE_DIALOG_SUB_GROUP_VISIBILITY = "add_field_sub_group_visibility";
    public static final String STATE_INITIATIVE_TYPE = "initiative_type";

    public static final String STATE_COPY_DESIGN_STATUS = "copy_design_status";
    public static final String STATE_COPY_FORM_STATUS = "copy_form_status";

    /**
     * DialogFragment tags
     */
    public static final String PROGRESS_DIALOG_FRAGMENT = "ProgressDialogFragment";


    /**
     * Service tags
     */
    public static final String SERVICE_CHECK_REG_UPDATES = "checking_reg_updates";
    public static final String SERVICE_CHECK_REG_UPDATES_LOLLIPOP = "checking_reg_updates_lollipop";

    /**
     * Receiver tags
     */
    public static final String RECEIVER_SHOW_REG_UPDATES = "show_reg_updates";
    public static final String RECEIVER_SERVICE_WAS_KILLED = "service_was_killed";

    /**
     * Dialog action type tags
     */
    public static final String DIALOG_ACTION_NORMAL = "default";
    public static final String DIALOG_ACTION_SAVE_BRAND_FRAGMENT = "saveBrandFragment";
    public static final String DIALOG_ACTION_SAVE_DESIGN_FRAGMENT = "saveDesignFragment";
    public static final String DIALOG_ACTION_SAVE_FORM_FRAGMENT = "saveFormFragment";
    public static final String DIALOG_ACTION_ADD_FORM_FIELD = "addFormField";
    public static final String DIALOG_ACTION_UPDATE_FORM_FIELD = "updateFormField";
    public static final String DIALOG_ACTION_SAVE_REGISTRATION_DETAILS = "saveRegistrationDetails";
    public static final String DIALOG_ACTION_UPDATE_REGISTRATION_DETAILS = "updateRegistrationDetails";
    public static final String DIALOG_RESET_DESIGN_SETTINGS = "resetDesignSettings";
    public static final String DIALOG_RESET_FORM_PAGE = "resetFormPage";
    public static final String DIALOG_ACTION_UPDATE_REGISTRATION_DETAILS_LOCALLY = "updateRegistrationDetailsLocally";
    public static final String DIALOG_ACTION_SAVE_REGISTRATION_DETAILS_LOCALLY = "saveRegistrationDetailsLocally";
    public static final String DIALOG_ACTION_NO_DESIGN_CHANGES = "noDesignChanges";
    public static final String DIALOG_ACTION_NO_FORM_CHANGES = "noFormChanges";

    /**
     * Registration tags
     */
    public static final String PROMPT_EXISTING_REG_DETAILS = "promptExistingRegistrationData";
    public static final String UPDATE_REG_DETAILS = "updateRegistrationDetails";
    public static final String CREATE_REG_DETAILS = "createRegistrationDetails";

    /**
     * Form Sub-grouping API tags
     */
    public static final String TAG_DELETING_GROUP = "0";
    public static final String TAG_UPDATING_GROUP = "1";

}
