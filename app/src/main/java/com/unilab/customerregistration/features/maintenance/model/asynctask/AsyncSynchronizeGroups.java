package com.unilab.customerregistration.features.maintenance.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;

import java.util.List;

public class AsyncSynchronizeGroups extends AsyncTask<Void, Void, Boolean> {

    private final String TAG = this.getClass().getSimpleName();
    private DatabaseHelper mDatabaseHelper;

    private Context mContext;
    private List<GroupsModel> mGroupsModelArrayList;

    public AsyncSynchronizeGroups(Context context, List<GroupsModel> groupsModelArrayList) {
        Log.e(TAG, "AsyncSynchronizeGroups: ");
        mDatabaseHelper = new DatabaseHelper(context);

        mContext = context;
        mGroupsModelArrayList = groupsModelArrayList;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        return synchronizingGroups();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
    }

    private boolean synchronizingGroups() {

        for (GroupsModel groupsModel : mGroupsModelArrayList) {
            if (mDatabaseHelper.checkIfGroupExistById(groupsModel)) {
                Log.e(TAG, "synchronizingGroups: GroupID: " + groupsModel.getId()
                        + " GroupName: " + groupsModel.getName()
                        + " is exist");

                mDatabaseHelper.updateGroupById(groupsModel);
            } else {
                Log.e(TAG, "synchronizingGroups: GroupID: " + groupsModel.getId()
                        + " GroupName: " + groupsModel.getName()
                        + " is not exist");
                mDatabaseHelper.createGroup(groupsModel);
            }
        }

        return true;
    }
}
