package com.unilab.customerregistration.features.maintenance.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CampaignEventModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String code;

    @SerializedName("initiative_title")
    @Expose
    private String name;

    protected CampaignEventModel(Parcel in) {
        code = in.readString();
        name = in.readString();
    }

    public CampaignEventModel() {
    }

    public static final Creator<CampaignEventModel> CREATOR = new Creator<CampaignEventModel>() {
        @Override
        public CampaignEventModel createFromParcel(Parcel in) {
            return new CampaignEventModel(in);
        }

        @Override
        public CampaignEventModel[] newArray(int size) {
            return new CampaignEventModel[size];
        }
    };

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(name);
    }
}
