package com.unilab.customerregistration.features.dashboard.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.dashboard.presenter.DashboardContract;
import com.unilab.customerregistration.features.dashboard.model.ProvinceModel;
import com.unilab.customerregistration.features.dashboard.model.RegionModel;
import com.unilab.customerregistration.features.dashboard.model.api.DashboardApiProvider;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsyncFetchRegion extends AsyncTask<Void, Void, Void> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private DatabaseHelper mDatabaseHelper;
    private List<RegionModel> mRegionList;
    private DashboardContract mDashboardContract;

    public AsyncFetchRegion(Context context, List<RegionModel> regionList) {
        DialogHelper.init(context);
        mDatabaseHelper = new DatabaseHelper(context);

        mContext = context;
        mRegionList = regionList;

        mDashboardContract = (DashboardContract) context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        DialogHelper.getInstance().showProgressDialog(mContext, "Getting region data");

    }

    @Override
    protected Void doInBackground(Void... params) {
        for (RegionModel regionModel : mRegionList) {
            Log.e(TAG, "onResponse: " + regionModel.getRegionName());
            if (!mDatabaseHelper.checkRegionIdIfExist(regionModel)) {
                mDatabaseHelper.createRegion(regionModel);
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
//        DialogHelper.getInstance().hideProgressDialog(mContext);
        getProvinceAPI();
    }

    private void getProvinceAPI() {
        Log.e(TAG, "getProvinceAPI: ");
        Call<List<ProvinceModel>> getProvinceCall = DashboardApiProvider.getDashboardApiRoutes()
                .getProvinceList(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_GET_PROVINCE);
        getProvinceCall.enqueue(new Callback<List<ProvinceModel>>() {
            @Override
            public void onResponse(Call<List<ProvinceModel>> call, Response<List<ProvinceModel>> response) {
                Log.e(TAG, "onResponse: " + response.code());
                if (response.body() != null) {
                    List<ProvinceModel> provinceModelList = response.body();
//                    for (ProvinceModel provinceModel : provinceModelList) {
//                        Log.e(TAG, "onResponse: " + provinceModel.getProvinceName());
//                        mDatabaseHelper.createProvince(provinceModel);
//                    }
//                    getCityAPI();
                    new AsyncFetchProvince(mContext, provinceModelList).execute();
                } else {
                    DialogHelper.getInstance().hideProgressDialog(mContext);
                }
            }

            @Override
            public void onFailure(Call<List<ProvinceModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mDashboardContract.showAlertInfoDialog("Alert", mContext.getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(mContext);
            }
        });
    }
}
