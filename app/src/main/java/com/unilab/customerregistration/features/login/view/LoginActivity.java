package com.unilab.customerregistration.features.login.view;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.base.view.BaseActivity;
import com.unilab.customerregistration.features.dashboard.view.DashboardActivity;
import com.unilab.customerregistration.features.login.model.web.api.LoginApiProvider;
import com.unilab.customerregistration.features.maintenance.view.MaintenanceActivity;
import com.unilab.customerregistration.features.registration.view.ActivityRegistration;
import com.unilab.customerregistration.utilities.applicationcontroller.ApplicationController;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.network.RestManager;
import com.unilab.customerregistration.utilities.permissions.PermissionHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener, View.OnTouchListener {

    private final String TAG = this.getClass().getSimpleName();
    private EditText edtEmail, edtPassword;
    Button btnLogin;
    private String strLoginType;
    private RelativeLayout rlRootView;
    private ImageView imgCmsLogo, imgBackground;
    private DatabaseHelper mDatabaseHelper;
    private IntentFilter mIntentFilter;
    private BroadcastReceiver mBroadcastReceiver;

    public static boolean isActivityIsRunning = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        mDatabaseHelper = new DatabaseHelper(this);
        mDatabaseHelper  = ((ApplicationController) getApplicationContext()).getDatabaseInstance();
        SharedPrefHelper.init(this);
        RestManager.init();
        DialogHelper.init(this);

        setupViews();
        setupBroadcastReceiver();

        Log.e(TAG, "onCreate: " + (savedInstanceState == null));
        if (savedInstanceState == null) {
            strLoginType = getIntent().getAction();
//            edtEmail.setText("phpdeveloper7@unilab.com.ph");
//            edtPassword.setText("password");
        }

        PermissionHelper.verifyStoragePermissions(this);

    }

    private void setupViews() {
        imgBackground = (ImageView) findViewById(R.id.imgBackground);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        imgCmsLogo = (ImageView) findViewById(R.id.imgCmsLogo);

        Utils.loadImageFromDrawable(this, imgBackground, R.drawable.bg_crs);
        Utils.loadImageFromDrawable(this, imgCmsLogo, R.drawable.ic_crs_logo);

        rlRootView = (RelativeLayout) findViewById(R.id.rlRootView);

        /**Listeners*/
        btnLogin.setOnClickListener(this);
        rlRootView.setOnTouchListener(this);

    }

    private void setupBroadcastReceiver() {
        mIntentFilter = new IntentFilter(Constants.RECEIVER_SHOW_REG_UPDATES);
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case Constants.RECEIVER_SHOW_REG_UPDATES:
                        onShowRegUpdateDialog(intent);
                        break;
                }
            }
        };
        registerReceiver(mBroadcastReceiver, mIntentFilter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnLogin:
//                /**Temporary*/
//                String email = edtEmail.getText().toString();
//                SharedPrefHelper.getInstance().putBrandCodeToShared("467");
//                SharedPrefHelper.getInstance().putCampaignCodeToShared("1500611830");
//                SharedPrefHelper.getInstance().putEventCodeToShared("118");
//                SharedPrefHelper.getInstance().putEmailToShared(email);
//                startActivity(new Intent(this, MaintenanceActivity.class));
//                finish();

                String email = edtEmail.getText().toString();
                String password = edtPassword.getText().toString();
                if (email.isEmpty() && password.isEmpty() || email.isEmpty() || password.isEmpty()) {
                    onShowAlertDialog("Alert", "Please enter Email address and Password!", Constants.DIALOG_ACTION_NORMAL);
                } else {
                    callLoginAPI();
                }

                break;
        }
    }

    private void callLoginAPI() {

        DialogHelper.getInstance().hideProgressDialog(this);
        DialogHelper.getInstance().showProgressDialog(this, "Login In...");
        final String email = edtEmail.getText().toString();
        final String password = edtPassword.getText().toString();
        final String brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        final String campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();

        Call<ResponseBody> onLoginCall = LoginApiProvider
                .getLoginApiRoutes()
                .onGetLoginStatus(
                        Constants.API_DBM_TOKEN,
                        Constants.CMD_EVENT_AUTH_LOGIN,
                        email,
                        password,
                        Constants.DBM_HOSTNAME);
        onLoginCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e(TAG, "onResponse: " + response.code());
                Log.e(TAG, "onResponse: " + response.message());

                switch (response.code()) {
                    case Constants.API_RESPONSE_OK:
                        if (response.body() != null) {
                            try {
                                String responseBody = response.body().string();
                                Log.e(TAG, "onResponse: " + responseBody);
                                switch (responseBody) {
                                    case "1":
                                        Log.e(TAG, "onResponse: " + "Login Success");
                                        SharedPrefHelper.getInstance().putEmailToShared(email);
                                        SharedPrefHelper.getInstance().putPasswordToShared(password);
                                        if (brandCode.isEmpty() || campaignCode.isEmpty()) {
                                            startActivity(new Intent(LoginActivity.this, MaintenanceActivity.class));
                                        } else {
                                            if (strLoginType.equals("register"))
                                                startActivity(new Intent(LoginActivity.this, ActivityRegistration.class));
                                            else
                                                startActivity(new Intent(LoginActivity.this, MaintenanceActivity.class));
                                        }
                                        finish();
                                        break;

                                    case "2":
                                        onShowAlertDialog("Alert", "User is not registered, please request admin assistance.", Constants.DIALOG_ACTION_NORMAL);
                                        break;

                                    case "3":
                                        onShowAlertDialog("Alert", "Login failed. Account doesn't exist.", Constants.DIALOG_ACTION_NORMAL);
                                        break;

                                    default:
                                        onShowAlertDialog("Alert", "User is not registered, please request admin assistance.", Constants.DIALOG_ACTION_NORMAL);
                                        break;
                                }
                            } catch (IOException e) {
                                Log.e(TAG, "onResponse: " + e.getMessage());
                                onShowAlertDialog("Alert", e.getMessage(), Constants.DIALOG_ACTION_NORMAL);
                            }
                        }
                        break;

                    case Constants.API_RESPONSE_FORBIDDEN:
                        onShowAlertDialog("Alert", "Connection has restriction", Constants.DIALOG_ACTION_NORMAL);
                        break;

                    default:
                        onShowAlertDialog("Alert", response.message(), Constants.DIALOG_ACTION_NORMAL);
                        break;
                }

                DialogHelper.getInstance().hideProgressDialog(LoginActivity.this);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                onShowAlertDialog("Alert", getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(LoginActivity.this);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityIsRunning = true;
        String email = SharedPrefHelper.getInstance().getEmailFromShared();
        String password = SharedPrefHelper.getInstance().getPasswordFromShared();

        if (!email.isEmpty() || !password.isEmpty()) {
            edtEmail.setText(email);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.rlRootView:
                Utils.hideKeyboard(this);
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, DashboardActivity.class));
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActivityIsRunning = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Constants.STATE_LOGIN_TYPE, strLoginType);
        outState.putString(Constants.STATE_LOGIN_EMAIL, edtEmail.getText().toString());
        outState.putString(Constants.STATE_LOGIN_PASSWORD, edtPassword.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        Log.e(TAG, "onRestoreInstanceState: " + (savedInstanceState == null));
        if (savedInstanceState != null) {
            strLoginType = savedInstanceState.getString(Constants.STATE_LOGIN_TYPE);
            String email = savedInstanceState.getString(Constants.STATE_LOGIN_EMAIL);
            String password = savedInstanceState.getString(Constants.STATE_LOGIN_PASSWORD);

            edtEmail.setText(email);
            edtPassword.setText(password);
        }

    }
}
