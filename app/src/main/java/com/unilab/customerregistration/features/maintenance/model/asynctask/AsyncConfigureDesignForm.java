package com.unilab.customerregistration.features.maintenance.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.model.InputModel;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.maintenance.model.web.api.MaintenanceApiProvider;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.features.maintenance.view.MaintenanceActivity;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.network.NetworkUtils;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsyncConfigureDesignForm extends AsyncTask<Void, Void, Void> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private DatabaseHelper mDatabaseHelper;
    private MaintenancePresenter mMaintenancePresenter;
    private String brandCode, campaignCode, eventCode, formattedEventName;

    public AsyncConfigureDesignForm(Context context) {
        Log.e(TAG, "AsyncConfigureDesignForm: ");
        mDatabaseHelper = new DatabaseHelper(context);
        mMaintenancePresenter = (MaintenancePresenter) context;
        mContext = context;

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();
        formattedEventName = SharedPrefHelper.getInstance().getFormattedCampaignNameFromShared();
    }

    @Override
    protected void onPreExecute() {
        DialogHelper.getInstance().hideProgressDialog(mContext);
        DialogHelper.getInstance().showProgressDialog(mContext, "Synchronize settings");
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        callGetSettingAPI();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    private void callGetSettingAPI() {
        Log.e(TAG, "callGetSettingAPI: ");
        Call<List<SettingsModel>> getSettingCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .getSetting(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_GET_SETTING, campaignCode, brandCode,
                        eventCode, formattedEventName);
        getSettingCall.enqueue(new Callback<List<SettingsModel>>() {
            @Override
            public void onResponse(Call<List<SettingsModel>> call, Response<List<SettingsModel>> response) {
                int responseCode = response.code();
                Log.e(TAG, "onResponse: " + responseCode);
                switch (responseCode) {
                    case Constants.API_RESPONSE_OK:
                        List<SettingsModel> settingsModelList = response.body();
                        Log.e(TAG, "onResponse: " + settingsModelList.size());

                        if (settingsModelList.size() > 0) {
                            for (SettingsModel settingsModel : settingsModelList) {
                                if (mDatabaseHelper.checkSettingsIfExist(brandCode, campaignCode,
                                        eventCode, formattedEventName)) {
                                    Log.e(TAG, "onResponse: " + "DB Setting exist");
                                    mDatabaseHelper.updateDbSetting(settingsModel);
                                    Log.e(TAG, "onResponse: ");
                                } else {
                                    Log.e(TAG, "onResponse: " + "DB Setting not exist");
                                    mDatabaseHelper.createDbSetting(settingsModel);
                                }
                            }
                        }

                        syncGroups(true);
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<SettingsModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", t.getMessage(), Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(mContext);
            }
        });
    }

    private void syncGroups(final boolean isSynchronizingForm) {
        Log.e(TAG, "syncGroups: ");
        Call<List<GroupsModel>> syncGroupsCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .getGroups(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_GET_GROUP, campaignCode, brandCode, eventCode);
        syncGroupsCall.enqueue(new Callback<List<GroupsModel>>() {
            @Override
            public void onResponse(Call<List<GroupsModel>> call, Response<List<GroupsModel>> response) {
                if (response.body() != null) {
                    List<GroupsModel> groupsModelList = response.body();
                    for (GroupsModel groupsModel : groupsModelList) {
                        groupsModel.setStatus("0");
                    }
                    new AsyncSynchronizeGroups(mContext, groupsModelList).execute();

                }
                syncFields(isSynchronizingForm);
            }

            @Override
            public void onFailure(Call<List<GroupsModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: syncGroups: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", t.getMessage(), Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(mContext);
            }
        });
    }

    private void syncFields(final boolean isSynchronizingForm) {
        Log.e(TAG, "syncFields: ");
        Call<List<InputModel>> syncFieldsCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .getFields(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_GET_INPUT, campaignCode, brandCode, eventCode);
        syncFieldsCall.enqueue(new Callback<List<InputModel>>() {
            @Override
            public void onResponse(Call<List<InputModel>> call, Response<List<InputModel>> response) {

                if (response.body() != null) {
                    List<InputModel> inputModelList = response.body();
                    Log.e(TAG, "onResponse: InputFields Size: " + inputModelList.size());
                    if (inputModelList.size() > 0) {
                        new AsyncSynchronizeFields(MaintenanceActivity.mainActivityContext, inputModelList,
                                isSynchronizingForm, true).execute();
                    } else {
                        DialogHelper.getInstance().hideProgressDialog(mContext);

                        /**
                         * Note: Below lines will configuring the form fields in web
                         * if there is no default fields assigned in the
                         * selected brand, campaign and initiative title
                         *
                         * */
                        List<GroupsModel> groupsModelList = mDatabaseHelper.getAllGroupsByBrandCampaignEvent(brandCode, campaignCode, eventCode);
                        List<FieldsModel> fieldsModelList = mDatabaseHelper.getAllFormFields(brandCode, campaignCode, eventCode);

                        if (NetworkUtils.isNetworkConnected(mContext)) {
                            new AsyncCopyingFormFields(mContext, fieldsModelList, groupsModelList, true, true).execute();

                        } else {
                            mMaintenancePresenter.showAlertDialog("Alert", "Error", Constants.DIALOG_ACTION_NORMAL);
                        }

                    }
                } else {
                    Log.e(TAG, "onResponse: " + "No response");
                    DialogHelper.getInstance().hideProgressDialog(mContext);
                }
            }

            @Override
            public void onFailure(Call<List<InputModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: syncFields: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", t.getMessage(), Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(mContext);

            }
        });
    }

}
