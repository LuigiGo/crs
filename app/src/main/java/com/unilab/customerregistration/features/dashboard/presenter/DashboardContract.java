package com.unilab.customerregistration.features.dashboard.presenter;

public interface DashboardContract {

    void showAlertInfoDialog(String title, String message, String actionType);

}
