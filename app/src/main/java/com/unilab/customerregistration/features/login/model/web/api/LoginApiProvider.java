package com.unilab.customerregistration.features.login.model.web.api;


import com.unilab.customerregistration.utilities.network.RestManager;

import retrofit2.Retrofit;

public class LoginApiProvider {

    public static LoginApiRoutes getLoginApiRoutes() {
        Retrofit mRetrofit = RestManager.getInstance().getDBMRetrofitAdapter();
        return mRetrofit.create(LoginApiRoutes.class);
    }
}

