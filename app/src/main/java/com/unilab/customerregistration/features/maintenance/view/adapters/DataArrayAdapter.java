package com.unilab.customerregistration.features.maintenance.view.adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.util.List;

public class DataArrayAdapter extends RecyclerView.Adapter<DataArrayAdapter.ViewHolder> {

    private String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private List<DataArray> mDataArrayList;
    boolean isAdapterInitializing = true;

    public DataArrayAdapter(Context context, List<DataArray> dataArrayList) {
        mContext = context;
        mDataArrayList = dataArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_input_type_options, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final DataArray dataArray = mDataArrayList.get(position);

        if (position == 0) {
            holder.imgAddMinusField.setImageResource(R.drawable.ic_add);
        } else {
            holder.imgAddMinusField.setImageResource(R.drawable.ic_minus);
        }

        /**Cheking for data array items has value*/
        if (!isAdapterInitializing) {
            if (dataArray.getValue().isEmpty()) {
                holder.edtFieldValue.setBackgroundResource(R.drawable.bg_edittext_red_border);
            } else {
                holder.edtFieldValue.setBackgroundResource(R.drawable.bg_edittext_white_border);
            }
        }

        holder.txtTitle.setText(dataArray.getTitle());
        holder.txtValue.setText(dataArray.getValue());
        holder.edtFieldValue.setText(dataArray.getValue());

        holder.edtFieldValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String value = s.toString();
                dataArray.setValue(value);
                dataArray.setTitle(value);
                dataArray.setName(value.replace(" ", "_"));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.imgAddMinusField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAdapterInitializing = false;
                if (position == 0) {
                    if (isDataArrayHasItemsEmpty()) {
                        Log.e(TAG, "onClick: " + "No value");

                    } else {
                        Log.e(TAG, "onClick: " + "Has value");
                        DataArray dataArray = new DataArray();
                        dataArray.setTitle("");
                        dataArray.setName("");
                        dataArray.setValue("");
                        dataArray.setIsActive("1");
                        dataArray.setDateTime(Utils.getDateTime());
                        mDataArrayList.add(dataArray);
                        isAdapterInitializing = true;
                    }
                } else {
                    Log.e(TAG, "onClick: " + "Remove item");
                    mDataArrayList.remove(position);
                }
                notifyDataSetChanged();
            }
        });

    }

    private boolean isDataArrayHasItemsEmpty() {
        boolean hasEmptyValues = false;
        for (int i = 0; i < mDataArrayList.size(); i++) {
            DataArray dataArray = mDataArrayList.get(i);
            if (dataArray.getValue().isEmpty()) {
                hasEmptyValues = true;
            }
        }
        return hasEmptyValues;
    }

    @Override
    public int getItemCount() {
        return mDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtTitle, txtValue;
        private EditText edtFieldValue;
        private ImageView imgAddMinusField;

        public ViewHolder(View v) {
            super(v);
            setIsRecyclable(false);
            txtTitle = (TextView) v.findViewById(R.id.txtTitle);
            txtValue = (TextView) v.findViewById(R.id.txtValue);

            edtFieldValue = (EditText) v.findViewById(R.id.edtFieldValue);
            imgAddMinusField = (ImageView) v.findViewById(R.id.imgAddMinusField);

        }
    }
}
