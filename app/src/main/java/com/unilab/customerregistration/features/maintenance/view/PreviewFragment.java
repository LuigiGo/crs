package com.unilab.customerregistration.features.maintenance.view;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.DefaultSettingModel;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.features.registration.model.RegistrationModel;
import com.unilab.customerregistration.features.registration.model.asynctask.AsyncSendRegistration;
import com.unilab.customerregistration.features.registration.model.asynctask.AsyncUpdateRegistration;
import com.unilab.customerregistration.features.registration.model.web.api.RegistrationApiProvider;
import com.unilab.customerregistration.features.registration.presenter.RegistrationContract;
import com.unilab.customerregistration.features.registration.view.ActivityRegistration;
import com.unilab.customerregistration.utilities.captchagenerator.CaptchaImageView;
import com.unilab.customerregistration.utilities.checkers.CheckerUtils;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.converters.ConverterUtil;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.network.NetworkUtils;
import com.unilab.customerregistration.utilities.network.RestManager;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PreviewFragment extends Fragment implements View.OnClickListener,
        AdapterView.OnItemSelectedListener, RegistrationContract, View.OnTouchListener {

    private static boolean isEmailHasPreviousFocus = false;
    private final String TAG = this.getClass().getSimpleName();
    ScrollView svRegForm;
    List<FieldsModel> fieldsModelList;
    Button btnSubmit, btnReset;
    List<String> countryList, regionList, provinceList, cityList, groupList;
    ArrayAdapter<String> countryAdapter, regionAdapter, provinceAdapter, cityAdapter;
    String emailAddress = null, password = "", confirm_pass = "";
    ImageView imgRegistrationLogo, imgRefreshCaptcha, imgBackground;
    EditText edtCaptchaInput;
    int orientation = 0;
    private LinearLayout llFieldsContainer, llDefaults, llCaptchaContainer, llSubmitButtonContainer;
    private DatabaseHelper mDatabaseHelper;
    private String brandCode, campaignCode, eventCode, brandName, eventName;
    private List<RegistrationModel> registrationModelList;
    private RelativeLayout rlRootView, rlHeader;
    private TextView txtFooter, txtSubscription;
    private SettingsModel mSettingsModel;
    private GradientDrawable gd;
    private CaptchaImageView captchaImageView;
    private Typeface tfBase, tfHeadFamily;
    private CheckBox chAgreement, chSubscription;
    private boolean isFieldIsValid = true, isEmailIsValid = true, isPasswordMatch = true;
    private AlertDialog mAlertInfoDialog, mAlertAgreementPromptDialog,
            mAlertUpdateExistingDataDialog, mAlertPromptExistingDataDialog;
    private Handler mTextWatcherHandler;
    private Runnable mTextWatcherRunnable;
    private IntentFilter mIntentFilter;
    private BroadcastReceiver mBroadcastReceiver;
    private boolean emailValidationEnabled = false, isUpdatingDetails = false,
            isDisplayingExistingData = false, isFieldsIsValidating = false,
            isMobileNumberInitializedTouch = false, isSecondaryMobileNumberInitializedTouch = false,
            isMobileNumberValid = true, isSecondaryMobileNumberValid = true;
    private TextView txtPreviewPageNote;
    private MaintenancePresenter mMaintenancePresenter;
    private RegistrationContract mRegistrationContract;
    private RegistrationContract.DialogListener mRegistrationContractDialogListener;
    private View mView;

    private DefaultSettingModel mDefaultSettingModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.activity_registrations, container, false);
        mDatabaseHelper = new DatabaseHelper(getActivity());
        RestManager.init();
        SharedPrefHelper.init(getActivity());
        DialogHelper.init(getActivity());

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();
        brandName = SharedPrefHelper.getInstance().getBrandNameFromShared();
        eventName = SharedPrefHelper.getInstance().getFormattedCampaignNameFromShared();

        mMaintenancePresenter = (MaintenancePresenter) getActivity();
        mRegistrationContract = (RegistrationContract) getActivity();
        mRegistrationContractDialogListener = (RegistrationContract.DialogListener) getActivity();

        /**Initialize default settings*/
        mDefaultSettingModel = mDatabaseHelper.getDefaultSettingsByBrand(brandCode);

        setupViews();
        setupFormSettings();
        setupPreloadedDatalist();
        displayActiveFields();

        /**Note: Disabling all fields if email is not validated*/
        emailAddress = null;
        setFieldsActivation(false);
        if (savedInstanceState == null) {
            emailValidationEnabled = true;
        }

        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        /**Setting Agreement text and privacy policy*/
        if (brandName.toLowerCase().equals("health+")) {
            brandName = "Unilab Health+";
        }

        String strAgreement = "<html>" +
                "<head><title></title></head>" +
                "<body>" +
                "I want to receive exclusive offers and updates from " +
                "<strong>" + brandName + "</strong>" +
                " and UNILAB" +
                "</body>" +
                "</html>";

        String strPrivacyPolicy = "<html>" +
                "<head><title></title></head>" +
                "<body>" +
                "I have read and understood the <a href=\"\">Privacy Policy</a>." +
                "</body>" +
                "</html>";

        chAgreement.setText(Html.fromHtml(strAgreement));
        txtSubscription.setText(Html.fromHtml(strPrivacyPolicy));

        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    captchaImageView.regenerate();
                }
            }, 3000);
        } catch (Exception e) {
            Log.e(TAG, "onStart: " + e.getMessage());

        }
    }

    private void setupFormSettings() {
        mSettingsModel = mDatabaseHelper.getSettingsConfig(brandCode, campaignCode, eventCode);
        Log.e(TAG, "setupFormSettings-0: " + mSettingsModel.toString());
        if (mSettingsModel != null) {
            rlHeader.setBackgroundColor(ConverterUtil.hexColorToInt(mSettingsModel.getHeader(), TAG, Constants.SETTING_TYPE_NONE));
            if (mSettingsModel.getBackground() == null || mSettingsModel.getBackground().isEmpty()) {
                Utils.loadImageFromDrawable(getActivity(), imgBackground, R.drawable.bg_crs);
            } else {
                imgBackground.setBackgroundColor(ConverterUtil.hexColorToInt(mSettingsModel.getBackground(), TAG, Constants.SETTING_TYPE_NONE));
            }
            txtFooter.setBackgroundColor(ConverterUtil.hexColorToInt(mSettingsModel.getFooter(), TAG, Constants.SETTING_TYPE_NONE));

            /**Text color*/
            tfHeadFamily = Typeface.createFromAsset(getActivity().getAssets(),
                    Utils.getDefaultTypeface(mSettingsModel.getFontHeadfamily()));
            tfBase = Typeface.createFromAsset(getActivity().getAssets(),
                    Utils.getDefaultTypeface(mSettingsModel.getFontBasefamily()));
            chAgreement.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            chSubscription.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            edtCaptchaInput.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            btnSubmit.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            btnReset.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtFooter.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtFooter.setText(mSettingsModel.getFooterText());

            /**Typeface*/
            chAgreement.setTypeface(tfBase);
            chSubscription.setTypeface(tfBase);
            edtCaptchaInput.setTypeface(tfBase);
            btnSubmit.setTypeface(tfBase);
            btnReset.setTypeface(tfBase);
            txtFooter.setTypeface(tfBase);


            /**Logo*/
            if (mSettingsModel.getMobLogo() != null) {
                switch (mSettingsModel.getMobLogo()) {
                    case Constants.LEFT_ALIGNMENT:
                        rlHeader.setGravity(Gravity.LEFT);
                        break;

                    case Constants.CENTER_ALIGNMENT:
                        rlHeader.setGravity(Gravity.CENTER);
                        break;

                    case Constants.RIGHT_ALIGNMENT:
                        rlHeader.setGravity(Gravity.RIGHT);
                        break;
                }
            }

            Log.e(TAG, "setupFormSettings-1: " + mSettingsModel.getMobLogoImg().isEmpty());
            if (mSettingsModel.getMobLogoImg() != null && !mSettingsModel.getMobLogoImg().isEmpty()) {
                if ((mSettingsModel.getMobLogoImg().startsWith("content://media/"))) {
                    Glide.with(this)
                            .load(mSettingsModel.getMobLogoImg())
                            .placeholder(R.drawable.ic_crs_logo)
                            .diskCacheStrategy(DiskCacheStrategy.RESULT)
                            .dontAnimate()
                            .into(imgRegistrationLogo);
                    Log.e(TAG, "setupFormSettings-2: " + mSettingsModel.getMobLogoImg());
                } else {
                    Glide.with(this)
                            .load(Constants.API_WEB_BASE_URL3 + mSettingsModel.getMobLogoImg())
                            .placeholder(R.drawable.ic_crs_logo)
                            .diskCacheStrategy(DiskCacheStrategy.RESULT)
                            .dontAnimate()
                            .into(imgRegistrationLogo);
                    Log.e(TAG, "setupFormSettings-3: " + mSettingsModel.getMobLogoImg());
                }
            } else {
                Log.e(TAG, "setupFormSettings-4: " + mSettingsModel.getMobLogoImg());
                Glide.with(this)
                        .load(mDefaultSettingModel.getImage())
                        .placeholder(R.drawable.ic_crs_logo)
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.RESULT)
                        .into(imgRegistrationLogo);
            }

            /**Setting up submit button*/
            gd.setShape(GradientDrawable.RECTANGLE);
            gd.setCornerRadius(Integer.parseInt(mSettingsModel.getSubmitBorderRadius()));
            gd.setColor(ConverterUtil.hexColorToInt(mSettingsModel.getSubmitBackground(), TAG, Constants.SETTING_TYPE_NONE));
            if (mSettingsModel.getSubmitBorderWidth().isEmpty() || mSettingsModel.getSubmitBorderWidth().equals("0")) {
                gd.setStroke(Integer.parseInt("3"),
                        ConverterUtil.hexColorToInt("", TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            } else {
                gd.setStroke(Integer.parseInt(mSettingsModel.getSubmitBorderWidth()),
                        ConverterUtil.hexColorToInt("", TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            }
            btnReset.setBackgroundDrawable(gd);

            switch (mSettingsModel.getSubmitAlignment()) {
                case Constants.LEFT_ALIGNMENT:
                    llSubmitButtonContainer.setGravity(Gravity.LEFT);
                    break;

                case Constants.CENTER_ALIGNMENT:
                    llSubmitButtonContainer.setGravity(Gravity.CENTER);
                    break;

                case Constants.RIGHT_ALIGNMENT:
                    llSubmitButtonContainer.setGravity(Gravity.RIGHT);
                    break;
            }
        }
    }

    private void setupPreloadedDatalist() {
        countryList = new ArrayList<>();
        countryList.add("--SELECT COUNTRY--");
        countryList.add("PHILIPPINES");
        countryAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_list_item, countryList) {

            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                final View v = super.getView(position, convertView, parent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, "run: onItemSelected: " + (mSettingsModel.getForeground() == null));
                        if (tfBase != null && mSettingsModel.getForeground() != null) {
                            ((TextView) v).setTypeface(tfBase);
                            ((TextView) v).setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                            v.setPadding(0, 0, 20, 0);
                        }
                        ((TextView) v).setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
                    }
                }, 50);

                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                final View v = super.getDropDownView(position, convertView, parent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, "run: onItemSelected: " + (mSettingsModel.getForeground() == null));
                        if (tfBase != null && mSettingsModel.getForeground() != null) {
                            ((TextView) v).setTypeface(tfBase);
                            ((TextView) v).setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                        }
                        ((TextView) v).setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
                        ((TextView) v).setSingleLine(false);

                    }
                }, 50);
                return v;
            }
        };
        regionList = new ArrayList<>();
        regionList.add("--SELECT REGION--");
        regionList.addAll(mDatabaseHelper.getRegionList());
        regionAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_list_item, regionList) {

            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                final View v = super.getView(position, convertView, parent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, "run: onItemSelected: " + (mSettingsModel.getForeground() == null));
                        if (tfBase != null && mSettingsModel.getForeground() != null) {
                            ((TextView) v).setTypeface(tfBase);
                            ((TextView) v).setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                            v.setPadding(0, 0, 20, 0);
                        }
                        ((TextView) v).setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
                    }
                }, 50);

                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                final View v = super.getDropDownView(position, convertView, parent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, "run: onItemSelected: " + (mSettingsModel.getForeground() == null));
                        if (tfBase != null && mSettingsModel.getForeground() != null) {
                            ((TextView) v).setTypeface(tfBase);
                            ((TextView) v).setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                        }
                        ((TextView) v).setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
                        ((TextView) v).setSingleLine(false);

                    }
                }, 50);
                return v;
            }
        };

        provinceList = new ArrayList<>();
        provinceList.add("--SELECT PROVINCE--");
        provinceAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_list_item, provinceList) {

            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                final View v = super.getView(position, convertView, parent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, "run: onItemSelected: " + (mSettingsModel.getForeground() == null));
                        if (tfBase != null && mSettingsModel.getForeground() != null) {
                            ((TextView) v).setTypeface(tfBase);
                            ((TextView) v).setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                            v.setPadding(0, 0, 20, 0);
                        }
                        ((TextView) v).setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
                    }
                }, 50);

                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                final View v = super.getDropDownView(position, convertView, parent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, "run: onItemSelected: " + (mSettingsModel.getForeground() == null));
                        if (tfBase != null && mSettingsModel.getForeground() != null) {
                            ((TextView) v).setTypeface(tfBase);
                            ((TextView) v).setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                        }
                        ((TextView) v).setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
                        ((TextView) v).setSingleLine(false);

                    }
                }, 50);
                return v;
            }
        };

        cityList = new ArrayList<>();
        cityList.add("--SELECT CITY--");
        cityAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_list_item, cityList) {

            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                final View v = super.getView(position, convertView, parent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, "run: onItemSelected: " + (mSettingsModel.getForeground() == null));
                        if (tfBase != null && mSettingsModel.getForeground() != null) {
                            ((TextView) v).setTypeface(tfBase);
                            ((TextView) v).setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                            v.setPadding(0, 0, 20, 0);
                        }
                        ((TextView) v).setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
                    }
                }, 50);

                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                final View v = super.getDropDownView(position, convertView, parent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, "run: onItemSelected: " + (mSettingsModel.getForeground() == null));
                        if (tfBase != null && mSettingsModel.getForeground() != null) {
                            ((TextView) v).setTypeface(tfBase);
                            ((TextView) v).setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                        }
                        ((TextView) v).setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
                        ((TextView) v).setSingleLine(false);

                    }
                }, 50);
                return v;
            }
        };
    }

    private void setupViews() {
        orientation = getResources().getConfiguration().orientation;

        svRegForm = (ScrollView) mView.findViewById(R.id.svRegForm);
        llFieldsContainer = (LinearLayout) mView.findViewById(R.id.llFieldsContainer);
        llFieldsContainer.setTag("llfieldscontainer");
        llDefaults = (LinearLayout) mView.findViewById(R.id.llDefaults);
        llDefaults.setTag("llDefaultsContainer");
        llCaptchaContainer = (LinearLayout) mView.findViewById(R.id.llCaptchaContainer);
        llCaptchaContainer.setTag("llCaptchaContainer");
        llSubmitButtonContainer = (LinearLayout) mView.findViewById(R.id.llSubmitButtonContainer);
        llSubmitButtonContainer.setTag("llSubmitButtonContainer");

        imgBackground = (ImageView) mView.findViewById(R.id.imgBackground);

        chAgreement = (CheckBox) mView.findViewById(R.id.chAgreement);
        chSubscription = (CheckBox) mView.findViewById(R.id.chSubscription);
        txtSubscription = (TextView) mView.findViewById(R.id.txtSubscription);
        captchaImageView = (CaptchaImageView) mView.findViewById(R.id.captchaImage);
        captchaImageView.setIsDotNeeded(true);
        edtCaptchaInput = (EditText) mView.findViewById(R.id.edtCaptchaInput);
        imgRegistrationLogo = (ImageView) mView.findViewById(R.id.imgRegistrationLogo);
        imgRefreshCaptcha = (ImageView) mView.findViewById(R.id.imgRefreshCaptcha);
        btnSubmit = (Button) mView.findViewById(R.id.btnSubmit);
        /**Note: Disabling Submit Button for Preview Page only*/
        btnSubmit.setEnabled(false);
        btnSubmit.setBackgroundColor(getActivity().getResources().getColor(R.color.darker_gray));

        btnReset = (Button) mView.findViewById(R.id.btnReset);

        txtPreviewPageNote = (TextView) mView.findViewById(R.id.txtPreviewNote);
        txtPreviewPageNote.setVisibility(View.VISIBLE);

        rlRootView = (RelativeLayout) mView.findViewById(R.id.rlRootView);
        rlHeader = (RelativeLayout) mView.findViewById(R.id.rlHeader);
        txtFooter = (TextView) mView.findViewById(R.id.txtFooter);

        gd = new GradientDrawable();
        groupList = new ArrayList<>();

        /**Listeners*/
        btnSubmit.setOnClickListener(this);
        btnReset.setOnClickListener(this);
        imgRefreshCaptcha.setOnClickListener(this);
        chAgreement.setOnClickListener(this);
        chSubscription.setOnClickListener(this);
        txtSubscription.setOnClickListener(this);

        llFieldsContainer.setOnTouchListener(this);
        llDefaults.setOnTouchListener(this);
        llCaptchaContainer.setOnTouchListener(this);
        llSubmitButtonContainer.setOnTouchListener(this);
        mTextWatcherHandler = new Handler();
    }

    private void displayActiveFields() {
        fieldsModelList = mDatabaseHelper.getAllActiveFields(brandCode, campaignCode, eventCode);
        Log.e(TAG, "displayActiveFields: " + fieldsModelList.size());
        for (FieldsModel fieldsModel : fieldsModelList) {
            try {
                String groupTitle = mDatabaseHelper.getGroupTitleByGroupId(fieldsModel.getGroupId());
                if (groupTitle != null && !groupList.contains(groupTitle)) {
                    LinearLayout.LayoutParams groupNameParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

                    TextView txtGroupName = new TextView(getActivity());
                    txtGroupName.setText(groupTitle);
                    txtGroupName.setPadding(5, 10, 5, 10);
                    txtGroupName.setLayoutParams(groupNameParams);

                    if (tfBase != null) {
                        txtGroupName.setTypeface(tfHeadFamily);
                    }

                    /**Setting up Textsize*/
                    txtGroupName.setTextSize(Utils.getDefaultFontHeadSize(mSettingsModel.getFontHeadsize()));

                    if (mSettingsModel.getForeground() != null) {
                        txtGroupName.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                    }

//                    /**Setting group name for portrait view*/
//                    switch (orientation) {
//                        case Configuration.ORIENTATION_PORTRAIT:
//                            txtGroupName.setGravity(Gravity.CENTER);
//                            if (tfBase != null) {
//                                txtGroupName.setTypeface(tfBase, Typeface.BOLD);
//                            }
//                            break;
//                    }
                    groupList.add(groupTitle);
                    llFieldsContainer.addView(txtGroupName);
                }

                llFieldsContainer.addView(createViews(fieldsModel));

            } catch (Exception e) {
                Log.e(TAG, "displayActiveFields: " + e.getMessage());
            }
        }
    }

    private LinearLayout createViews(FieldsModel fieldsModel) {
        LinearLayout llFieldsRow = new LinearLayout(getActivity());
        llFieldsRow.setGravity(Gravity.CENTER_VERTICAL);
        llFieldsRow.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout.LayoutParams llFieldsRowParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llFieldsRowParams.setMargins(20, 0, 10, 0);

        LinearLayout.LayoutParams llFieldsParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
        llFieldsParams.setMargins(5, 5, 5, 5);

        TextView txtFieldName = new TextView(getActivity());
        txtFieldName.setText(fieldsModel.getTitle().concat(" *"));
        txtFieldName.setLayoutParams(llFieldsParams);
        txtFieldName.setPadding(5, 10, 5, 10);
        llFieldsRow.addView(txtFieldName);

        final EditText edtFieldValue = new EditText(getActivity());
        edtFieldValue.setBackgroundResource(R.drawable.bg_registration_field_active);
        edtFieldValue.setPadding(5, 10, 5, 10);
        edtFieldValue.setLayoutParams(llFieldsParams);

        RadioGroup rgFieldValue = new RadioGroup(getActivity());
        rgFieldValue.setPadding(0, 5, 0, 5);
        rgFieldValue.setLayoutParams(llFieldsParams);
        rgFieldValue.setOrientation(LinearLayout.VERTICAL);

        Spinner spnFieldValue = new Spinner(getActivity());
        spnFieldValue.setBackgroundResource(R.drawable.bg_registration_field_active);
        spnFieldValue.setLayoutParams(llFieldsParams);

        Button btnFieldValue = new Button(getActivity());
        btnFieldValue.setBackgroundResource(R.drawable.bg_registration_field_active);
        btnFieldValue.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        btnFieldValue.setPadding(5, 3, 3, 3);
        btnFieldValue.setLayoutParams(llFieldsParams);

        /**Setting typeface and text color of views*/
        if (tfBase != null) {
            txtFieldName.setTypeface(tfBase, Typeface.BOLD);
            edtFieldValue.setTypeface(tfBase);
            btnFieldValue.setTypeface(tfBase);
        }

        /**Setting up textsizes*/
        txtFieldName.setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
        edtFieldValue.setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
        btnFieldValue.setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
        chAgreement.setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
        chSubscription.setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
        btnSubmit.setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
        btnReset.setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
        txtFooter.setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontHeadsize()));

        if (mSettingsModel.getForeground() != null) {
            txtFieldName.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            edtFieldValue.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            btnFieldValue.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
        }

        switch (fieldsModel.getTypeId()) {
            case "1":
                edtFieldValue.setTag(fieldsModel.getName());
                edtFieldValue.setOnTouchListener(this);
                edtFieldValue.setImeOptions(EditorInfo.IME_ACTION_DONE);
                edtFieldValue.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        switch (edtFieldValue.getTag().toString()) {
                            case "mobile":
                                if (edtFieldValue.getText().toString().startsWith("0")) {
                                    edtFieldValue.setText("");
                                }

                                if (edtFieldValue.getText().toString().length() < 10) {
                                    isMobileNumberValid = false;
                                } else {
                                    isMobileNumberValid = true;
                                }

                                break;

                            case "landline":
                                if (edtFieldValue.getText().toString().startsWith("0")) {
                                    edtFieldValue.setText("");
                                }
                                break;

                            case "secondary_mobile":
                                if (edtFieldValue.getText().toString().startsWith("0")) {
                                    edtFieldValue.setText("");
                                }

                                if (edtFieldValue.getText().toString().length() < 10) {
                                    isSecondaryMobileNumberValid = false;
                                } else {
                                    isSecondaryMobileNumberValid = true;
                                }

                                break;
                        }
//                        mTextWatcherHandler.removeCallbacks(mTextWatcherRunnable);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        switch (edtFieldValue.getTag().toString()) {
                            case "email":
                                emailAddress = edtFieldValue.getText().toString();
                                isFieldsIsValidating = false;
                                break;
                        }

//                        final String email = edtFieldValue.getText().toString();
//                        mTextWatcherRunnable = new Runnable() {
//                            @Override
//                            public void run() {
//                                switch (edtFieldValue.getTag().toString()) {
//                                    case "email":
//                                        isUpdatingDetails = false;
//
//                                        /**Clear registration form details*/
//                                        displayExistingData(clearRegistrationFormDetails(), true);
//
//                                        if (emailValidationEnabled) {
//                                            /**Note: Disabling all fields if email is not validated*/
//                                            setFieldsActivation(CheckerUtils.validateEmail(email));
//
//                                            if (CheckerUtils.validateEmail(email)) {
//                                                RegistrationModel registrationModel = new RegistrationModel();
//                                                registrationModel.setEmail(email);
//
//                                                Log.e(TAG, "run: FieldName: " + edtFieldValue.getTag() + " FieldValue: " + email);
//                                                if (NetworkUtils.isNetworkConnected(getActivity())) {
//                                                    onAuthenticateEmailAPI(email);
//
//                                                } else {
//                                                    if (mDatabaseHelper.checkIfRegistrationEmailExist(email, brandCode, campaignCode, eventCode)) {
//                                                        Log.e(TAG, "run: FieldValue: " + email + " exist!");
////                                                        promptExistingDataDetected(getActivity(),
////                                                                "Notification", "Your email account already exists in our database. Do you wish to update your information?",
////                                                                registrationModel);
//
//                                                        mRegistrationContractDialogListener.showRegistrationUpdateAndSavePromptDialog("Notification", "Your email account already exists in our database. Do you wish to update your information?",
//                                                                Constants.PROMPT_EXISTING_REG_DETAILS, mSettingsModel, registrationModel);
//                                                    }
//                                                }
//                                            } else {
//                                                mRegistrationContractDialogListener.showRegistrationAlertDialog("Alert", "Please input valid email address.",
//                                                        Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);
//                                            }
//                                        }
//                                        break;
//                                }
//                            }
//                        };
//                        mTextWatcherHandler.postDelayed(mTextWatcherRunnable, 1000);
                    }
                });
                edtFieldValue.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        Log.e(TAG, "onEditorAction: " + v.getTag() + "---" + edtFieldValue.getTag());
                        Log.e(TAG, "onEditorAction: " + actionId);

                        switch (actionId) {
                            case EditorInfo.IME_ACTION_DONE:
                                if (v.getTag().equals("email")) {
                                    validatingEmailAddress(v);

                                }
                                break;
                        }
                        return false;
                    }
                });

                switch (fieldsModel.getName()) {
                    case "email":
                        edtFieldValue.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        break;

                    case "firstname":
                        setAlphabetsOnly(edtFieldValue);
                        break;

                    case "lastname":
                        setAlphabetsOnly(edtFieldValue);
                        break;

                    case "middlename":
                        setAlphabetsOnly(edtFieldValue);
                        break;

                    case "mobile":
                        edtFieldValue.setInputType(InputType.TYPE_CLASS_NUMBER);
                        edtFieldValue.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                        break;

                    case "landline":
                        edtFieldValue.setInputType(InputType.TYPE_CLASS_NUMBER);
                        edtFieldValue.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
                        break;

                    case "secondary_mobile":
                        edtFieldValue.setInputType(InputType.TYPE_CLASS_NUMBER);
                        edtFieldValue.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                        break;
                }

//                /**Setting fields for portrait view*/
//                switch (orientation) {
//                    case Configuration.ORIENTATION_PORTRAIT:
//                        edtFieldValue.setHint(fieldsModel.getTitle());
//                        edtFieldValue.setGravity(Gravity.CENTER);
//                        break;
//                }

                llFieldsRow.addView(edtFieldValue);
                break;

            case "3": {
                rgFieldValue.setTag(fieldsModel.getName());
                List<String> list = mDatabaseHelper.getDataArray(fieldsModel);
                for (String title : list) {
                    RadioButton rb = new RadioButton(getActivity());
                    rb.setText(title);

                    if (tfBase != null) {
                        rb.setTypeface(tfBase);
                        rb.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                        rb.setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
                    }

                    rgFieldValue.addView(rb);
                }
                llFieldsRow.addView(rgFieldValue);
            }
            break;

            case "4": {
                spnFieldValue.setTag(fieldsModel.getName());
                spnFieldValue.setOnTouchListener(this);
                spnFieldValue.setOnItemSelectedListener(this);
                llFieldsRow.addView(spnFieldValue);

                switch (fieldsModel.getName()) {
                    case "country_id":
                        spnFieldValue.setAdapter(countryAdapter);
                        spnFieldValue.setSelection(1);
                        spnFieldValue.setEnabled(false);
                        break;

                    case "regionid":
                        spnFieldValue.setAdapter(regionAdapter);
                        break;

                    case "provinceid":
                        spnFieldValue.setAdapter(provinceAdapter);
                        break;

                    case "cityid":
                        spnFieldValue.setAdapter(cityAdapter);
                        break;

                    default:
                        String fieldName = fieldsModel.getName();
                        List<String> list = new ArrayList<>();
                        list.add("-- SELECT " + fieldName.replace("_", " ").toUpperCase() + " --");
                        list.addAll(mDatabaseHelper.getDataArray(fieldsModel));
                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_list_item, list) {

                            @NonNull
                            @Override
                            public View getView(int position, View convertView, ViewGroup parent) {
                                final View v = super.getView(position, convertView, parent);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.e(TAG, "run: onItemSelected: " + (mSettingsModel.getForeground() == null));
                                        if (tfBase != null && mSettingsModel.getForeground() != null) {
                                            ((TextView) v).setTypeface(tfBase);
                                            ((TextView) v).setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                                            v.setPadding(0, 0, 20, 0);
                                        }
                                        ((TextView) v).setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
                                    }
                                }, 50);

                                return v;
                            }

                            @Override
                            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                final View v = super.getDropDownView(position, convertView, parent);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.e(TAG, "run: onItemSelected: " + (mSettingsModel.getForeground() == null));
                                        if (tfBase != null && mSettingsModel.getForeground() != null) {
                                            ((TextView) v).setTypeface(tfBase);
                                            ((TextView) v).setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                                        }
                                        ((TextView) v).setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
                                    }
                                }, 50);
                                return v;
                            }
                        };
                        spnFieldValue.setAdapter(arrayAdapter);
                        break;
                }
            }
            break;

            case "5":
                edtFieldValue.setTag(fieldsModel.getName());
                Log.e(TAG, "createViews: type 5: " + fieldsModel.getName());
                switch (fieldsModel.getName()) {
                    case "confirm_pass":
                        edtFieldValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;

                    case "password":
                        edtFieldValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;
                }

                llFieldsRow.addView(edtFieldValue);
                break;

            case "6":
                btnFieldValue.setTag(fieldsModel.getName());
                btnFieldValue.setOnTouchListener(this);
                btnFieldValue.setText("YY-MM-DD");

                switch (fieldsModel.getName()) {
                    case "dob":
                        btnFieldValue.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (view.isFocusable()) {
                                    if (CheckerUtils.validateEmail(emailAddress)) {
                                        showDatePicker(view);
                                    }
                                }
                            }
                        });
                        break;
                }
                llFieldsRow.addView(btnFieldValue);
                break;
        }

        llFieldsRow.setLayoutParams(llFieldsRowParams);
        return llFieldsRow;
    }

    private boolean validateViews() {
        isFieldsIsValidating = true;
        isFieldIsValid = true;
        isEmailIsValid = true;
        isPasswordMatch = true;
        isMobileNumberValid = true;
        isSecondaryMobileNumberValid = true;

        registrationModelList = new ArrayList<>();
        for (int i = 0; i < llFieldsContainer.getChildCount(); i++) {
            RegistrationModel registrationModel = new RegistrationModel();
            View vFieldsRow = llFieldsContainer.getChildAt(i);

            if (vFieldsRow instanceof LinearLayout) {
                LinearLayout llFieldsRow = (LinearLayout) vFieldsRow;
                for (int j = 1; j < llFieldsRow.getChildCount(); j++) {
                    View vFieldsValue = llFieldsRow.getChildAt(1);

                    /**EditText*/
                    if (vFieldsValue instanceof EditText) {
                        EditText edtField = (EditText) vFieldsValue;

                        if (edtField.getText().toString().isEmpty()) {
                            edtField.setBackgroundResource(R.drawable.bg_error_registration_field_active);
                            isFieldIsValid = false;
                        } else {
                            edtField.setBackgroundResource(R.drawable.bg_registration_field_active);
                            switch (edtField.getTag().toString()) {
                                case "email":
                                    String email = edtField.getText().toString();
                                    if (CheckerUtils.validateEmail(email)) {
                                        emailAddress = email;
                                    } else {
                                        isEmailIsValid = false;
                                        edtField.setBackgroundResource(R.drawable.bg_error_registration_field_active);
                                    }
                                    break;

                                case "password":
                                    password = edtField.getText().toString();
                                    break;

                                case "confirm_pass":
                                    confirm_pass = edtField.getText().toString();
                                    if (password.equals(confirm_pass)) {
                                        isPasswordMatch = true;

                                    } else {
                                        isPasswordMatch = false;
                                        edtField.setBackgroundResource(R.drawable.bg_error_registration_field_active);
                                    }
                                    break;

                                case "mobile":
                                    if (edtField.getText().toString().length() < 10) {
                                        isMobileNumberValid = false;
                                        edtField.setBackgroundResource(R.drawable.bg_error_registration_field_active);
                                    }
                                    break;
                                case "secondary_mobile":
                                    if (edtField.getText().toString().length() < 10) {
                                        isSecondaryMobileNumberValid = false;
                                        edtField.setBackgroundResource(R.drawable.bg_error_registration_field_active);
                                    }

                            }
                        }
                        registrationModel.setName(edtField.getTag().toString());
                        registrationModel.setValue(edtField.getText().toString());
                        registrationModelList.add(registrationModel);
                        Log.e(TAG, "validateViews: " + edtField.getTag() + " answer: " + edtField.getText().toString());

                        /**RadioGroup*/
                    } else if (vFieldsValue instanceof RadioGroup) {
                        RadioGroup rgFields = (RadioGroup) vFieldsValue;
                        if (rgFields.getCheckedRadioButtonId() != -1) {
                            int selectedRadioButton = rgFields.getCheckedRadioButtonId();
                            RadioButton rb = (RadioButton) mView.findViewById(selectedRadioButton);
                            Log.e(TAG, "validateViews: " + rgFields.getTag() + " answer: " + rb.getText().toString());
                            registrationModel.setName(rgFields.getTag().toString());
                            registrationModel.setValue(rb.getText().toString());
                            registrationModelList.add(registrationModel);

                        }

                        boolean isRadioGroupHasSelectedChild = false;
                        for (int k = 0; k < rgFields.getChildCount(); k++) {
                            View rgFieldChildViews = rgFields.getChildAt(k);
                            RadioButton radioButton = (RadioButton) rgFieldChildViews;
                            if (radioButton.isChecked()) {
                                isRadioGroupHasSelectedChild = true;
                            }
                        }

                        if (!isRadioGroupHasSelectedChild) {
                            isFieldIsValid = isRadioGroupHasSelectedChild;
                        }

                        /**Spinner*/
                    } else if (vFieldsValue instanceof Spinner) {
                        Spinner spnFields = (Spinner) vFieldsValue;
                        String selectedItems = spnFields.getSelectedItem().toString();
                        if (selectedItems != null) {
                            switch (spnFields.getTag().toString()) {
                                case "gender":
                                    if (selectedItems.equals("Male")) {
                                        registrationModel.setValue("1");

                                    } else if (selectedItems.equals("Female")) {
                                        registrationModel.setValue("2");

                                    } else {
                                        registrationModel.setValue("0");
                                    }
                                    break;

                                case "country_id":
                                    if (selectedItems.equals("PHILIPPINES")) {
                                        registrationModel.setValue("PH");
                                    } else {
                                        registrationModel.setValue("");
                                    }
                                    break;

                                case "regionid":
                                    String regionId = mDatabaseHelper.getRegionIdByName(selectedItems.toLowerCase());
                                    registrationModel.setValue(regionId);
                                    break;

                                case "provinceid":
                                    String provinceId = mDatabaseHelper.getProvinceIdByName(selectedItems.toLowerCase());
                                    registrationModel.setValue(provinceId);
                                    break;

                                case "cityid":
                                    String cityId = mDatabaseHelper.getCityIdByCityName(selectedItems.toLowerCase());
                                    registrationModel.setValue(cityId);
                                    break;

                                default:
                                    registrationModel.setValue(selectedItems);
                                    break;
                            }

                            Log.e(TAG, "validateViews: " + spnFields.getTag() + " answer: " + registrationModel.getValue());
                            registrationModel.setName(spnFields.getTag().toString());
                            registrationModelList.add(registrationModel);
                        }

                        String answer = spnFields.getSelectedItem().toString();
                        if (answer.startsWith("--")) {
                            spnFields.setBackgroundResource(R.drawable.bg_error_registration_spinner_active);
                            isFieldIsValid = false;
                        } else {
                            spnFields.setBackgroundResource(R.drawable.bg_registration_spinner_active);
                        }

                        /**Button*/
                    } else if (vFieldsValue instanceof Button) {
                        Button btnFields = (Button) vFieldsValue;
                        if (btnFields.getText().toString().equals("YY-MM-DD")) {
                            isFieldIsValid = false;
                            btnFields.setBackgroundResource(R.drawable.bg_error_registration_field_active);
                        } else {
                            btnFields.setBackgroundResource(R.drawable.bg_registration_field_active);
                        }
                        registrationModel.setName(btnFields.getTag().toString());
                        registrationModel.setValue(btnFields.getText().toString());
                        registrationModelList.add(registrationModel);
                        Log.e(TAG, "validateViews: " + btnFields.getTag() + " answer: " + btnFields.getText().toString());
                    }
                }
            }
        }
        return true;
    }

    public void showDatePicker(final View v) {
        final Calendar currentDate = Calendar.getInstance();
        int mYear = currentDate.get(Calendar.YEAR);
        int mMonth = currentDate.get(Calendar.MONTH);
        int mDay = currentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar selectedDate = Calendar.getInstance();
                        selectedDate.set(Calendar.MONTH, monthOfYear);
                        selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        selectedDate.set(Calendar.YEAR, year);

                        Date myDate = selectedDate.getTime();
                        String date = new SimpleDateFormat("yyyy-MM-dd").format(myDate);

                        if (selectedDate.after(currentDate)) {
                            mRegistrationContractDialogListener.showRegistrationAlertDialog("Notification",
                                    "Please select valid date.", Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);
                        } else if (selectedDate.before(currentDate)) {
                            ((Button) v).setText(date);
                        }

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void displayExistingData(RegistrationModel model, boolean isDataFromOnline) {
        isDisplayingExistingData = true;
        Log.e(TAG, "displayExistingData: " + model.toString() + " " + isDataFromOnline);
        String countryId = null, regionId = null, provinceId = null, cityId = null;

        Type listType = new TypeToken<List<RegistrationModel>>() {
        }.getType();
        List<RegistrationModel> registrationDataArray = new ArrayList<>();

        if (isDataFromOnline) {
            registrationDataArray = new Gson().fromJson(model.getData(), listType);

        } else {
            List<RegistrationModel> registrationModelList = mDatabaseHelper.getRegistrationDetails(model.getEmail(),
                    brandCode, campaignCode);
            for (RegistrationModel regModel : registrationModelList) {
                registrationDataArray = new Gson().fromJson(regModel.getData(), listType);
                Log.e(TAG, "displayExistingData: " + registrationModelList.size());
            }
        }

        /**Note: Checking data for country, region, province and city since it is related to each other*/
        for (RegistrationModel registrationModel : registrationDataArray) {
            Log.e(TAG, "displayExistingData: ---> name: " + registrationModel.getName() + " value: " + registrationModel.getValue());
            switch (registrationModel.getName()) {
                case "country_id":
                    countryId = registrationModel.getValue();

                    break;

                case "region":
                    regionId = registrationModel.getValue();
                    if (regionId != null && !regionId.isEmpty()) {
                        provinceList.clear();
                        provinceList.add("--SELECT PROVINCE--");
                        provinceList.addAll(mDatabaseHelper.getProvinceNameByRegionId(regionId));
                        provinceAdapter.notifyDataSetChanged();
                    }
                    break;

                case "province_id":
                    provinceId = registrationModel.getValue();
                    if (provinceId != null && !provinceId.isEmpty()) {
                        cityList.clear();
                        cityList.add("--SELECT CITY--");
                        cityList.addAll(mDatabaseHelper.getCityNameByProvinceId(provinceId));
                        cityAdapter.notifyDataSetChanged();
                    }

                    break;

                case "city_id":
                    cityId = registrationModel.getValue();
                    break;


                case "regionid":
                    regionId = registrationModel.getValue();
                    if (regionId != null && !regionId.isEmpty()) {
                        provinceList.clear();
                        provinceList.add("--SELECT PROVINCE--");
                        provinceList.addAll(mDatabaseHelper.getProvinceNameByRegionId(regionId));
                        provinceAdapter.notifyDataSetChanged();
                    }
                    break;

                case "provinceid":
                    provinceId = registrationModel.getValue();
                    if (provinceId != null && !provinceId.isEmpty()) {
                        cityList.clear();
                        cityList.add("--SELECT CITY--");
                        cityList.addAll(mDatabaseHelper.getCityNameByProvinceId(provinceId));
                        cityAdapter.notifyDataSetChanged();
                    }

                    break;

                case "cityid":
                    cityId = registrationModel.getValue();
                    break;
            }
        }


        Log.e(TAG, "displayExistingData: " + isDataFromOnline + "-" + registrationDataArray.size());

        for (int i = 0; i < llFieldsContainer.getChildCount(); i++) {
            View vFieldsRow = llFieldsContainer.getChildAt(i);

            if (vFieldsRow instanceof LinearLayout) {
                LinearLayout llFieldsRow = (LinearLayout) vFieldsRow;
                for (int j = 1; j < llFieldsRow.getChildCount(); j++) {
                    View vFieldsValue = llFieldsRow.getChildAt(1);

                    /**EditText*/
                    if (vFieldsValue instanceof EditText) {
                        EditText edtField = (EditText) vFieldsValue;
                        for (RegistrationModel registrationModel : registrationDataArray) {
                            if (edtField.getTag().toString().equals(registrationModel.getName())) {
                                if (!registrationModel.getValue().equals("null")) {
                                    edtField.setText(registrationModel.getValue());
                                }
                            }
                        }

                        /**RadioGroup*/
                    } else if (vFieldsValue instanceof RadioGroup) {
                        RadioGroup rgFields = (RadioGroup) vFieldsValue;

                        /*Checking radioGroup name*/
                        for (RegistrationModel registrationModel : registrationDataArray) {
                            if (rgFields.getTag().equals(registrationModel.getName())) {

                                /*Looping radioGroupChildViews*/
                                for (int k = 0; k < rgFields.getChildCount(); k++) {
                                    View rgFieldsChildViews = rgFields.getChildAt(k);

                                    /*Checking radioButton text if equal to existing registration data*/
                                    if (rgFieldsChildViews instanceof RadioButton) {
                                        RadioButton radiobutton = (RadioButton) rgFieldsChildViews;
                                        String rbValue = radiobutton.getText().toString();
                                        if (rbValue.equals(registrationModel.getValue())) {
                                            radiobutton.setChecked(true);
                                        } else {
                                            radiobutton.setChecked(false);
                                        }
                                    }
                                }
                            }
                        }

                        /**Spinner*/
                    } else if (vFieldsValue instanceof Spinner) {
                        Spinner spnFields = (Spinner) vFieldsValue;
                        int selectedItemPosition = -1;
                        for (RegistrationModel registrationModel : registrationDataArray) {
                            if (spnFields.getTag().equals(registrationModel.getName())) {
                                switch (spnFields.getTag().toString()) {
                                    case "gender":
                                        Log.e(TAG, "displayExistingData: GENDER: " + registrationModel.getValue());
                                        switch (registrationModel.getValue()) {
                                            case "0":
                                                selectedItemPosition = 0;
                                                break;

                                            case "1":
                                                selectedItemPosition = 1;
                                                break;

                                            case "2":
                                                selectedItemPosition = 2;
                                                break;
                                        }
                                        break;

                                    case "regionid":
                                        if (regionId != null && !regionId.isEmpty()) {
                                            String regionName = mDatabaseHelper.getRegionNameByRegionId(regionId);
                                            Adapter adapter = spnFields.getAdapter();
                                            int spinnerItemsCount = adapter.getCount();
                                            for (int k = 0; k < spinnerItemsCount; k++) {
                                                if (adapter.getItem(k).equals(regionName.toUpperCase())) {
                                                    selectedItemPosition = k;
                                                }
                                            }
                                        }
                                        break;

                                    case "provinceid":
                                        if (provinceId != null && !provinceId.isEmpty()) {
                                            String provinceName = mDatabaseHelper.getProvinceNameByProvinceId(provinceId);
                                            Adapter adapter = spnFields.getAdapter();
                                            int spinnerItemsCount = adapter.getCount();
                                            for (int k = 0; k < spinnerItemsCount; k++) {
                                                if (adapter.getItem(k).equals(provinceName.toUpperCase())) {
                                                    selectedItemPosition = k;
                                                }
                                            }
                                        }
                                        break;

                                    case "cityid":
                                        if (cityId != null && !cityId.isEmpty()) {
                                            String cityName = mDatabaseHelper.getCityNameByCityId(cityId);
                                            Adapter adapter = spnFields.getAdapter();
                                            int spinnerItemsCount = adapter.getCount();
                                            for (int k = 0; k < spinnerItemsCount; k++) {
                                                if (adapter.getItem(k).equals(cityName.toUpperCase())) {
                                                    selectedItemPosition = k;
                                                }
                                            }
                                        }
                                        break;

                                    default:
                                        Adapter adapter = spnFields.getAdapter();
                                        int spinnerItemsCount = adapter.getCount();
                                        for (int k = 0; k < spinnerItemsCount; k++) {
                                            if (adapter.getItem(k).equals(registrationModel.getValue())) {
                                                selectedItemPosition = k;
                                            }
                                        }
                                        break;
                                }
                            }

                            /**Temporary Code*/
                            else if (spnFields.getTag().equals("country_id")) {
//                                if (countryId.equals("PH")) {
//                                    spnFields.setSelection(1);
//                                } else {
//                                    spnFields.setSelection(0);
//                                }

                                spnFields.setSelection(1);
                                spnFields.setEnabled(false);

                            } else if (spnFields.getTag().equals("regionid") && registrationModel.getName().equals("region")) {
                                String regionName = mDatabaseHelper.getRegionNameByRegionId(regionId);
                                Adapter adapter = spnFields.getAdapter();
                                int spinnerItemsCount = adapter.getCount();
                                for (int k = 0; k < spinnerItemsCount; k++) {
                                    if (adapter.getItem(k).equals(regionName.toUpperCase())) {
                                        selectedItemPosition = k;
                                    }
                                }
                            } else if (spnFields.getTag().equals("provinceid") && registrationModel.getName().equals("province_id")) {
                                String provinceName = mDatabaseHelper.getProvinceNameByProvinceId(provinceId);
                                Adapter adapter = spnFields.getAdapter();
                                int spinnerItemsCount = adapter.getCount();
                                for (int k = 0; k < spinnerItemsCount; k++) {
                                    if (adapter.getItem(k).equals(provinceName.toUpperCase())) {
                                        selectedItemPosition = k;
                                    }
                                }
                            } else if (spnFields.getTag().equals("cityid") && registrationModel.getName().equals("city_id")) {
                                String cityName = mDatabaseHelper.getCityNameByCityId(cityId);
                                Adapter adapter = spnFields.getAdapter();
                                int spinnerItemsCount = adapter.getCount();
                                for (int k = 0; k < spinnerItemsCount; k++) {
                                    if (adapter.getItem(k).equals(cityName.toUpperCase())) {
                                        selectedItemPosition = k;
                                    }
                                }
                            }
                        }

                        if (selectedItemPosition != -1) {
                            spnFields.setSelection(selectedItemPosition);
                        }

                        /**Button*/
                    } else if (vFieldsValue instanceof Button) {
                        Button btnFields = (Button) vFieldsValue;
                        for (RegistrationModel registrationModel : registrationDataArray) {
                            if (btnFields.getTag().equals(registrationModel.getName())) {
                                if (registrationModel.getValue().isEmpty()) {
                                    btnFields.setText("YY-MM-DD");
//                                    switch (orientation) {
//                                        case Configuration.ORIENTATION_PORTRAIT:
//                                            btnFields.setText("YY-MM-DD");
//                                            break;
//
//                                        case Configuration.ORIENTATION_LANDSCAPE:
//                                            btnFields.setText("Select Birthday");
//                                            break;
//                                    }
                                } else {
                                    btnFields.setText(registrationModel.getValue());
                                }
                            }
                        }
                    }
                }
            }
        }

        Log.e(TAG, "onItemSelected: isDisplayingData: End: " + isDisplayingExistingData);
    }

    private void onAuthenticateEmailAPI(final String email) {
        Log.e(TAG, "onAuthenticateEmailAPI: ");
        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Checking email");
        Call<ResponseBody> onAuthenticateEmailCall = RegistrationApiProvider.getMaintenanceDbmApiRoutes()
                .onAuthenticateEmail(Constants.API_DBM_TOKEN, Constants.CMD_EVENT_AUTHENTICATE_EMAIL,
                        email, Constants.DBM_HOSTNAME);
        onAuthenticateEmailCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                boolean isAuthenticated = false;

                try {
                    if (response.body() != null) {
                        String strResponse = response.body().string();
                        Log.e(TAG, "onResponse: " + strResponse);
                        if (strResponse == null) {
                            Log.e(TAG, "onResponse: " + "Null");
                        } else if (strResponse.isEmpty()) {
                            Log.e(TAG, "onResponse: " + "Empty");
                        } else {
                            Log.e(TAG, "onResponse: " + "Not NULL");
                            JSONObject jsonObject = new JSONObject(strResponse);
                            String message = jsonObject.getString("message");

                            switch (message) {
                                case "Email is already exists!":
                                    RegistrationModel registrationModel = new RegistrationModel();
                                    registrationModel.setEmail(email);
                                    mRegistrationContractDialogListener.showRegistrationUpdateAndSavePromptDialog("Notification", "Your email account already exists in our database. Do you wish to update your information?",
                                            Constants.PROMPT_EXISTING_REG_DETAILS, mSettingsModel, registrationModel);
                                    break;

                                default:
                                    mRegistrationContractDialogListener.showRegistrationAlertDialog("Alert", message, Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);
                                    break;
                            }
                        }
                    }

                    Log.e(TAG, "onAuthentication-2: ");
                    if (!isAuthenticated) {
                        String strResponse = response.body().string();
                        Log.e(TAG, "onAuthentication-onResponse: " + strResponse);
                        if (strResponse == null) {
                            Log.e(TAG, "onAuthentication-onResponse: " + "Null");
                        } else if (strResponse.isEmpty()) {
                            Log.e(TAG, "onAuthentication-onResponse: " + "Empty");
                        } else {
                            Log.e(TAG, "onAuthentication-onResponse: " + "Not NULL");
                            JSONObject jsonObject = new JSONObject(strResponse);
                            String message = jsonObject.getString("message");

                            switch (message) {
                                case "Email is already exists!":
                                    RegistrationModel registrationModel = new RegistrationModel();
                                    registrationModel.setEmail(email);
                                    mRegistrationContractDialogListener.showRegistrationUpdateAndSavePromptDialog("Notification", "Your email account already exists in our database. Do you wish to update your information?",
                                            Constants.PROMPT_EXISTING_REG_DETAILS, mSettingsModel, registrationModel);
                                    break;

                                default:
                                    mRegistrationContractDialogListener.showRegistrationAlertDialog("Alert", message, Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);
                                    break;
                            }
                        }
                    }

                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, "onResponse: Error: " + e.getMessage());
                    mRegistrationContractDialogListener.showRegistrationAlertDialog("Alert", e.getMessage(), Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);
                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                DialogHelper.getInstance().hideProgressDialog(getActivity());
                mRegistrationContractDialogListener.showRegistrationAlertDialog("Alert", t.getMessage(), Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);

            }
        });

    }

    private void getCustomerDetailsAPI(final RegistrationModel registrationModel) {
        Log.e(TAG, "getCustomerDetailsAPI: ");
        DialogHelper.getInstance().hideProgressDialog(MaintenanceActivity.mainActivityContext);
        DialogHelper.getInstance().showProgressDialog(MaintenanceActivity.mainActivityContext, "Getting Customer Details");
        Call<ResponseBody> getCustomerDetailsCall = RegistrationApiProvider.getMaintenanceDbmApiRoutes()
                .onGetCustomerDetails(Constants.API_DBM_TOKEN, Constants.CMD_EVENT_GET_CUSTOMER_DETAILS,
                        registrationModel.getEmail(), eventCode, Constants.DBM_HOSTNAME);
        getCustomerDetailsCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                List<RegistrationModel> registrationDataArray = new ArrayList<>();
                try {
                    if (response.body() != null) {
                        String strResponse = response.body().string();
                        Log.e(TAG, "onResponse: " + strResponse);
                        if (strResponse == null) {
                            Log.e(TAG, "onResponse: " + "Null");
                        } else if (strResponse.isEmpty()) {
                            Log.e(TAG, "onResponse: " + "Empty");
                        } else {
                            Log.e(TAG, "onResponse: " + "Not NULL");

//                            JSONArray jsonArray = new JSONArray(strResponse);
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                Log.e(TAG, "onResponse: " + jsonArray.get(i).toString());
//                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                            JSONObject jsonObject = new JSONObject(strResponse);
                            for (Iterator<String> iter = jsonObject.keys(); iter.hasNext(); ) {
                                String key = iter.next();
                                Object obj = jsonObject.get(key);
                                if (obj instanceof JSONObject) {
                                    /**Key value is json object*/

                                } else if (obj instanceof JSONArray) {
                                    /**Key value is json array*/
                                    JSONArray jsonArray1 = ((JSONArray) obj);
                                    for (int j = 0; j < jsonArray1.length(); j++) {
                                        JSONObject jsonObject1 = jsonArray1.getJSONObject(j);
                                        Log.e(TAG, "onResponse: " + jsonObject1.toString());
                                    }

                                } else {
                                    /**Key value is a string */
                                    String value = jsonObject.getString(key);
                                    RegistrationModel rgModel = new RegistrationModel();
                                    rgModel.setName(key);
                                    rgModel.setValue(value);
                                    registrationDataArray.add(rgModel);
                                    Log.e(TAG, "onResponse: keyvalue " + key + " - " + value);
                                }
                            }

                            Gson gson = new Gson();
                            String strData = gson.toJson(registrationDataArray);
                            registrationModel.setData(strData);
                            Log.e(TAG, "onResponse: regModel: " + registrationModel.getData());
                            displayExistingData(registrationModel, true);
                        }
                    }
//                    }
                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, "onResponse: Error: " + e.getMessage());
                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                    mRegistrationContractDialogListener.showRegistrationAlertDialog("Alert", e.getMessage(), Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mRegistrationContractDialogListener.showRegistrationAlertDialog("Alert", t.getMessage(), Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgRefreshCaptcha:
                captchaImageView.regenerate();
                edtCaptchaInput.setText("");
                break;

            case R.id.chAgreement:
                Utils.hideKeyboard(getActivity());
                break;

            case R.id.txtSubscription:
                Utils.hideKeyboard(getActivity());
                mRegistrationContractDialogListener.showRegistrationAgreementDialog(mSettingsModel);
                chSubscription.setEnabled(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        chSubscription.setEnabled(true);
                    }
                }, 1500);
                break;

            case R.id.btnReset:
                mRegistrationContract.refreshRegistrationForm();
                break;

            case R.id.btnSubmit:
                String captchaCode = captchaImageView.getCaptchaCode();
                String captchaInput = edtCaptchaInput.getText().toString();

                if (validateViews()) {
                    if (!isFieldIsValid) {
                        mRegistrationContractDialogListener.showRegistrationAlertDialog("Missing Information", "Please fill out all required fields", Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);

                    } else if (emailAddress == null) {
                        mRegistrationContractDialogListener.showRegistrationAlertDialog("Notification", "Email Address is required", Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);

                    } else if (!isEmailIsValid) {
                        mRegistrationContractDialogListener.showRegistrationAlertDialog("Email Verification", "Please input valid email address", Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);

                    } else if (!isMobileNumberValid) {
                        mRegistrationContractDialogListener.showRegistrationAlertDialog("Notification", "Please input valid mobile number", Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);

                    } else if (!isSecondaryMobileNumberValid) {
                        mRegistrationContractDialogListener.showRegistrationAlertDialog("Notification", "Please input valid secondary mobile number", Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);

                    } else if (!isPasswordMatch) {
                        mRegistrationContractDialogListener.showRegistrationAlertDialog("Notification", "Password do not match", Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);

                    } else if (!chSubscription.isChecked()) {
                        mRegistrationContractDialogListener.showRegistrationAlertDialog("Notification", "To continue, you must check the box acknowledging you have read and agree to the Terms of Use and Privacy Policy.",
                                Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);

                    } else if (captchaInput.isEmpty()) {
                        mRegistrationContractDialogListener.showRegistrationAlertDialog("Notification", "Captcha required!", Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);

                    } else if (!captchaInput.equals(captchaCode)) {
                        mRegistrationContractDialogListener.showRegistrationAlertDialog("Notification", "Invalid captcha!", Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);

                    } else {

                        if (chAgreement.isChecked()) {
                            RegistrationModel registrationModel = new RegistrationModel();
                            registrationModel.setName("subscription");
                            registrationModel.setValue("1");
                            registrationModelList.add(registrationModel);
                        }

                        if (chSubscription.isChecked()) {
                            RegistrationModel registrationModel = new RegistrationModel();
                            registrationModel.setName("agree_on_terms");
                            registrationModel.setValue("1");
                            registrationModelList.add(registrationModel);
                        }

                        Gson gson = new Gson();
                        String strData = gson.toJson(registrationModelList);
                        int maxRegistrationId = mDatabaseHelper.getMaxRegistrationId();
                        maxRegistrationId++;

                        RegistrationModel registrationModel = new RegistrationModel();
                        registrationModel.setId(String.valueOf(maxRegistrationId));
                        registrationModel.setEmail(emailAddress);
                        registrationModel.setBrandCode(brandCode);
                        registrationModel.setCampaignCode(campaignCode);
                        registrationModel.setEventCode(eventCode);
                        registrationModel.setEventName(eventName);
                        registrationModel.setData(strData);
                        registrationModel.setStatus("1");
                        registrationModel.setDateTime(Utils.getDateTime());

                        if (isUpdatingDetails) {
                            mRegistrationContractDialogListener.showRegistrationUpdateAndSavePromptDialog("Confirm", "Are you sure you want to update?",
                                    Constants.UPDATE_REG_DETAILS, mSettingsModel, registrationModel);
                        } else {
                            if (mDatabaseHelper.checkIfRegistrationEmailExist(emailAddress, brandCode, campaignCode, eventCode)) {
                                mRegistrationContractDialogListener.showRegistrationUpdateAndSavePromptDialog("Confirm", "Are you sure you want to update?",
                                        Constants.UPDATE_REG_DETAILS, mSettingsModel, registrationModel);
                            } else {
                                mRegistrationContractDialogListener.showRegistrationUpdateAndSavePromptDialog("Confirm", "Are you sure you want to submit?",
                                        Constants.CREATE_REG_DETAILS, mSettingsModel, registrationModel);
//                                registrationModel.setActionType(Constants.CREATE_REG_DETAILS);
//                                mDatabaseHelper.createRegistration(registrationModel);
//                                if (NetworkUtils.isNetworkConnected(getActivity())) {
//                                    new AsyncSendRegistration(getActivity(), registrationModel, false, mSettingsModel).execute();
//                                } else {
//                                    mRegistrationContractDialogListener.showRegistrationAlertDialog("Alert", "Registration successfully saved locally. It will be sent to the server once internet is available.",
//                                            Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);
//                                }
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, final View view, int i, long l) {
        Log.e(TAG, "onItemSelected: isDisplayingData: " + isDisplayingExistingData + " isEmailValidating: " + emailValidationEnabled);

        if (mSettingsModel != null && view != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "run: onItemSelected: " + (mSettingsModel.getForeground() == null));
                    if (tfBase != null && mSettingsModel.getForeground() != null) {
                        ((TextView) view).setTypeface(tfBase);
                        ((TextView) view).setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                    }
                    ((TextView) view).setTextSize(Utils.getDefaultFontBaseSize(mSettingsModel.getFontBasesize()));
                }
            }, 50);

        }

        if (adapterView.getTag() != null) {
            String selectedItem = adapterView.getSelectedItem().toString();
            switch (adapterView.getTag().toString()) {
                case "regionid":
                    String regionId = mDatabaseHelper.getRegionIdByRegionName(selectedItem.toLowerCase());
                    if (regionId != null) {
                        provinceList.clear();
                        provinceList.add("--SELECT PROVINCE--");
                        provinceList.addAll(mDatabaseHelper.getProvinceNameByRegionId(regionId));
                        provinceAdapter.notifyDataSetChanged();

                        if (!isDisplayingExistingData && emailValidationEnabled) {
                            Spinner spnProvince = (Spinner) llFieldsContainer.findViewWithTag("provinceid");
                            if (spnProvince != null)
                                spnProvince.setSelection(0);

                            Spinner spnCity = (Spinner) llFieldsContainer.findViewWithTag("cityid");
                            if (spnCity != null)
                                spnCity.setSelection(0);
                        }
                    }
                    break;

                case "provinceid":
                    String provinceId = mDatabaseHelper.getProvinceIdByProvinceName(selectedItem.toLowerCase());
                    if (provinceId != null) {
                        cityList.clear();
                        cityList.add("--SELECT CITY--");
                        cityList.addAll(mDatabaseHelper.getCityNameByProvinceId(provinceId));
                        cityAdapter.notifyDataSetChanged();

                        if (!isDisplayingExistingData && emailValidationEnabled) {
                            Spinner spnCity = (Spinner) llFieldsContainer.findViewWithTag("cityid");
                            if (spnCity != null)
                                spnCity.setSelection(0);
                        }
                    }

                    isDisplayingExistingData = false;
                    break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void refreshRegistrationForm() {
        getActivity().finish();
        startActivity(new Intent(getActivity(), ActivityRegistration.class));
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                switch (v.getTag().toString()) {
                    case "email":
                        isEmailHasPreviousFocus = true;
//                        isFieldsValidated = false;
                        break;

                    case "mobile":
                        validatingEmailAddress(v);
                        isEmailHasPreviousFocus = false;
                        isMobileNumberInitializedTouch = true;
                        break;

                    case "secondary_mobile":
                        validatingEmailAddress(v);
                        isEmailHasPreviousFocus = false;
                        isSecondaryMobileNumberInitializedTouch = true;
                        break;

                    case "llfieldscontainer":
                        Utils.hideKeyboard(getActivity());
                        validatingEmailAddress(v);
                        isEmailHasPreviousFocus = false;
                        break;

                    default:
                        if (v instanceof EditText) {
                            Log.e(TAG, "onTouch: " + v.getTag() + " is EditText");
                        } else {
                            Utils.hideKeyboard(getActivity());
                        }

                        /**Note: Validating email address when field is in focus*/
                        validatingEmailAddress(v);
                        isEmailHasPreviousFocus = false;
                        break;
                }
                break;
        }
        return false;
    }

    private void validatingEmailAddress(View v) {
        if (emailAddress != null) {
//            isUpdatingDetails = false;

            if (isEmailHasPreviousFocus) {
                /**Clear registration form details*/
                displayExistingData(clearRegistrationFormDetails(), true);
                isUpdatingDetails = false;
            }

            if (emailValidationEnabled) {
                /**Note: Disabling all fields if email is not validated*/
                setFieldsActivation(CheckerUtils.validateEmail(emailAddress));

                if (CheckerUtils.validateEmail(emailAddress)) {
                    if (isEmailHasPreviousFocus) {
                        RegistrationModel registrationModel = new RegistrationModel();
                        registrationModel.setEmail(emailAddress);

                        if (NetworkUtils.isNetworkConnected(getActivity())) {
                            /**
                             * Note: When internet connection is available the application
                             * will get the customers data from server
                             * and display it in registration form.
                             * */
                            onAuthenticateEmailAPI(emailAddress);

                        } else {
                            if (mDatabaseHelper.checkIfRegistrationEmailExist(emailAddress, brandCode, campaignCode, eventCode)) {
                                Log.e(TAG, "run: FieldValue: " + emailAddress + " exist!");
                                /**
                                 * Note: When internet connection is available the application
                                 * will get the customers data from local database
                                 * and display it in registration form.
                                 * */
                                mRegistrationContractDialogListener.showRegistrationUpdateAndSavePromptDialog("Notification",
                                        "Your email account already exists in our database. Do you wish to update your information?",
                                        Constants.PROMPT_EXISTING_REG_DETAILS, mSettingsModel, registrationModel);
                            }
                        }
                    }
                }
            }
        }

        if (v instanceof EditText) {
            EditText edtFields = (EditText) v;
            Log.e(TAG, "onTouch: " + edtFields.getTag().toString());
            if (!edtFields.isFocusable()) {
                mRegistrationContractDialogListener.showRegistrationAlertDialog("Email Verification", "Please input valid email address.",
                        Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);
            }

            /**Note: This code sets the focus to the next edit text if email is valid*/
            if (emailAddress != null && CheckerUtils.validateEmail(emailAddress)) {
                edtFields.focusSearch(View.FOCUS_DOWN).requestFocus();
            } else {
                edtFields.requestFocus();
            }

        } else if (v instanceof LinearLayout) {
            if (emailAddress != null) {
                if (!emailAddress.isEmpty()) {
                    if (!CheckerUtils.validateEmail(emailAddress)) {
                        mRegistrationContractDialogListener.showRegistrationAlertDialog("Email Verification", "Please input valid email address.",
                                Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);
                    }
                }
            }
        } else if (v instanceof Button) {
            if (emailAddress == null || !CheckerUtils.validateEmail(emailAddress)) {
                mRegistrationContractDialogListener.showRegistrationAlertDialog("Email Verification", "Please input valid email address.",
                        Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);
            }
        } else if (v instanceof Spinner) {
            Spinner spinner = (Spinner) v;
            if (!spinner.isEnabled()) {
                mRegistrationContractDialogListener.showRegistrationAlertDialog("Email Verification", "Please input valid email address.",
                        Constants.DIALOG_ACTION_NORMAL, "", mSettingsModel);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.e(TAG, "onSaveInstanceState: ");

        validateViews();
        Gson gson = new Gson();
        String strData = gson.toJson(registrationModelList);
        RegistrationModel registrationModel = new RegistrationModel();
        registrationModel.setData(strData);

        outState.putParcelable(Constants.STATE_REGISTRATION_MODEL, registrationModel);
        outState.putString(Constants.STATE_REGISTRATION_EMAIL, emailAddress);
        outState.putBoolean(Constants.STATE_MOBILE_INITIALIZATION_TOUCH, isMobileNumberInitializedTouch);
        outState.putBoolean(Constants.STATE_SECONDARY_MOBILE_INITIALIZATION_TOUCH, isSecondaryMobileNumberInitializedTouch);

        emailValidationEnabled = false;

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            String email = savedInstanceState.getString(Constants.STATE_REGISTRATION_EMAIL, "");
            RegistrationModel registrationModel = savedInstanceState.getParcelable(Constants.STATE_REGISTRATION_MODEL);
            isMobileNumberInitializedTouch = savedInstanceState.getBoolean(Constants.STATE_MOBILE_INITIALIZATION_TOUCH);
            isSecondaryMobileNumberInitializedTouch = savedInstanceState.getBoolean(Constants.STATE_SECONDARY_MOBILE_INITIALIZATION_TOUCH);

            emailAddress = email;
            displayExistingData(registrationModel, true);
            setFieldsActivation(CheckerUtils.validateEmail(email));

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    emailValidationEnabled = true;
                }
            }, 500);

        }
    }

    public void updateExistingRegistrationData(RegistrationModel registrationModel) {
        registrationModel.setActionType(Constants.UPDATE_REG_DETAILS);
        if (NetworkUtils.isNetworkConnected(getActivity())) {
            new AsyncUpdateRegistration(getActivity(), registrationModel, false, mSettingsModel).execute();

        } else {
            registrationModel.setActionType(Constants.CREATE_REG_DETAILS);
            if (mDatabaseHelper.checkIfRegistrationEmailExist(emailAddress, brandCode, campaignCode, eventCode)) {
                mDatabaseHelper.updateRegistration(registrationModel, brandCode, campaignCode, eventCode, "1");
            } else {
                mDatabaseHelper.createRegistration(registrationModel);
            }
            mRegistrationContractDialogListener.showRegistrationAlertDialog("Alert", getActivity().getResources()
                            .getString(R.string.success_registration_details_saving_locally),
                    Constants.DIALOG_ACTION_UPDATE_REGISTRATION_DETAILS_LOCALLY, "", mSettingsModel);
        }
    }

    public void promptExistingRegistrationDataDetected(RegistrationModel registrationModel) {
        isUpdatingDetails = true;
        if (NetworkUtils.isNetworkConnected(MaintenanceActivity.mainActivityContext)) {
            getCustomerDetailsAPI(registrationModel);
        } else {
            if (mDatabaseHelper.checkIfRegistrationEmailExist(registrationModel.getEmail(), brandCode, campaignCode, eventCode)) {
                displayExistingData(registrationModel, false);
            }
        }
    }

    public void saveRegistrationDetails(RegistrationModel registrationModel) {
        registrationModel.setActionType(Constants.CREATE_REG_DETAILS);
        mDatabaseHelper.createRegistration(registrationModel);
        if (NetworkUtils.isNetworkConnected(getActivity())) {
            new AsyncSendRegistration(getActivity(), registrationModel, false, mSettingsModel).execute();
        } else {
            mRegistrationContractDialogListener.showRegistrationAlertDialog("Alert", getActivity().getResources()
                            .getString(R.string.success_registration_details_saving_locally),
                    Constants.DIALOG_ACTION_SAVE_REGISTRATION_DETAILS_LOCALLY, "", mSettingsModel);
        }
    }

    private RegistrationModel clearRegistrationFormDetails() {
        ArrayList<RegistrationModel> registrationModelArrayList = new ArrayList<>();
        for (int i = 0; i < llFieldsContainer.getChildCount(); i++) {
            RegistrationModel registrationModel = new RegistrationModel();
            View vFieldsRow = llFieldsContainer.getChildAt(i);

            if (vFieldsRow instanceof LinearLayout) {
                LinearLayout llFieldsRow = (LinearLayout) vFieldsRow;
                for (int j = 1; j < llFieldsRow.getChildCount(); j++) {
                    View vFieldsValue = llFieldsRow.getChildAt(1);

                    /**EditText*/
                    if (vFieldsValue instanceof EditText) {
                        EditText edtField = (EditText) vFieldsValue;
                        switch (edtField.getTag().toString()) {
                            case "email":

                                break;

                            default:
                                registrationModel.setName(edtField.getTag().toString());
                                registrationModel.setValue("");
                                registrationModelArrayList.add(registrationModel);
                                break;
                        }

                        /**RadioGroup*/
                    } else if (vFieldsValue instanceof RadioGroup) {
                        RadioGroup rgFields = (RadioGroup) vFieldsValue;
                        registrationModel.setName(rgFields.getTag().toString());
                        registrationModel.setValue("");
                        registrationModelArrayList.add(registrationModel);

                        /**Spinner*/
                    } else if (vFieldsValue instanceof Spinner) {
                        Spinner spnFields = (Spinner) vFieldsValue;
                        registrationModel.setName(spnFields.getTag().toString());
                        registrationModel.setValue("");
                        registrationModelArrayList.add(registrationModel);

                        /**Setting default for spinner*/
                        spnFields.setSelection(0);

                        /**Button*/
                    } else if (vFieldsValue instanceof Button) {
                        Button btnFields = (Button) vFieldsValue;
                        registrationModel.setName(btnFields.getTag().toString());
                        registrationModel.setValue("");
                        registrationModelArrayList.add(registrationModel);
                    }
                }
            }
        }

        Gson gson = new Gson();
        String clearedRegistrationData = gson.toJson(registrationModelArrayList);
        RegistrationModel registrationModel = new RegistrationModel();
        registrationModel.setData(clearedRegistrationData);
        return registrationModel;
    }

    public void setFieldsActivation(boolean isEnabled) {
        imgRefreshCaptcha.setEnabled(isEnabled);
        edtCaptchaInput.setEnabled(isEnabled);
        btnReset.setEnabled(isEnabled);
//        btnSubmit.setEnabled(isEnabled);

        for (int i = 0; i < llFieldsContainer.getChildCount(); i++) {
            View vFieldsRow = llFieldsContainer.getChildAt(i);

            if (vFieldsRow instanceof LinearLayout) {
                LinearLayout llFieldsRow = (LinearLayout) vFieldsRow;
                for (int j = 1; j < llFieldsRow.getChildCount(); j++) {
                    View vFieldsValue = llFieldsRow.getChildAt(1);

                    /**EditText*/
                    if (vFieldsValue instanceof EditText) {
                        EditText edtField = (EditText) vFieldsValue;
                        switch (edtField.getTag().toString()) {
                            case "email":
                                if (emailAddress == null) {
                                    edtField.requestFocus();
                                    edtField.setBackgroundResource(R.drawable.bg_error_registration_field_active);
                                } else if (emailAddress.isEmpty()) {
                                    edtField.requestFocus();
                                    edtField.setBackgroundResource(R.drawable.bg_error_registration_field_active);
                                } else if (!CheckerUtils.validateEmail(emailAddress)) {
                                    edtField.requestFocus();
                                    edtField.setBackgroundResource(R.drawable.bg_error_registration_field_active);
                                } else {
                                    edtField.setBackgroundResource(R.drawable.bg_registration_field_active);
                                }
                                break;

                            default:
                                edtField.setFocusableInTouchMode(isEnabled);
                                edtField.setFocusable(isEnabled);
                                edtField.setClickable(isEnabled);
                                if (!isFieldsIsValidating) {
                                    if (isEnabled) {
                                        edtField.setBackgroundResource(R.drawable.bg_registration_field_active);
                                        setSpecialValidation(edtField);
                                    } else {
                                        isMobileNumberInitializedTouch = false;
                                        isSecondaryMobileNumberInitializedTouch = false;
                                        edtField.setBackgroundResource(R.drawable.bg_registration_field_inactive);
                                    }
                                } else {
                                    setSpecialValidation(edtField);
                                }
                                break;
                        }

                        /**RadioGroup*/
                    } else if (vFieldsValue instanceof RadioGroup) {
                        RadioGroup rgFields = (RadioGroup) vFieldsValue;
                        Log.e(TAG, "setFieldsActivation: RadioGroupChild: " + rgFields.getChildCount());
                        for (int k = 0; k < rgFields.getChildCount(); k++) {
                            View rgChildView = rgFields.getChildAt(k);
                            if (rgChildView instanceof RadioButton) {
                                RadioButton radioButton = (RadioButton) rgChildView;
                                radioButton.setEnabled(isEnabled);
                            }
                        }

                        /**Spinner*/
                    } else if (vFieldsValue instanceof Spinner) {
                        Log.e(TAG, "setFieldsActivation: " + vFieldsValue.getTag());
                        Spinner spnFields = (Spinner) vFieldsValue;
                        switch (vFieldsValue.getTag().toString()) {
                            case "country_id":

                                break;

                            default:
                                spnFields.setEnabled(isEnabled);
                                break;
                        }

                        if (!isFieldsIsValidating) {
                            if (isEnabled) {
                                spnFields.setBackgroundResource(R.drawable.bg_registration_spinner_active);
                            } else {
                                spnFields.setBackgroundResource(R.drawable.bg_registration_spinner_inactive);
                            }
                        }

                        /**Button*/
                    } else if (vFieldsValue instanceof Button) {
                        Button btnFields = (Button) vFieldsValue;
                        btnFields.setFocusableInTouchMode(isEnabled);
                        btnFields.setFocusable(isEnabled);

                        if (!isFieldsIsValidating) {
                            if (isEnabled) {
                                btnFields.setBackgroundResource(R.drawable.bg_registration_field_active);
                            } else {
                                btnFields.setBackgroundResource(R.drawable.bg_registration_field_inactive);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Note: Special validation method is used for changing field UI based on its requirement
     */
    private void setSpecialValidation(EditText edtField) {
        switch (edtField.getTag().toString()) {
            case "mobile":
                if (isMobileNumberInitializedTouch) {
                    if (edtField.getText().length() < 10) {
                        edtField.setBackgroundResource(R.drawable.bg_error_registration_field_active);
                        edtField.setError("Please input valid mobile number");
                    } else {
                        edtField.setBackgroundResource(R.drawable.bg_registration_field_active);
                    }
                }
                break;

            case "secondary_mobile":
                if (isSecondaryMobileNumberInitializedTouch) {
                    if (edtField.getText().length() < 10) {
                        edtField.setBackgroundResource(R.drawable.bg_error_registration_field_active);
                        edtField.setError("Please input valid mobile number");
                    } else {
                        edtField.setBackgroundResource(R.drawable.bg_registration_field_active);
                    }
                }
                break;
        }
    }

    public void setSubscriptionStatus(boolean status) {
        chSubscription.setChecked(status);
    }

    @Override
    public void onResume() {
        super.onResume();
        emailValidationEnabled = true;

        /**Validating fields onResume*/
        if (emailAddress == null || emailAddress.isEmpty()) {
            isFieldsIsValidating = false;
            if (emailAddress != null) {
                Log.e(TAG, "onResume: " + "Checker 1:"
                        + " Email Address: " + emailAddress
                        + " isEmailValid: " + CheckerUtils.validateEmail(emailAddress));
                setFieldsActivation(CheckerUtils.validateEmail(emailAddress));
            } else {
                setFieldsActivation(false);
            }
        } else {
            if (CheckerUtils.validateEmail(emailAddress)) {
                Log.e(TAG, "onResume: " + "Checker 2" + " Email Address: " + emailAddress + " isEmailValid: " + CheckerUtils.validateEmail(emailAddress));
                setFieldsActivation(CheckerUtils.validateEmail(emailAddress));
            } else {
                Log.e(TAG, "onResume: " + "Checker 3" + " Email Address: " + emailAddress + " isEmailValid: " + CheckerUtils.validateEmail(emailAddress));
                isFieldsIsValidating = false;
                setFieldsActivation(false);
            }
        }
    }


    private void setAlphabetsOnly(EditText editText) {
        editText.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[a-zA-Z ]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });
    }
}

