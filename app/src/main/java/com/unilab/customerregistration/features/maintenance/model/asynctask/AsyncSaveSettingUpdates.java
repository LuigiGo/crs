package com.unilab.customerregistration.features.maintenance.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.unilab.customerregistration.features.maintenance.model.DefaultSettingModel;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.maintenance.model.SettingsResponseModel;
import com.unilab.customerregistration.features.maintenance.model.web.api.MaintenanceApiProvider;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.converters.ConverterUtil;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsyncSaveSettingUpdates extends AsyncTask<Void, Void, Boolean> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private SettingsModel mSettingModel;
    private MaintenancePresenter mMaintenancePresenter;

    private boolean mHasMobileLogo = false, mHasWebLogo = false, isCopyingDesign = false;
    private String brandCode, campaignCode, eventCode, formattedEventName;

    private boolean copyWebLogo = false, copyMobileLogo = false;
    private String mActionType;
    private DefaultSettingModel mDefaultSettingModel;
    private DatabaseHelper mDatabaseHelper;

    public AsyncSaveSettingUpdates(Context context, String actionType, SettingsModel settingsModel,
                                   boolean hasMobileLogo, boolean hasWebLogo, boolean copyingDesign) {
        Log.e(TAG, "AsyncSaveSettingUpdates: ");
        DialogHelper.init(context);
        SharedPrefHelper.init(context);
        mDatabaseHelper = new DatabaseHelper(context);

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();
        formattedEventName = SharedPrefHelper.getInstance().getFormattedCampaignNameFromShared();

        mContext = context;
        mActionType = actionType;
        mMaintenancePresenter = (MaintenancePresenter) context;

        mHasMobileLogo = hasMobileLogo;
        mHasWebLogo = hasWebLogo;
        isCopyingDesign = copyingDesign;
        mSettingModel = settingsModel;

        /**Initialize default settings model*/
        mDefaultSettingModel = mDatabaseHelper.getDefaultSettingsByBrand(brandCode);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        DialogHelper.getInstance().hideProgressDialog(mContext);
        DialogHelper.getInstance().showProgressDialog(mContext, "Saving settings update");
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        return updateSettings();
    }

    private boolean updateSettings() {
        Log.e(TAG, "updateSettings-1: " + "webLogo " + mHasWebLogo + " mobLogo " + mHasMobileLogo + " isCopyingDesign: " + isCopyingDesign);
        Log.e(TAG, "updateSettings-2: " + mSettingModel.toString());
        if (mHasWebLogo && mHasMobileLogo) {
            try {
                if (!isCopyingDesign) {
                    Log.e(TAG, "onClick: " + "hasSelectedWebLogo & mHasMobileLogo");
                    copyWebLogo = false;
                    copyMobileLogo = false;
                    mSettingModel.setWebLogoImg(ConverterUtil.convertUriToBase64(mContext, mSettingModel.getWebLogoImg()));
                    mSettingModel.setMobLogoImg(ConverterUtil.convertUriToBase64(mContext, mSettingModel.getMobLogoImg()));

                } else {
                    copyWebLogo = true;
                    copyMobileLogo = true;

                    if (mSettingModel.getWebLogoImg() == null) {
                        mSettingModel.setWebLogoImg(Utils.getDateTime());
                    }

                    if (mSettingModel.getMobLogoImg() == null) {
                        mSettingModel.setMobLogoImg(Utils.getDateTime());
                    }

                    mSettingModel.setWebLogoImgFileName(Utils.getLastWordInString(mSettingModel.getWebLogoImg()));
                    mSettingModel.setMobLogoImgFileName(Utils.getLastWordInString(mSettingModel.getMobLogoImg()));

                }
                callUpdateAllSettingAPI();

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (mHasWebLogo) {
            try {
                Log.e(TAG, "onClick: " + "hasSelectedWebLogo");
                Log.e(TAG, "updateSettings: webLogoImg: " + (mSettingModel.getWebLogoImg() == null) + " value: " + mSettingModel.toString());
                copyWebLogo = false;
                if (mSettingModel.getWebLogoImg().isEmpty()) {
                    mSettingModel.setWebLogoImg("");
                    mSettingModel.setWebLogoImgFileName("");
//                    mSettingModel.setWebLogoImgFileName("uploads/brand/".concat(mDefaultSettingModel.getFilename() + ".png"));
                } else {
                    mSettingModel.setWebLogoImg(ConverterUtil.convertUriToBase64(mContext, mSettingModel.getWebLogoImg()));
                }
                callUpdateSettingWithWebLogoAPI();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (mHasMobileLogo) {
            try {
                Log.e(TAG, "onClick: " + "mHasMobileLogo");
                Log.e(TAG, "updateSettings: mobLogoImg: " + (mSettingModel.getMobLogoImg() == null) + " value: " + mSettingModel.toString());
                copyMobileLogo = false;
                if (mSettingModel.getMobLogoImg().isEmpty()) {
                    mSettingModel.setMobLogoImg("");
                    mSettingModel.setMobLogoImgFileName("");
//                    mSettingModel.setMobLogoImgFileName("uploads/brand/".concat(mDefaultSettingModel.getFilename() + ".png"));

                } else {
                    mSettingModel.setMobLogoImg(ConverterUtil.convertUriToBase64(mContext, mSettingModel.getMobLogoImg()));
                }

                callUpdateSettingWithMobLogoAPI();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            callUpdateSettingWithoutLogoAPI();
        }
        return false;
    }

    private void callUpdateAllSettingAPI() {
        Log.e(TAG, "callUpdateAllSettingAPI: " + mSettingModel.toString());
        Call<SettingsResponseModel> updateSettingCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .updateAllSetting(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_UPDATE_SETTING,
                        campaignCode, brandCode, eventCode, formattedEventName,
                        mSettingModel.getForeground(),
                        mSettingModel.getBackground(),
                        mSettingModel.getHeader(),
                        mSettingModel.getFooter(),
                        mSettingModel.getFooterText(),
                        mSettingModel.getWebLogoImg(),
                        mSettingModel.getWebLogo(),
                        mSettingModel.getMobLogoImg(),
                        mSettingModel.getMobLogo(),
                        mSettingModel.getFontHeadfamily(),
                        mSettingModel.getFontHeadsize(),
                        mSettingModel.getFontBasefamily(),
                        mSettingModel.getFontBasesize(),
                        mSettingModel.getSubmitBorderWidth(),
                        mSettingModel.getSubmitBorderRadius(),
                        mSettingModel.getSubmitBackground(),
                        mSettingModel.getSubmitAlignment(),
                        mSettingModel.getDateTime(),
                        mSettingModel.getWebLogoImgFileName(),
                        mSettingModel.getMobLogoImgFileName(),
                        copyWebLogo ? Constants.COPYING_LOGO : Constants.UPLOAD_LOGO,
                        copyMobileLogo ? Constants.COPYING_LOGO : Constants.UPLOAD_LOGO);
        updateSettingCall.enqueue(new Callback<SettingsResponseModel>() {

            @Override
            public void onResponse(Call<SettingsResponseModel> call, Response<SettingsResponseModel> response) {
                if (response.body() != null) {
                    Log.e(TAG, "onResponse: " + response.body().getStatusResponse() + " " + response.body().getMessageResponse());
                    String status = response.body().getStatusResponse();
                    switch (status) {
                        case "success":
                            Log.e(TAG, "onResponse: " + "Success");
                            switch (mActionType) {
                                case Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT:
                                    mMaintenancePresenter.showAlertDialog("Alert", "Changes have been applied successfully!",
                                            Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT);
                                    break;
                                case Constants.DIALOG_RESET_DESIGN_SETTINGS:
                                    mMaintenancePresenter.showAlertDialog("Alert", "Changes have been applied successfully!",
                                            Constants.DIALOG_RESET_DESIGN_SETTINGS);
                                    break;
                            }
                            break;

                        case "failed":
                            String message = response.body().getMessageResponse();
                            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                            break;
                    }
                } else {
                    Log.e(TAG, "onResponse: " + response.toString());
                }
                DialogHelper.getInstance().hideProgressDialog(mContext);
            }

            @Override
            public void onFailure(Call<SettingsResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                DialogHelper.getInstance().hideProgressDialog(mContext);
            }
        });
    }

    private void callUpdateSettingWithoutLogoAPI() {
        Log.e(TAG, "callUpdateSettingWithoutLogoAPI: " + mSettingModel.toString());
        Call<SettingsResponseModel> updateSettingCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .updateSettingWithoutLogo(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_UPDATE_SETTING,
                        campaignCode, brandCode, eventCode, formattedEventName,
                        mSettingModel.getForeground(),
                        mSettingModel.getBackground(),
                        mSettingModel.getHeader(),
                        mSettingModel.getFooter(),
                        mSettingModel.getFooterText(),
                        mSettingModel.getWebLogo(),
                        mSettingModel.getMobLogo(),
                        mSettingModel.getFontHeadfamily(),
                        mSettingModel.getFontHeadsize(),
                        mSettingModel.getFontBasefamily(),
                        mSettingModel.getFontBasesize(),
                        mSettingModel.getSubmitBorderWidth(),
                        mSettingModel.getSubmitBorderRadius(),
                        mSettingModel.getSubmitBackground(),
                        mSettingModel.getSubmitAlignment(),
                        mSettingModel.getDateTime(),
                        copyWebLogo ? Constants.COPYING_LOGO : Constants.UPLOAD_LOGO,
                        copyMobileLogo ? Constants.COPYING_LOGO : Constants.UPLOAD_LOGO);
        updateSettingCall.enqueue(new Callback<SettingsResponseModel>() {
            @Override
            public void onResponse(Call<SettingsResponseModel> call, Response<SettingsResponseModel> response) {
                String status = response.body().getStatusResponse();
                switch (status) {
                    case "success":
                        Log.e(TAG, "onResponse: callUpdateSettingAPI" + "Success");
                        switch (mActionType) {
                            case Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT:
                                mMaintenancePresenter.showAlertDialog("Alert", "Changes have been applied successfully!",
                                        Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT);
                                break;
                            case Constants.DIALOG_RESET_DESIGN_SETTINGS:
                                mMaintenancePresenter.showAlertDialog("Alert", "Changes have been applied successfully!",
                                        Constants.DIALOG_RESET_DESIGN_SETTINGS);
                                break;
                        }
                        break;

                    case "failed":
                        String message = response.body().getMessageResponse();
                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        break;
                }
                DialogHelper.getInstance().hideProgressDialog(mContext);
            }

            @Override
            public void onFailure(Call<SettingsResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                DialogHelper.getInstance().hideProgressDialog(mContext);
            }
        });

    }

    private void callUpdateSettingWithMobLogoAPI() {
        Log.e(TAG, "callUpdateSettingWithMobLogoAPI: " + mSettingModel.toString());
        Call<SettingsResponseModel> updateSettingCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .updateSettingWithMobLogo(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_UPDATE_SETTING,
                        campaignCode, brandCode, eventCode, formattedEventName,
                        mSettingModel.getForeground(),
                        mSettingModel.getBackground(),
                        mSettingModel.getHeader(),
                        mSettingModel.getFooter(),
                        mSettingModel.getFooterText(),
                        mSettingModel.getWebLogo(),
                        mSettingModel.getMobLogoImg(),
                        mSettingModel.getMobLogo(),
                        mSettingModel.getFontHeadfamily(),
                        mSettingModel.getFontHeadsize(),
                        mSettingModel.getFontBasefamily(),
                        mSettingModel.getFontBasesize(),
                        mSettingModel.getSubmitBorderWidth(),
                        mSettingModel.getSubmitBorderRadius(),
                        mSettingModel.getSubmitBackground(),
                        mSettingModel.getSubmitAlignment(),
                        mSettingModel.getDateTime(),
                        mSettingModel.getMobLogoImgFileName(),
                        copyWebLogo ? Constants.COPYING_LOGO : Constants.UPLOAD_LOGO,
                        copyMobileLogo ? Constants.COPYING_LOGO : Constants.UPLOAD_LOGO);
        updateSettingCall.enqueue(new Callback<SettingsResponseModel>() {
            @Override
            public void onResponse(Call<SettingsResponseModel> call, Response<SettingsResponseModel> response) {
                Log.e(TAG, "onResponse: " + response.body().getStatusResponse() + " " + response.body().getMessageResponse());
                String status = response.body().getStatusResponse();
                switch (status) {
                    case "success":
                        Log.e(TAG, "onResponse: " + "Success");
                        switch (mActionType) {
                            case Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT:
                                mMaintenancePresenter.showAlertDialog("Alert", "Changes have been applied successfully!",
                                        Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT);
                                break;
                            case Constants.DIALOG_RESET_DESIGN_SETTINGS:
                                mMaintenancePresenter.showAlertDialog("Alert", "Changes have been applied successfully!",
                                        Constants.DIALOG_RESET_DESIGN_SETTINGS);
                                break;
                        }
                        break;

                    case "failed":
                        String message = response.body().getMessageResponse();
                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        break;
                }
                DialogHelper.getInstance().hideProgressDialog(mContext);
            }

            @Override
            public void onFailure(Call<SettingsResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                DialogHelper.getInstance().hideProgressDialog(mContext);
            }
        });

    }

    private void callUpdateSettingWithWebLogoAPI() {
        Log.e(TAG, "callUpdateSettingWithWebLogoAPI: " + mSettingModel.toString());
        Call<SettingsResponseModel> updateSettingCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .updateSettingWithWebLogo(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_UPDATE_SETTING,
                        campaignCode, brandCode, eventCode, formattedEventName,
                        mSettingModel.getForeground(),
                        mSettingModel.getBackground(),
                        mSettingModel.getHeader(),
                        mSettingModel.getFooter(),
                        mSettingModel.getFooterText(),
                        mSettingModel.getWebLogo(),
                        mSettingModel.getWebLogoImg(),
                        mSettingModel.getMobLogo(),
                        mSettingModel.getFontHeadfamily(),
                        mSettingModel.getFontHeadsize(),
                        mSettingModel.getFontBasefamily(),
                        mSettingModel.getFontBasesize(),
                        mSettingModel.getSubmitBorderWidth(),
                        mSettingModel.getSubmitBorderRadius(),
                        mSettingModel.getSubmitBackground(),
                        mSettingModel.getSubmitAlignment(),
                        mSettingModel.getDateTime(),
                        mSettingModel.getWebLogoImgFileName(),
                        copyWebLogo ? Constants.COPYING_LOGO : Constants.UPLOAD_LOGO,
                        copyMobileLogo ? Constants.COPYING_LOGO : Constants.UPLOAD_LOGO);
        updateSettingCall.enqueue(new Callback<SettingsResponseModel>() {
            @Override
            public void onResponse(Call<SettingsResponseModel> call, Response<SettingsResponseModel> response) {
                if (response.body() != null) {
                    Log.e(TAG, "onResponse: " + response.body().getStatusResponse() + " " + response.body().getMessageResponse());
                    String status = response.body().getStatusResponse();
                    switch (status) {
                        case "success":
                            Log.e(TAG, "onResponse: " + "Success");
                            switch (mActionType) {
                                case Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT:
                                    mMaintenancePresenter.showAlertDialog("Alert", "Changes have been applied successfully!",
                                            Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT);
                                    break;
                                case Constants.DIALOG_RESET_DESIGN_SETTINGS:
                                    mMaintenancePresenter.showAlertDialog("Alert", "Changes have been applied successfully!",
                                            Constants.DIALOG_RESET_DESIGN_SETTINGS);
                                    break;
                            }
                            break;

                        case "failed":
                            String message = response.body().getMessageResponse();
                            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                            break;
                    }
                } else {
                    Log.e(TAG, "onResponse: " + response.toString());
                }
                DialogHelper.getInstance().hideProgressDialog(mContext);
            }

            @Override
            public void onFailure(Call<SettingsResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                DialogHelper.getInstance().hideProgressDialog(mContext);
            }
        });

    }
}
