package com.unilab.customerregistration.features.maintenance.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.BrandModel;
import com.unilab.customerregistration.features.maintenance.model.CampaignEventModel;
import com.unilab.customerregistration.features.maintenance.model.CampaignModel;
import com.unilab.customerregistration.features.maintenance.model.DataSourceModel;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.maintenance.model.asynctask.AsyncConfigureDesignForm;
import com.unilab.customerregistration.features.maintenance.model.asynctask.AsyncTaskFetchDefaultFields;
import com.unilab.customerregistration.features.maintenance.model.web.api.MaintenanceApiProvider;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.BrandArrayAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.CampaignArrayAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.CampaignEventArrayAdapter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.network.NetworkUtils;
import com.unilab.customerregistration.utilities.network.RestManager;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.smtp.asynctask.AsyncSendEmail;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BrandFragment extends Fragment implements View.OnClickListener {

    private final String TAG = this.getClass().getSimpleName();
    private View mView;
    RecyclerView mRecyclerViewBrands;
    Spinner spnBrands, spnCampaigns, spnEvent;
    private BrandArrayAdapter mBrandArrayAdapter;
    private CampaignArrayAdapter mCampaignArrayAdapter;
    private CampaignEventArrayAdapter mCampaignEventArrayAdapter;
    private ArrayList<BrandModel> mBrandModelList;
    private ArrayList<CampaignModel> mCampaignModelList;
    private ArrayList<CampaignEventModel> mCampaignEventModelList;
    Button btnSave, btnSendUrl;
    private String mSelectedBrandCode, mSelectedCampaignCode, mSelectedCampaignEventCode;
    private String mSelectedBrandName, mSelectedCampaignName, mSelectedCampaignEventName, mFormattedCampaignEventName;
    private AlertDialog mAlertSuccessPromptDialog;
    private DatabaseHelper mDatabaseHelper;
    private MaintenancePresenter mMaintenancePresenter;
    EditText edtGeneratedUrl;
    String email, brandCode, campaignCode, eventCode, brandName, campaignName, eventName, formattedEventName, generatedUrl;
    private boolean isInitialLoad = true;

    private TextView txtInitiativeType;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        mView = inflater.inflate(R.layout.fragment_brand, container, false);
        mDatabaseHelper = new DatabaseHelper(getActivity());

        SharedPrefHelper.init(getActivity());
        RestManager.init();
        DialogHelper.init(getActivity());

        mMaintenancePresenter = (MaintenancePresenter) getActivity();

        email = SharedPrefHelper.getInstance().getEmailFromShared();
        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();
        formattedEventName = SharedPrefHelper.getInstance().getFormattedCampaignNameFromShared();

        brandName = SharedPrefHelper.getInstance().getBrandNameFromShared();
        campaignName = SharedPrefHelper.getInstance().getCampaignNameFromShared();
        eventName = SharedPrefHelper.getInstance().getEventNameFromShared();

        generatedUrl = SharedPrefHelper.getInstance().getGeneratedUrlFromShared();

        Log.e(TAG, "onCreateView: " + (savedInstanceState == null));
        setupViews();

        if (savedInstanceState == null) {
            mSelectedBrandCode = brandCode;
            mSelectedCampaignCode = campaignCode;
            mSelectedCampaignEventCode = eventCode;
            mSelectedBrandName = brandName;
            mSelectedCampaignCode = campaignName;
            mSelectedCampaignEventName = eventName;
            mFormattedCampaignEventName = formattedEventName;

            if (brandCode.isEmpty() || campaignCode.isEmpty() || eventCode.isEmpty()
                    || brandCode == null || campaignCode == null || eventCode == null) {
                callBrandAPI();

            } else {
                callBrandAPI();
                callCampaignAPI(mSelectedBrandCode, mSelectedBrandName);
                callCampaignEventAPI(campaignCode, mSelectedCampaignName);
                callDataSourceAPI();
            }

            isInitialLoad = false;
        }


        Log.e(TAG, "onCreateView: BrandCode: " + mSelectedBrandCode + " CampaignCode: " + mSelectedCampaignCode + " EventCode: " + mSelectedCampaignEventCode);
        return mView;
    }

    private void setupViews() {
        mRecyclerViewBrands = (RecyclerView) mView.findViewById(R.id.recyclerViewBrands);
        GridLayoutManager mGridLayoutManager = new GridLayoutManager(getActivity(), 4);
        mRecyclerViewBrands.setHasFixedSize(true);
        mRecyclerViewBrands.setLayoutManager(mGridLayoutManager);

        txtInitiativeType = (TextView) mView.findViewById(R.id.txtInitiativeType);

        spnBrands = (Spinner) mView.findViewById(R.id.spnBrands);
        spnCampaigns = (Spinner) mView.findViewById(R.id.spnCampaigns);
        spnEvent = (Spinner) mView.findViewById(R.id.spnEvent);

        edtGeneratedUrl = (EditText) mView.findViewById(R.id.edtGeneratedURL);

        btnSendUrl = (Button) mView.findViewById(R.id.btnSendUrl);
        btnSave = (Button) mView.findViewById(R.id.btnSave);

        /**Listeners*/
        btnSave.setOnClickListener(this);
        btnSendUrl.setOnClickListener(this);

        /**Setting up lists*/
        mBrandModelList = new ArrayList<>();
        initialListPrompts(Constants.BRAND_LIST_PROMPT);
        mBrandArrayAdapter = new BrandArrayAdapter(getActivity(), R.layout.list_spinner_default, mBrandModelList);
        spnBrands.setAdapter(mBrandArrayAdapter);

        mCampaignModelList = new ArrayList<>();
        initialListPrompts(Constants.CAMPAIGN_LIST_PROMPT);
        mCampaignArrayAdapter = new CampaignArrayAdapter(getActivity(), R.layout.list_spinner_default, mCampaignModelList);
        spnCampaigns.setAdapter(mCampaignArrayAdapter);

        mCampaignEventModelList = new ArrayList<>();
        initialListPrompts(Constants.EVENT_LIST_PROMPT);
        mCampaignEventArrayAdapter = new CampaignEventArrayAdapter(getActivity(), R.layout.list_spinner_default, mCampaignEventModelList);
        spnEvent.setAdapter(mCampaignEventArrayAdapter);

        edtGeneratedUrl.setText(generatedUrl);

    }

    private void callBrandAPI() {
        Log.e(TAG, "callBrandAPI: ");
        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Loading Brands");

        Call<List<BrandModel>> onGetBrandListsCall = MaintenanceApiProvider.getMaintenanceDbmApiRoutes()
                .onGetBrandLists(Constants.API_DBM_TOKEN, Constants.CMD_EVENT_GET_BRAND, email, Constants.DBM_HOSTNAME);
        onGetBrandListsCall.enqueue(new Callback<List<BrandModel>>() {

            @Override
            public void onResponse(Call<List<BrandModel>> call, Response<List<BrandModel>> response) {
                if (response.body() != null) {
                    List<BrandModel> brandModelList = response.body();

                    mBrandModelList.clear();
                    if (brandModelList.size() > 0) {
                        BrandModel brandModel = new BrandModel();
                        brandModel.setName("--Please select your brand--");
                        brandModel.setBrandCode(null);
                        mBrandModelList.add(brandModel);
                        mBrandModelList.addAll(brandModelList);

                        /**Sorting into ascending order*/
                        Collections.sort(mBrandModelList, new Comparator<BrandModel>() {
                            public int compare(BrandModel obj1, BrandModel obj2) {
                                return obj1.getName().compareToIgnoreCase(obj2.getName());
                            }
                        });

                        int selectedItemPosition = -1;
                        for (int i = 0; i < mBrandArrayAdapter.getCount(); i++) {
                            if (mBrandArrayAdapter.getItem(i).getName().equals(brandName)) {
                                selectedItemPosition = i;
                            }
                        }
                        spnBrands.setSelection(selectedItemPosition);

                    } else {
                        BrandModel brandModel = new BrandModel();
                        brandModel.setName("No brand found!");
                        brandModel.setBrandCode(null);
                        mBrandModelList.add(brandModel);
                    }
                    mBrandArrayAdapter.notifyDataSetChanged();
                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());

            }

            @Override
            public void onFailure(Call<List<BrandModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(getActivity());


            }
        });
    }

    public void callCampaignAPI(final String brandCode, String brandName) {
        if (!isInitialLoad) {
            edtGeneratedUrl.setText("");
            txtInitiativeType.setText("No data source");
        }

        Log.e(TAG, "callCampaignAPI: BrandCode: " + brandCode + " BrandName: " + brandName);
        mSelectedBrandCode = brandCode;
        mSelectedBrandName = brandName;

        mCampaignModelList.clear();
        mCampaignArrayAdapter.notifyDataSetChanged();
        mSelectedCampaignCode = null;

        mCampaignEventModelList.clear();
        mCampaignEventArrayAdapter.notifyDataSetChanged();
//        mSelectedCampaignEventCode = null;

        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Loading campaigns");
        Call<List<CampaignModel>> onGetCampaignList = MaintenanceApiProvider.getMaintenanceDbmApiRoutes()
                .onGetCampaignLists(Constants.API_DBM_TOKEN, Constants.CMD_EVENT_GET_CAMPAIGN, brandCode, Constants.DBM_HOSTNAME);
        onGetCampaignList.enqueue(new Callback<List<CampaignModel>>() {

            @Override
            public void onResponse(Call<List<CampaignModel>> call, Response<List<CampaignModel>> response) {
                if (response.body() != null) {
                    List<CampaignModel> campaignModelList = response.body();
                    Log.e(TAG, "onResponse: campaignModelListSize: " + campaignModelList.size());

                    mCampaignModelList.clear();
                    if (campaignModelList.size() > 0) {
                        CampaignModel campaignModel = new CampaignModel();
                        campaignModel.setName("--Please select your campaign--");
                        campaignModel.setCode(null);
                        mCampaignModelList.add(campaignModel);
                        mCampaignModelList.addAll(campaignModelList);

                        /**Sorting into ascending order*/
                        Collections.sort(mCampaignModelList, new Comparator<CampaignModel>() {
                            public int compare(CampaignModel obj1, CampaignModel obj2) {
                                return obj1.getName().compareToIgnoreCase(obj2.getName());
                            }
                        });

                        int selectedItemPosition = -1;
                        for (int i = 0; i < mCampaignArrayAdapter.getCount(); i++) {
                            if (mCampaignArrayAdapter.getItem(i).getName().equals(campaignName)) {
                                selectedItemPosition = i;
                            }
                        }
                        spnCampaigns.setSelection(selectedItemPosition);
                    } else {
                        CampaignModel campaignModel = new CampaignModel();
                        campaignModel.setName("No campaign found!");
                        campaignModel.setCode(null);
                        mCampaignModelList.add(campaignModel);
                    }
                    mCampaignArrayAdapter.notifyDataSetChanged();
                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }

            @Override
            public void onFailure(Call<List<CampaignModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(getActivity());

            }
        });
    }

    public void callCampaignEventAPI(final String campaignCode, String campaignName) {
        mSelectedCampaignCode = campaignCode;
        mSelectedCampaignName = campaignName;
        if (!isInitialLoad) {
            edtGeneratedUrl.setText("");
            txtInitiativeType.setText("No data source");
        }

        /**Get data source of selected brand and campaign*/
        Log.e(TAG, "callCampaignEventAPI: CampaignCode: " + mSelectedCampaignCode);
        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Loading Initiative");

        Call<List<CampaignEventModel>> onGetCampaignEventList = MaintenanceApiProvider.getMaintenanceDbmApiRoutes()
                .onGetCampaignEventLists(Constants.API_DBM_TOKEN, Constants.CMD_EVENT_GET_CAMPAIGN_EVENT,
                        mSelectedCampaignCode, Constants.DBM_HOSTNAME);
        onGetCampaignEventList.enqueue(new Callback<List<CampaignEventModel>>() {
            @Override
            public void onResponse(Call<List<CampaignEventModel>> call, Response<List<CampaignEventModel>> response) {
                if (response.body() != null) {
                    List<CampaignEventModel> campaignEventModels = response.body();
                    mCampaignEventModelList.clear();
                    if (campaignEventModels.size() > 0) {
                        CampaignEventModel campaignEventModel = new CampaignEventModel();
                        campaignEventModel.setName("--Please select your Initiative--");
                        campaignEventModel.setCode(null);
                        mCampaignEventModelList.add(campaignEventModel);
                        mCampaignEventModelList.addAll(campaignEventModels);

                        /**Sorting into ascending order*/
                        Collections.sort(mCampaignEventModelList, new Comparator<CampaignEventModel>() {
                            public int compare(CampaignEventModel obj1, CampaignEventModel obj2) {
                                return obj1.getName().compareToIgnoreCase(obj2.getName());
                            }
                        });

                        int selectedItemPosition = -1;
                        for (int i = 0; i < mCampaignEventArrayAdapter.getCount(); i++) {
                            if (mCampaignEventArrayAdapter.getItem(i).getName().equals(eventName)) {
                                selectedItemPosition = i;
                            }
                        }
                        spnEvent.setSelection(selectedItemPosition);

                    } else {
                        CampaignEventModel campaignEventModel = new CampaignEventModel();
                        campaignEventModel.setName("No Initiative found!");
                        campaignEventModel.setCode(null);
                        mCampaignEventModelList.add(campaignEventModel);
                    }
                    mCampaignEventArrayAdapter.notifyDataSetChanged();
                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }

            @Override
            public void onFailure(Call<List<CampaignEventModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                DialogHelper.getInstance().hideProgressDialog(getActivity());
                mMaintenancePresenter.showAlertDialog("Alert", getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
            }
        });
    }

    public void callDataSourceAPI() {
        Log.e(TAG, "callDataSourceAPI: ");
        Call<List<DataSourceModel>> onGetDataSourceList = MaintenanceApiProvider.getMaintenanceDbmApiRoutes()
                .onGetDataSource(Constants.API_DBM_TOKEN, Constants.CMD_EVENT_GET_DATA_SOURCE, mSelectedCampaignEventCode, Constants.DBM_HOSTNAME);
        onGetDataSourceList.enqueue(new Callback<List<DataSourceModel>>() {

            @Override
            public void onResponse(Call<List<DataSourceModel>> call, Response<List<DataSourceModel>> response) {
                if (response.body() != null) {
                    List<DataSourceModel> dataSourceModelList = response.body();
                    if (dataSourceModelList.size() != 0) {
                        for (DataSourceModel dataSourceModel : dataSourceModelList) {
                            Log.e(TAG, "onResponse: CampaignId: " + dataSourceModel.getCampaignId());
                            Log.e(TAG, "onResponse: InitiativeTitle: " + dataSourceModel.getInitiativeTitle());
                            Log.e(TAG, "onResponse: Title: " + dataSourceModel.getTitle());
                            txtInitiativeType.setText(dataSourceModel.getTitle());
                        }
                    } else {
                        txtInitiativeType.setText("No data source");
                    }
                } else {
                    Log.e(TAG, "onResponse: callDataSourceAPI: " + (response.body() == null));
                    txtInitiativeType.setText("No data source");
                }
            }

            @Override
            public void onFailure(Call<List<DataSourceModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: callDataSourceAPI: " + t.getMessage());
            }
        });
    }

    public void saveConfiguredSettings(String campaignEventCode, String campaignEventName) {
        mSelectedCampaignEventCode = campaignEventCode;
        mSelectedCampaignEventName = campaignEventName;

        String baseUrl = Constants.API_WEB_BASE_URL3;
        String formattedEventName = mSelectedCampaignEventName.replace(" ", "-").toLowerCase();
        mFormattedCampaignEventName = formattedEventName;

        /**Getting data source*/
        callDataSourceAPI();

        if (!formattedEventName.equals("--please-select-your-initiative--") &&
                !formattedEventName.equals("no-initiative-found!")) {
            edtGeneratedUrl.setText(baseUrl.concat(formattedEventName));
        } else {
            edtGeneratedUrl.setText("");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSave:
                if (mSelectedBrandCode == null || mSelectedBrandCode.isEmpty()) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please select Brand", Constants.DIALOG_ACTION_NORMAL);

                } else if (mSelectedCampaignCode == null || mSelectedCampaignCode.isEmpty()) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please select Campaign", Constants.DIALOG_ACTION_NORMAL);

                } else if (mSelectedCampaignEventCode == null || mSelectedCampaignEventCode.isEmpty()) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please select Initiative", Constants.DIALOG_ACTION_NORMAL);

                } else {
                    String generatedUrl = edtGeneratedUrl.getText().toString();

                    SharedPrefHelper.getInstance().putBrandCodeToShared(mSelectedBrandCode);
                    SharedPrefHelper.getInstance().putBrandNameToShared(mSelectedBrandName);
                    SharedPrefHelper.getInstance().putCampaignCodeToShared(mSelectedCampaignCode);
                    SharedPrefHelper.getInstance().putCampaignNameToShared(mSelectedCampaignName);
                    SharedPrefHelper.getInstance().putEventCodeToShared(mSelectedCampaignEventCode);
                    SharedPrefHelper.getInstance().putEventNameToShared(mSelectedCampaignEventName);
                    SharedPrefHelper.getInstance().putFormattedCampaignNameToShared(mFormattedCampaignEventName);
                    SharedPrefHelper.getInstance().putGeneratedUrlToShared(generatedUrl);

                    new AsyncTaskFetchDefaultFields(getActivity()).execute();
                    new AsyncConfigureDesignForm(getActivity()).execute();
//                    callGetSettingAPI();
                }

                Log.e(TAG, "saveConfiguredSettings: Selected Brand Code: " + mSelectedBrandCode);
                Log.e(TAG, "saveConfiguredSettings: Selected Brand Name: " + mSelectedBrandName);
                Log.e(TAG, "saveConfiguredSettings: Selected Campaign Code: " + mSelectedCampaignCode);
                Log.e(TAG, "saveConfiguredSettings: Selected Campaign Name: " + mSelectedCampaignName);
                Log.e(TAG, "saveConfiguredSettings: Selected Campaign Event Code: " + mSelectedCampaignEventCode);
                Log.e(TAG, "saveConfiguredSettings: Selected Campaign Event Name: " + mSelectedCampaignEventName);
                Log.e(TAG, "saveConfiguredSettings: Selected Formatted Campaign Event Name: " + mFormattedCampaignEventName);
                break;

            case R.id.btnSendUrl:
                String email = SharedPrefHelper.getInstance().getEmailFromShared();
                Log.e(TAG, "onClick: email: " + email);

                String generatedURL = edtGeneratedUrl.getText().toString();

                if (NetworkUtils.isNetworkConnected(getActivity())) {
                    if (email == null || email.isEmpty()) {
                        mMaintenancePresenter.showAlertDialog("Alert", "Please check email recipient", Constants.DIALOG_ACTION_NORMAL);
                    }
                    if (mSelectedBrandCode == null || mSelectedBrandCode.isEmpty()) {
                        mMaintenancePresenter.showAlertDialog("Alert", "Please select Brand", Constants.DIALOG_ACTION_NORMAL);

                    } else if (mSelectedCampaignCode == null || mSelectedCampaignCode.isEmpty()) {
                        mMaintenancePresenter.showAlertDialog("Alert", "Please select Campaign", Constants.DIALOG_ACTION_NORMAL);

                    } else if (mSelectedCampaignEventCode == null || mSelectedCampaignEventCode.isEmpty()) {
                        mMaintenancePresenter.showAlertDialog("Alert", "Please select Initiative", Constants.DIALOG_ACTION_NORMAL);

                    } else if (generatedURL.isEmpty()) {
                        mMaintenancePresenter.showAlertDialog("Alert", "Please configure your account first", Constants.DIALOG_ACTION_NORMAL);

                    } else {
                        new AsyncSendEmail(getActivity(), email, generatedURL).execute();
                    }
                } else {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please check your internet connection.", Constants.DIALOG_ACTION_NORMAL);
                }
                break;
        }
    }

    private void callGetSettingAPI() {
        Log.e(TAG, "callGetSettingAPI: ");
        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Synchronize settings");
        Call<List<SettingsModel>> getSettingCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .getSetting(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_GET_SETTING, mSelectedCampaignCode, mSelectedBrandCode,
                        mSelectedCampaignEventCode, formattedEventName);
        getSettingCall.enqueue(new Callback<List<SettingsModel>>() {
            @Override
            public void onResponse(Call<List<SettingsModel>> call, Response<List<SettingsModel>> response) {
                int responseCode = response.code();
                Log.e(TAG, "onResponse: " + responseCode);
                switch (responseCode) {
                    case Constants.API_RESPONSE_OK:
                        List<SettingsModel> settingsModelList = response.body();
                        Log.e(TAG, "onResponse: " + settingsModelList.size());

                        if (settingsModelList.size() > 0) {
                            for (SettingsModel settingsModel : settingsModelList) {
                                if (mDatabaseHelper.checkSettingsIfExist(mSelectedBrandCode, mSelectedCampaignCode,
                                        mSelectedCampaignEventCode, mFormattedCampaignEventName)) {
                                    Log.e(TAG, "onResponse: " + "DB Setting exist");
                                    mDatabaseHelper.updateDbSetting(settingsModel);
                                    Log.e(TAG, "onResponse: ");
                                } else {
                                    Log.e(TAG, "onResponse: " + "DB Setting not exist");
                                    mDatabaseHelper.createDbSetting(settingsModel);
                                }
                            }

                        }
                        mMaintenancePresenter.showAlertDialog("Alert", "Settings saved successfully!",
                                Constants.DIALOG_ACTION_SAVE_BRAND_FRAGMENT);
                        break;
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());

            }

            @Override
            public void onFailure(Call<List<SettingsModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", t.getMessage(), Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.hideKeyboard(getActivity());
        Log.e(TAG, "onResume: " + mSelectedBrandCode);
        Log.e(TAG, "onResume: " + mSelectedCampaignCode);
        Log.e(TAG, "onResume: " + mSelectedCampaignEventCode);
    }

    private void initialListPrompts(int type) {
        String brandName = SharedPrefHelper.getInstance().getBrandNameFromShared();
        String brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        String campaignName = SharedPrefHelper.getInstance().getCampaignNameFromShared();
        String campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        String eventName = SharedPrefHelper.getInstance().getEventNameFromShared();
        String eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        switch (type) {
            case Constants.BRAND_LIST_PROMPT:
                BrandModel brandModel = new BrandModel();
                if (brandName == null || brandCode == null || brandName.isEmpty() || brandCode.isEmpty()) {
                    brandModel.setName("--Please select your brand--");
                    brandModel.setBrandCode(null);
                } else {
                    brandModel.setName(brandName);
                    brandModel.setBrandCode(brandCode);
                    mSelectedBrandCode = brandCode;
                    mSelectedBrandName = brandName;
                }
                mBrandModelList.add(brandModel);
                break;

            case Constants.CAMPAIGN_LIST_PROMPT:
                CampaignModel campaignModel = new CampaignModel();
                if (campaignName == null || campaignCode == null || campaignName.isEmpty() || campaignCode.isEmpty()) {
                    campaignModel.setName("--Please select your campaign--");
                    campaignModel.setCode(null);
                } else {
                    campaignModel.setName(campaignName);
                    campaignModel.setCode(campaignCode);
                    mSelectedCampaignCode = campaignCode;
                    mSelectedCampaignName = campaignName;
                }

                mCampaignModelList.add(campaignModel);
                break;

            case Constants.EVENT_LIST_PROMPT:
                CampaignEventModel campaignEventModel = new CampaignEventModel();
                if (eventName.isEmpty() && eventCode.isEmpty()) {
                    campaignEventModel.setName("--Please select your event--");
                    campaignEventModel.setCode(null);
                } else {
                    campaignEventModel.setName(eventName);
                    campaignEventModel.setCode(eventCode);
                    mSelectedCampaignEventCode = eventCode;
                    mSelectedCampaignEventName = eventName;
                }
                mCampaignEventModelList.add(campaignEventModel);
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.e(TAG, "onSaveInstanceState: ");
        outState.putString(Constants.STATE_SELECTED_BRAND_NAME, mSelectedBrandName);
        outState.putString(Constants.STATE_SELECTED_CAMPAIGN_NAME, mSelectedCampaignName);
        outState.putString(Constants.STATE_SELECTED_EVENT_NAME, mSelectedCampaignEventName);

        outState.putParcelableArrayList(Constants.STATE_BRAND_LIST, mBrandModelList);
        outState.putParcelableArrayList(Constants.STATE_CAMPAIGN_LIST, mCampaignModelList);
        outState.putParcelableArrayList(Constants.STATE_EVENT_LIST, mCampaignEventModelList);

        String initiativeType = txtInitiativeType.getText().toString();
        outState.putString(Constants.STATE_INITIATIVE_TYPE, initiativeType);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.e(TAG, "onViewStateRestored: " + (savedInstanceState == null));
        if (savedInstanceState != null) {
            Log.e(TAG, "onViewStateRestored: BrandName: " + savedInstanceState.getString(Constants.STATE_SELECTED_BRAND_NAME));
            Log.e(TAG, "onViewStateRestored: CampaignName: " + savedInstanceState.getString(Constants.STATE_SELECTED_CAMPAIGN_NAME));
            Log.e(TAG, "onViewStateRestored: EventName: " + savedInstanceState.getString(Constants.STATE_SELECTED_EVENT_NAME));

            ArrayList<BrandModel> brandListFromParcel = savedInstanceState.getParcelableArrayList(Constants.STATE_BRAND_LIST);
            mBrandModelList.clear();
            mBrandModelList.addAll(brandListFromParcel);
            mBrandArrayAdapter.notifyDataSetChanged();

            ArrayList<CampaignModel> campaignListFromParcel = savedInstanceState.getParcelableArrayList(Constants.STATE_CAMPAIGN_LIST);
            mCampaignModelList.clear();
            mCampaignModelList.addAll(campaignListFromParcel);
            mCampaignArrayAdapter.notifyDataSetChanged();

            ArrayList<CampaignEventModel> campaignEventFromParcel = savedInstanceState.getParcelableArrayList(Constants.STATE_EVENT_LIST);
            mCampaignEventModelList.clear();
            mCampaignEventModelList.addAll(campaignEventFromParcel);
            mCampaignEventArrayAdapter.notifyDataSetChanged();

            String initiativeType = savedInstanceState.getString(Constants.STATE_INITIATIVE_TYPE);
            txtInitiativeType.setText(initiativeType);
        }
    }

}
