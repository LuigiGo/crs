package com.unilab.customerregistration.features.maintenance.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class FieldsModel implements Parcelable {

    String a_id;
    String campaignCode;
    String brandCode;
    String inputId;
    String userId;
    String a_isActive;
    String a_dateTime;
    String a_status;
    String b_id;
    String typeId;
    String title;
    String name;
    String description;
    String groupId;
    String isRequired;
    String b_isActive;
    String b_dateTime;
    String category;
    String b_status;
    String c_title;
    String value = "";
    int sort;
    List<DataArray> dataArrayList;
    boolean viewed;
    String editStatus;
    String isMandatory;
    String eventCode;
    String groupName;
    String dbmStatus;

    public FieldsModel() {
    }

    protected FieldsModel(Parcel in) {
        a_id = in.readString();
        campaignCode = in.readString();
        brandCode = in.readString();
        inputId = in.readString();
        userId = in.readString();
        a_isActive = in.readString();
        a_dateTime = in.readString();
        a_status = in.readString();
        b_id = in.readString();
        typeId = in.readString();
        title = in.readString();
        name = in.readString();
        description = in.readString();
        groupId = in.readString();
        isRequired = in.readString();
        b_isActive = in.readString();
        b_dateTime = in.readString();
        category = in.readString();
        b_status = in.readString();
        c_title = in.readString();
        value = in.readString();
        sort = in.readInt();
        viewed = in.readByte() != 0;
        editStatus = in.readString();
        isMandatory = in.readString();
        eventCode = in.readString();
        groupName = in.readString();
        dbmStatus = in.readString();
    }

    public static final Creator<FieldsModel> CREATOR = new Creator<FieldsModel>() {
        @Override
        public FieldsModel createFromParcel(Parcel in) {
            return new FieldsModel(in);
        }

        @Override
        public FieldsModel[] newArray(int size) {
            return new FieldsModel[size];
        }
    };

    public String getA_id() {
        return a_id;
    }

    public void setA_id(String a_id) {
        this.a_id = a_id;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getA_isActive() {
        return a_isActive;
    }

    public void setA_isActive(String a_isActive) {
        this.a_isActive = a_isActive;
    }

    public String getA_dateTime() {
        return a_dateTime;
    }

    public void setA_dateTime(String a_dateTime) {
        this.a_dateTime = a_dateTime;
    }

    public String getA_status() {
        return a_status;
    }

    public void setA_status(String a_status) {
        this.a_status = a_status;
    }

    public String getB_id() {
        return b_id;
    }

    public void setB_id(String b_id) {
        this.b_id = b_id;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(String isRequired) {
        this.isRequired = isRequired;
    }

    public String getB_isActive() {
        return b_isActive;
    }

    public void setB_isActive(String b_isActive) {
        this.b_isActive = b_isActive;
    }

    public String getB_dateTime() {
        return b_dateTime;
    }

    public void setB_dateTime(String b_dateTime) {
        this.b_dateTime = b_dateTime;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getB_status() {
        return b_status;
    }

    public void setB_status(String b_status) {
        this.b_status = b_status;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getC_title() {
        return c_title;
    }

    public void setC_title(String c_title) {
        this.c_title = c_title;
    }

    public List<DataArray> getDataArrayList() {
        return dataArrayList;
    }

    public void setDataArrayList(List<DataArray> dataArrayList) {
        this.dataArrayList = dataArrayList;
    }

    public boolean isViewed() {
        return viewed;
    }

    public void setViewed(boolean viewed) {
        this.viewed = viewed;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getEditStatus() {
        return editStatus;
    }

    public void setEditStatus(String editStatus) {
        this.editStatus = editStatus;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDbmStatus() {
        return dbmStatus;
    }

    public void setDbmStatus(String dbmStatus) {
        this.dbmStatus = dbmStatus;
    }

    @Override
    public String toString() {
        return "FieldsModel{" +
                "a_id='" + a_id + '\'' +
                ", campaignCode='" + campaignCode + '\'' +
                ", brandCode='" + brandCode + '\'' +
                ", inputId='" + inputId + '\'' +
                ", userId='" + userId + '\'' +
                ", a_isActive='" + a_isActive + '\'' +
                ", a_dateTime='" + a_dateTime + '\'' +
                ", a_status='" + a_status + '\'' +
                ", b_id='" + b_id + '\'' +
                ", typeId='" + typeId + '\'' +
                ", title='" + title + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", groupId='" + groupId + '\'' +
                ", isRequired='" + isRequired + '\'' +
                ", b_isActive='" + b_isActive + '\'' +
                ", b_dateTime='" + b_dateTime + '\'' +
                ", category='" + category + '\'' +
                ", b_status='" + b_status + '\'' +
                ", c_title='" + c_title + '\'' +
                ", value='" + value + '\'' +
                ", sort=" + sort +
                ", dataArrayList=" + dataArrayList +
                ", viewed=" + viewed +
                ", editStatus='" + editStatus + '\'' +
                ", isMandatory='" + isMandatory + '\'' +
                ", eventCode='" + eventCode + '\'' +
                ", groupName='" + groupName + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(a_id);
        dest.writeString(campaignCode);
        dest.writeString(brandCode);
        dest.writeString(inputId);
        dest.writeString(userId);
        dest.writeString(a_isActive);
        dest.writeString(a_dateTime);
        dest.writeString(a_status);
        dest.writeString(b_id);
        dest.writeString(typeId);
        dest.writeString(title);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(groupId);
        dest.writeString(isRequired);
        dest.writeString(b_isActive);
        dest.writeString(b_dateTime);
        dest.writeString(category);
        dest.writeString(b_status);
        dest.writeString(c_title);
        dest.writeString(value);
        dest.writeInt(sort);
        dest.writeByte((byte) (viewed ? 1 : 0));
        dest.writeString(editStatus);
        dest.writeString(isMandatory);
        dest.writeString(eventCode);
        dest.writeString(groupName);
        dest.writeString(dbmStatus);
    }

    public String checkFieldsChanges() {
        return "a_isActive='" + a_isActive + '\'' +
                ", typeId='" + typeId + '\'' +
                ", title='" + title + '\'' +
                ", groupId='" + groupId + '\'' +
                ", sort=" + sort +
                ", dataArrayList=" + dataArrayList;
    }
}
