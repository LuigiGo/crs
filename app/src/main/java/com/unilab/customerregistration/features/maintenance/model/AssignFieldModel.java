package com.unilab.customerregistration.features.maintenance.model;

import android.os.Parcel;
import android.os.Parcelable;

public class AssignFieldModel implements Parcelable {
    private String id;
    private String campaignCode;
    private String brandCode;
    private String inputId;
    private String userId;
    private String isActive;
    private String dateTime;
    private String status;
    private String eventCode;
    private int sort;

    public AssignFieldModel() {
    }

    protected AssignFieldModel(Parcel in) {
        id = in.readString();
        campaignCode = in.readString();
        brandCode = in.readString();
        inputId = in.readString();
        userId = in.readString();
        isActive = in.readString();
        dateTime = in.readString();
        status = in.readString();
        eventCode = in.readString();
        sort = in.readInt();
    }

    public static final Creator<AssignFieldModel> CREATOR = new Creator<AssignFieldModel>() {
        @Override
        public AssignFieldModel createFromParcel(Parcel in) {
            return new AssignFieldModel(in);
        }

        @Override
        public AssignFieldModel[] newArray(int size) {
            return new AssignFieldModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(campaignCode);
        dest.writeString(brandCode);
        dest.writeString(inputId);
        dest.writeString(userId);
        dest.writeString(isActive);
        dest.writeString(dateTime);
        dest.writeString(status);
        dest.writeString(eventCode);
        dest.writeInt(sort);
    }
}
