package com.unilab.customerregistration.features.registration.view.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.dashboard.model.RegionModel;
import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.constants.DatabaseConstants;
import com.unilab.customerregistration.utilities.converters.ConverterUtil;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RegistrationAdapter extends RecyclerView.Adapter<RegistrationAdapter.ViewHolder> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private List<FieldsModel> mFieldsModelList;
    private String filterGroupList = "";
    private DatabaseHelper mDatabaseHelper;

    private List<String> provinceList, cityList;
    private ArrayAdapter<String> provinceListAdapter, cityListAdapter;
    private int mYear, mMonth, mDay;

    SettingsModel mSettingModel;
    String brandCode, campaignCode;


    public RegistrationAdapter(Context context, List<FieldsModel> fieldsModelList) {
        mDatabaseHelper = new DatabaseHelper(context);
        SharedPrefHelper.init(context);

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();

        mContext = context;
        mFieldsModelList = fieldsModelList;
        //filterGroupList = new ArrayList<>();
        getSetting();

        /**Province*/
        provinceList = new ArrayList<>();
        provinceListAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, provinceList);

        /**City*/
        cityList = new ArrayList<>();
        cityListAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, cityList);


    }

    @Override
    public RegistrationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View views = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_registration, parent, false);
        return new ViewHolder(views);
    }

    @Override
    public void onBindViewHolder(final RegistrationAdapter.ViewHolder holder, int position) {
        final FieldsModel fieldsModel = mFieldsModelList.get(position);
        if (!fieldsModel.isViewed()) {
            fieldsModel.setViewed(true);
            //fieldsModel.setValue("");
            holder.txtFieldName.setText(fieldsModel.getTitle());
            if (fieldsModel.getValue().isEmpty()) {
                holder.txtFieldName.setError("");
            }
            Log.e(TAG, "Test : " + filterGroupList);
            try {
                if (!filterGroupList.contains(fieldsModel.getC_title())) {
                    holder.txtGroupName.setText(fieldsModel.getC_title());
                    //filterGroupList.add(fieldsModel.getC_title());
                    filterGroupList = fieldsModel.getC_title();
                } else {
                    holder.txtGroupName.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                Log.e(TAG, "onBindViewHolder: " + e.getMessage());
            }

            Log.e(TAG, "onBindViewHolder: " + fieldsModel.getTypeId());
            switch (fieldsModel.getTypeId()) {
                case "1":
                    holder.edtChildView.setVisibility(View.VISIBLE);
                    if (!fieldsModel.getValue().isEmpty()) {
                        holder.edtChildView.setText(fieldsModel.getValue());
                    }
                    break;

                case "3":
                    holder.hsRadioGroup.setVisibility(View.VISIBLE);
                    holder.dataArrayList = mDatabaseHelper.getDataArrayByFieldId(DatabaseConstants.TBL_RADIO,
                            fieldsModel.getB_id());
                    for (DataArray dataArray : holder.dataArrayList) {
                        RadioButton rb = new RadioButton(mContext);
                        rb.setText(dataArray.getTitle());
                        holder.rgChildView.addView(rb);
                    }

                    break;

                case "4":
                    holder.spnChildView.setVisibility(View.VISIBLE);
                    holder.dataArrayList = mDatabaseHelper.getDataArrayByFieldId(DatabaseConstants.TBL_SELECT,
                            fieldsModel.getB_id());
                    List<String> list = new ArrayList<>();
                    for (DataArray dataArray : holder.dataArrayList) {
                        list.add(dataArray.getTitle());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, list);
                    holder.spnChildView.setAdapter(adapter);

                    switch (fieldsModel.getName()) {
                        case "country_id":
                            list.add("Philippines");
                            adapter.notifyDataSetChanged();
                            break;
                        case "region":
                            for (RegionModel regionModel : mDatabaseHelper.getRegion()) {
                                list.add(regionModel.getRegionName());
                                adapter.notifyDataSetChanged();
                            }
                            break;

                        case "province_id":
                            holder.spnChildView.setAdapter(provinceListAdapter);
                            break;

                        case "cityid":
                            holder.spnChildView.setAdapter(cityListAdapter);
                            break;
                    }

                    break;
                case "5":
                    holder.btnChildView.setVisibility(View.VISIBLE);

                    break;
            }

            /**Listeners*/
            holder.edtChildView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    fieldsModel.setValue(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            holder.spnChildView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (mSettingModel != null) {
                        Typeface tfBase = Typeface.createFromAsset(mContext.getAssets(),
                                Utils.getDefaultTypeface(mSettingModel.getFontBasefamily()));

                        if (view != null) {
                            ((TextView) view).setTextColor(ConverterUtil.hexColorToInt(mSettingModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                            ((TextView) view).setTypeface(tfBase);
                            ((TextView) view).setTextSize(Utils.getDefaultFontBaseSize(mSettingModel.getFontBasesize()));
                        }

                        if (fieldsModel.getName().equals("region")) {
                            String region = mDatabaseHelper.getRegionIdByName(adapterView.getSelectedItem().toString());
                            provinceList.clear();
                            provinceList.addAll(mDatabaseHelper.getProvinceByRegionId(region));
                            provinceListAdapter.notifyDataSetChanged();

                        } else if (fieldsModel.getName().equals("province_id")) {
                            String province = mDatabaseHelper.getProvinceIdByName(adapterView.getSelectedItem().toString());
                            cityList.clear();
                            cityList.addAll(mDatabaseHelper.getCityByProvinceId(province));
                            cityListAdapter.notifyDataSetChanged();
                        }
                        fieldsModel.setValue(adapterView.getSelectedItem().toString());

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            holder.btnChildView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getSelectedDate(v);
                }
            });

            holder.rgChildView.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

                }
            });

        }

    }

    public List<FieldsModel> getRegistrationList() {
        return mFieldsModelList;
    }

    public void getSelectedDate(final View v) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar cal = Calendar.getInstance();
                        cal.set(Calendar.MONTH, monthOfYear);
                        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        cal.set(Calendar.YEAR, year);
                        Date myDate = cal.getTime();

                        String date = new SimpleDateFormat("yyyy-MM-dd").format(myDate);
                        ((Button) v).setText(date);
//                                data.setValue(date);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public int getItemCount() {
        return mFieldsModelList.size();
    }

    private void getSetting() {
//        mSettingModel = mDatabaseHelper.getDbSetting(brandCode, campaignCode);
//        Log.e(TAG, "getSetting: " + mSettingModel.toString());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RadioGroup rgChildView;
        private TextView txtGroupName, txtFieldName;
        private EditText edtChildView;
        private Spinner spnChildView;
        private Button btnChildView;
        private HorizontalScrollView hsRadioGroup;
        private SettingsModel settingsModel;
        private List<DataArray> dataArrayList;

        public ViewHolder(View v) {
            super(v);
            txtGroupName = (TextView) v.findViewById(R.id.txtGroupName);
            txtFieldName = (TextView) v.findViewById(R.id.txtFieldName);
            rgChildView = (RadioGroup) v.findViewById(R.id.rgChildView);
            edtChildView = (EditText) v.findViewById(R.id.edtChildView);
            spnChildView = (Spinner) v.findViewById(R.id.spnChildView);
            btnChildView = (Button) v.findViewById(R.id.btnChildView);
            hsRadioGroup = (HorizontalScrollView) v.findViewById(R.id.hsRadioGroup);

            if (mSettingModel != null) {
                /**Typeface*/
                Typeface tfHeader = Typeface.createFromAsset(mContext.getAssets(),
                        Utils.getDefaultTypeface(mSettingModel.getFontHeadfamily()));
                Typeface tfBase = Typeface.createFromAsset(mContext.getAssets(),
                        Utils.getDefaultTypeface(mSettingModel.getFontBasefamily()));

                txtGroupName.setTypeface(tfHeader);
                txtFieldName.setTypeface(tfBase);
                txtGroupName.setTextColor(ConverterUtil.hexColorToInt(mSettingModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                txtFieldName.setTextColor(ConverterUtil.hexColorToInt(mSettingModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));

                /**Text size*/
                txtGroupName.setTextSize(Utils.getDefaultFontBaseSize(mSettingModel.getFontHeadsize()));
                txtFieldName.setTextSize(Utils.getDefaultFontBaseSize(mSettingModel.getFontBasesize()));

            }
        }


    }
}
