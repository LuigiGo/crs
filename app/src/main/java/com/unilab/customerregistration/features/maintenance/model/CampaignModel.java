package com.unilab.customerregistration.features.maintenance.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CampaignModel implements Parcelable{

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("name")
    @Expose
    private String name;

    protected CampaignModel(Parcel in) {
        code = in.readString();
        name = in.readString();
    }

    public CampaignModel() {
    }

    public static final Creator<CampaignModel> CREATOR = new Creator<CampaignModel>() {
        @Override
        public CampaignModel createFromParcel(Parcel in) {
            return new CampaignModel(in);
        }

        @Override
        public CampaignModel[] newArray(int size) {
            return new CampaignModel[size];
        }
    };

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(name);
    }
}
