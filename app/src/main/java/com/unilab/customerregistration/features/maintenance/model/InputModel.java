package com.unilab.customerregistration.features.maintenance.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InputModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("type_id")
    @Expose
    private String typeId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("group_id")
    @Expose
    private String groupId;
    @SerializedName("is_required")
    @Expose
    private String isRequired;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("date_time")
    @Expose
    private String dateTime;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("group_name")
    @Expose
    private String groupName;
    @SerializedName("input_type")
    @Expose
    private String inputType;
    @SerializedName("input_id")
    @Expose
    private String inputId;
    @SerializedName("data_array")
    @Expose
    private List<DataArray> dataArray = null;
    private String status;
    @SerializedName("assign_is_active")
    @Expose
    private String assignIsActive;
    @SerializedName("sort")
    @Expose
    private String sort;
    private String editStatus;
    @SerializedName("standard")
    @Expose
    private String isMandatory;
    private String dbmStatus;

    public InputModel() {
    }

    protected InputModel(Parcel in) {
        id = in.readString();
        typeId = in.readString();
        title = in.readString();
        name = in.readString();
        description = in.readString();
        groupId = in.readString();
        isRequired = in.readString();
        isActive = in.readString();
        dateTime = in.readString();
        category = in.readString();
        groupName = in.readString();
        inputType = in.readString();
        inputId = in.readString();
        dataArray = in.createTypedArrayList(DataArray.CREATOR);
        status = in.readString();
        assignIsActive = in.readString();
        sort = in.readString();
        editStatus = in.readString();
        isMandatory = in.readString();
        dbmStatus = in.readString();
    }

    public static final Creator<InputModel> CREATOR = new Creator<InputModel>() {
        @Override
        public InputModel createFromParcel(Parcel in) {
            return new InputModel(in);
        }

        @Override
        public InputModel[] newArray(int size) {
            return new InputModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(String isRequired) {
        this.isRequired = isRequired;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public List<DataArray> getDataArray() {
        return dataArray;
    }

    public void setDataArray(List<DataArray> dataArray) {
        this.dataArray = dataArray;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAssignIsActive() {
        return assignIsActive;
    }

    public void setAssignIsActive(String assignIsActive) {
        this.assignIsActive = assignIsActive;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getEditStatus() {
        return editStatus;
    }

    public void setEditStatus(String editStatus) {
        this.editStatus = editStatus;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }

    public String getDbmStatus() {
        return dbmStatus;
    }

    public void setDbmStatus(String dbmStatus) {
        this.dbmStatus = dbmStatus;
    }

    @Override
    public String toString() {
        return "InputModel{" +
                "id='" + id + '\'' +
                ", typeId='" + typeId + '\'' +
                ", title='" + title + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", groupId='" + groupId + '\'' +
                ", isRequired='" + isRequired + '\'' +
                ", isActive='" + isActive + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", category='" + category + '\'' +
                ", groupName='" + groupName + '\'' +
                ", inputType='" + inputType + '\'' +
                ", inputId='" + inputId + '\'' +
                ", dataArray=" + dataArray +
                ", status='" + status + '\'' +
                ", assignIsActive='" + assignIsActive + '\'' +
                ", sort='" + sort + '\'' +
                ", editStatus='" + editStatus + '\'' +
                ", isMandatory='" + isMandatory + '\'' +
                ", dbmStatus='" + dbmStatus + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(typeId);
        dest.writeString(title);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(groupId);
        dest.writeString(isRequired);
        dest.writeString(isActive);
        dest.writeString(dateTime);
        dest.writeString(category);
        dest.writeString(groupName);
        dest.writeString(inputType);
        dest.writeString(inputId);
        dest.writeTypedList(dataArray);
        dest.writeString(status);
        dest.writeString(assignIsActive);
        dest.writeString(sort);
        dest.writeString(editStatus);
        dest.writeString(isMandatory);
        dest.writeString(dbmStatus);
    }
}
