package com.unilab.customerregistration.features.maintenance.view;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.AssignFieldModel;
import com.unilab.customerregistration.features.maintenance.model.BrandModel;
import com.unilab.customerregistration.features.maintenance.model.CampaignEventModel;
import com.unilab.customerregistration.features.maintenance.model.CampaignModel;
import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.DbmAddFieldsModel;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.GroupApiResponseModel;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.model.InputModel;
import com.unilab.customerregistration.features.maintenance.model.asynctask.AsyncAddField;
import com.unilab.customerregistration.features.maintenance.model.asynctask.AsyncCopyingFormFields;
import com.unilab.customerregistration.features.maintenance.model.asynctask.AsyncSaveFormUpdates;
import com.unilab.customerregistration.features.maintenance.model.asynctask.AsyncSendEditedField;
import com.unilab.customerregistration.features.maintenance.model.asynctask.AsyncSynchronizeFields;
import com.unilab.customerregistration.features.maintenance.model.asynctask.AsyncSynchronizeGroups;
import com.unilab.customerregistration.features.maintenance.model.web.api.MaintenanceApiProvider;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.features.maintenance.view.adapters.CustomGroupsAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.NonStandardGroupsAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.StandardGroupsAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.SubGroupingAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.BrandArrayAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.CampaignArrayAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.CampaignEventArrayAdapter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.constants.DatabaseConstants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.network.NetworkUtils;
import com.unilab.customerregistration.utilities.network.RestManager;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {

    private final String TAG = this.getClass().getSimpleName();
    Button btnAddFields, btnSave, btnReset;
    private View mView;
    private RecyclerView rvStandardFields, rvNonStandardFields, rvCustomFields;
    private RecyclerView.LayoutManager mLayoutManager;
    private DatabaseHelper mDatabaseHelper;
    private String brandCode, campaignCode, eventCode;
    private ImageView imgSynchronize;

    private ArrayList<GroupsModel> standardGroupNamesList = new ArrayList<>();
    private ArrayList<GroupsModel> nonStandardGroupNamesList = new ArrayList<>();
    private ArrayList<GroupsModel> customGroupNamesList = new ArrayList<>();
    private StandardGroupsAdapter standardGroupsAdapter;
    private NonStandardGroupsAdapter nonStandardGroupsAdapter;
    private CustomGroupsAdapter customGroupsAdapter;
    private boolean hasChanges = false;

    private BrandArrayAdapter mBrandArrayAdapter;
    private CampaignArrayAdapter mCampaignArrayAdapter;
    private CampaignEventArrayAdapter mCampaignEventArrayAdapter;
    private List<BrandModel> mBrandModelList;
    private List<CampaignModel> mCampaignModelList;
    private List<CampaignEventModel> mCampaignEventModelList;
    private String mSelectedBrandCode, mSelectedCampaignCode, mSelectedCampaignEventCode;
    Spinner spnBrands, spnCampaigns, spnEvent;
    CheckBox chCopyForm;
    Button btnApplyCopyForm;

    private List<FieldsModel> mSaveFormFieldList = new ArrayList<>();
    private ArrayList<FieldsModel> mCopyFormFieldList = new ArrayList<>();
    private List<String> mCopyFormGroupTempList = new ArrayList<>();
    private List<GroupsModel> mCopyFormGroupList = new ArrayList<>();
    private List<GroupsModel> mFilteredCopyStandardGroupList = new ArrayList<>();
    private List<GroupsModel> mFilteredCopyNonStandardGroupList = new ArrayList<>();
    private List<GroupsModel> mFilteredCopyCustomGroupList = new ArrayList<>();
    private List<FieldsModel> mFormFieldsChangesValidatorList = new ArrayList<>();

    public static boolean isCopyingForm = false;
    private MaintenancePresenter mMaintenancePresenter;

    private HorizontalScrollView horizontalScrollView;
    private RelativeLayout rlFormInactiveLayout, rlCopyFormInactiveLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_form, container, false);
        mDatabaseHelper = new DatabaseHelper(getActivity());
        SharedPrefHelper.init(getActivity());
        RestManager.init();
        DialogHelper.init(getActivity());

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        mMaintenancePresenter = (MaintenancePresenter) getActivity();

        setupViews();
        setAdapters();

        Log.e(TAG, "onCreateView: " + (savedInstanceState == null));
        if (savedInstanceState == null) {
            isCopyingForm(false);
            synchronize(true);
        }

        return mView;
    }

    private void setupViews() {

        /**Recycler Standard Fields*/
        rvStandardFields = (RecyclerView) mView.findViewById(R.id.rvStandardFields);
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        rvStandardFields.setLayoutManager(mLayoutManager);
        rvStandardFields.setItemAnimator(new DefaultItemAnimator());

        /**Recycler Non-Standard Fields*/
        rvNonStandardFields = (RecyclerView) mView.findViewById(R.id.rvNonStandardField);
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        rvNonStandardFields.setLayoutManager(mLayoutManager);
        rvNonStandardFields.setItemAnimator(new DefaultItemAnimator());

        /**Recycler Additional Fields*/
        rvCustomFields = (RecyclerView) mView.findViewById(R.id.rvAdditionalFields);
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        rvCustomFields.setLayoutManager(mLayoutManager);
        rvCustomFields.setItemAnimator(new DefaultItemAnimator());

        imgSynchronize = (ImageView) mView.findViewById(R.id.imgSynchornize);

        btnAddFields = (Button) mView.findViewById(R.id.btnAddFields);
        btnSave = (Button) mView.findViewById(R.id.btnSave);
        btnReset = (Button) mView.findViewById(R.id.btnReset);

        chCopyForm = (CheckBox) mView.findViewById(R.id.chCopyForm);
        spnBrands = (Spinner) mView.findViewById(R.id.spnBrands);
        spnCampaigns = (Spinner) mView.findViewById(R.id.spnCampaigns);
        spnEvent = (Spinner) mView.findViewById(R.id.spnCampaignEvents);
        btnApplyCopyForm = (Button) mView.findViewById(R.id.btnApplyCopyForm);

        horizontalScrollView = (HorizontalScrollView) mView.findViewById(R.id.horizontalScrollView);
        rlFormInactiveLayout = (RelativeLayout) mView.findViewById(R.id.rlFormInactiveLayout);
        rlCopyFormInactiveLayout = (RelativeLayout) mView.findViewById(R.id.rlCopyFormInactiveLayout);

        /**Listeners*/
        imgSynchronize.setOnClickListener(this);
        btnAddFields.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnReset.setOnClickListener(this);
        chCopyForm.setOnClickListener(this);
        btnApplyCopyForm.setOnClickListener(this);

        /**Setting up lists*/
        mBrandModelList = new ArrayList<>();
        setupInitialListPrompt(Constants.BRAND_LIST_PROMPT);
        mBrandArrayAdapter = new BrandArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, mBrandModelList);
        spnBrands.setAdapter(mBrandArrayAdapter);

        mCampaignModelList = new ArrayList<>();
        mCampaignArrayAdapter = new CampaignArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, mCampaignModelList);
        spnCampaigns.setAdapter(mCampaignArrayAdapter);

        mCampaignEventModelList = new ArrayList<>();
        mCampaignEventArrayAdapter = new CampaignEventArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, mCampaignEventModelList);
        spnEvent.setAdapter(mCampaignEventArrayAdapter);

        /**Hide add field button if total field count is exceeded or equal to 20*/
        if (mDatabaseHelper.getCustomFieldsCount(brandCode, campaignCode) >= 20) {
            setAddFieldButtonVisibility(View.GONE);
        } else {
            setAddFieldButtonVisibility(View.VISIBLE);
        }
    }

    private void setAdapters() {
        standardGroupNamesList = mDatabaseHelper.getStandardGroupNames(brandCode, campaignCode, eventCode);
        standardGroupsAdapter = new StandardGroupsAdapter(getActivity(), standardGroupNamesList, mCopyFormFieldList);
        rvStandardFields.setAdapter(standardGroupsAdapter);

        nonStandardGroupNamesList = mDatabaseHelper.getNonStandardGroupNames(brandCode, campaignCode, eventCode);
        nonStandardGroupsAdapter = new NonStandardGroupsAdapter(getActivity(), nonStandardGroupNamesList, mCopyFormFieldList);
        rvNonStandardFields.setAdapter(nonStandardGroupsAdapter);

        customGroupNamesList = mDatabaseHelper.getCustomGroupNames(brandCode, campaignCode, eventCode);
        customGroupsAdapter = new CustomGroupsAdapter(getActivity(), customGroupNamesList, mCopyFormFieldList);
        rvCustomFields.setAdapter(customGroupsAdapter);

    }

    public void refreshAdapters() {
        isCopyingForm = false;

        if (rlFormInactiveLayout != null && rlCopyFormInactiveLayout != null) {
            isCopyingForm(false);
            chCopyForm.setChecked(false);
        }

        if (standardGroupNamesList != null && standardGroupsAdapter != null) {
            standardGroupNamesList.clear();
            standardGroupNamesList.addAll(mDatabaseHelper.getStandardGroupNames(brandCode, campaignCode, eventCode));
            standardGroupsAdapter.notifyDataSetChanged();
        }

        if (nonStandardGroupNamesList != null && nonStandardGroupsAdapter != null) {
            nonStandardGroupNamesList.clear();
            nonStandardGroupNamesList.addAll(mDatabaseHelper.getNonStandardGroupNames(brandCode, campaignCode, eventCode));
            nonStandardGroupsAdapter.notifyDataSetChanged();
        }

        if (customGroupNamesList != null && customGroupsAdapter != null) {
            customGroupNamesList.clear();
            customGroupNamesList.addAll(mDatabaseHelper.getCustomGroupNames(brandCode, campaignCode, eventCode));
            customGroupsAdapter.notifyDataSetChanged();
        }
    }

    public void refreshCustomFieldsAdapter() {
        isCopyingForm = false;
        if (rlFormInactiveLayout != null && rlCopyFormInactiveLayout != null) {
            isCopyingForm(false);
            chCopyForm.setChecked(false);
        }

        if (customGroupNamesList != null && customGroupsAdapter != null) {
            customGroupNamesList.clear();
            customGroupNamesList.addAll(mDatabaseHelper.getCustomGroupNames(brandCode, campaignCode, eventCode));
            customGroupsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddFields:
                mMaintenancePresenter.showAddFormFieldDialog();
                break;

            case R.id.imgSynchornize:
                synchronize(true);
                break;

            case R.id.chCopyForm:
                isCopyingForm(chCopyForm.isChecked());
                break;

            case R.id.btnSave:
                if (chCopyForm.isChecked()) {
                    if (mSelectedBrandCode == null || mSelectedBrandCode.isEmpty()) {
                        mMaintenancePresenter.showAlertDialog("Alert", "Please select Brand", Constants.DIALOG_ACTION_NORMAL);

                    } else if (mSelectedCampaignCode == null || mSelectedCampaignCode.isEmpty()) {
                        mMaintenancePresenter.showAlertDialog("Alert", "Please select Campaign", Constants.DIALOG_ACTION_NORMAL);

                    } else if (mSelectedCampaignEventCode == null || mSelectedCampaignEventCode.isEmpty()) {
                        mMaintenancePresenter.showAlertDialog("Alert", "Please select Initiative", Constants.DIALOG_ACTION_NORMAL);
                    } else {
                        mMaintenancePresenter.showSaveFormPromptDialog("Confirm", "Are you sure you want to save?",
                                Constants.DIALOG_ACTION_SAVE_FORM_FRAGMENT);
                    }
                } else {
                    mMaintenancePresenter.showSaveFormPromptDialog("Confirm", "Are you sure you want to save?",
                            Constants.DIALOG_ACTION_SAVE_FORM_FRAGMENT);

                }
                break;

            case R.id.btnReset:
                mMaintenancePresenter.showSaveFormPromptDialog("Confirm", "Are you sure you want to reset the current set-up??",
                        Constants.DIALOG_RESET_FORM_PAGE);
                break;

            case R.id.btnApplyCopyForm:
                if (mSelectedBrandCode == null || mSelectedBrandCode.isEmpty()) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please select Brand", Constants.DIALOG_ACTION_NORMAL);

                } else if (mSelectedCampaignCode == null || mSelectedCampaignCode.isEmpty()) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please select Campaign", Constants.DIALOG_ACTION_NORMAL);

                } else if (mSelectedCampaignEventCode == null || mSelectedCampaignEventCode.isEmpty()) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please select Initiative", Constants.DIALOG_ACTION_NORMAL);
                } else {
                    copyFields();
                }
                break;
        }
    }

    public void synchronize(boolean isSynchronizingForm) {
        /**Hide add fiefld button if total field count is exceeded or equal to 20*/
        if (mDatabaseHelper.getCustomFieldsCount(brandCode, campaignCode) >= 20) {
            setAddFieldButtonVisibility(View.GONE);
        } else {
            setAddFieldButtonVisibility(View.VISIBLE);
        }

        /**Resetting copy form lists*/
        mSelectedBrandCode = null;
        mSelectedCampaignCode = null;
        mSelectedCampaignEventCode = null;
        mBrandModelList.clear();
        mCampaignModelList.clear();
        mCampaignEventModelList.clear();
        mBrandArrayAdapter.notifyDataSetChanged();
        mCampaignArrayAdapter.notifyDataSetChanged();
        mCampaignEventArrayAdapter.notifyDataSetChanged();
        callBrandAPI();

        if (NetworkUtils.isNetworkConnected(getActivity())) {
            Log.e(TAG, "synchronizeForm: " + "Connected");
            syncUpdates(isSynchronizingForm);
        }

        if (isSynchronizingForm) {
            refreshAdapters();
        }
    }

    public void setAddFieldButtonVisibility(int visibility) {
        switch (visibility) {
            case View.GONE:
                btnAddFields.setVisibility(View.GONE);
                break;

            case View.VISIBLE:
                btnAddFields.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void addGroupAPI(final GroupsModel groupsModel, final EditText edtGroupName,
                            final List<GroupsModel> groupNameList, final ArrayAdapter<GroupsModel> groupsAdapter,
                            final List<GroupsModel> groupsModelList, final SubGroupingAdapter subGroupingAdapter) {
        Call<ResponseBody> postGroupsCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .addGroup(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_POST_GROUP,
                        groupsModel.getCampaignCode(), groupsModel.getBrandCode(), groupsModel.getEventCode(),
                        groupsModel.getTitle(), groupsModel.getName(), groupsModel.getDescription(), groupsModel.getIsActive(),
                        groupsModel.getDateTime());
        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Adding group");
        postGroupsCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    try {
                        String result = response.body().string();
                        JSONObject jsonObject = new JSONObject(result);
                        String status = jsonObject.getString("status");

                        switch (status) {
                            case "success":
                                String id = jsonObject.getString("id");
                                edtGroupName.setText("");
                                groupsModel.setId(id);
                                groupsModel.setStatus("0");
                                mDatabaseHelper.createGroup(groupsModel);
                                groupNameList.clear();
                                groupNameList.addAll(mDatabaseHelper.getGroupNames(brandCode, campaignCode, eventCode));
                                groupsAdapter.notifyDataSetChanged();

                                groupsModelList.clear();
                                groupsModelList.addAll(mDatabaseHelper.getAllGroupsByBrandCampaignEvent(brandCode, campaignCode, eventCode));
                                subGroupingAdapter.notifyDataSetChanged();
                                mMaintenancePresenter.showAlertDialog("Alert", "New Sub-group is successfully added.",
                                        Constants.DIALOG_ACTION_NORMAL);
                                break;

                            case "failed":
                                String message = jsonObject.getString("msg");
                                Log.e(TAG, "onResponse: " + message);
                                mMaintenancePresenter.showAlertDialog("Alert", message,
                                        Constants.DIALOG_ACTION_NORMAL);
                                break;

                            default:
                                mMaintenancePresenter.showAlertDialog("Alert", "Opss! Something went wrong",
                                        Constants.DIALOG_ACTION_NORMAL);
                                Log.e(TAG, "onResponse: " + result);
                                break;
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }
        });
    }

    public void updateGroupAPI(final String groupTitle, final String groupName,
                               final List<GroupsModel> groupNameList, final ArrayAdapter<GroupsModel> groupNameAdapter,
                               final GroupsModel groupsModel, final List<GroupsModel> groupsModelList,
                               final SubGroupingAdapter subGroupingAdapter, final int itemPosition) {
        Log.e(TAG, "updateGroupAPI: Tag: Update group " + groupsModel.toString());
        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Updating group");
        Call<GroupApiResponseModel> updateGroupApiCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .updateGroup(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_UPDATE_GROUP,
                        groupsModel.getId(), groupTitle, Constants.TAG_UPDATING_GROUP);
        updateGroupApiCall.enqueue(new Callback<GroupApiResponseModel>() {
            @Override
            public void onResponse(Call<GroupApiResponseModel> call, Response<GroupApiResponseModel> response) {
                if (response.body() != null) {
                    GroupApiResponseModel groupApiResponseModel = response.body();
                    if (groupApiResponseModel.getStatus().equals("success")) {
                        /**For updating manage sub-group name list*/
                        groupsModel.setTitle(groupTitle);
                        groupsModel.setName(groupName);
                        subGroupingAdapter.notifyDataSetChanged();
                        mDatabaseHelper.updateGroupById(groupsModel);

                        /**For updating group name dropdown*/
                        groupNameList.clear();
                        groupNameList.addAll(mDatabaseHelper.getGroupNames(brandCode, campaignCode, eventCode));
                        groupNameAdapter.notifyDataSetChanged();

                        mMaintenancePresenter.showAlertDialog("Alert", "Subgroup is successfully updated.", Constants.DIALOG_ACTION_NORMAL);

                    } else {
                        mMaintenancePresenter.showAlertDialog("Alert", groupApiResponseModel.getMessage(), Constants.DIALOG_ACTION_NORMAL);
                    }
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }

            @Override
            public void onFailure(Call<GroupApiResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", t.getMessage(), Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }
        });
    }

    public void deleteGroupAPI(final GroupsModel groupsModel, final List<GroupsModel> groupsModelList,
                               final List<GroupsModel> groupNameList, final ArrayAdapter<GroupsModel> groupNameAdapter,
                               final SubGroupingAdapter subGroupingAdapter, final int itemPosition) {
        Log.e(TAG, "deleteGroupAPI: Tag: Deleting group " + groupsModel.toString());
        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Deleting group");
        Call<GroupApiResponseModel> deleteGroupApiCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .deleteGroup(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_UPDATE_GROUP,
                        groupsModel.getId(), groupsModel.getTitle(), Constants.TAG_DELETING_GROUP);
        deleteGroupApiCall.enqueue(new Callback<GroupApiResponseModel>() {
            @Override
            public void onResponse(Call<GroupApiResponseModel> call, Response<GroupApiResponseModel> response) {
                if (response.body() != null) {
                    GroupApiResponseModel groupApiResponseModel = response.body();
                    if (groupApiResponseModel.getStatus().equals("success")) {
                        /**For updating group name dropdown*/
                        groupNameList.clear();
                        groupNameList.addAll(mDatabaseHelper.getGroupNames(brandCode, campaignCode, eventCode));
                        groupNameAdapter.notifyDataSetChanged();

                        /**For updating manage sub-group name list*/
                        groupsModelList.remove(itemPosition);
                        subGroupingAdapter.notifyDataSetChanged();
                        mDatabaseHelper.deleteGroupById(groupsModel);
                        mMaintenancePresenter.showAlertDialog("Alert", "A subgroup is successfully deleted.", Constants.DIALOG_ACTION_NORMAL);

                    } else {
                        mMaintenancePresenter.showAlertDialog("Alert", groupApiResponseModel.getMessage(), Constants.DIALOG_ACTION_NORMAL);
                    }
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }

            @Override
            public void onFailure(Call<GroupApiResponseModel> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", t.getMessage(), Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }
        });
    }

    public void postFieldToDbmForOnline(final FieldsModel fieldsModel, final List<DataArray> dataArrayList,
                                        final InputModel inputModel, final AssignFieldModel assignFieldModel,
                                        final String actionType) {
        Log.e(TAG, "postFieldToDbmForOnline: ");
        Call<DbmAddFieldsModel> dbmAddFieldsModelCall = MaintenanceApiProvider.getMaintenanceDbmApiRoutes()
                .addDbmField(Constants.API_DBM_TOKEN,
                        Constants.CMD_EVENT_DBM_POST_FIELD,
                        fieldsModel.getCampaignCode(),
                        fieldsModel.getBrandCode(),
                        fieldsModel.getEventCode(),
                        fieldsModel.getTypeId(),
                        fieldsModel.getTitle(),
                        fieldsModel.getName(),
                        fieldsModel.getDescription(),
                        fieldsModel.getGroupId(),
                        fieldsModel.getIsRequired(),
                        fieldsModel.getA_isActive(),
                        Utils.getDateTime(),
                        fieldsModel.getCategory(),
                        Constants.DBM_HOSTNAME);
        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(),
                (actionType.equals(Constants.DIALOG_ACTION_ADD_FORM_FIELD)) ? "Adding form field" : "Updating form field");
        dbmAddFieldsModelCall.enqueue(new Callback<DbmAddFieldsModel>() {
            @Override
            public void onResponse(Call<DbmAddFieldsModel> call, Response<DbmAddFieldsModel> response) {
                if (response.body() != null) {
                    DbmAddFieldsModel dbmAddFieldsModel = response.body();
                    String dbmMessage = dbmAddFieldsModel.getMessage();
                    Log.e(TAG, "postFieldToDbmForOnline-onResponse: " + dbmMessage);
                    if (actionType.equals(Constants.DIALOG_ACTION_ADD_FORM_FIELD)) {
                        switch (dbmAddFieldsModel.getMessage()) {
                            case "Field is successfully added!":
                                inputModel.setDbmStatus("0");
                                break;

                            case "Field is already exists!":
                                inputModel.setDbmStatus("2");
                                break;

                            default:
                                inputModel.setDbmStatus("1");
                                break;
                        }

                        new AsyncAddField(getActivity(), dataArrayList, inputModel, assignFieldModel,
                                fieldsModel, false).execute();
                    } else if (actionType.equals(Constants.DIALOG_ACTION_UPDATE_FORM_FIELD)) {
                        switch (dbmAddFieldsModel.getMessage()) {
                            case "Field is successfully added!":
                                fieldsModel.setDbmStatus("0");
                                break;

                            case "Field is already exists!":
                                fieldsModel.setDbmStatus("2");
                                break;

                            default:
                                fieldsModel.setDbmStatus("1");
                                break;
                        }

                        new AsyncSendEditedField(getActivity(), fieldsModel, false, false).execute();
                    }

                } else {
                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                }
            }

            @Override
            public void onFailure(Call<DbmAddFieldsModel> call, Throwable t) {
                Log.e(TAG, "onFailure: postFieldToDbmForOnline: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", t.getMessage(), Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }
        });
    }

    private void syncFields(final boolean isSynchronizingForm) {
        Log.e(TAG, "syncFields: ");
        Call<List<InputModel>> syncFieldsCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .getFields(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_GET_INPUT, campaignCode, brandCode, eventCode);
        syncFieldsCall.enqueue(new Callback<List<InputModel>>() {
            @Override
            public void onResponse(Call<List<InputModel>> call, Response<List<InputModel>> response) {

                if (response.body() != null) {
                    List<InputModel> inputModelList = response.body();
                    new AsyncSynchronizeFields(MaintenanceActivity.mainActivityContext, inputModelList,
                            isSynchronizingForm, false).execute();

                } else {
                    Log.e(TAG, "onResponse: " + "No response");
                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                    if (isSynchronizingForm) {
                        mMaintenancePresenter.refreshFormAdapters();
                    } else {
                        mMaintenancePresenter.showPreviewPage();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<InputModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: syncFields: " + t.getMessage());
                DialogHelper.getInstance().hideProgressDialog(getActivity());
                if (isSynchronizingForm) {
                    mMaintenancePresenter.refreshFormAdapters();
                } else {
                    mMaintenancePresenter.showAlertDialog("Alert", t.getMessage(), Constants.DIALOG_ACTION_NO_FORM_CHANGES);
                }
            }
        });
    }

    private void syncGroups(final boolean isSynchronizingForm) {
        Log.e(TAG, "syncGroups: ");
        Call<List<GroupsModel>> syncGroupsCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .getGroups(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_GET_GROUP, campaignCode, brandCode, eventCode);
        syncGroupsCall.enqueue(new Callback<List<GroupsModel>>() {
            @Override
            public void onResponse(Call<List<GroupsModel>> call, Response<List<GroupsModel>> response) {
                if (response.body() != null) {
                    List<GroupsModel> groupsModelList = response.body();
                    for (GroupsModel groupsModel : groupsModelList) {
                        groupsModel.setStatus("0");
                    }
                    new AsyncSynchronizeGroups(getActivity(), groupsModelList).execute();

                } else {
                    Toast.makeText(getActivity(), "No groups", Toast.LENGTH_SHORT).show();
                    mMaintenancePresenter.refreshFormAdapters();
                }
                syncFields(isSynchronizingForm);
            }

            @Override
            public void onFailure(Call<List<GroupsModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: syncGroups: " + t.getMessage());
                DialogHelper.getInstance().hideProgressDialog(getActivity());
                if (isSynchronizingForm) {
                    mMaintenancePresenter.refreshFormAdapters();
                } else {
                    mMaintenancePresenter.showAlertDialog("Alert", t.getMessage(), Constants.DIALOG_ACTION_NO_FORM_CHANGES);
                }
            }
        });
    }

    private void syncUpdates(boolean isSynchronizingForm) {
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Synchronizing Form");
        new AsyncSaveFormUpdates(getActivity(), false).execute();
        syncGroups(isSynchronizingForm);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.llAddFieldRootView:
                Utils.hideKeyboard(getActivity());
                break;

            default:
                Utils.hideKeyboard(getActivity());
                break;
        }
        return false;
    }

    private void callBrandAPI() {
        Log.e(TAG, "callBrandAPI: ");
//        DialogHelper.getInstance().hideProgressDialog(getActivity());
//        DialogHelper.getInstance().showProgressDialog(getActivity(), "Loading Brands");
        String email = SharedPrefHelper.getInstance().getEmailFromShared();
        Call<List<BrandModel>> onGetBrandListsCall = MaintenanceApiProvider.getMaintenanceDbmApiRoutes()
                .onGetBrandLists(Constants.API_DBM_TOKEN, Constants.CMD_EVENT_GET_BRAND, email, Constants.DBM_HOSTNAME);
        onGetBrandListsCall.enqueue(new Callback<List<BrandModel>>() {

            @Override
            public void onResponse(Call<List<BrandModel>> call, Response<List<BrandModel>> response) {
                if (response.body() != null) {
                    List<BrandModel> brandModelList = response.body();
                    mBrandModelList.clear();
                    if (brandModelList.size() > 0) {
                        BrandModel brandModel = new BrandModel();
                        brandModel.setName("--Please select your brand--");
                        brandModel.setBrandCode(null);
                        mBrandModelList.add(brandModel);
                        mBrandModelList.addAll(brandModelList);

                        /**Sorting into ascending order*/
                        Collections.sort(mBrandModelList, new Comparator<BrandModel>() {
                            public int compare(BrandModel obj1, BrandModel obj2) {
                                return obj1.getName().compareToIgnoreCase(obj2.getName());
                            }
                        });
                    } else {
                        BrandModel brandModel = new BrandModel();
                        brandModel.setName("No brand found!");
                        brandModel.setBrandCode(null);
                        mBrandModelList.add(brandModel);
                    }
                    mBrandArrayAdapter.notifyDataSetChanged();
//                    DialogHelper.getInstance().hideProgressDialog(getActivity());

//                mBrandListAdapter = new BrandListAdapter(getActivity(), brandModelList);
//                mRecyclerViewBrands.setAdapter(mBrandListAdapter);
                }
//                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }

            @Override
            public void onFailure(Call<List<BrandModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
//                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }
        });
    }

    public void callCampaignAPI(final String brandCode, String brandName) {
        Log.e(TAG, "callCampaignAPI: BrandCode: " + brandCode + " BrandName: " + brandName);
        mSelectedBrandCode = brandCode;
        mCampaignEventModelList.clear();
        mCampaignEventArrayAdapter.notifyDataSetChanged();

        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Loading campaigns");
        Call<List<CampaignModel>> onGetCampaignList = MaintenanceApiProvider.getMaintenanceDbmApiRoutes()
                .onGetCampaignLists(Constants.API_DBM_TOKEN, Constants.CMD_EVENT_GET_CAMPAIGN, brandCode,
                        Constants.DBM_HOSTNAME);
        onGetCampaignList.enqueue(new Callback<List<CampaignModel>>() {

            @Override
            public void onResponse(Call<List<CampaignModel>> call, Response<List<CampaignModel>> response) {
                if (response.body() != null) {
                    Log.e(TAG, "onResponse: ResponseCode: " + response.code() + " ResponseBody: " + response.body().toString());
                    List<CampaignModel> campaignModelList = response.body();
                    mCampaignModelList.clear();
                    if (campaignModelList.size() > 0) {
                        CampaignModel campaignModel = new CampaignModel();
                        campaignModel.setName("--Please select your campaign--");
                        campaignModel.setCode(null);
                        mCampaignModelList.add(campaignModel);
                        mCampaignModelList.addAll(campaignModelList);

                        /**Sorting into ascending order*/
                        Collections.sort(mCampaignModelList, new Comparator<CampaignModel>() {
                            public int compare(CampaignModel obj1, CampaignModel obj2) {
                                return obj1.getName().compareToIgnoreCase(obj2.getName());
                            }
                        });
                    } else {
                        CampaignModel campaignModel = new CampaignModel();
                        campaignModel.setName("No campaign found!");
                        campaignModel.setCode(null);
                        mCampaignModelList.add(campaignModel);
                    }
                    mCampaignArrayAdapter.notifyDataSetChanged();

                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }

            @Override
            public void onFailure(Call<List<CampaignModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }
        });
    }

    public void callCampaignEventAPI(final String campaignCode, String campaignName) {
        Log.e(TAG, "callCampaignEventAPI: CampaignCode: " + campaignCode + " CampaignName: " + campaignName);
        mSelectedCampaignCode = campaignCode;
        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Loading campaign events");

        Call<List<CampaignEventModel>> onGetCampaignEventList = MaintenanceApiProvider.getMaintenanceDbmApiRoutes()
                .onGetCampaignEventLists(Constants.API_DBM_TOKEN, Constants.CMD_EVENT_GET_CAMPAIGN_EVENT, mSelectedCampaignCode, Constants.DBM_HOSTNAME);
        onGetCampaignEventList.enqueue(new Callback<List<CampaignEventModel>>() {
            @Override
            public void onResponse(Call<List<CampaignEventModel>> call, Response<List<CampaignEventModel>> response) {
                if (response.body() != null) {
                    Log.e(TAG, "onResponse: ResponseCode: " + response.code() + " ResponseBody: " + response.body().toString());
                    List<CampaignEventModel> campaignEventModels = response.body();

                    mCampaignEventModelList.clear();
                    if (campaignEventModels.size() > 0) {
                        CampaignEventModel campaignEventModel = new CampaignEventModel();
                        campaignEventModel.setName("--Please select your Initiative--");
                        campaignEventModel.setCode(null);
                        mCampaignEventModelList.add(campaignEventModel);
                        mCampaignEventModelList.addAll(campaignEventModels);

                        /**Sorting into ascending order*/
                        Collections.sort(mCampaignEventModelList, new Comparator<CampaignEventModel>() {
                            public int compare(CampaignEventModel obj1, CampaignEventModel obj2) {
                                return obj1.getName().compareToIgnoreCase(obj2.getName());
                            }
                        });
                    } else {
                        CampaignEventModel campaignEventModel = new CampaignEventModel();
                        campaignEventModel.setName("No Initiative found!");
                        campaignEventModel.setCode(null);
                        mCampaignEventModelList.add(campaignEventModel);
                    }
                    mCampaignEventArrayAdapter.notifyDataSetChanged();
                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }

            @Override
            public void onFailure(Call<List<CampaignEventModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                DialogHelper.getInstance().hideProgressDialog(getActivity());
                mMaintenancePresenter.showAlertDialog("Alert", getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
            }
        });
    }

    public void saveConfiguredSettings(String campaignEventCode, String campaignEventName) {
        mSelectedCampaignEventCode = campaignEventCode;
        Log.e(TAG, "saveConfiguredSettings: Selected Brand Code: " + mSelectedBrandCode);
        Log.e(TAG, "saveConfiguredSettings: Selected Campaign Code: " + mSelectedCampaignCode);
        Log.e(TAG, "saveConfiguredSettings: Selected Campaign Event Code: " + mSelectedCampaignEventCode);

    }

    public void isCopyingForm(boolean status) {
        if (status) {
            rlFormInactiveLayout.setVisibility(View.VISIBLE);

            rlCopyFormInactiveLayout.setVisibility(View.GONE);
            btnApplyCopyForm.setEnabled(true);
        } else {
            rlFormInactiveLayout.setVisibility(View.GONE);

            rlCopyFormInactiveLayout.setVisibility(View.VISIBLE);
            btnApplyCopyForm.setEnabled(false);
        }
    }

    public void copyFields() {
        Log.e(TAG, "copyFields: ");
        mCopyFormFieldList.clear();
        mCopyFormFieldList.addAll(mDatabaseHelper.getAllDefaultFields(brandCode, campaignCode, eventCode));

        mCopyFormGroupTempList.clear();
        mCopyFormGroupList.clear();
        mFilteredCopyStandardGroupList.clear();
        mFilteredCopyNonStandardGroupList.clear();
        mFilteredCopyCustomGroupList.clear();

        Call<List<InputModel>> onCopyFieldsCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .getFields(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_GET_INPUT, mSelectedCampaignCode, mSelectedBrandCode,
                        mSelectedCampaignEventCode);

        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Copying form fields");
        onCopyFieldsCall.enqueue(new Callback<List<InputModel>>() {
            @Override
            public void onResponse(Call<List<InputModel>> call, Response<List<InputModel>> response) {

                if (response.body() != null) {
                    List<InputModel> inputModelList = response.body();
                    Log.e(TAG, "onResponse: copyFormFieldSize: " + inputModelList.size());
                    for (int i = 0; i < inputModelList.size(); i++) {
                        InputModel inputModel = inputModelList.get(i);
                        Log.e(TAG, "copy-FormFields: onResponse: " + inputModel.toString());
                        Log.e(TAG, "Gee: onResponse: ID " + inputModel.getId()
                                + " name: " + inputModel.getName()
                                + " groupID: " + inputModel.getGroupId());

                        boolean isFieldNameExist = false;
                        FieldsModel fieldsModel = new FieldsModel();
                        fieldsModel.setBrandCode(brandCode);
                        fieldsModel.setCampaignCode(campaignCode);
                        fieldsModel.setEventCode(eventCode);
                        fieldsModel.setB_id(inputModel.getId());
                        fieldsModel.setTypeId(inputModel.getTypeId());
                        fieldsModel.setTitle(inputModel.getTitle());
                        fieldsModel.setName(inputModel.getName());
                        fieldsModel.setDescription(inputModel.getDescription());
                        fieldsModel.setGroupId(inputModel.getGroupId());
                        fieldsModel.setIsRequired(inputModel.getIsRequired());
                        fieldsModel.setB_isActive(inputModel.getIsActive());
                        fieldsModel.setB_dateTime(inputModel.getDateTime());
                        fieldsModel.setCategory(inputModel.getCategory());
                        fieldsModel.setA_id(inputModel.getId());
                        fieldsModel.setSort(Integer.parseInt(inputModel.getSort()));
                        fieldsModel.setA_isActive(inputModel.getAssignIsActive());
                        fieldsModel.setDataArrayList(inputModel.getDataArray());
                        fieldsModel.setIsMandatory(inputModel.getIsMandatory());
                        fieldsModel.setGroupName(inputModel.getGroupName());

                        /**Checking fields for duplicates*/
                        for (int j = 0; j < mCopyFormFieldList.size(); j++) {
                            FieldsModel fieldsModel1 = mCopyFormFieldList.get(j);
                            if (inputModel.getName().equals(fieldsModel1.getName())) {
                                fieldsModel1.setA_isActive(inputModel.getAssignIsActive());
                                fieldsModel1.setSort(Integer.parseInt(inputModel.getSort()));
                                fieldsModel1.setIsMandatory(inputModel.getIsMandatory());
                                fieldsModel1.setGroupId(inputModel.getGroupId());
                                fieldsModel1.setGroupName(inputModel.getGroupName());
                                isFieldNameExist = true;
                            }
                        }

                        /**Adding field into list if doesn't exist*/
                        if (!isFieldNameExist) {
                            mCopyFormFieldList.add(fieldsModel);
                        }
                    }

                    /**Getting groups*/
                    for (FieldsModel fieldsModel : mCopyFormFieldList) {
                        if (fieldsModel.getGroupName() == null) {
                            fieldsModel.setGroupName(mDatabaseHelper.getGroupNameById(Integer.parseInt(fieldsModel.getGroupId())));
                        }

                        String strGroupName = fieldsModel.getGroupName();
                        String strFormattedGroupName = fieldsModel.getGroupName().replace(" ", "_").toLowerCase();

                        GroupsModel groupsModel = new GroupsModel();
                        groupsModel.setId(fieldsModel.getGroupId());
                        groupsModel.setTitle(strGroupName);
                        groupsModel.setName(strFormattedGroupName);
                        groupsModel.setDescription("");
                        groupsModel.setBrandCode(mSelectedBrandCode);
                        groupsModel.setCampaignCode(mSelectedCampaignCode);
                        groupsModel.setEventCode(mSelectedCampaignEventCode);
                        groupsModel.setIsActive("1");
                        groupsModel.setUserId("");
                        groupsModel.setStatus("0");

                        Log.e(TAG, "onResponse: GroupsLogsList: " + mCopyFormGroupList.size());
                        Log.e(TAG, "onResponse: GroupsLogsList: " + mCopyFormGroupList);

                        if (mCopyFormGroupTempList.contains(groupsModel.toString())) {
                            Log.e(TAG, "onResponse: --> " + "group contain");
                        } else {
                            Log.e(TAG, "onResponse: --> " + "not contain");
                            mCopyFormGroupTempList.add(groupsModel.toString());
                            mCopyFormGroupList.add(groupsModel);
                        }
                    }

                    /**Getting data array of standard and non-standard field*/
                    for (FieldsModel fieldsModel : mCopyFormFieldList) {
                        if (fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_STANDARD_FIELDS)) ||
                                fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_NON_STANDARD_FIELDS))) {

                            switch (fieldsModel.getTypeId()) {
                                case "3":
                                    fieldsModel.setDataArrayList(mDatabaseHelper
                                            .getDataArrayByFieldId(DatabaseConstants.TBL_RADIO, fieldsModel.getB_id()));
                                    break;

                                case "4":
                                    fieldsModel.setDataArrayList(mDatabaseHelper
                                            .getDataArrayByFieldId(DatabaseConstants.TBL_SELECT, fieldsModel.getB_id()));
                                    break;
                            }
                        }
                    }

                    /**Sorting copied fields*/
                    Collections.sort(mCopyFormFieldList, new Comparator<FieldsModel>() {
                        @Override
                        public int compare(FieldsModel fieldsModel, FieldsModel t1) {
                            return Integer.valueOf(fieldsModel.getSort()).compareTo(t1.getSort());
                        }
                    });

                    /**Filtering groups for standard fields*/
                    for (GroupsModel mainGroupList : mCopyFormGroupList) {
                        for (FieldsModel fieldsModel : mCopyFormFieldList) {
                            Log.e(TAG, "onResponse: " + fieldsModel.toString());
                            Log.e(TAG, "onResponse: " + mainGroupList.getId());

                            if (fieldsModel.getGroupId().equals(mainGroupList.getId()) &&
                                    fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_STANDARD_FIELDS))) {
                                Log.e(TAG, "onResponse: " + "Group Name: " + mainGroupList.getName() + " is in used by Field Name " + fieldsModel.getName());

                                if (mFilteredCopyStandardGroupList.contains(mainGroupList)) {
                                    Log.e(TAG, "onResponse: " + "filtered contains " + mainGroupList.getName());

                                } else {
                                    Log.e(TAG, "onResponse: " + "filtered not contains " + mainGroupList.getName());
                                    mFilteredCopyStandardGroupList.add(mainGroupList);
                                }

                            }
                        }
                    }

                    /**Filtering groups for non-standard fields*/
                    for (GroupsModel mainGroupList : mCopyFormGroupList) {
                        for (FieldsModel fieldsModel : mCopyFormFieldList) {
                            if (fieldsModel.getGroupId().equals(mainGroupList.getId()) &&
                                    fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_NON_STANDARD_FIELDS))) {
                                Log.e(TAG, "onResponse: " + "Group Name: " + mainGroupList.getName() + " is in used by Field Name " + fieldsModel.getName());

                                if (mFilteredCopyNonStandardGroupList.contains(mainGroupList)) {
                                    Log.e(TAG, "onResponse: " + "filtered contains " + mainGroupList.getName());
                                } else {
                                    Log.e(TAG, "onResponse: " + "filtered not contains " + mainGroupList.getName());
                                    mFilteredCopyNonStandardGroupList.add(mainGroupList);
                                }

                            }
                        }
                    }

                    /**Filtering groups for custom fields*/
                    for (GroupsModel mainGroupList : mCopyFormGroupList) {
                        for (FieldsModel fieldsModel : mCopyFormFieldList) {
                            Log.e(TAG, "onResponse: " + fieldsModel.toString());
                            if (fieldsModel.getGroupId().equals(mainGroupList.getId()) &&
                                    fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_ADDITIONAL_FIELDS))) {
                                Log.e(TAG, "Gee: " + "Group Name: " + mainGroupList.getName() + " is in used by Field Name " + fieldsModel.getName());

                                if (mFilteredCopyCustomGroupList.contains(mainGroupList)) {
                                    Log.e(TAG, "onResponse: " + "filtered contains " + mainGroupList.getName());
                                } else {
                                    Log.e(TAG, "onResponse: " + "filtered not contains " + mainGroupList.getName());
                                    mFilteredCopyCustomGroupList.add(mainGroupList);
                                }

                            }
                        }
                    }

                    isCopyingForm = true;

                    standardGroupNamesList.clear();
                    standardGroupNamesList.addAll(mFilteredCopyStandardGroupList);
                    standardGroupsAdapter.notifyDataSetChanged();

                    nonStandardGroupNamesList.clear();
                    nonStandardGroupNamesList.addAll(mFilteredCopyNonStandardGroupList);
                    nonStandardGroupsAdapter.notifyDataSetChanged();

                    customGroupNamesList.clear();
                    customGroupNamesList.addAll(mFilteredCopyCustomGroupList);
                    customGroupsAdapter.notifyDataSetChanged();

                    for (GroupsModel groupsModel : mCopyFormGroupList) {
                        Log.e(TAG, "copyGroups: " + groupsModel.getId() + "--" + groupsModel.getName());
                    }
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());
                mMaintenancePresenter.showAlertDialog("Confirm", "Form has been successfully copied.",
                        Constants.DIALOG_ACTION_NORMAL);
            }

            @Override
            public void onFailure(Call<List<InputModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", t.getMessage(), Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }
        });
    }

    private void setupInitialListPrompt(int listType) {
        switch (listType) {
            case Constants.BRAND_LIST_PROMPT:
                BrandModel brandModel = new BrandModel();
                brandModel.setName("--Please select your brand--");
                brandModel.setBrandCode(null);
                mBrandModelList.add(brandModel);
                break;

            case Constants.CAMPAIGN_LIST_PROMPT:
                CampaignModel campaignModel = new CampaignModel();
                campaignModel.setName("--Please select your campaign--");
                campaignModel.setCode(null);
                mCampaignModelList.add(campaignModel);
                break;

            case Constants.EVENT_LIST_PROMPT:
                CampaignEventModel campaignEventModel = new CampaignEventModel();
                campaignEventModel.setName("--Please select your event--");
                campaignEventModel.setCode(null);
                mCampaignEventModelList.add(campaignEventModel);
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.e(TAG, "onSaveInstanceState: ");

//        Log.e(TAG, "-> StandardGroups: " + standardGroupsAdapter.getGroupsList().size());
//        Log.e(TAG, "-> NonStandardGroups: " + nonStandardGroupsAdapter.getGroupsList().size());
//        Log.e(TAG, "-> CustomGroups: " + customGroupsAdapter.getGroupsList().size());

        outState.putParcelableArrayList(Constants.STATE_STANDARD_GROUPS_LIST, standardGroupsAdapter.getGroupsList());
        outState.putParcelableArrayList(Constants.STATE_NON_STANDARD_GROUPS_LIST, nonStandardGroupsAdapter.getGroupsList());
        outState.putParcelableArrayList(Constants.STATE_CUSTOM_GROUPS_LIST, customGroupsAdapter.getGroupsList());

        outState.putParcelableArrayList(Constants.STATE_STANDARD_FIELDS_LIST, standardGroupsAdapter.getAllFormFields());
        outState.putParcelableArrayList(Constants.STATE_NON_STANDARD_FIELDS_LIST, nonStandardGroupsAdapter.getAllFormFields());
        outState.putParcelableArrayList(Constants.STATE_CUSTOM_FIELDS_LIST, customGroupsAdapter.getAllFormFields());

        outState.putBoolean(Constants.STATE_COPY_FORM_STATUS, chCopyForm.isChecked());

        for (FieldsModel fieldsModel : standardGroupsAdapter.getAllFormFields()) {
            Log.e(TAG, "onSaveInstanceState: " + fieldsModel.toString());
        }

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.e(TAG, "onViewStateRestored: ");

        if (savedInstanceState != null) {

            isCopyingForm = true;

            boolean copyFormStatus = savedInstanceState.getBoolean(Constants.STATE_COPY_FORM_STATUS);
            chCopyForm.setChecked(copyFormStatus);
            isCopyingForm(copyFormStatus);

            ArrayList<GroupsModel> standardGroupListFromParcel = savedInstanceState.getParcelableArrayList(Constants.STATE_STANDARD_GROUPS_LIST);
            ArrayList<GroupsModel> nonStandardGroupListFromParcel = savedInstanceState.getParcelableArrayList(Constants.STATE_NON_STANDARD_GROUPS_LIST);
            ArrayList<GroupsModel> customGroupListFromParcel = savedInstanceState.getParcelableArrayList(Constants.STATE_CUSTOM_GROUPS_LIST);

            ArrayList<FieldsModel> standardFieldListFromParcel = savedInstanceState.getParcelableArrayList(Constants.STATE_STANDARD_FIELDS_LIST);
            ArrayList<FieldsModel> nonStandardFieldListFromParcel = savedInstanceState.getParcelableArrayList(Constants.STATE_NON_STANDARD_FIELDS_LIST);
            ArrayList<FieldsModel> customFieldListFromParcel = savedInstanceState.getParcelableArrayList(Constants.STATE_CUSTOM_FIELDS_LIST);

            standardGroupNamesList.clear();
            nonStandardGroupNamesList.clear();
            customGroupNamesList.clear();

            standardGroupNamesList.addAll(standardGroupListFromParcel);
            nonStandardGroupNamesList.addAll(nonStandardGroupListFromParcel);
            customGroupNamesList.addAll(customGroupListFromParcel);

            mCopyFormFieldList.clear();
            mCopyFormFieldList.addAll(standardFieldListFromParcel);
            mCopyFormFieldList.addAll(nonStandardFieldListFromParcel);
            mCopyFormFieldList.addAll(customFieldListFromParcel);

            standardGroupsAdapter.notifyDataSetChanged();
            nonStandardGroupsAdapter.notifyDataSetChanged();
            customGroupsAdapter.notifyDataSetChanged();
        }
    }

    public void stopHorizontalScrollViewScroll() {

    }

    public void setHorizontalScrollViewPosition(final int position) {
        String screenSizes = getActivity().getResources().getString(R.string.screen_size);
        if (!screenSizes.equals("small")) {
            horizontalScrollView.setSmoothScrollingEnabled(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    switch (position) {
                        case HorizontalScrollView.FOCUS_LEFT:
                            horizontalScrollView.fullScroll(HorizontalScrollView.FOCUS_LEFT);
                            break;

                        case HorizontalScrollView.FOCUS_RIGHT:
                            horizontalScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                            break;
                    }
                }
            }, 0);
        }
    }

    public void resetForm() {
        isCopyingForm = false;

        if (rlFormInactiveLayout != null && rlCopyFormInactiveLayout != null) {
            isCopyingForm(false);
            chCopyForm.setChecked(false);
        }

        mCopyFormFieldList.clear();
        mCopyFormGroupTempList.clear();
        mCopyFormGroupList.clear();
        mFilteredCopyStandardGroupList.clear();
        mFilteredCopyNonStandardGroupList.clear();
        mFilteredCopyCustomGroupList.clear();

        List<InputModel> defaultFields = mDatabaseHelper.getDefaultFields();
        for (int i = 0; i < defaultFields.size(); i++) {
            if (defaultFields.get(i).getIsActive().equals("1")) {
                InputModel inputModel = defaultFields.get(i);
                Log.e(TAG, "copy-FormFields: onResponse: " + inputModel.toString());
                Log.e(TAG, "Gee: onResponse: ID " + inputModel.getId()
                        + " name: " + inputModel.getName()
                        + " groupID: " + inputModel.getGroupId());

                boolean isFieldNameExist = false;
                FieldsModel fieldsModel = new FieldsModel();
                fieldsModel.setBrandCode(brandCode);
                fieldsModel.setCampaignCode(campaignCode);
                fieldsModel.setEventCode(eventCode);
                fieldsModel.setB_id(inputModel.getId());
                fieldsModel.setTypeId(inputModel.getTypeId());
                fieldsModel.setTitle(inputModel.getTitle());
                fieldsModel.setName(inputModel.getName());
                fieldsModel.setDescription(inputModel.getDescription());
                fieldsModel.setGroupId(inputModel.getGroupId());
                fieldsModel.setIsRequired(inputModel.getIsRequired());
                fieldsModel.setB_isActive(inputModel.getIsActive());
                fieldsModel.setB_dateTime(inputModel.getDateTime());
                fieldsModel.setCategory(inputModel.getCategory());
                fieldsModel.setA_id(inputModel.getId());
                fieldsModel.setSort(Integer.parseInt(inputModel.getId()));
                fieldsModel.setA_isActive((inputModel.getIsMandatory().equals("1") ? "1" : "0"));
                fieldsModel.setDataArrayList(inputModel.getDataArray());
                fieldsModel.setIsMandatory(inputModel.getIsMandatory());
                fieldsModel.setGroupName(inputModel.getGroupName());

//            /**Checking fields for duplicates*/
//            for (int j = 0; j < mCopyFormFieldList.size(); j++) {
//                FieldsModel fieldsModel1 = mCopyFormFieldList.get(j);
//                if (inputModel.getName().equals(fieldsModel1.getName())) {
//                    fieldsModel1.setA_isActive(inputModel.getAssignIsActive());
//                    fieldsModel1.setSort(Integer.parseInt(inputModel.getSort()));
//                    fieldsModel1.setIsMandatory(inputModel.getIsMandatory());
//                    fieldsModel1.setGroupId(inputModel.getGroupId());
//                    fieldsModel1.setGroupName(inputModel.getGroupName());
//                    isFieldNameExist = true;
//                }
//            }

                /**Adding field into list if doesn't exist*/
                if (!isFieldNameExist) {
                    mCopyFormFieldList.add(fieldsModel);
                }
            }
        }

        /**Getting groups*/
        for (FieldsModel fieldsModel : mCopyFormFieldList) {
            if (fieldsModel.getGroupName() == null) {
                fieldsModel.setGroupName(mDatabaseHelper.getGroupNameById(Integer.parseInt(fieldsModel.getGroupId())));
            }

            String strGroupName = fieldsModel.getGroupName();
            String strFormattedGroupName = fieldsModel.getGroupName().replace(" ", "_").toLowerCase();

            GroupsModel groupsModel = new GroupsModel();
            groupsModel.setId(fieldsModel.getGroupId());
            groupsModel.setTitle(strGroupName);
            groupsModel.setName(strFormattedGroupName);
            groupsModel.setDescription("");
            groupsModel.setBrandCode(mSelectedBrandCode);
            groupsModel.setCampaignCode(mSelectedCampaignCode);
            groupsModel.setEventCode(mSelectedCampaignEventCode);
            groupsModel.setIsActive("1");
            groupsModel.setUserId("");
            groupsModel.setStatus("0");

            Log.e(TAG, "onResponse: GroupsLogsList: " + mCopyFormGroupList.size());
            Log.e(TAG, "onResponse: GroupsLogsList: " + mCopyFormGroupList);

            if (mCopyFormGroupTempList.contains(groupsModel.toString())) {
                Log.e(TAG, "onResponse: --> " + "group contain");
            } else {
                Log.e(TAG, "onResponse: --> " + "not contain");
                mCopyFormGroupTempList.add(groupsModel.toString());
                mCopyFormGroupList.add(groupsModel);
            }
        }

        /**Getting data array of standard and non-standard field*/
        for (FieldsModel fieldsModel : mCopyFormFieldList) {
            if (fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_STANDARD_FIELDS)) ||
                    fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_NON_STANDARD_FIELDS))) {

                switch (fieldsModel.getTypeId()) {
                    case "3":
                        fieldsModel.setDataArrayList(mDatabaseHelper
                                .getDataArrayByFieldId(DatabaseConstants.TBL_RADIO, fieldsModel.getB_id()));
                        break;

                    case "4":
                        fieldsModel.setDataArrayList(mDatabaseHelper
                                .getDataArrayByFieldId(DatabaseConstants.TBL_SELECT, fieldsModel.getB_id()));
                        break;
                }
            }
        }

        /**Sorting copied fields*/
        Collections.sort(mCopyFormFieldList, new Comparator<FieldsModel>() {
            @Override
            public int compare(FieldsModel fieldsModel, FieldsModel t1) {
                return Integer.valueOf(fieldsModel.getSort()).compareTo(t1.getSort());
            }
        });

        /**Filtering groups for standard fields*/
        for (GroupsModel mainGroupList : mCopyFormGroupList) {
            for (FieldsModel fieldsModel : mCopyFormFieldList) {
                Log.e(TAG, "onResponse: " + fieldsModel.toString());
                Log.e(TAG, "onResponse: " + mainGroupList.getId());

                if (fieldsModel.getGroupId().equals(mainGroupList.getId()) &&
                        fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_STANDARD_FIELDS))) {
                    Log.e(TAG, "onResponse: " + "Group Name: " + mainGroupList.getName() + " is in used by Field Name " + fieldsModel.getName());

                    if (mFilteredCopyStandardGroupList.contains(mainGroupList)) {
                        Log.e(TAG, "onResponse: " + "filtered contains " + mainGroupList.getName());

                    } else {
                        Log.e(TAG, "onResponse: " + "filtered not contains " + mainGroupList.getName());
                        mFilteredCopyStandardGroupList.add(mainGroupList);
                    }

                }
            }
        }

        /**Filtering groups for non-standard fields*/
        for (GroupsModel mainGroupList : mCopyFormGroupList) {
            for (FieldsModel fieldsModel : mCopyFormFieldList) {
                if (fieldsModel.getGroupId().equals(mainGroupList.getId()) &&
                        fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_NON_STANDARD_FIELDS))) {
                    Log.e(TAG, "onResponse: " + "Group Name: " + mainGroupList.getName() + " is in used by Field Name " + fieldsModel.getName());

                    if (mFilteredCopyNonStandardGroupList.contains(mainGroupList)) {
                        Log.e(TAG, "onResponse: " + "filtered contains " + mainGroupList.getName());
                    } else {
                        Log.e(TAG, "onResponse: " + "filtered not contains " + mainGroupList.getName());
                        mFilteredCopyNonStandardGroupList.add(mainGroupList);
                    }

                }
            }
        }

        /**Filtering groups for custom fields*/
        for (GroupsModel mainGroupList : mCopyFormGroupList) {
            for (FieldsModel fieldsModel : mCopyFormFieldList) {
                Log.e(TAG, "onResponse: " + fieldsModel.toString());
                if (fieldsModel.getGroupId().equals(mainGroupList.getId()) &&
                        fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_ADDITIONAL_FIELDS))) {
                    Log.e(TAG, "Gee: " + "Group Name: " + mainGroupList.getName() + " is in used by Field Name " + fieldsModel.getName());

                    if (mFilteredCopyCustomGroupList.contains(mainGroupList)) {
                        Log.e(TAG, "onResponse: " + "filtered contains " + mainGroupList.getName());
                    } else {
                        Log.e(TAG, "onResponse: " + "filtered not contains " + mainGroupList.getName());
                        mFilteredCopyCustomGroupList.add(mainGroupList);
                    }

                }
            }
        }

        isCopyingForm = true;

        standardGroupNamesList.clear();
        standardGroupNamesList.addAll(mFilteredCopyStandardGroupList);
        standardGroupsAdapter.notifyDataSetChanged();

        nonStandardGroupNamesList.clear();
        nonStandardGroupNamesList.addAll(mFilteredCopyNonStandardGroupList);
        nonStandardGroupsAdapter.notifyDataSetChanged();

        customGroupNamesList.clear();
        customGroupNamesList.addAll(mFilteredCopyCustomGroupList);
        customGroupsAdapter.notifyDataSetChanged();
    }

    public void saveForm() {
        Log.e(TAG, "saveForm: ");
        /**Getting form fields base on copy status variable*/
        mSaveFormFieldList.clear();

        if (isCopyingForm) {
            mSaveFormFieldList.addAll(mCopyFormFieldList);
        } else {
            mSaveFormFieldList.addAll(standardGroupsAdapter.getAllFormFields());
            mSaveFormFieldList.addAll(nonStandardGroupsAdapter.getAllFormFields());
            mSaveFormFieldList.addAll(customGroupsAdapter.getAllFormFields());
        }

        /**Validate fields if has changes*/
        hasChanges = false;
        mFormFieldsChangesValidatorList.clear();
        mFormFieldsChangesValidatorList.addAll(mDatabaseHelper.getAllFormFields(brandCode, campaignCode, eventCode));

        Log.e(TAG, "saveForm: " + mFormFieldsChangesValidatorList.size());
        /**Inserting data arrays to field validator list*/
        for (FieldsModel fieldsModel : mFormFieldsChangesValidatorList) {
            Log.e(TAG, "saveForm: " + fieldsModel.toString());
            switch (fieldsModel.getTypeId()) {
                case "3":
                    fieldsModel.setDataArrayList(mDatabaseHelper
                            .getDataArrayByFieldId(DatabaseConstants.TBL_RADIO, fieldsModel.getB_id()));
                    break;

                case "4":
                    fieldsModel.setDataArrayList(mDatabaseHelper
                            .getDataArrayByFieldId(DatabaseConstants.TBL_SELECT, fieldsModel.getB_id()));
                    break;
            }
        }

        for (FieldsModel fm1 : mSaveFormFieldList) {
            if (isValidatorListContainField(mFormFieldsChangesValidatorList, fm1.getA_id())) {
                for (FieldsModel fm2 : mFormFieldsChangesValidatorList) {
                    if (fm1.getA_id().equals(fm2.getA_id())) {
                        if (!fm1.checkFieldsChanges().equals(fm2.checkFieldsChanges())) {
                            Log.e(TAG, "saveForm: hasChanges-1a: " + fm1.checkFieldsChanges());
                            Log.e(TAG, "saveForm: hasChanges-1b: " + fm2.checkFieldsChanges());
                            hasChanges = true;
                        }
                    }
                }
            } else {
                Log.e(TAG, "saveForm: hasChanges-2a: " +
                        "name: " + fm1.getName() +
                        " id: " + fm1.getA_id() +
                        " isActive: " + fm1.getA_isActive());
                hasChanges = true;
            }
        }

//        if (hasChanges) {
            if (NetworkUtils.isNetworkConnected(getActivity())) {
                /**
                 * Note: isCopy parameter in AsyncCopyingFormFields class
                 *  is always true to clear all existing fields in server,
                 *  and update a new one
                 */
                new AsyncCopyingFormFields(getActivity(), mSaveFormFieldList, mCopyFormGroupList, true, false).execute();

            } else {
                standardGroupsAdapter.saveFormFields();
                nonStandardGroupsAdapter.saveFormFields();
                customGroupsAdapter.saveFormFields();
            }
//        } else {
//            mMaintenancePresenter.showAlertDialog("Alert", "No changes detected", Constants.DIALOG_ACTION_NO_FORM_CHANGES);
//        }
    }

    /**
     * Note: This method will be use for checking
     * changes in form field upon saving
     */
    private boolean isValidatorListContainField(List<FieldsModel> fieldsModelList, String id) {
        for (FieldsModel fieldsModel : fieldsModelList) {
            if (fieldsModel.getA_id().equals(id)) {
                return true;
            }
        }
        return false;
    }

}
