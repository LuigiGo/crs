package com.unilab.customerregistration.features.maintenance.view.adapters;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.view.FormFragment;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.constants.DatabaseConstants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;

import java.util.ArrayList;
import java.util.List;

public class StandardGroupsAdapter extends RecyclerView.Adapter<StandardGroupsAdapter.ViewHolder> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private ArrayList<GroupsModel> mGroupsModelList;
    private DatabaseHelper mDatabaseHelper;
    private String brandCode, campaignCode, eventCode;
    private List<FieldsModel> mCopiedFormFields;

    private StandardFieldByGroupsAdapter standardFieldByGroupsAdapter;
    private List<StandardFieldByGroupsAdapter> mStandardFieldByGroupAdapterList;


    public StandardGroupsAdapter(Context context, ArrayList<GroupsModel> groupsModelList, ArrayList<FieldsModel> copiedFields) {
        mDatabaseHelper = new DatabaseHelper(context);
        SharedPrefHelper.init(context);

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        mContext = context;
        mGroupsModelList = groupsModelList;
        mStandardFieldByGroupAdapterList = new ArrayList<>();

        mCopiedFormFields = copiedFields;
        Log.e(TAG, "StandardGroupsAdapter: ");

    }

    @Override
    public StandardGroupsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View views = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_groups, parent, false);
        Log.e(TAG, "onCreateViewHolder: ");
        return new ViewHolder(views);
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        Log.e(TAG, "onViewRecycled: ");
        mStandardFieldByGroupAdapterList.clear();
    }

    @Override
    public void onBindViewHolder(StandardGroupsAdapter.ViewHolder holder, int position) {
        final GroupsModel mGroupsModel = mGroupsModelList.get(position);

        Log.e(TAG, "onBindViewHolder: " + mGroupsModel.getName());
        holder.txtGroupNames.setText(mGroupsModel.getTitle());

        List<FieldsModel> fieldsModelArrayList = new ArrayList<>();

        if (FormFragment.isCopyingForm) {
            Log.e(TAG, "onBindViewHolder: CopyForm is true");
            for (FieldsModel fieldsModel : mCopiedFormFields) {
                if (fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_STANDARD_FIELDS)) &&
                        fieldsModel.getGroupId().equals(mGroupsModel.getId())) {
                    fieldsModelArrayList.add(fieldsModel);
                }
            }
        } else {
            Log.e(TAG, "onBindViewHolder: CopyForm is false");
            fieldsModelArrayList = mDatabaseHelper.getStandardFieldsByGroupId(brandCode, campaignCode, eventCode, mGroupsModel.getId());
            /**Getting data array for standard fields*/
            for (FieldsModel fieldsModel : fieldsModelArrayList) {
                switch (fieldsModel.getTypeId()) {
                    case "3":
                        fieldsModel.setDataArrayList(mDatabaseHelper.getDataArrayByFieldId(DatabaseConstants.TBL_RADIO, fieldsModel.getB_id()));
                        break;

                    case "4":
                        fieldsModel.setDataArrayList(mDatabaseHelper.getDataArrayByFieldId(DatabaseConstants.TBL_SELECT, fieldsModel.getB_id()));
                        break;
                }
            }
        }

        standardFieldByGroupsAdapter = new StandardFieldByGroupsAdapter(mContext, fieldsModelArrayList, mGroupsModel.getId());
        mStandardFieldByGroupAdapterList.add(standardFieldByGroupsAdapter);
        holder.recyclerInputFields.setAdapter(standardFieldByGroupsAdapter);
    }

    @Override
    public int getItemCount() {
        return mGroupsModelList.size();
    }

    /**
     * SaveFormFields method will be used for saving fields status for offline purposes
     */
    public void saveFormFields() {
        for (int i = 0; i < mStandardFieldByGroupAdapterList.size(); i++) {
            mStandardFieldByGroupAdapterList.get(i).saveFormFields();
        }
    }

    public ArrayList<FieldsModel> getAllFormFields() {
        ArrayList<FieldsModel> allFormFieldsList = new ArrayList<>();
        for (int i = 0; i < mStandardFieldByGroupAdapterList.size(); i++) {
            List<FieldsModel> fieldsModelList = mStandardFieldByGroupAdapterList.get(i).getFields();
            allFormFieldsList.addAll(fieldsModelList);
        }
        return allFormFieldsList;
    }

    public ArrayList<GroupsModel> getGroupsList() {
        return mGroupsModelList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtGroupNames;
        private RecyclerView recyclerInputFields;

        public ViewHolder(View v) {
            super(v);

            txtGroupNames = (TextView) v.findViewById(R.id.txtGroupName);

            recyclerInputFields = (RecyclerView) v.findViewById(R.id.recyclerInputFields);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext.getApplicationContext());
            recyclerInputFields.setLayoutManager(mLayoutManager);
            recyclerInputFields.setItemAnimator(new DefaultItemAnimator());
        }
    }
}
