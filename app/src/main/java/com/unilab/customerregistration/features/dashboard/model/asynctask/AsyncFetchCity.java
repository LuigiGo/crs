package com.unilab.customerregistration.features.dashboard.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.unilab.customerregistration.features.dashboard.model.CityModel;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;

import java.util.List;

public class AsyncFetchCity extends AsyncTask<Void, Void, Void> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private DatabaseHelper mDatabaseHelper;
    private List<CityModel> mCityModelList;

    public AsyncFetchCity(Context context, List<CityModel> cityModelList) {
        DialogHelper.init(context);
        mDatabaseHelper = new DatabaseHelper(context);

        mContext = context;
        mCityModelList = cityModelList;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        DialogHelper.getInstance().showProgressDialog(mContext, "Getting city data");

    }

    @Override
    protected Void doInBackground(Void... params) {
        for (CityModel cityModel : mCityModelList) {
            Log.e(TAG, "onResponse: " + cityModel.getCityName());
            if (!mDatabaseHelper.checkCityIdIfExist(cityModel)) {
                mDatabaseHelper.createCity(cityModel);
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        SharedPrefHelper.getInstance().putPreLoadedDataStatusToShared(true);
        DialogHelper.getInstance().hideProgressDialog(mContext);
    }

}
