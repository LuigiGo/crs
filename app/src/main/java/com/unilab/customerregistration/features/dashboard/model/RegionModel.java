package com.unilab.customerregistration.features.dashboard.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegionModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("region_id")
    @Expose
    private String regionId;
    @SerializedName("region_name")
    @Expose
    private String regionName;
    @SerializedName("country_id")
    @Expose
    private String countryId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }
}
