package com.unilab.customerregistration.features.registration.presenter;

import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.registration.model.RegistrationModel;

public interface RegistrationContract {

    void refreshRegistrationForm();

    interface DialogListener {

        void showRegistrationAlertDialog(String title, String message, String actionType, String result,
                                         SettingsModel settingsModel);

        void showRegistrationUpdateAndSavePromptDialog(String title, String message, String actionType,
                                                       SettingsModel settingsModel, RegistrationModel registrationModel);

        void updateExistingRegistrationData(RegistrationModel registrationModel);

        void promptExistingRegistrationDataDetected(RegistrationModel registrationModel);

        void saveRegistrationDetails(RegistrationModel registrationModel);

        void showRegistrationAgreementDialog(SettingsModel settingsModel);

        void setSubscriptionStatus(boolean status);


    }
}
