package com.unilab.customerregistration.features.maintenance.view;

import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.base.view.BaseActivity;
import com.unilab.customerregistration.features.dashboard.view.DashboardActivity;
import com.unilab.customerregistration.features.maintenance.model.AssignFieldModel;
import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.model.InputModel;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.features.maintenance.view.adapters.SubGroupingAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.AddUpdateFieldSubGroupNameAdapter;
import com.unilab.customerregistration.features.registration.model.RegistrationModel;
import com.unilab.customerregistration.features.registration.presenter.RegistrationContract;
import com.unilab.customerregistration.features.registration.view.ActivityRegistration;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.AddFieldAdapterDialogFragment;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.AddFieldDialogFragment;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.AddFormFieldSavePromptDialog;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.AlertImageAlignmentDialogFragment;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.AlertRegistrationDialogFragment;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.ColorPickerAlertDialogFragment;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.FontsAlertDialogFragment;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.AddFormFieldAdapterSavePromptDialog;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.FormSavePromptDialogFragment;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.MaintenanceAlertDialogFragment;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.DesignSavePromptDialogFragment;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.RegistrationAgreementDialogFragment;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.RegistrationUpdateAndSavePromptDialogFragment;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.SubmitButtonAlertDialogFragment;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.util.List;

public class MaintenanceActivity extends BaseActivity implements View.OnClickListener,
        MaintenancePresenter, RegistrationContract, RegistrationContract.DialogListener {

    private final String TAG = this.getClass().getSimpleName();
    private DrawerLayout mDrawer;
    ImageView btnOpenDrawer, btnCloseDrawer;
    ImageView imgConfigure, imgCustomize, imgConfigureDrawer, imgCustomizeDrawer;
    ImageView imgBackground, imgCmslogo, imgDrawerLogo;
    TextView txtUser, txtUserDrawer;
    DatabaseHelper mDatabaseHelper;
    private String brandCode, campaignCode, eventCode;
    TextView txtLogOut, txtConfigure, txtCustomize, txtDesign, txtForm, txtPreview;
    TextView txtLogOutDrawer, txtConfigureDrawer, txtCustomizeDrawer, txtDesignDrawer, txtFormDrawer, txtPreviewDrawer;
    private TextView txtSubPageHeader;
    Button btnMenuOptions;
    private RelativeLayout rlSubToolbar;

    private BrandFragment brandFragment;
    private DesignFragment designFragment;
    private FormFragment formFragment;
    private PreviewFragment previewFragment;
    private AlertDialog mAlertInfoDialog;
    private DialogFragment mMaintenanceAlertDialogFragment, alertColorPickerDialogFragment,
            alertFontsDialogFragment, alertSubmitButtonDialogFragment,
            alertSavePromptDialogFragment, mAddFormFieldDialogFragment,
            mAddFormFieldAdapterDialogFragment, mAddFormFieldAdapterSavePromptDialog,
            mAddFormFieldSavePromptDialog, mAlertRegistrationDialog, alertImageAlignmentDialog,
            mAlertRegistrationUpdateSaveDialog, mAlertAgreementPromptDialog, mAlertFormSavePromptDialog;

    private Handler mHandler;

    protected int FRAGMENT_PAGE = Constants.DESIGN_FRAGMENT;
    private PopupWindow popupWindow;

    public static Context mainActivityContext;
    public static boolean isActivityIsRunning = false;

    /**
     * Broadcast Receivers
     */
    private BroadcastReceiver mBroadcastReceiver;
    private IntentFilter mIntentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintenance);
        mDatabaseHelper = new DatabaseHelper(this);
        SharedPrefHelper.init(this);
        DialogHelper.init(this);

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        mainActivityContext = this;

        setupViews();
        setupBroadcastReceiver();
        loadDetails();

        if (savedInstanceState == null) {
//            if (!brandCode.isEmpty() && !campaignCode.isEmpty()) {
//                showDesignPage();
//                txtSubPageHeader.setText("Design");
//                FRAGMENT_PAGE = Constants.DESIGN_FRAGMENT;
//
//            } else {
            showBrandPage();
            txtSubPageHeader.setText("");
            FRAGMENT_PAGE = Constants.CONFIGURE_FRAGMENT;
//            }
        }
    }

    private void loadDetails() {
        String userEmailFromShared = SharedPrefHelper.getInstance().getEmailFromShared();
        String lblWelcome = "Welcome, ";
        txtUserDrawer.setText(userEmailFromShared.substring(0, userEmailFromShared.indexOf("@")));
        txtUser.setText(userEmailFromShared.substring(0, userEmailFromShared.indexOf("@")));
        btnMenuOptions.setText(lblWelcome.concat(userEmailFromShared));

        Utils.loadImageFromDrawable(this, btnOpenDrawer, R.drawable.ic_drawer_close);
        Utils.loadImageFromDrawable(this, btnCloseDrawer, R.drawable.ic_drawer_open);
        Utils.loadImageFromDrawable(this, imgConfigure, R.drawable.ic_confiure_pressed);
        Utils.loadImageFromDrawable(this, imgCustomize, R.drawable.ic_customize_unpressed);
        Utils.loadImageFromDrawable(this, imgConfigureDrawer, R.drawable.ic_confiure_pressed);
        Utils.loadImageFromDrawable(this, imgCustomizeDrawer, R.drawable.ic_customize_unpressed);
        Utils.loadImageFromDrawable(this, imgBackground, R.drawable.bg_crs);
        Utils.loadImageFromDrawable(this, imgCmslogo, R.drawable.ic_crs_subtoolbar_logo);
        Utils.loadImageFromDrawable(this, imgDrawerLogo, R.drawable.ic_crs_drawer_logo);

        int orientation = getResources().getConfiguration().orientation;
        switch (orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                break;
        }
    }

    private void setupViews() {
        imgBackground = (ImageView) findViewById(R.id.imgBackground);
        imgCmslogo = (ImageView) findViewById(R.id.imgCmsLogo);
        imgDrawerLogo = (ImageView) findViewById(R.id.imgDrawerLogo);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        btnOpenDrawer = (ImageView) findViewById(R.id.btnOpenDrawer);
        btnCloseDrawer = (ImageView) findViewById(R.id.btnCloseDrawer);
        txtLogOutDrawer = (TextView) findViewById(R.id.txtLogOutDrawer);
        imgConfigureDrawer = (ImageView) findViewById(R.id.imgConfigureDrawer);
        txtConfigureDrawer = (TextView) findViewById(R.id.txtConfigureDrawer);
        imgCustomizeDrawer = (ImageView) findViewById(R.id.imgCustomizeDrawer);
        txtCustomizeDrawer = (TextView) findViewById(R.id.txtCustomizeDrawer);
        txtDesignDrawer = (TextView) findViewById(R.id.txtDesignDrawer);
        txtFormDrawer = (TextView) findViewById(R.id.txtFormDrawer);
        txtPreviewDrawer = (TextView) findViewById(R.id.txtPreviewDrawer);
        txtUserDrawer = (TextView) findViewById(R.id.txtUserDrawer);

        txtSubPageHeader = (TextView) findViewById(R.id.txtSubPageHeader);
        btnMenuOptions = (Button) findViewById(R.id.btnOptionsMenu);
        imgConfigure = (ImageView) findViewById(R.id.imgConfigure);
        imgCustomize = (ImageView) findViewById(R.id.imgCustomize);

        txtLogOut = (TextView) findViewById(R.id.txtLogOut);
        txtConfigure = (TextView) findViewById(R.id.txtConfigure);
        txtCustomize = (TextView) findViewById(R.id.txtCustomize);
        txtDesign = (TextView) findViewById(R.id.txtDesign);
        txtForm = (TextView) findViewById(R.id.txtForm);
        txtPreview = (TextView) findViewById(R.id.txtPreview);
        txtUser = (TextView) findViewById(R.id.txtUser);

        rlSubToolbar = (RelativeLayout) findViewById(R.id.llSubToolbar);

        btnOpenDrawer.setOnClickListener(this);
        btnCloseDrawer.setOnClickListener(this);
        txtLogOutDrawer.setOnClickListener(this);
        imgConfigureDrawer.setOnClickListener(this);
        txtConfigureDrawer.setOnClickListener(this);
        txtCustomizeDrawer.setOnClickListener(this);
        txtDesignDrawer.setOnClickListener(this);
        txtFormDrawer.setOnClickListener(this);
        txtPreviewDrawer.setOnClickListener(this);

        imgConfigure.setOnClickListener(this);
        txtLogOut.setOnClickListener(this);
        txtConfigure.setOnClickListener(this);
        txtCustomize.setOnClickListener(this);
        txtDesign.setOnClickListener(this);
        txtForm.setOnClickListener(this);
        txtPreview.setOnClickListener(this);

        btnMenuOptions.setOnClickListener(this);

        mHandler = new Handler();

        /**Managing fragments for device rotation*/
        brandFragment = (BrandFragment) getSupportFragmentManager().findFragmentByTag("BrandFragment");
        designFragment = (DesignFragment) getSupportFragmentManager().findFragmentByTag("DesignFragment");
        formFragment = (FormFragment) getSupportFragmentManager().findFragmentByTag("FormFragment");
        previewFragment = (PreviewFragment) getSupportFragmentManager().findFragmentByTag("PreviewFragment");

        if (brandFragment == null) {
            brandFragment = new BrandFragment();
        }

        if (designFragment == null) {
            designFragment = new DesignFragment();
        }

        if (formFragment == null) {
            formFragment = new FormFragment();
        }

        if (previewFragment == null) {
            previewFragment = new PreviewFragment();
        }

    }

    private void setupBroadcastReceiver() {
        mIntentFilter = new IntentFilter(Constants.RECEIVER_SHOW_REG_UPDATES);
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case Constants.RECEIVER_SHOW_REG_UPDATES:
                        onShowRegUpdateDialog(intent);
                        break;
                }
            }
        };
        registerReceiver(mBroadcastReceiver, mIntentFilter);
    }

    private void toggleDrawer() {
        Utils.hideKeyboard(this);
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            mDrawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onClick(View v) {
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        switch (v.getId()) {
            case R.id.btnOpenDrawer:
                toggleDrawer();
                break;

            case R.id.btnCloseDrawer:
                toggleDrawer();
                break;

            case R.id.txtLogOutDrawer:
//                clearSharedData();
//                onBackPressed();
                if (brandCode.isEmpty() && campaignCode.isEmpty() && eventCode.isEmpty()) {
                    startActivity(new Intent(this, DashboardActivity.class));
                } else {
                    startActivity(new Intent(this, ActivityRegistration.class));
                }
                finish();
                break;

            case R.id.imgConfigureDrawer:
                toggleDrawer();
                if (isConfigured()) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            FRAGMENT_PAGE = Constants.CONFIGURE_FRAGMENT;
                            showBrandPage();
                            txtSubPageHeader.setText("");
                        }
                    }, 300);
                }
                break;

            case R.id.txtConfigureDrawer:
                toggleDrawer();
                if (isConfigured()) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            FRAGMENT_PAGE = Constants.CONFIGURE_FRAGMENT;
                            showBrandPage();
                            txtSubPageHeader.setText("");
                        }
                    }, 300);
                }
                break;

            case R.id.txtDesignDrawer:
                toggleDrawer();
                if (isConfigured()) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            FRAGMENT_PAGE = Constants.DESIGN_FRAGMENT;
                            showDesignPage();
                            txtSubPageHeader.setText("Design");
                        }
                    }, 300);
                }
                break;

            case R.id.txtFormDrawer:
                toggleDrawer();
                if (isConfigured()) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            FRAGMENT_PAGE = Constants.FORM_FRAGMENT;
                            changeFragment(formFragment);
                            txtSubPageHeader.setText("Form");
                        }
                    }, 300);
                }
                break;

            case R.id.txtPreviewDrawer:
                toggleDrawer();
                if (isConfigured()) {
                    FRAGMENT_PAGE = Constants.PREVIEW_FRAGMENT;
                    previewFragment = new PreviewFragment();
                    changeFragment(previewFragment);
                    txtSubPageHeader.setText("Online Customer Registration");
                }
                break;

            case R.id.imgConfigure:
                FRAGMENT_PAGE = Constants.CONFIGURE_FRAGMENT;
                showBrandPage();
                txtSubPageHeader.setText("");
                break;

            case R.id.txtConfigure:
                FRAGMENT_PAGE = Constants.CONFIGURE_FRAGMENT;
                showBrandPage();
                txtSubPageHeader.setText("");
                break;

            case R.id.txtLogOut:
//                clearSharedData();
//                onBackPressed();
                if (brandCode.isEmpty() && campaignCode.isEmpty() && eventCode.isEmpty()) {
                    startActivity(new Intent(this, DashboardActivity.class));
                } else {
                    startActivity(new Intent(this, ActivityRegistration.class));
                }
                finish();
                break;

            case R.id.txtDesign:
                if (isConfigured()) {
                    FRAGMENT_PAGE = Constants.DESIGN_FRAGMENT;
                    showDesignPage();
                    txtSubPageHeader.setText("Design");
                }
                break;

            case R.id.txtForm:
                if (isConfigured()) {
                    FRAGMENT_PAGE = Constants.FORM_FRAGMENT;
                    changeFragment(formFragment);
                    txtSubPageHeader.setText("Form");
                }
                break;

            case R.id.txtPreview:
                if (isConfigured()) {
                    FRAGMENT_PAGE = Constants.PREVIEW_FRAGMENT;
                    previewFragment = new PreviewFragment();
                    changeFragment(previewFragment);
                    txtSubPageHeader.setText("Online Customer Registration");
                }
                break;

            case R.id.btnOptionsMenu:
                subToolbarPopupWidndow();
                break;
        }

        highlightSelectedPage();
    }

    @Override
    public void showBrandPage() {
        changeFragment(brandFragment);
    }

    @Override
    public void showDesignPage() {
        FRAGMENT_PAGE = Constants.DESIGN_FRAGMENT;
        changeFragment(designFragment);
        highlightSelectedPage();
    }

    @Override
    public void showFormPage() {
        FRAGMENT_PAGE = Constants.FORM_FRAGMENT;
        changeFragment(formFragment);
        highlightSelectedPage();
    }

    @Override
    public void showPreviewPage() {
        FRAGMENT_PAGE = Constants.PREVIEW_FRAGMENT;
        changeFragment(previewFragment);
        highlightSelectedPage();
    }

    @Override
    public void refreshFormAdapters() {
        mAddFormFieldAdapterSavePromptDialog = (DialogFragment) getFragmentManager()
                .findFragmentByTag("add_form_field_adapter_prompt_dialog");
        if (mAddFormFieldAdapterSavePromptDialog != null) {
            mAddFormFieldAdapterSavePromptDialog.dismiss();
        }

        mAddFormFieldDialogFragment = (DialogFragment) getFragmentManager()
                .findFragmentByTag("add_form_field_dialog");
        if (mAddFormFieldDialogFragment != null) {
            mAddFormFieldDialogFragment.dismiss();
        }

        formFragment.refreshAdapters();
    }

    @Override
    public void refreshCustomFieldsAdapter() {
        mAddFormFieldAdapterSavePromptDialog = (DialogFragment) getFragmentManager()
                .findFragmentByTag("add_form_field_adapter_prompt_dialog");
        if (mAddFormFieldAdapterSavePromptDialog != null) {
            mAddFormFieldAdapterSavePromptDialog.dismiss();
        }

        mAddFormFieldDialogFragment = (DialogFragment) getFragmentManager()
                .findFragmentByTag("add_form_field_dialog");
        if (mAddFormFieldDialogFragment != null) {
            mAddFormFieldDialogFragment.dismiss();
        }

        formFragment.refreshCustomFieldsAdapter();
    }

    @Override
    public void loadCampaignList(String brandCode, String brandName) {
        Log.e(TAG, "loadCampaignList: ");
        switch (FRAGMENT_PAGE) {
            case Constants.DESIGN_FRAGMENT:
                designFragment.callCampaignAPI(brandCode, brandName);
                break;

            case Constants.FORM_FRAGMENT:
                formFragment.callCampaignAPI(brandCode, brandName);
                break;

            default:
                brandFragment.callCampaignAPI(brandCode, brandName);
                break;
        }

    }

    @Override
    public void loadCampaignEventList(String campaignCode, String campaignName) {
        Log.e(TAG, "loadCampaignEventList: ");
        switch (FRAGMENT_PAGE) {
            case Constants.DESIGN_FRAGMENT:
                designFragment.callCampaignEventAPI(campaignCode, campaignName);
                break;

            case Constants.FORM_FRAGMENT:
                formFragment.callCampaignEventAPI(campaignCode, campaignName);
                break;

            default:
                brandFragment.callCampaignEventAPI(campaignCode, campaignName);
                break;
        }
    }

    @Override
    public void saveConfiguredSettings(String campaignEventCode, String campaignEventName) {
        switch (FRAGMENT_PAGE) {
            case Constants.DESIGN_FRAGMENT:
                designFragment.saveConfiguredSettings(campaignEventCode, campaignEventName);
                break;

            case Constants.FORM_FRAGMENT:
                formFragment.saveConfiguredSettings(campaignEventCode, campaignEventName);
                break;

            default:
                brandFragment.saveConfiguredSettings(campaignEventCode, campaignEventName);
                break;
        }
    }

    @Override
    public void synchronizeForm(boolean isSynchronizingForm) {
        formFragment.synchronize(isSynchronizingForm);
    }

    @Override
    public void setAddFieldButtonVisibility(int visibility) {
        formFragment.setAddFieldButtonVisibility(visibility);
    }

    private boolean isConfigured() {
        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();

        if (!brandCode.isEmpty() && !campaignCode.isEmpty()) {
            Log.e(TAG, "isConfigured: " + "true");
            return true;
        }
        Log.e(TAG, "isConfigured: " + "false");
        showConfigPrompt(this, "Alert", "Please select Brand to start configuration ");
        return false;
    }

    public void showConfigPrompt(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View v = LayoutInflater.from(context).inflate(R.layout.dialog_information, null);
        builder.setView(v);

        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        Button btnOk = (Button) v.findViewById(R.id.btnOk);

        txtTitle.setText(title);
        txtMessage.setText(message);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertInfoDialog.dismiss();
                showBrandPage();
            }
        });

        mAlertInfoDialog = builder.create();
        if (mAlertInfoDialog != null) {
            if (mAlertInfoDialog.isShowing()) {
                mAlertInfoDialog.dismiss();
            }
            mAlertInfoDialog.show();
        }
    }

    public void subToolbarPopupWidndow() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = inflater.inflate(R.layout.popup_window, null);

        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        int widthSubToolbar = rlSubToolbar.getWidth();
        popupWindow.showAsDropDown(btnMenuOptions, widthSubToolbar - 10, 0);

        TextView txtLogOutToolbar = (TextView) popupView.findViewById(R.id.txtLogOutToolbar);
        ImageView imgLogOutToolbar = (ImageView) popupView.findViewById(R.id.imgLogOutToolbar);

        txtLogOutToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                clearSharedData();
                if (popupWindow != null) {
                    popupWindow.dismiss();
                }
//                onBackPressed();
                startActivity(new Intent(MaintenanceActivity.this, ActivityRegistration.class));
                finish();
            }
        });

        imgLogOutToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                clearSharedData();
                if (popupWindow != null) {
                    popupWindow.dismiss();
                }
//                onBackPressed();
                startActivity(new Intent(MaintenanceActivity.this, ActivityRegistration.class));
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityIsRunning = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActivityIsRunning = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler = null;
        unregisterReceiver(mBroadcastReceiver);
    }

    private void clearSharedData() {
        SharedPrefHelper.getInstance().putBrandCodeToShared("");
        SharedPrefHelper.getInstance().putCampaignCodeToShared("");
        SharedPrefHelper.getInstance().putEventCodeToShared("");
        SharedPrefHelper.getInstance().putBrandNameToShared("");
        SharedPrefHelper.getInstance().putCampaignNameToShared("");
        SharedPrefHelper.getInstance().putEventNameToShared("");
        SharedPrefHelper.getInstance().putEmailToShared("");
        SharedPrefHelper.getInstance().putPasswordToShared("");
        SharedPrefHelper.getInstance().putGeneratedUrlToShared("");
        SharedPrefHelper.getInstance().putFormattedCampaignNameToShared("");
    }

    private void highlightSelectedPage() {
        int whiteColor = getResources().getColor(android.R.color.white);
        int orangeColor = getResources().getColor(R.color.orange);

        switch (FRAGMENT_PAGE) {
            case Constants.CONFIGURE_FRAGMENT:
                txtConfigureDrawer.setTextColor(orangeColor);
                txtCustomizeDrawer.setTextColor(whiteColor);
                txtDesignDrawer.setTextColor(whiteColor);
                txtFormDrawer.setTextColor(whiteColor);
                txtPreviewDrawer.setTextColor(whiteColor);

                txtConfigure.setTextColor(orangeColor);
                txtCustomize.setTextColor(whiteColor);
                txtDesign.setTextColor(whiteColor);
                txtForm.setTextColor(whiteColor);
                txtPreview.setTextColor(whiteColor);
                break;

            case Constants.DESIGN_FRAGMENT:
                txtConfigureDrawer.setTextColor(whiteColor);
                txtDesignDrawer.setTextColor(orangeColor);
                txtFormDrawer.setTextColor(whiteColor);
                txtPreviewDrawer.setTextColor(whiteColor);

                txtConfigure.setTextColor(whiteColor);
                txtDesign.setTextColor(orangeColor);
                txtForm.setTextColor(whiteColor);
                txtPreview.setTextColor(whiteColor);

                txtCustomizeDrawer.setTextColor(orangeColor);
                txtCustomize.setTextColor(orangeColor);


                break;

            case Constants.FORM_FRAGMENT:
                txtConfigureDrawer.setTextColor(whiteColor);
                txtDesignDrawer.setTextColor(whiteColor);
                txtFormDrawer.setTextColor(orangeColor);
                txtPreviewDrawer.setTextColor(whiteColor);

                txtConfigure.setTextColor(whiteColor);
                txtDesign.setTextColor(whiteColor);
                txtForm.setTextColor(orangeColor);
                txtPreview.setTextColor(whiteColor);

                txtCustomizeDrawer.setTextColor(orangeColor);
                txtCustomize.setTextColor(orangeColor);
                break;

            case Constants.PREVIEW_FRAGMENT:
                txtConfigureDrawer.setTextColor(whiteColor);
                txtDesignDrawer.setTextColor(whiteColor);
                txtFormDrawer.setTextColor(whiteColor);
                txtPreviewDrawer.setTextColor(orangeColor);

                txtConfigure.setTextColor(whiteColor);
                txtDesign.setTextColor(whiteColor);
                txtForm.setTextColor(whiteColor);
                txtPreview.setTextColor(orangeColor);

                txtCustomizeDrawer.setTextColor(orangeColor);
                txtCustomize.setTextColor(orangeColor);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        highlightSelectedPage();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Constants.STATE_FRAGMENT_PAGE, FRAGMENT_PAGE);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        FRAGMENT_PAGE = savedInstanceState.getInt(Constants.STATE_FRAGMENT_PAGE);
        if (!brandCode.isEmpty() && !campaignCode.isEmpty()) {
            /**Loading last fragment*/
            switch (FRAGMENT_PAGE) {
                case 0:
                    showBrandPage();
                    txtSubPageHeader.setText("");
                    break;

                case 1:
                    changeFragment(designFragment);
                    txtSubPageHeader.setText("Design");
                    break;

                case 2:
                    changeFragment(formFragment);
                    txtSubPageHeader.setText("Form");
                    break;

                case 3:
                    changeFragment(previewFragment);
                    txtSubPageHeader.setText("Online Customer Registration");
                    break;
            }
        } else {
            showBrandPage();
        }

        highlightSelectedPage();
    }

    public void showAlertDialog(String title, String message, String actionType) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("maintenance_alert_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        mMaintenanceAlertDialogFragment = MaintenanceAlertDialogFragment.newInstance(title, message, actionType);
        mMaintenanceAlertDialogFragment.setCancelable(false);
        mMaintenanceAlertDialogFragment.show(getFragmentManager(), "maintenance_alert_dialog");
    }

    @Override
    public void showProgressDialog(String message) {
        onShowProgressDialog(message);
    }

    @Override
    public void showColorPickerDialog(int type) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("alert_color_picker_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        alertColorPickerDialogFragment = ColorPickerAlertDialogFragment.newInstance(type);
        alertColorPickerDialogFragment.show(getFragmentManager(), "alert_color_picker_dialog");
    }

    @Override
    public void showImageAlignmentDialog(int logoType, SettingsModel settingsModel) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("alert_image_alignment_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        alertImageAlignmentDialog = AlertImageAlignmentDialogFragment.newInstance(logoType, settingsModel);
        alertImageAlignmentDialog.show(getFragmentManager(), "alert_image_alignment_dialog");
    }

    @Override
    public void setImageAlignment(SettingsModel settingsModel) {
        designFragment.setImageAlignment(settingsModel);
    }

    @Override
    public void setDesignColors(int type, int color, String hexColor) {
        designFragment.setColors(type, color, hexColor);
    }

    @Override
    public void showDesignFontsDialog(SettingsModel settingsModel) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("alert_fonts_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        alertFontsDialogFragment = FontsAlertDialogFragment.newInstance(settingsModel);
        alertFontsDialogFragment.show(getFragmentManager(), "alert_fonts_dialog");
    }

    @Override
    public void setDesignFonts(SettingsModel settingsModel) {
        designFragment.setFonts(settingsModel);
    }

    @Override
    public void showSubmitButtonDialog(SettingsModel settingsModel) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("alert_submit_button_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        alertSubmitButtonDialogFragment = SubmitButtonAlertDialogFragment.newInstance(settingsModel);
        alertSubmitButtonDialogFragment.show(getFragmentManager(), "alert_submit_button_dialog");
    }

    @Override
    public void showDesignFragmentSavePrompt(String title, String message, String actionType, SettingsModel settingsModel,
                                             boolean hasSelectedMobileLogo, boolean hasSelectedWebLogo, boolean isCopyingSettings) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("alert_submit_button_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        alertSavePromptDialogFragment = DesignSavePromptDialogFragment
                .newInstance(title, message, actionType, settingsModel, hasSelectedMobileLogo,
                        hasSelectedWebLogo, isCopyingSettings);
        alertSavePromptDialogFragment.show(getFragmentManager(), "alert_submit_button_dialog");
    }

    @Override
    public void saveDesign() {
        designFragment.saveDesign();
    }

    @Override
    public void resetDesign() {
        designFragment.resetDesign();
    }

    @Override
    public void refreshDesign() {
        designFragment.onResume();
    }

    @Override
    public void showAddFormFieldDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("add_form_field_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        mAddFormFieldDialogFragment = AddFieldDialogFragment.newInstance();
        mAddFormFieldDialogFragment.setCancelable(false);
        mAddFormFieldDialogFragment.show(getFragmentManager(), "add_form_field_dialog");
    }

    @Override
    public void stopHorizontalScrollViewScroll() {
        formFragment.stopHorizontalScrollViewScroll();
    }

    @Override
    public void updateGroup(String groupTitle, String groupName,
                            List<GroupsModel> groupNameList, ArrayAdapter<GroupsModel> groupNameAdapter,
                            GroupsModel groupsModel, List<GroupsModel> groupsModelList,
                            SubGroupingAdapter subGroupingAdapter, int position) {
        formFragment.updateGroupAPI(groupTitle, groupName, groupNameList, groupNameAdapter,
                groupsModel, groupsModelList, subGroupingAdapter, position);
    }

    @Override
    public void deleteGroup(GroupsModel groupsModel, List<GroupsModel> groupsModelList,
                            List<GroupsModel> groupNameList, ArrayAdapter<GroupsModel> groupNameAdapter,
                            SubGroupingAdapter subGroupingAdapter, int itemPosition) {
        formFragment.deleteGroupAPI(groupsModel, groupsModelList, groupNameList, groupNameAdapter, subGroupingAdapter, itemPosition);
    }

    @Override
    public void addFormGroups(GroupsModel groupsModel, EditText edtGroupName,
                              List<GroupsModel> groupNameList, ArrayAdapter<GroupsModel> groupsAdapter,
                              List<GroupsModel> groupsModelList, SubGroupingAdapter subGroupingAdapter) {
        formFragment.addGroupAPI(groupsModel, edtGroupName, groupNameList, groupsAdapter, groupsModelList, subGroupingAdapter);
    }

    @Override
    public void addFormField(FieldsModel fieldsModel, List<DataArray> dataArrayList, InputModel inputModel,
                             AssignFieldModel assignFieldModel, String actionType) {
        formFragment.postFieldToDbmForOnline(fieldsModel, dataArrayList, inputModel, assignFieldModel, actionType);
    }

    @Override
    public void showAddFormFieldAdapterDialog(FieldsModel fieldsModel) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("add_form_field_adapter_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        mAddFormFieldAdapterDialogFragment = AddFieldAdapterDialogFragment.newInstance(fieldsModel);
        mAddFormFieldAdapterDialogFragment.setCancelable(false);
        mAddFormFieldAdapterDialogFragment.show(getFragmentManager(), "add_form_field_adapter_dialog");
    }

    @Override
    public void dismissAddFormFieldAdapterDialog() {
        mAddFormFieldAdapterDialogFragment.dismiss();
    }

    @Override
    public void showSaveFormPromptDialog(String title, String message, String actionType) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("show_form_save_prompt_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        mAlertFormSavePromptDialog = FormSavePromptDialogFragment.newInstance(title, message, actionType);
        mAlertFormSavePromptDialog.setCancelable(false);
        mAlertFormSavePromptDialog.show(getFragmentManager(), "show_form_save_prompt_dialog");
    }

    @Override
    public void resetForm() {
        formFragment.resetForm();
        formFragment.saveForm();
    }

    @Override
    public void saveForm() {
        formFragment.saveForm();
    }

    @Override
    public void showAddFormFieldAdapterSavePrompt(String title, String message, EditText edtFieldName, List<DataArray> dataArrayList,
                                                  Spinner spnInputType, Spinner spnSubGroup, AddUpdateFieldSubGroupNameAdapter addUpdateFieldSubGroupNameAdapter,
                                                  EditText edtDescription, FieldsModel fieldsModel, CheckBox chRequired) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("add_form_field_adapter_prompt_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        mAddFormFieldAdapterSavePromptDialog = AddFormFieldAdapterSavePromptDialog.newInstance(title, message,
                edtFieldName, dataArrayList, spnInputType, spnSubGroup, addUpdateFieldSubGroupNameAdapter,
                edtDescription, fieldsModel, chRequired);
        mAddFormFieldAdapterSavePromptDialog.setCancelable(false);
        mAddFormFieldAdapterSavePromptDialog.show(getFragmentManager(), "add_form_field_adapter_prompt_dialog");
    }

    @Override
    public void showAddFormFieldSavePrompt(String title, String message, List<DataArray> dataArrayList,
                                           InputModel inputModel, AssignFieldModel assignFieldModel,
                                           FieldsModel fieldsModel) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("add_form_field_prompt_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        mAddFormFieldSavePromptDialog = AddFormFieldSavePromptDialog.newInstance(title, message,
                dataArrayList, inputModel, assignFieldModel, fieldsModel);
        mAddFormFieldSavePromptDialog.setCancelable(false);
        mAddFormFieldSavePromptDialog.show(getFragmentManager(), "add_form_field_prompt_dialog");
    }

    @Override
    public void setHorizontalScrollViewPosition(int position) {
        formFragment.setHorizontalScrollViewPosition(position);
    }

    @Override
    public void hideKeyboardInFragment(Context context, IBinder windowToken) {
        Utils.hideKeyboardInFragment(context, windowToken);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, DashboardActivity.class));
        finish();
    }

    @Override
    public void refreshRegistrationForm() {
        previewFragment = new PreviewFragment();
        changeFragment(previewFragment);
    }

    @Override
    public void showRegistrationAlertDialog(String title, String message, String actionType, String result, SettingsModel settingsModel) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("alert_registration_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        mAlertRegistrationDialog = AlertRegistrationDialogFragment.newInstance(title, message, actionType, result, settingsModel);
        mAlertRegistrationDialog.show(getFragmentManager(), "alert_registration_dialog");
    }

    @Override
    public void showRegistrationUpdateAndSavePromptDialog(String title, String message, String actionType, SettingsModel settingsModel,
                                                          RegistrationModel registrationModel) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("alert_registration_update_save_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        mAlertRegistrationUpdateSaveDialog = RegistrationUpdateAndSavePromptDialogFragment.newInstance(title, message, actionType, settingsModel, registrationModel);
        mAlertRegistrationUpdateSaveDialog.setCancelable(false);
        mAlertRegistrationUpdateSaveDialog.show(getFragmentManager(), "alert_registration_update_save_dialog");
    }

    @Override
    public void updateExistingRegistrationData(RegistrationModel registrationModel) {
        previewFragment.updateExistingRegistrationData(registrationModel);
    }

    @Override
    public void promptExistingRegistrationDataDetected(RegistrationModel registrationModel) {
        previewFragment.promptExistingRegistrationDataDetected(registrationModel);
    }

    @Override
    public void saveRegistrationDetails(RegistrationModel registrationModel) {
        previewFragment.saveRegistrationDetails(registrationModel);
    }

    @Override
    public void showRegistrationAgreementDialog(SettingsModel settingsModel) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("alert_registration_agreement_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        mAlertAgreementPromptDialog = RegistrationAgreementDialogFragment.newInstance(settingsModel);
        mAlertAgreementPromptDialog.show(getFragmentManager(), "alert_registration_agreement_dialog");
        mAlertAgreementPromptDialog.setCancelable(false);
    }

    @Override
    public void setSubscriptionStatus(boolean status) {
        previewFragment.setSubscriptionStatus(status);
    }
}
