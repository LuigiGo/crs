package com.unilab.customerregistration.features.dashboard.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.base.view.BaseActivity;
import com.unilab.customerregistration.features.dashboard.presenter.DashboardContract;
import com.unilab.customerregistration.features.dashboard.model.CountryModel;
import com.unilab.customerregistration.features.dashboard.model.api.DashboardApiProvider;
import com.unilab.customerregistration.features.dashboard.model.asynctask.AsyncFetchCounty;
import com.unilab.customerregistration.features.login.view.LoginActivity;
import com.unilab.customerregistration.features.maintenance.model.DefaultSettingModel;
import com.unilab.customerregistration.features.registration.presenter.RegistrationContract;
import com.unilab.customerregistration.features.registration.view.ActivityRegistration;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.constants.DatabaseConstants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.network.NetworkUtils;
import com.unilab.customerregistration.utilities.network.RestManager;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends BaseActivity implements View.OnClickListener, RegistrationContract, DashboardContract {

    private final String TAG = this.getClass().getSimpleName();
    private Button btnRegister, btnMaitenance, btnLogout;
    private String brandCode, campaignCode, eventCode;
    private DatabaseHelper mDatabaseHelper;
    private ImageView imgBackground;
    boolean hasPreloadedData;

    public static boolean isActivityIsRunning = false;

    /**
     * Broadcast Receiver
     */
    private IntentFilter mIntentFilter;
    private BroadcastReceiver mBroadcastReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        mDatabaseHelper = new DatabaseHelper(this);
        SharedPrefHelper.init(this);
        RestManager.init();
        DialogHelper.init(this);

        Log.e(TAG, "onCreate: " + Constants.API_WEB_BASE_URL3);

        hasPreloadedData = SharedPrefHelper.getInstance().getPreLoadedDataStatusFromShared();

        setupViews();
        setupBroadcastReceiver();
        getPreloadedData();

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

    }

    private void setupViews() {
        imgBackground = (ImageView) findViewById(R.id.imgBackground);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnMaitenance = (Button) findViewById(R.id.btnMaintenance);
        btnLogout = (Button) findViewById(R.id.btnLogout);

        Utils.loadImageFromDrawable(this, imgBackground, R.drawable.bg_crs);

        /**Listeners*/
        btnRegister.setOnClickListener(this);
        btnMaitenance.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
    }

    private void setupBroadcastReceiver() {
        mIntentFilter = new IntentFilter(Constants.RECEIVER_SHOW_REG_UPDATES);
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case Constants.RECEIVER_SHOW_REG_UPDATES:
                        onShowRegUpdateDialog(intent);
                        break;
                }
            }
        };
        registerReceiver(mBroadcastReceiver, mIntentFilter);
    }

    private void getPreloadedData() {
        Log.e(TAG, "getPreloadedData: " + hasPreloadedData);

        if (!hasPreloadedData) {
            if (NetworkUtils.isNetworkConnected(this)) {
                mDatabaseHelper.deletePreloadedData(DatabaseConstants.TBL_COUNTRY);
                mDatabaseHelper.deletePreloadedData(DatabaseConstants.TBL_REGION);
                mDatabaseHelper.deletePreloadedData(DatabaseConstants.TBL_PROVINCE);
                mDatabaseHelper.deletePreloadedData(DatabaseConstants.TBL_CITY);
                getCountryAPI();
            } else {
                Toast.makeText(this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getCountryAPI() {
        Log.e(TAG, "getCountryAPI: ");
        DialogHelper.getInstance().hideProgressDialog(DashboardActivity.this);
        DialogHelper.getInstance().showProgressDialog(this, "Synchronizing data");

        Call<List<CountryModel>> getCountryCall = DashboardApiProvider.getDashboardApiRoutes()
                .getCountry(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_GET_COUNTRY);
        getCountryCall.enqueue(new Callback<List<CountryModel>>() {
            @Override
            public void onResponse(Call<List<CountryModel>> call, Response<List<CountryModel>> response) {
                Log.e(TAG, "onResponse: " + response.code());
                if (response.body() != null) {
                    List<CountryModel> countryModelList = response.body();
                    new AsyncFetchCounty(DashboardActivity.this, countryModelList).execute();

                } else {
                    DialogHelper.getInstance().hideProgressDialog(DashboardActivity.this);
                }
            }

            @Override
            public void onFailure(Call<List<CountryModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                showAlertInfoDialog("Alert", getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(DashboardActivity.this);
            }
        });
    }

    @Override
    public void onClick(View v) {
        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        switch (v.getId()) {
            case R.id.btnRegister:
                Log.e(TAG, "onClick: BrandCode: " + brandCode + " CampaignCode: " + campaignCode);
                hasPreloadedData = SharedPrefHelper.getInstance().getPreLoadedDataStatusFromShared();
                if (!hasPreloadedData) {
                    getPreloadedData();

                } else {
                    if (brandCode.isEmpty() || campaignCode.isEmpty()) {
                        startActivity(new Intent(this, LoginActivity.class).setAction("register"));
                    } else {
                        startActivity(new Intent(this, ActivityRegistration.class));
                    }
                    finish();
                }
                break;

            case R.id.btnMaintenance:
                Log.e(TAG, "onClick: BrandCode: " + brandCode + " CampaignCode: " + campaignCode);
                hasPreloadedData = SharedPrefHelper.getInstance().getPreLoadedDataStatusFromShared();
                if (!hasPreloadedData) {
                    getPreloadedData();
                } else {
                    startActivity(new Intent(this, LoginActivity.class).setAction("maintenance"));
                    finish();
                }
                break;

            case R.id.btnLogout:
                SharedPrefHelper.getInstance().putBrandCodeToShared("");
                SharedPrefHelper.getInstance().putCampaignCodeToShared("");
                SharedPrefHelper.getInstance().putEventCodeToShared("");
                SharedPrefHelper.getInstance().putBrandNameToShared("");
                SharedPrefHelper.getInstance().putCampaignNameToShared("");
                SharedPrefHelper.getInstance().putEventNameToShared("");
                SharedPrefHelper.getInstance().putEmailToShared("");
                SharedPrefHelper.getInstance().putPasswordToShared("");
                SharedPrefHelper.getInstance().putGeneratedUrlToShared("");
                btnLogout.setVisibility(View.GONE);
                break;


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityIsRunning = true;

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();

        String email = SharedPrefHelper.getInstance().getEmailFromShared();
        String password = SharedPrefHelper.getInstance().getPasswordFromShared();
        if (!email.isEmpty() || !password.isEmpty()) {
            btnLogout.setVisibility(View.VISIBLE);
        } else {
            btnLogout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActivityIsRunning = false;
    }

    @Override
    public void refreshRegistrationForm() {
        Log.e(TAG, "refreshRegistrationForm: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void showAlertInfoDialog(String title, String message, String actionType) {
        onShowAlertDialog(title, message, actionType);
    }

}
