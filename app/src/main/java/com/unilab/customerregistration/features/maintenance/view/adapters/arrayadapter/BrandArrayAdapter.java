package com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.BrandModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BrandArrayAdapter extends ArrayAdapter<BrandModel> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private List<BrandModel> mBrandModelList;
    private MaintenancePresenter mMaintenancePresenter;
    private boolean isInitial = true;

    public BrandArrayAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<BrandModel> brandModelList) {
        super(context, resource, brandModelList);
        SharedPrefHelper.init(context);

        mContext = context;
        mBrandModelList = brandModelList;
        mMaintenancePresenter = (MaintenancePresenter) context;

    }

    @Override
    public int getCount() {
        return mBrandModelList.size();
    }

    @Nullable
    @Override
    public BrandModel getItem(int position) {
        return mBrandModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final BrandModel brandModel = mBrandModelList.get(position);
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_spinner_default, parent, false);
        TextView label = (TextView) v.findViewById(R.id.txtLabel);
        label.setText(brandModel.getName());

        if (!isInitial) {
            Log.e(TAG, "getView: BrandName: " + brandModel.getName() + " BrandCode: " + brandModel.getBrandCode());
            if (!brandModel.getName().equals("--Please select your brand--")) {
                mMaintenancePresenter.loadCampaignList(brandModel.getBrandCode(), brandModel.getName());
            }
            isInitial = true;
        }

        return v;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final BrandModel brandModel = mBrandModelList.get(position);
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_spinner_dropdown, parent, false);
        TextView label = (TextView) v.findViewById(R.id.txtLabel);

        String brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        if (brandCode.equals(brandModel.getBrandCode())) {
            label.setBackgroundColor(mContext.getResources().getColor(R.color.light_blue));
        }

//        if (position != 0) {
        label.setText(brandModel.getName());
//        } else {
//            label.setVisibility(View.GONE);
//        }
        isInitial = false;
        return v;
    }

    @Override
    public boolean isEnabled(int position) {
        if (position == 0) {
            return false;
        } else {
            return true;
        }
    }
}
