package com.unilab.customerregistration.features.maintenance.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupsModel implements Parcelable{


    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("brand_code")
    @Expose
    private String brandCode;
    @SerializedName("campaign_code")
    @Expose
    private String campaignCode;
    @SerializedName("campaign_event")
    @Expose
    private String eventCode;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("date_time")
    @Expose
    private String dateTime;

    private String status;

    public GroupsModel() {
    }

    protected GroupsModel(Parcel in) {
        id = in.readString();
        title = in.readString();
        name = in.readString();
        description = in.readString();
        brandCode = in.readString();
        campaignCode = in.readString();
        eventCode = in.readString();
        userId = in.readString();
        isActive = in.readString();
        dateTime = in.readString();
        status = in.readString();
    }

    public static final Creator<GroupsModel> CREATOR = new Creator<GroupsModel>() {
        @Override
        public GroupsModel createFromParcel(Parcel in) {
            return new GroupsModel(in);
        }

        @Override
        public GroupsModel[] newArray(int size) {
            return new GroupsModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "GroupsModel{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", brandCode='" + brandCode + '\'' +
                ", campaignCode='" + campaignCode + '\'' +
                ", eventCode='" + eventCode + '\'' +
                ", userId='" + userId + '\'' +
                ", isActive='" + isActive + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(brandCode);
        parcel.writeString(campaignCode);
        parcel.writeString(eventCode);
        parcel.writeString(userId);
        parcel.writeString(isActive);
        parcel.writeString(dateTime);
        parcel.writeString(status);
    }
}
