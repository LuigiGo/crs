package com.unilab.customerregistration.features.dashboard.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProvinceModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("province_id")
    @Expose
    private String provinceId;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("province_code")
    @Expose
    private String provinceCode;
    @SerializedName("province_name")
    @Expose
    private String provinceName;
    @SerializedName("region_id")
    @Expose
    private String regionId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }
}
