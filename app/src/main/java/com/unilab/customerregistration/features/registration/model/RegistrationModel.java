package com.unilab.customerregistration.features.registration.model;

import android.os.Parcel;
import android.os.Parcelable;

public class RegistrationModel implements Parcelable {

    private String name, value;
    private String id, email, brandCode, campaignCode, data, status, dateTime, eventCode, actionType, eventName;


    public RegistrationModel() {

    }

    protected RegistrationModel(Parcel in) {
        name = in.readString();
        value = in.readString();
        id = in.readString();
        email = in.readString();
        brandCode = in.readString();
        campaignCode = in.readString();
        data = in.readString();
        status = in.readString();
        dateTime = in.readString();
        eventCode = in.readString();
        actionType = in.readString();
        eventName = in.readString();
    }

    public static final Creator<RegistrationModel> CREATOR = new Creator<RegistrationModel>() {
        @Override
        public RegistrationModel createFromParcel(Parcel in) {
            return new RegistrationModel(in);
        }

        @Override
        public RegistrationModel[] newArray(int size) {
            return new RegistrationModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(value);
        dest.writeString(id);
        dest.writeString(email);
        dest.writeString(brandCode);
        dest.writeString(campaignCode);
        dest.writeString(data);
        dest.writeString(status);
        dest.writeString(dateTime);
        dest.writeString(eventCode);
        dest.writeString(actionType);
        dest.writeString(eventName);
    }
}
