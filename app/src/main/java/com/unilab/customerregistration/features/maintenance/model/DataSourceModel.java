package com.unilab.customerregistration.features.maintenance.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataSourceModel {

    @SerializedName("campaign_id")
    @Expose
    private String campaignId;
    @SerializedName("initiative_title")
    @Expose
    private String initiativeTitle;
    @SerializedName("title")
    @Expose
    private String title;

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getInitiativeTitle() {
        return initiativeTitle;
    }

    public void setInitiativeTitle(String initiativeTitle) {
        this.initiativeTitle = initiativeTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
