package com.unilab.customerregistration.features.maintenance.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.unilab.customerregistration.features.maintenance.model.AssignFieldModel;
import com.unilab.customerregistration.features.maintenance.model.InputModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.util.List;

public class AsyncTaskFetchDefaultFields extends AsyncTask<Void, Void, Void> {

    private final String TAG = this.getClass().getSimpleName();
    private DatabaseHelper mDatabaseHelper;
    private MaintenancePresenter maintenancePresenter;

    public AsyncTaskFetchDefaultFields(Context context) {
        Log.e(TAG, "AsyncTaskFetchDefaultFields: ");
        mDatabaseHelper = new DatabaseHelper(context);
        SharedPrefHelper.init(context);

        maintenancePresenter = (MaintenancePresenter) context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        String campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        String eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        Log.e(TAG, "doInBackground: " + brandCode + " " + campaignCode + " " + eventCode);
        List<InputModel> inputModelList = mDatabaseHelper.getDefaultFields();
        for (InputModel inputModel : inputModelList) {
            if (mDatabaseHelper.checkDefaultFieldIfAssigned(brandCode, campaignCode, eventCode, inputModel.getId())) {
                Log.e(TAG, "doInBackground: " + "Field Assigned");

            } else {
                Log.e(TAG, "doInBackground: " + "Field not assigned");
                Log.e(TAG, "doInBackground: isMandatory: " + inputModel.getIsMandatory() + " name: " + inputModel.getName());
                if (inputModel.getIsActive().equals("1")) {
                    int maxInputAssignedId = mDatabaseHelper.getMaxAssignedFieldId();
                    maxInputAssignedId++;
                    AssignFieldModel assignFieldModel = new AssignFieldModel();
                    assignFieldModel.setId(String.valueOf(maxInputAssignedId));
                    assignFieldModel.setCampaignCode(campaignCode);
                    assignFieldModel.setBrandCode(brandCode);
                    assignFieldModel.setEventCode(eventCode);
                    assignFieldModel.setInputId(inputModel.getId());
                    assignFieldModel.setUserId("");
                    assignFieldModel.setIsActive((inputModel.getIsMandatory().equals("1") ? "1" : "0"));
                    assignFieldModel.setDateTime(Utils.getDateTime());
                    assignFieldModel.setSort(Integer.parseInt(inputModel.getId()));
                    assignFieldModel.setStatus(inputModel.getStatus());
                    mDatabaseHelper.assignField(assignFieldModel);
                }
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
//        maintenancePresenter.showFormPage();
    }
}
