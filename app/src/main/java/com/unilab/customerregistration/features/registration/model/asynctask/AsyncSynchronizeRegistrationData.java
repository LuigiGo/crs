package com.unilab.customerregistration.features.registration.model.asynctask;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.unilab.customerregistration.features.dashboard.view.DashboardActivity;
import com.unilab.customerregistration.features.login.view.LoginActivity;
import com.unilab.customerregistration.features.maintenance.view.MaintenanceActivity;
import com.unilab.customerregistration.features.registration.model.RegistrationModel;
import com.unilab.customerregistration.features.registration.view.ActivityRegistration;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.notification.NotificationHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AsyncSynchronizeRegistrationData extends AsyncTask<Void, Void, String> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private String brandCode, campaignCode, eventCode, formattedEventName;
    private String message, totalRecords = "0", newRegistered = "0", duplicateRecords = "0";
    private List<RegistrationModel> registrationModelDataList, mRegistrationModelList;
    private DatabaseHelper mDatabaseHelper;
    private Type listType;
    private int totalNoOfUpdates = 0;
    private NotificationHelper mNotificationHelper;

    public AsyncSynchronizeRegistrationData(Context context, List<RegistrationModel> registrationModelList) {
        Log.e(TAG, "AsyncSendRegistration: ");
        mDatabaseHelper = new DatabaseHelper(context);
        mNotificationHelper = new NotificationHelper(context);
        SharedPrefHelper.init(context);
        DialogHelper.init(context);

        mContext = context;
        mRegistrationModelList = registrationModelList;

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();
        formattedEventName = SharedPrefHelper.getInstance().getFormattedCampaignNameFromShared();
        Log.e(TAG, "AsyncSendRegistration: " + campaignCode + " " + brandCode + " " + eventCode);

        listType = new TypeToken<List<RegistrationModel>>() {
        }.getType();

        totalNoOfUpdates = mRegistrationModelList.size();
        totalRecords = String.valueOf(totalNoOfUpdates);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {
        /**Note: Code below will combine all registration details into one list*/
        List<HashMap<String, String>> registrationDetailsList = new ArrayList<>();
        for (RegistrationModel registrationModel : mRegistrationModelList) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("brand_id", registrationModel.getBrandCode());
            hm.put("campaign_code", registrationModel.getCampaignCode());
            hm.put("campaign_event_id", registrationModel.getEventCode());
            hm.put("campaign_name", registrationModel.getEventName());

            registrationModelDataList = new Gson().fromJson(registrationModel.getData(), listType);
            for (int i = 0; i < registrationModelDataList.size(); i++) {
                final RegistrationModel registrationModelData = registrationModelDataList.get(i);
                Log.e(TAG, "postData: " + registrationModelData.getName() + " " + registrationModelData.getValue());
                hm.put(registrationModelData.getName(), registrationModelData.getValue());
            }
            registrationDetailsList.add(hm);
        }

        /**Note: Converting registration list into json format and convert it into string as whole data*/
        Gson gson = new Gson();
        String data = gson.toJson(registrationDetailsList);
        Log.e(TAG, "AsyncSynchronizeRegistrationData: " + data);
        if (totalNoOfUpdates > 0) {
            String response = postData(data);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String result = jsonObject.getString("result");
                    Log.e(TAG, "doInBackground: " + jsonObject.toString(4));
                    Log.e(TAG, "doInBackground: ResultCode: " + result);

                    switch (result) {
                        case "0":
                            message = response;
                            break;

                        case "1":
                            Log.e(TAG, "doInBackground: Message: " + jsonObject.getString("message"));
                            message = jsonObject.getString("message");
                            totalRecords = jsonObject.getString("total records");
                            newRegistered = jsonObject.getString("new_registration");
                            duplicateRecords = jsonObject.getString("duplicate");

                            /**Changing status*/
                            for (RegistrationModel registrationModel : mRegistrationModelList) {
                                mDatabaseHelper.updateRegistration(registrationModel, brandCode, campaignCode, eventCode, "2");
                            }
                            break;

                        default:
                            message = response;
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, "doInBackground: " + e.getMessage());
                }
            }
        }

        return null;
    }

    private String postData(String data) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Constants.API_DBM_BASE_URL.concat("webapp"));

        try {
            List fieldList = new ArrayList();
            fieldList.add(new BasicNameValuePair("token", Constants.API_DBM_TOKEN));
            fieldList.add(new BasicNameValuePair("cmdEvent", Constants.CMD_SENDING_BULK_REGISTRATION_DETAILS));
            fieldList.add(new BasicNameValuePair("host", Constants.DBM_HOSTNAME));
            fieldList.add(new BasicNameValuePair("data", data));

            httppost.setEntity(new UrlEncodedFormEntity(fieldList));
            HttpResponse response = httpclient.execute(httppost);
            return EntityUtils.toString(response.getEntity());
        } catch (ClientProtocolException e) {
            Log.e(TAG, "postData: " + e.getMessage());
            return null;

        } catch (IOException e) {
            Log.e(TAG, "postData: " + e.getMessage());
            return null;
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        Log.e(TAG, "onPostExecute: DashboardActivity: " + DashboardActivity.isActivityIsRunning);
        Log.e(TAG, "onPostExecute: LoginActivity: " + LoginActivity.isActivityIsRunning);
        Log.e(TAG, "onPostExecute: MaintenanceActivity: " + MaintenanceActivity.isActivityIsRunning);
        Log.e(TAG, "onPostExecute: ActivityRegistration: " + ActivityRegistration.isActivityIsRunning);

        if (totalNoOfUpdates > 0) {
            if (DashboardActivity.isActivityIsRunning || LoginActivity.isActivityIsRunning
                    || MaintenanceActivity.isActivityIsRunning || ActivityRegistration.isActivityIsRunning) {
                mContext.sendBroadcast(new Intent(Constants.RECEIVER_SHOW_REG_UPDATES)
                        .putExtra("message", message)
                        .putExtra("totalRecords", totalRecords)
                        .putExtra("newRegistered", newRegistered)
                        .putExtra("duplicateRecords", duplicateRecords));

            } else {
                message = message + "\nTotal records: " + totalRecords + " | New Records: " + newRegistered + " | Duplicate Records: " + duplicateRecords;
                mNotificationHelper.showNotification(message);
            }
        }
    }
}
