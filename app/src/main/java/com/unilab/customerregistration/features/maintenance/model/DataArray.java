package com.unilab.customerregistration.features.maintenance.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataArray implements Parcelable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("input_id")
    @Expose
    private String inputId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("date_time")
    @Expose
    private String dateTime;
    @SerializedName("is_parent")
    @Expose
    private String isParent;
    @SerializedName("is_secondary")
    @Expose
    private String isSecondary;
    @SerializedName("child")
    @Expose
    private String child;

    private String sort;

    public DataArray() {
    }

    protected DataArray(Parcel in) {
        id = in.readString();
        inputId = in.readString();
        title = in.readString();
        name = in.readString();
        value = in.readString();
        isActive = in.readString();
        dateTime = in.readString();
        isParent = in.readString();
        isSecondary = in.readString();
        child = in.readString();
        sort = in.readString();
    }

    public static final Creator<DataArray> CREATOR = new Creator<DataArray>() {
        @Override
        public DataArray createFromParcel(Parcel in) {
            return new DataArray(in);
        }

        @Override
        public DataArray[] newArray(int size) {
            return new DataArray[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getIsParent() {
        return isParent;
    }

    public void setIsParent(String isParent) {
        this.isParent = isParent;
    }

    public String getIsSecondary() {
        return isSecondary;
    }

    public void setIsSecondary(String isSecondary) {
        this.isSecondary = isSecondary;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "DataArray{" +
                "id='" + id + '\'' +
                ", inputId='" + inputId + '\'' +
                ", title='" + title + '\'' +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", isActive='" + isActive + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", isParent='" + isParent + '\'' +
                ", isSecondary='" + isSecondary + '\'' +
                ", child='" + child + '\'' +
                ", sort='" + sort + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(inputId);
        dest.writeString(title);
        dest.writeString(name);
        dest.writeString(value);
        dest.writeString(isActive);
        dest.writeString(dateTime);
        dest.writeString(isParent);
        dest.writeString(isSecondary);
        dest.writeString(child);
        dest.writeString(sort);
    }
}