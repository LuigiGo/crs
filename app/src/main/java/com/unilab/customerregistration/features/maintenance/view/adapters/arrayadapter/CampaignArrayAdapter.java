package com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.CampaignModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;

import java.util.List;

public class CampaignArrayAdapter extends ArrayAdapter<CampaignModel> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private List<CampaignModel> mCampaignModelList;
    private MaintenancePresenter mMaintenancePresenter;
    private boolean isInitial = true;

    public CampaignArrayAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<CampaignModel> campaignModelList) {
        super(context, resource, campaignModelList);
        SharedPrefHelper.init(context);

        mContext = context;
        mCampaignModelList = campaignModelList;
        mMaintenancePresenter = (MaintenancePresenter) context;
    }

    @Override
    public int getCount() {
        return mCampaignModelList.size();
    }

    @Nullable
    @Override
    public CampaignModel getItem(int position) {
        return mCampaignModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final CampaignModel campaignModel = mCampaignModelList.get(position);
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_spinner_default, parent, false);
        TextView label = (TextView) v.findViewById(R.id.txtLabel);
        label.setText(campaignModel.getName());

        if (!isInitial) {
            Log.e(TAG, "getView: CampaignName: " + campaignModel.getName() + " CampaignCode: " + campaignModel.getCode());
            if (!campaignModel.getName().equals("--Please select your campaign--")) {
                mMaintenancePresenter.loadCampaignEventList(campaignModel.getCode(), campaignModel.getName());
            }
            isInitial = true;
        }

        return v;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final CampaignModel campaignModel = mCampaignModelList.get(position);
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_spinner_dropdown, parent, false);
        TextView label = (TextView) v.findViewById(R.id.txtLabel);
        label.setText(campaignModel.getName());

        String campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        if (campaignCode.equals(campaignModel.getCode())) {
            label.setBackgroundColor(mContext.getResources().getColor(R.color.light_blue));
        }

//        if (position != 0) {
        label.setText(campaignModel.getName());
//        } else {
//            label.setVisibility(View.GONE);
//        }
        isInitial = false;
        return v;
    }

    @Override
    public boolean isEnabled(int position) {
        if (position == 0) {
            return false;
        } else {
            return true;
        }
    }
}
