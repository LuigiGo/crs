package com.unilab.customerregistration.features.maintenance.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.constants.DatabaseConstants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AsyncSendAddedOfflineField extends AsyncTask<Void, Void, String> {

    private final String TAG = this.getClass().getSimpleName();
    private DatabaseHelper mDatabaseHelper;
    private Context mContext;
    private FieldsModel mFieldsModel;
    private String brandCode, campaignCode, eventCode, eventName, formattedEventName;

    public AsyncSendAddedOfflineField(Context context, FieldsModel fieldsModel) {
        Log.e(TAG, "AsyncUpdateFieldTable: " + fieldsModel.toString());
        mDatabaseHelper = new DatabaseHelper(context);
        SharedPrefHelper.init(context);

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();
        eventName = SharedPrefHelper.getInstance().getEventNameFromShared();
        formattedEventName = SharedPrefHelper.getInstance().getFormattedCampaignNameFromShared();

        mContext = context;
        mFieldsModel = fieldsModel;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {
        return postData();
    }

    private String postData() {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Constants.API_WEB_BASE_URL3.concat("api/"));
        try {
            List fieldList = new ArrayList();
            fieldList.add(new BasicNameValuePair("token", Constants.API_WEB_TOKEN));
            fieldList.add(new BasicNameValuePair("cmdEvent", Constants.CMD_EVENT_POST_INPUT));
            fieldList.add(new BasicNameValuePair("campaign_code", mFieldsModel.getCampaignCode()));
            fieldList.add(new BasicNameValuePair("brand_code", mFieldsModel.getBrandCode()));
            fieldList.add(new BasicNameValuePair("campaign_event", mFieldsModel.getEventCode()));
            fieldList.add(new BasicNameValuePair("campaign_name", formattedEventName));
            fieldList.add(new BasicNameValuePair("type_id", mFieldsModel.getTypeId()));
            fieldList.add(new BasicNameValuePair("title", mFieldsModel.getTitle()));
            fieldList.add(new BasicNameValuePair("name", mFieldsModel.getName()));
            fieldList.add(new BasicNameValuePair("description", mFieldsModel.getDescription()));
            fieldList.add(new BasicNameValuePair("group_id", mFieldsModel.getGroupId()));
            fieldList.add(new BasicNameValuePair("is_required", mFieldsModel.getIsRequired()));
            fieldList.add(new BasicNameValuePair("is_active", mFieldsModel.getB_isActive()));
            fieldList.add(new BasicNameValuePair("date_time", mFieldsModel.getA_dateTime()));
            fieldList.add(new BasicNameValuePair("category", mFieldsModel.getCategory()));
            fieldList.add(new BasicNameValuePair("sort", String.valueOf(mFieldsModel.getSort())));
            fieldList.add(new BasicNameValuePair("assign_is_active", mFieldsModel.getA_isActive()));
            fieldList.add(new BasicNameValuePair("isCopy", "0"));

            if (mFieldsModel.getDataArrayList().size() > 0) {
                for (int i = 0; i < mFieldsModel.getDataArrayList().size(); i++) {
                    final DataArray dataArray = mFieldsModel.getDataArrayList().get(i);
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][title]", dataArray.getTitle()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][name]", dataArray.getName()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][value]", dataArray.getValue()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][is_active]", dataArray.getIsActive()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][date_time]", dataArray.getDateTime()));
                }
            } else {
                fieldList.add(new BasicNameValuePair("data_array[]", ""));
            }

            httppost.setEntity(new UrlEncodedFormEntity(fieldList));
            HttpResponse response = httpclient.execute(httppost);
            return EntityUtils.toString(response.getEntity());
        } catch (ClientProtocolException e) {
            Log.e(TAG, "postData: " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e(TAG, "postData: " + e.getMessage());
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result != null) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String status = jsonObject.getString("status");
                switch (status) {
                    case "success":
                        String id = jsonObject.getString("id");
                        String oldFieldId = mFieldsModel.getB_id();
                        mFieldsModel.setB_id(id);
                        mFieldsModel.setA_status("0");
                        mFieldsModel.setB_status("0");
                        mDatabaseHelper.updateExistingFieldId(mFieldsModel, oldFieldId);
                        mDatabaseHelper.updateExistingAssignFieldId(mFieldsModel, oldFieldId, brandCode, campaignCode, eventCode);
                        mDatabaseHelper.updateExistingDataArrayFieldId(mFieldsModel, oldFieldId);
                        break;

                    case "failed":
                        String message = jsonObject.getString("msg");
                        Log.e(TAG, "onPostExecute: " + message);

                        if (message.equals("Custom field exceeded the maximum limit.")) {
                            mDatabaseHelper.deleteFieldByFieldId(mFieldsModel);
                            mDatabaseHelper.deleteAssignByFieldId(mFieldsModel);
                            mDatabaseHelper.deleteDataArrayByInputId(mFieldsModel.getA_id(), DatabaseConstants.TBL_TEXTBOX);
                            mDatabaseHelper.deleteDataArrayByInputId(mFieldsModel.getA_id(), DatabaseConstants.TBL_RADIO);
                            mDatabaseHelper.deleteDataArrayByInputId(mFieldsModel.getA_id(), DatabaseConstants.TBL_SELECT);
                        }
                        break;

                    default:
                        Log.e(TAG, "onPostExecute: " + "Added offline field failed");
                        break;
                }
            } catch (JSONException e) {
                Log.e(TAG, "onPostExecute: " + e.getMessage());
                Log.e(TAG, "onPostExecute: " + "Added offline field failed");
            }
        }
    }
}
