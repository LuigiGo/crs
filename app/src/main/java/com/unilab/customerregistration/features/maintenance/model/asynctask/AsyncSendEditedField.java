package com.unilab.customerregistration.features.maintenance.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.InputModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.constants.DatabaseConstants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AsyncSendEditedField extends AsyncTask<Void, Void, String> {

    private final String TAG = this.getClass().getSimpleName();
    private DatabaseHelper mDatabaseHelper;
    private Context mContext;
    private FieldsModel mFieldsModel;
    private String brandCode, campaignCode, eventCode, eventName, formattedEventName;
    private MaintenancePresenter mMaintenancePresenter;
    boolean mSynchronization = false, mIsCopy = false;

    public AsyncSendEditedField(Context context, FieldsModel fieldsModel, boolean synchronization, boolean isCopy) {
        mDatabaseHelper = new DatabaseHelper(context);
        SharedPrefHelper.init(context);
        DialogHelper.init(context);

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();
//        eventName = SharedPrefHelper.getInstance().getEventNameFromShared();
        formattedEventName = SharedPrefHelper.getInstance().getFormattedCampaignNameFromShared();

        mContext = context;
        mFieldsModel = fieldsModel;
        mMaintenancePresenter = (MaintenancePresenter) context;
        mSynchronization = synchronization;
        mIsCopy = isCopy;
        Log.e(TAG, "AsyncSendEditedField: " + brandCode + "-" + campaignCode + "-" + eventCode);
        Log.e(TAG, "AsyncSendEditedField: " + mFieldsModel.toString());
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (!mSynchronization) {
//            DialogHelper.getInstance().hideProgressDialog();
//            DialogHelper.getInstance().showProgressDialog(mContext, "Updating fields");
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        return postData();
    }

    private String postData() {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Constants.API_WEB_BASE_URL3.concat("api/"));
        try {
            List fieldList = new ArrayList();
            fieldList.add(new BasicNameValuePair("token", Constants.API_WEB_TOKEN));
            fieldList.add(new BasicNameValuePair("cmdEvent", Constants.CMD_EVENT_UPDATE_INPUT));
            fieldList.add(new BasicNameValuePair("input_id", mFieldsModel.getB_id()));
            fieldList.add(new BasicNameValuePair("campaign_code", mFieldsModel.getCampaignCode()));
            fieldList.add(new BasicNameValuePair("brand_code", mFieldsModel.getBrandCode()));
            fieldList.add(new BasicNameValuePair("campaign_event", mFieldsModel.getEventCode()));
            fieldList.add(new BasicNameValuePair("campaign_name", formattedEventName));
            fieldList.add(new BasicNameValuePair("type_id", mFieldsModel.getTypeId()));
            fieldList.add(new BasicNameValuePair("title", mFieldsModel.getTitle()));
            fieldList.add(new BasicNameValuePair("name", mFieldsModel.getName()));
            fieldList.add(new BasicNameValuePair("description", mFieldsModel.getDescription()));
            fieldList.add(new BasicNameValuePair("group_id", mFieldsModel.getGroupId()));
            fieldList.add(new BasicNameValuePair("is_required", mFieldsModel.getIsRequired()));
            fieldList.add(new BasicNameValuePair("is_active", mFieldsModel.getB_isActive()));
            fieldList.add(new BasicNameValuePair("date_time", mFieldsModel.getA_dateTime()));
            fieldList.add(new BasicNameValuePair("category", mFieldsModel.getCategory()));
            fieldList.add(new BasicNameValuePair("sort", String.valueOf(mFieldsModel.getSort())));
            fieldList.add(new BasicNameValuePair("assign_is_active", mFieldsModel.getA_isActive()));
            fieldList.add(new BasicNameValuePair("isCopy", "0"));

            Log.e(TAG, "postData: DataArraySize: " + mFieldsModel.getDataArrayList().size());
            if (mFieldsModel.getDataArrayList() != null) {
                for (int i = 0; i < mFieldsModel.getDataArrayList().size(); i++) {
                    final DataArray dataArray = mFieldsModel.getDataArrayList().get(i);

                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][title]", dataArray.getTitle()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][name]", dataArray.getName()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][value]", dataArray.getValue()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][is_active]", dataArray.getIsActive()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][date_time]", dataArray.getDateTime()));
                    Log.e(TAG, "postData: " + dataArray.toString());
                }
            } else {
                fieldList.add(new BasicNameValuePair("data_array[]", ""));
            }

            httppost.setEntity(new UrlEncodedFormEntity(fieldList));
            HttpResponse response = httpclient.execute(httppost);
            return EntityUtils.toString(response.getEntity());
        } catch (ClientProtocolException e) {
            Log.e(TAG, "postData: " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e(TAG, "postData: " + e.getMessage());
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Log.e(TAG, "onPostExecute: FieldName: " + mFieldsModel.getName()
                + " Field ID: " + mFieldsModel.getInputId()
                + " Result: " + result);

        DialogHelper.getInstance().hideProgressDialog(mContext);

        if (result != null) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String status = jsonObject.getString("status");
                switch (status) {
                    case "success":

                        /**Delete existing db data arrays*/
                        mDatabaseHelper.clearExistingDataArray(DatabaseConstants.TBL_TEXTBOX, mFieldsModel.getB_id());
                        mDatabaseHelper.clearExistingDataArray(DatabaseConstants.TBL_CHECKBOX, mFieldsModel.getB_id());
                        mDatabaseHelper.clearExistingDataArray(DatabaseConstants.TBL_RADIO, mFieldsModel.getB_id());
                        mDatabaseHelper.clearExistingDataArray(DatabaseConstants.TBL_SELECT, mFieldsModel.getB_id());

                        InputModel inputModel = new InputModel();
                        inputModel.setId(mFieldsModel.getB_id());
                        inputModel.setTypeId(mFieldsModel.getTypeId());
                        inputModel.setTitle(mFieldsModel.getTitle());
                        inputModel.setName(mFieldsModel.getName());
                        inputModel.setDescription(mFieldsModel.getDescription());
                        inputModel.setGroupId(mFieldsModel.getGroupId());
                        inputModel.setIsRequired(mFieldsModel.getIsRequired());
                        inputModel.setIsActive(mFieldsModel.getB_isActive());
                        inputModel.setDateTime(mFieldsModel.getB_dateTime());
                        inputModel.setCategory(mFieldsModel.getCategory());
                        inputModel.setStatus(mFieldsModel.getB_status());
                        inputModel.setIsMandatory(mFieldsModel.getIsMandatory());
                        inputModel.setEditStatus("0");

                        if (mFieldsModel.getDataArrayList().size() > 0) {
                            for (DataArray dataArray : mFieldsModel.getDataArrayList()) {

                                Log.e(TAG, "onClick: " + "InputOptions InputId: " + dataArray.getInputId());
                                Log.e(TAG, "onClick: " + "InputOptions Title: " + dataArray.getTitle());
                                Log.e(TAG, "onClick: " + "InputOptions Name: " + dataArray.getName());
                                Log.e(TAG, "onClick: " + "InputOptions Value: " + dataArray.getValue());
                                Log.e(TAG, "onClick: " + "InputOptions ActiveStatus: " + dataArray.getIsActive());

                                int maxDataArrayId = mDatabaseHelper.getMaxDataArrayId(mFieldsModel.getTypeId());
                                maxDataArrayId++;
                                dataArray.setId(String.valueOf(maxDataArrayId));
                                dataArray.setInputId(mFieldsModel.getB_id());
                                mDatabaseHelper.createDataArray(dataArray, mFieldsModel.getTypeId());
                            }
                        } else {
                            int maxDataArrayId = mDatabaseHelper.getMaxDataArrayId(mFieldsModel.getTypeId());
                            maxDataArrayId++;
                            DataArray dataArray = new DataArray();
                            dataArray.setId(String.valueOf(maxDataArrayId));
                            dataArray.setInputId(mFieldsModel.getB_id());
                            dataArray.setTitle(mFieldsModel.getTitle());
                            dataArray.setName(mFieldsModel.getName());
                            dataArray.setValue("");
                            dataArray.setIsActive("1");
                            dataArray.setDateTime(Utils.getDateTime());
                            mDatabaseHelper.createDataArray(dataArray, mFieldsModel.getTypeId());
                        }

                        mDatabaseHelper.updateFieldById(inputModel);
                        Log.e(TAG, "onPostExecute: " + result);

                        if (!mSynchronization) {
                            mMaintenancePresenter.dismissAddFormFieldAdapterDialog();
                            mMaintenancePresenter.showAlertDialog("Alert", "Changes saved successfully!", Constants.DIALOG_ACTION_NORMAL);
                        }
//                        mMaintenancePresenter.refreshFormAdapters();
                        mMaintenancePresenter.refreshCustomFieldsAdapter();
                        break;

                    case "failed":
                        String message = jsonObject.getString("msg");
                        Log.e(TAG, "onPostExecute: " + "Message: " + message + " name: " + mFieldsModel.getName());
//                        if (!message.equals("Please check your data.")) {
                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
//                        }
                        break;

                    default:
                        Toast.makeText(mContext, "Updating field failed", Toast.LENGTH_SHORT).show();
                        break;
                }
            } catch (JSONException e) {
                Log.e(TAG, "onPostExecute: " + e.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", e.getMessage(), Constants.DIALOG_ACTION_NORMAL);
            }
        }
    }
}
