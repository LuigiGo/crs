package com.unilab.customerregistration.features.base.view;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.tsengvn.typekit.TypekitContextWrapper;
import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.AlertInfoDialogFragment;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.AlertShowRegUpdatesDialogFragment;
import com.unilab.customerregistration.utilities.dialog.dialogfragments.ProgressDialogFragment;


public class BaseActivity extends AppCompatActivity implements MaintenancePresenter.DialogFragmentsListener {

    private String TAG = this.getClass().getSimpleName();
    private FragmentManager fm;
    private FragmentTransaction ft;
    DialogFragment alertDialogFragment, progressDialogFragment, alertRegUpdateDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    public void changeFragment(Fragment fragment) {
        fm = getSupportFragmentManager();
        ft = fm.beginTransaction().replace(R.id.fragmentContainer, fragment, fragment.getClass().getSimpleName());
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onShowAlertDialog(String title, String message, String actionType) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("alert_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        alertDialogFragment = AlertInfoDialogFragment.newInstance(title, message);
        alertDialogFragment.show(getFragmentManager(), "alert_dialog");
    }

    @Override
    public void onShowProgressDialog(String message) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("progress_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        progressDialogFragment = ProgressDialogFragment.newInstance(message);
        progressDialogFragment.setCancelable(false);
        progressDialogFragment.show(getFragmentManager(), "progress_dialog");
    }

    @Override
    public void onShowRegUpdateDialog(Intent intent) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("alert_dialog");
        if (prev != null) {
            Log.e(TAG, "onShowAlertDialog: " + "fragment is added already");
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        alertRegUpdateDialog = AlertShowRegUpdatesDialogFragment.newInstance(intent);
        alertRegUpdateDialog.show(getFragmentManager(), "alert_dialog");
    }

}
