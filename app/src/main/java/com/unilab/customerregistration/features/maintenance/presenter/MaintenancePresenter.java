package com.unilab.customerregistration.features.maintenance.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.unilab.customerregistration.features.maintenance.model.AssignFieldModel;
import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.model.InputModel;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.maintenance.view.adapters.SubGroupingAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.AddUpdateFieldSubGroupNameAdapter;

import java.util.ArrayList;
import java.util.List;

public interface MaintenancePresenter {

    void showBrandPage();

    void showDesignPage();

    void showFormPage();

    void showPreviewPage();

    void loadCampaignList(String brandCode, String brandName);

    void loadCampaignEventList(String campaignCode, String campaignName);

    void saveConfiguredSettings(String campaignEventCode, String campaignEventName);

    void showAlertDialog(String title, String message, String actionType);

    void showProgressDialog(String message);


    /**
     * Design interfaces
     */
    void showColorPickerDialog(int type);

    void showImageAlignmentDialog(int logoType, SettingsModel settingsModel);

    void setImageAlignment(SettingsModel settingsModel);

    void setDesignColors(int type, int color, String hexColor);

    void showDesignFontsDialog(SettingsModel settingsModel);

    void setDesignFonts(SettingsModel settingsModel);

    void showSubmitButtonDialog(SettingsModel settingsModel);

    void showDesignFragmentSavePrompt(String title, String message, String actionType, SettingsModel settingsModel,
                                      boolean hasSelectedMobileLogo, boolean hasSelectedWebLogo, boolean isCopyingSettings);

    void saveDesign();

    void resetDesign();

    void refreshDesign();

    /**
     * Form interfaces
     */
    void synchronizeForm(boolean isSynchronizingForm);

    void setAddFieldButtonVisibility(int visibility);

    void refreshFormAdapters();

    void refreshCustomFieldsAdapter();

    void showAddFormFieldDialog();

    void addFormGroups(GroupsModel groupsModel, EditText edtGroupName,
                       List<GroupsModel> groupNameList, ArrayAdapter<GroupsModel> groupsAdapter,
                       List<GroupsModel> groupsModelList, SubGroupingAdapter subGroupingAdapter);

    void addFormField(FieldsModel fieldsModel, List<DataArray> dataArrayList,
                      InputModel inputModel, AssignFieldModel assignFieldModel, String actionType);

    void showAddFormFieldAdapterDialog(FieldsModel fieldsModel);

    void showAddFormFieldAdapterSavePrompt(String title, String message, EditText edtFieldName,
                                           List<DataArray> dataArrayList, Spinner spnInputType, Spinner spnSubGroup,
                                           AddUpdateFieldSubGroupNameAdapter addUpdateFieldSubGroupNameAdapter,
                                           EditText edtDescription, FieldsModel fieldsModel, CheckBox chRequired);

    void showAddFormFieldSavePrompt(String title, String message, List<DataArray> dataArrayList, InputModel inputModel,
                                    AssignFieldModel assignFieldModel, FieldsModel fieldsModel);

    void dismissAddFormFieldAdapterDialog();

    void showSaveFormPromptDialog(String title, String message, String actionType);

    void saveForm();

    void resetForm();

    void setHorizontalScrollViewPosition(int position);

    void hideKeyboardInFragment(Context context, IBinder windowToken);

    void stopHorizontalScrollViewScroll();

    void updateGroup(String groupTitle, String groupName,
                     List<GroupsModel> groupNameList, ArrayAdapter<GroupsModel> groupNameAdapter,
                     GroupsModel groupsModel, List<GroupsModel> groupsModelList,
                     SubGroupingAdapter subGroupingAdapter, int position);

    void deleteGroup(GroupsModel groupsModel, List<GroupsModel> groupsModelList,
                     List<GroupsModel> groupNameList, ArrayAdapter<GroupsModel> groupNameAdapter,
                     SubGroupingAdapter subGroupingAdapter, int itemPosition);

    interface DialogFragmentsListener {

        void onShowAlertDialog(String title, String message, String actionType);

        void onShowProgressDialog(String message);

        void onShowRegUpdateDialog(Intent intent);

    }

}
