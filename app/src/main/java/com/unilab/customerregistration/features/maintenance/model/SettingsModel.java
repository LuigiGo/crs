package com.unilab.customerregistration.features.maintenance.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingsModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("campaign_code")
    @Expose
    private String campaignCode;
    @SerializedName("brand_code")
    @Expose
    private String brandCode;
    @SerializedName("campaign_event")
    @Expose
    private String eventCode;
    @SerializedName("campaign_name")
    @Expose
    private String formattedEventName;
    @SerializedName("foreground")
    @Expose
    private String foreground;
    @SerializedName("background")
    @Expose
    private String background;
    @SerializedName("header")
    @Expose
    private String header;
    @SerializedName("footer")
    @Expose
    private String footer;
    @SerializedName("footer_text")
    @Expose
    private String footerText;
    @SerializedName("web_logo")
    @Expose
    private String webLogo;
    @SerializedName("web_logo_img")
    @Expose
    private String webLogoImg;
    @SerializedName("mob_logo")
    @Expose
    private String mobLogo;
    @SerializedName("mob_logo_img")
    @Expose
    private String mobLogoImg;
    @SerializedName("font_headfamily")
    @Expose
    private String fontHeadfamily;
    @SerializedName("font_headsize")
    @Expose
    private String fontHeadsize;
    @SerializedName("font_basefamily")
    @Expose
    private String fontBasefamily;
    @SerializedName("font_basesize")
    @Expose
    private String fontBasesize;
    @SerializedName("submit_border_width")
    @Expose
    private String submitBorderWidth;
    @SerializedName("submit_border_radius")
    @Expose
    private String submitBorderRadius;
    @SerializedName("submit_background")
    @Expose
    private String submitBackground;
    @SerializedName("submit_alignment")
    @Expose
    private String submitAlignment;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("date_time")
    @Expose
    private String dateTime;
    private String status;
    private String webLogoImgFileName;
    private String mobLogoImgFileName;

    public SettingsModel() {
    }

    protected SettingsModel(Parcel in) {
        id = in.readString();
        campaignCode = in.readString();
        brandCode = in.readString();
        eventCode = in.readString();
        foreground = in.readString();
        background = in.readString();
        header = in.readString();
        footer = in.readString();
        footerText = in.readString();
        webLogo = in.readString();
        webLogoImg = in.readString();
        mobLogo = in.readString();
        mobLogoImg = in.readString();
        fontHeadfamily = in.readString();
        fontHeadsize = in.readString();
        fontBasefamily = in.readString();
        fontBasesize = in.readString();
        submitBorderWidth = in.readString();
        submitBorderRadius = in.readString();
        submitBackground = in.readString();
        submitAlignment = in.readString();
        isActive = in.readString();
        dateTime = in.readString();
        status = in.readString();
        webLogoImgFileName = in.readString();
        mobLogoImgFileName = in.readString();
    }

    public static final Creator<SettingsModel> CREATOR = new Creator<SettingsModel>() {
        @Override
        public SettingsModel createFromParcel(Parcel in) {
            return new SettingsModel(in);
        }

        @Override
        public SettingsModel[] newArray(int size) {
            return new SettingsModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getForeground() {
        return foreground;
    }

    public void setForeground(String foreground) {
        this.foreground = foreground;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getFooterText() {
        return footerText;
    }

    public void setFooterText(String footerText) {
        this.footerText = footerText;
    }

    public String getWebLogo() {
        return webLogo;
    }

    public void setWebLogo(String webLogo) {
        this.webLogo = webLogo;
    }

    public String getWebLogoImg() {
        return webLogoImg;
    }

    public void setWebLogoImg(String webLogoImg) {
        this.webLogoImg = webLogoImg;
    }

    public String getMobLogo() {
        return mobLogo;
    }

    public void setMobLogo(String mobLogo) {
        this.mobLogo = mobLogo;
    }

    public String getMobLogoImg() {
        return mobLogoImg;
    }

    public void setMobLogoImg(String mobLogoImg) {
        this.mobLogoImg = mobLogoImg;
    }

    public String getFontHeadfamily() {
        return fontHeadfamily;
    }

    public void setFontHeadfamily(String fontHeadfamily) {
        this.fontHeadfamily = fontHeadfamily;
    }

    public String getFontHeadsize() {
        return fontHeadsize;
    }

    public void setFontHeadsize(String fontHeadsize) {
        this.fontHeadsize = fontHeadsize;
    }

    public String getFontBasefamily() {
        return fontBasefamily;
    }

    public void setFontBasefamily(String fontBasefamily) {
        this.fontBasefamily = fontBasefamily;
    }

    public String getFontBasesize() {
        return fontBasesize;
    }

    public void setFontBasesize(String fontBasesize) {
        this.fontBasesize = fontBasesize;
    }

    public String getSubmitBorderWidth() {
        return submitBorderWidth;
    }

    public void setSubmitBorderWidth(String submitBorderWidth) {
        this.submitBorderWidth = submitBorderWidth;
    }

    public String getSubmitBorderRadius() {
        return submitBorderRadius;
    }

    public void setSubmitBorderRadius(String submitBorderRadius) {
        this.submitBorderRadius = submitBorderRadius;
    }

    public String getSubmitBackground() {
        return submitBackground;
    }

    public void setSubmitBackground(String submitBackground) {
        this.submitBackground = submitBackground;
    }

    public String getSubmitAlignment() {
        return submitAlignment;
    }

    public void setSubmitAlignment(String submitAlignment) {
        this.submitAlignment = submitAlignment;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWebLogoImgFileName() {
        return webLogoImgFileName;
    }

    public void setWebLogoImgFileName(String webLogoImgFileName) {
        this.webLogoImgFileName = webLogoImgFileName;
    }

    public String getMobLogoImgFileName() {
        return mobLogoImgFileName;
    }

    public void setMobLogoImgFileName(String mobLogoImgFileName) {
        this.mobLogoImgFileName = mobLogoImgFileName;
    }

    public String getFormattedEventName() {
        return formattedEventName;
    }

    public void setFormattedEventName(String formattedEventName) {
        this.formattedEventName = formattedEventName;
    }

    @Override
    public String toString() {
        return "SettingsModel{" +
                "id='" + id + '\'' +
                ", campaignCode='" + campaignCode + '\'' +
                ", brandCode='" + brandCode + '\'' +
                ", eventCode='" + eventCode + '\'' +
                ", formattedEventName='" + formattedEventName + '\'' +
                ", foreground='" + foreground + '\'' +
                ", background='" + background + '\'' +
                ", header='" + header + '\'' +
                ", footer='" + footer + '\'' +
                ", footerText='" + footerText + '\'' +
                ", webLogo='" + webLogo + '\'' +
                ", webLogoImg='" + webLogoImg + '\'' +
                ", mobLogo='" + mobLogo + '\'' +
                ", mobLogoImg='" + mobLogoImg + '\'' +
                ", fontHeadfamily='" + fontHeadfamily + '\'' +
                ", fontHeadsize='" + fontHeadsize + '\'' +
                ", fontBasefamily='" + fontBasefamily + '\'' +
                ", fontBasesize='" + fontBasesize + '\'' +
                ", submitBorderWidth='" + submitBorderWidth + '\'' +
                ", submitBorderRadius='" + submitBorderRadius + '\'' +
                ", submitBackground='" + submitBackground + '\'' +
                ", submitAlignment='" + submitAlignment + '\'' +
                ", isActive='" + isActive + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", status='" + status + '\'' +
                ", webLogoImgFileName='" + webLogoImgFileName + '\'' +
                ", mobLogoImgFileName='" + mobLogoImgFileName + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(campaignCode);
        dest.writeString(brandCode);
        dest.writeString(eventCode);
        dest.writeString(foreground);
        dest.writeString(background);
        dest.writeString(header);
        dest.writeString(footer);
        dest.writeString(footerText);
        dest.writeString(webLogo);
        dest.writeString(webLogoImg);
        dest.writeString(mobLogo);
        dest.writeString(mobLogoImg);
        dest.writeString(fontHeadfamily);
        dest.writeString(fontHeadsize);
        dest.writeString(fontBasefamily);
        dest.writeString(fontBasesize);
        dest.writeString(submitBorderWidth);
        dest.writeString(submitBorderRadius);
        dest.writeString(submitBackground);
        dest.writeString(submitAlignment);
        dest.writeString(isActive);
        dest.writeString(dateTime);
        dest.writeString(status);
        dest.writeString(webLogoImgFileName);
        dest.writeString(mobLogoImgFileName);
    }
}