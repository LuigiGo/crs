package com.unilab.customerregistration.features.registration.model.web.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RegistrationApiRoutes {

    @FormUrlEncoded
    @POST("webapp/")
    Call<ResponseBody> onAuthenticateEmail(@Field("token") String token,
                                           @Field("cmdEvent") String cmdEvent,
                                           @Field("email") String email,
                                           @Field("host") String hostname);

    @FormUrlEncoded
    @POST("webapp/")
    Call<ResponseBody> onGetCustomerDetails(@Field("token") String token,
                                            @Field("cmdEvent") String cmdEvent,
                                            @Field("email") String email,
                                            @Field("campaign_event") String campaignEventId,
                                            @Field("host") String hostname);
}
