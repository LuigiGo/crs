package com.unilab.customerregistration.features.registration.model.web.api;


import com.unilab.customerregistration.utilities.network.RestManager;

import retrofit2.Retrofit;

public class RegistrationApiProvider {

    public static RegistrationApiRoutes getMaintenanceDbmApiRoutes() {
        Retrofit retrofit = RestManager.getInstance().getDBMRetrofitAdapter();
        return retrofit.create(RegistrationApiRoutes.class);
    }

//    public static RegistrationApiRoutes getMaintenanceDbmApiRoutes2() {
//        Retrofit retrofit = RestManager.getInstance().getDBMRetrofitAdapter2();
//        return retrofit.create(RegistrationApiRoutes.class);
//    }

    public static RegistrationApiRoutes getMaintenanceWebApiRoutes() {
        Retrofit retrofit = RestManager.getInstance().getWebRetrofitAdapter();
        return retrofit.create(RegistrationApiRoutes.class);
    }
}
