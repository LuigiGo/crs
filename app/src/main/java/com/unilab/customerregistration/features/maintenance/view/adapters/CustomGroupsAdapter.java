package com.unilab.customerregistration.features.maintenance.view.adapters;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.view.FormFragment;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.constants.DatabaseConstants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;

import java.util.ArrayList;
import java.util.List;

public class CustomGroupsAdapter extends RecyclerView.Adapter<CustomGroupsAdapter.ViewHolder> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private ArrayList<GroupsModel> mGroupsModelList;
    private DatabaseHelper mDatabaseHelper;
    private String brandCode, campaignCode, eventCode;
    private List<FieldsModel> mCopiedFormFields;

    private CustomFieldByGroupAdapter customFieldByGroupAdapter;
    private List<CustomFieldByGroupAdapter> mCustomFieldByGroupAdapterList;


    public CustomGroupsAdapter(Context context, ArrayList<GroupsModel> groupsModelList, List<FieldsModel> copiedFields) {
        mDatabaseHelper = new DatabaseHelper(context);
        SharedPrefHelper.init(context);

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        mContext = context;
        mGroupsModelList = groupsModelList;
        mCustomFieldByGroupAdapterList = new ArrayList<>();

        mCopiedFormFields = copiedFields;

    }

    @Override
    public CustomGroupsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View views = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_groups, parent, false);
        return new ViewHolder(views);
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        mCustomFieldByGroupAdapterList.clear();
    }

    @Override
    public void onBindViewHolder(CustomGroupsAdapter.ViewHolder holder, int position) {
        final GroupsModel mGroupsModel = mGroupsModelList.get(position);
        holder.txtGroupNames.setText(mGroupsModel.getTitle());

        Log.e(TAG, "onBindViewHolder: " + mGroupsModel.getTitle() + " " + mGroupsModel.getId());
        List<FieldsModel> fieldsModelArrayList = new ArrayList<>();

        if (mGroupsModel.getTitle() != null && mGroupsModel.getId() != null) {
            if (FormFragment.isCopyingForm) {
                for (FieldsModel fieldsModel : mCopiedFormFields) {
                    if (fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_ADDITIONAL_FIELDS)) &&
                            fieldsModel.getGroupId().equals(mGroupsModel.getId())) {
                        fieldsModelArrayList.add(fieldsModel);
                    }
                }
            } else {
                fieldsModelArrayList = mDatabaseHelper.getCustomFieldsByGroupId(brandCode, campaignCode, eventCode, mGroupsModel.getId());
                /**Getting data array for standard fields*/
                for (FieldsModel fieldsModel : fieldsModelArrayList) {
                    switch (fieldsModel.getTypeId()) {
                        case "3":
                            fieldsModel.setDataArrayList(mDatabaseHelper.getDataArrayByFieldId(DatabaseConstants.TBL_RADIO, fieldsModel.getB_id()));
                            break;

                        case "4":
                            fieldsModel.setDataArrayList(mDatabaseHelper.getDataArrayByFieldId(DatabaseConstants.TBL_SELECT, fieldsModel.getB_id()));
                            break;
                    }
                }
            }

            customFieldByGroupAdapter = new CustomFieldByGroupAdapter(mContext, fieldsModelArrayList, mGroupsModel.getId());
            mCustomFieldByGroupAdapterList.add(customFieldByGroupAdapter);
            holder.recyclerInputFields.setAdapter(customFieldByGroupAdapter);
        }
    }

    public void saveFormFields() {
        for (int i = 0; i < mCustomFieldByGroupAdapterList.size(); i++) {
            mCustomFieldByGroupAdapterList.get(i).saveFormFields();
        }
    }

    public ArrayList<FieldsModel> getAllFormFields() {
        ArrayList<FieldsModel> allFormFieldsList = new ArrayList<>();
        for (int i = 0; i < mCustomFieldByGroupAdapterList.size(); i++) {
            List<FieldsModel> fieldsModelList = mCustomFieldByGroupAdapterList.get(i).getFields();
            allFormFieldsList.addAll(fieldsModelList);
        }
        return allFormFieldsList;
    }

    public ArrayList<GroupsModel> getGroupsList() {
        return mGroupsModelList;
    }

    @Override
    public int getItemCount() {
        return mGroupsModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtGroupNames;
        private RecyclerView recyclerInputFields;

        public ViewHolder(View v) {
            super(v);

            txtGroupNames = (TextView) v.findViewById(R.id.txtGroupName);

            recyclerInputFields = (RecyclerView) v.findViewById(R.id.recyclerInputFields);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext.getApplicationContext());
            recyclerInputFields.setLayoutManager(mLayoutManager);
            recyclerInputFields.setItemAnimator(new DefaultItemAnimator());
        }
    }
}
