package com.unilab.customerregistration.features.dashboard.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.dashboard.presenter.DashboardContract;
import com.unilab.customerregistration.features.dashboard.model.CityModel;
import com.unilab.customerregistration.features.dashboard.model.ProvinceModel;
import com.unilab.customerregistration.features.dashboard.model.api.DashboardApiProvider;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsyncFetchProvince extends AsyncTask<Void, Void, Void> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private DatabaseHelper mDatabaseHelper;
    private List<ProvinceModel> mProvinceModelList;
    private DashboardContract mDashboardContract;

    public AsyncFetchProvince(Context context, List<ProvinceModel> provinceModelList) {
        DialogHelper.init(context);
        mDatabaseHelper = new DatabaseHelper(context);

        mContext = context;
        mProvinceModelList = provinceModelList;
        mDashboardContract = (DashboardContract) context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        DialogHelper.getInstance().showProgressDialog(mContext, "Getting province data");

    }

    @Override
    protected Void doInBackground(Void... params) {
        for (ProvinceModel provinceModel : mProvinceModelList) {
            Log.e(TAG, "onResponse: " + provinceModel.getProvinceName());
            if (!mDatabaseHelper.checkProvinceIdIfExist(provinceModel)) {
                mDatabaseHelper.createProvince(provinceModel);
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
//        DialogHelper.getInstance().hideProgressDialog(mContext);
        getCityAPI();
    }

    private void getCityAPI() {
        Log.e(TAG, "getCityAPI: ");
        Call<List<CityModel>> getCityCall = DashboardApiProvider.getDashboardApiRoutes()
                .getCity(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_GET_CITY);
        getCityCall.enqueue(new Callback<List<CityModel>>() {
            @Override
            public void onResponse(Call<List<CityModel>> call, Response<List<CityModel>> response) {
                Log.e(TAG, "onResponse: " + response.code());
                if (response.body() != null) {
                    List<CityModel> cityModelList = response.body();
//                    for (CityModel cityModel : cityModelList) {
//                        Log.e(TAG, "onResponse: " + cityModel.getCityName());
//                        mDatabaseHelper.createCity(cityModel);
//                    }
//                    SharedPrefHelper.getInstance().putPreLoadedDataStatusToShared(true);
                    new AsyncFetchCity(mContext, cityModelList).execute();
                } else {
                    DialogHelper.getInstance().hideProgressDialog(mContext);
                }
            }

            @Override
            public void onFailure(Call<List<CityModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mDashboardContract.showAlertInfoDialog("Alert", mContext.getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(mContext);
            }

        });
    }

}
