package com.unilab.customerregistration.features.registration.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.registration.model.RegistrationModel;
import com.unilab.customerregistration.features.registration.presenter.RegistrationContract;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class AsyncUpdateRegistration extends AsyncTask<Void, Void, String> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private String brandCode, campaignCode, eventCode, formattedEventName;
    private RegistrationModel mRegistrationModel;
    private List<RegistrationModel> registrationModelList;
    private DatabaseHelper mDatabaseHelper;
    private RegistrationContract mRegistrationContract;
    private AlertDialog mAlertDialog;
    private boolean mIsSynchronization;
    private RegistrationContract.DialogListener mRegistrationDialogListener;
    private SettingsModel mSettingsModel;

    public AsyncUpdateRegistration(Context context, RegistrationModel registrationModel, boolean isSynchronization, SettingsModel settingsModel) {
        Log.e(TAG, "AsyncUpdateRegistration: ");
        mDatabaseHelper = new DatabaseHelper(context);
        SharedPrefHelper.init(context);
        DialogHelper.init(context);

        mContext = context;
        mRegistrationModel = registrationModel;
        mRegistrationContract = (RegistrationContract) mContext;
        mIsSynchronization = isSynchronization;
        mRegistrationDialogListener = (RegistrationContract.DialogListener) context;
        mSettingsModel = settingsModel;

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();
        formattedEventName = SharedPrefHelper.getInstance().getFormattedCampaignNameFromShared();
        Log.e(TAG, "AsyncUpdateRegistration: " + campaignCode + " " + brandCode + " " + eventCode);

        Type listType = new TypeToken<List<RegistrationModel>>() {
        }.getType();

        /**Preparing List of data for sending*/
        registrationModelList = new Gson().fromJson(registrationModel.getData(), listType);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (!mIsSynchronization) {
            DialogHelper.getInstance().showProgressDialog(mContext, "Saving registration...");
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        return postData();
    }

    private String postData() {
        Log.e(TAG, "postData: " + registrationModelList.size());
        Log.e(TAG, "postData: " + registrationModelList.toString());

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Constants.API_DBM_BASE_URL.concat("webapp"));

        try {
            List fieldList = new ArrayList();
            fieldList.add(new BasicNameValuePair("token", Constants.API_DBM_TOKEN));
            fieldList.add(new BasicNameValuePair("cmdEvent", Constants.CMD_EVENT_DBM_UPDATE_REGISTRATION));
            fieldList.add(new BasicNameValuePair("campaign_name", formattedEventName));
            fieldList.add(new BasicNameValuePair("campaign_code", campaignCode));
            fieldList.add(new BasicNameValuePair("brand_id", brandCode));
            fieldList.add(new BasicNameValuePair("campaign_event_id", eventCode));
            fieldList.add(new BasicNameValuePair("host", Constants.DBM_HOSTNAME));

            for (int i = 0; i < registrationModelList.size(); i++) {
                final RegistrationModel registrationModel = registrationModelList.get(i);
                fieldList.add(new BasicNameValuePair(registrationModel.getName(), registrationModel.getValue()));
            }

            httppost.setEntity(new UrlEncodedFormEntity(fieldList));
            HttpResponse response = httpclient.execute(httppost);
            return EntityUtils.toString(response.getEntity());
        } catch (ClientProtocolException e) {
            Log.e(TAG, "postData: " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e(TAG, "postData: " + e.getMessage());
            return null;
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        DialogHelper.getInstance().hideProgressDialog(mContext);

        Log.e(TAG, "onPostExecute: Result: " + s);
        if (s != null) {
            try {
                JSONObject jsonObject = new JSONObject(s.toString());
                Log.e(TAG, "onPostExecute: " + jsonObject);

                String result = jsonObject.getString("result");
                Log.e(TAG, "onPostExecute: ResultCode: " + result);

                String emailAddress = mRegistrationModel.getEmail();
                mRegistrationModel.setStatus("0");
                switch (result) {
                    case "1":
                        if (mDatabaseHelper.checkIfRegistrationEmailExist(emailAddress, brandCode, campaignCode, eventCode)) {
                            mDatabaseHelper.updateRegistration(mRegistrationModel, brandCode, campaignCode, eventCode, "0");
                        } else {
                            mDatabaseHelper.createRegistration(mRegistrationModel);

                        }

                        if (!mIsSynchronization) {
                            mRegistrationDialogListener.showRegistrationAlertDialog("Alert", jsonObject.getString("message"),
                                    Constants.DIALOG_ACTION_UPDATE_REGISTRATION_DETAILS, result, mSettingsModel);
                        }
                        break;

                    default:
                        if (mDatabaseHelper.checkIfRegistrationEmailExist(emailAddress, brandCode, campaignCode, eventCode)) {
                            mDatabaseHelper.updateRegistration(mRegistrationModel, brandCode, campaignCode, eventCode, "0");
                        } else {
                            mDatabaseHelper.createRegistration(mRegistrationModel);
                        }

                        if (!mIsSynchronization) {
                            mRegistrationDialogListener.showRegistrationAlertDialog("Alert", jsonObject.getString("message"),
                                    Constants.DIALOG_ACTION_NORMAL, result, mSettingsModel);
                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
