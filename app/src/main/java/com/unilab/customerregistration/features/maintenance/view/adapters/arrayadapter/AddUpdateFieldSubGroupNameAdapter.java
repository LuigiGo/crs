package com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;

import java.util.List;

public class AddUpdateFieldSubGroupNameAdapter extends ArrayAdapter<GroupsModel> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private List<GroupsModel> mGroupsModelList;

    public AddUpdateFieldSubGroupNameAdapter(@NonNull Context context, @LayoutRes int resource,
                                             @NonNull List<GroupsModel> groupsModelList) {
        super(context, resource, groupsModelList);
        mContext = context;
        mGroupsModelList = groupsModelList;
    }

    @Override
    public int getCount() {
        return mGroupsModelList.size();
    }

    @Nullable
    @Override
    public GroupsModel getItem(int position) {
        return mGroupsModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;

        if (v == null) {
            v = LayoutInflater.from(mContext).inflate(R.layout.list_spinner_default, parent, false);
            holder = new ViewHolder();
            holder.txtGroupName = (TextView) v.findViewById(R.id.txtLabel);
            holder.txtGroupName.setTextColor(mContext.getResources().getColor(android.R.color.white));
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        GroupsModel groupsModel = mGroupsModelList.get(position);
        holder.txtGroupName.setText(groupsModel.getTitle());

        return v;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;

        if (v == null) {
            v = LayoutInflater.from(mContext).inflate(R.layout.list_spinner_dropdown, parent, false);
            holder = new ViewHolder();
            holder.txtGroupName = (TextView) v.findViewById(R.id.txtLabel);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        GroupsModel groupsModel = mGroupsModelList.get(position);
        holder.txtGroupName.setText(groupsModel.getTitle());
        return v;
    }

    private static class ViewHolder {
        TextView txtGroupName;
    }
}
