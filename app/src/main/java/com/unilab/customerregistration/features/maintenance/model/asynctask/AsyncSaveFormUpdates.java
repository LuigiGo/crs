package com.unilab.customerregistration.features.maintenance.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.DbmAddFieldsModel;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.model.web.api.MaintenanceApiProvider;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.constants.DatabaseConstants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.network.NetworkUtils;
import com.unilab.customerregistration.utilities.network.RestManager;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsyncSaveFormUpdates extends AsyncTask<Void, Void, Boolean> {

    private final String TAG = this.getClass().getSimpleName();
    private DatabaseHelper mDatabaseHelper;
    private Context mContext;
    private MaintenancePresenter mMaintenancePresenter;

    private String brandCode, campaignCode, eventCode;
    private boolean mIsSaving = false;
    private AlertDialog mAlertSuccessPromptDialog;

    public AsyncSaveFormUpdates(Context context, boolean isSaving) {
        Log.e(TAG, "AsyncSaveFormUpdates: ");
        mDatabaseHelper = new DatabaseHelper(context);
        SharedPrefHelper.init(context);
        RestManager.init();
        DialogHelper.init(context);

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        mContext = context;
        mIsSaving = isSaving;
        mMaintenancePresenter = (MaintenancePresenter) context;

        Log.e(TAG, "AsyncSaveFormUpdates: " + brandCode + "-" + campaignCode + "-" + eventCode);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        return savingUpdates();
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        Log.e(TAG, "onPostExecute: ");
//        DialogHelper.getInstance().hideProgressDialog(mContext);

        if (mIsSaving) {
            showSuccessPromptDialog(mContext, "Alert", "Changes saved successfully!");
        }
    }

    private boolean savingUpdates() {
        if (NetworkUtils.isNetworkConnected(mContext)) {
            /**Groups update*/
            List<GroupsModel> addedOfflineGroupsList = mDatabaseHelper.checkGroupForUpdate(brandCode, campaignCode, eventCode);
            Log.e(TAG, "savingUpdates: " + addedOfflineGroupsList.size());
            for (GroupsModel groupsModel : addedOfflineGroupsList) {
                updateGroupTable(groupsModel);
            }

            /**Add fields to dbm*/
            List<FieldsModel> fieldUpdatesToDbmList = mDatabaseHelper.checkFieldUpdatesForDbm(brandCode, campaignCode, eventCode);
            Log.e(TAG, "checkFieldUpdates: " + fieldUpdatesToDbmList.size());
            for (FieldsModel fieldsModel : fieldUpdatesToDbmList) {
                postFieldToDbmForOffline(fieldsModel);
            }

            /**Add fields to web*/
            List<FieldsModel> fieldUpdatesToWebList = mDatabaseHelper.checkFieldUpdatesForWeb(brandCode, campaignCode, eventCode);
            Log.e(TAG, "checkFieldUpdates: " + fieldUpdatesToWebList.size());
            for (FieldsModel fieldsModel : fieldUpdatesToWebList) {
                String tableName = null;
                switch (fieldsModel.getTypeId()) {
                    case "1":
                        tableName = DatabaseConstants.TBL_TEXTBOX;
                        break;

                    case "2":
                        tableName = DatabaseConstants.TBL_CHECKBOX;
                        break;

                    case "3":
                        tableName = DatabaseConstants.TBL_RADIO;
                        break;

                    case "4":
                        tableName = DatabaseConstants.TBL_SELECT;
                        break;
                }

                fieldsModel.setDataArrayList(mDatabaseHelper.getDataArrayByFieldId(tableName, fieldsModel.getB_id()));
                new AsyncSendAddedOfflineField(mContext, fieldsModel).execute();
            }

            /**Edited fields updates*/
            List<FieldsModel> editedOfflineFieldList = mDatabaseHelper.checkEditedFieldsForUpdate(brandCode, campaignCode, eventCode);
            for (FieldsModel fieldsModel : editedOfflineFieldList) {
                Log.e(TAG, "savingUpdates: " + fieldsModel.toString());
                String tableName = null;
                switch (fieldsModel.getTypeId()) {
                    case "1":
                        tableName = DatabaseConstants.TBL_TEXTBOX;
                        break;

                    case "2":
                        tableName = DatabaseConstants.TBL_CHECKBOX;
                        break;

                    case "3":
                        tableName = DatabaseConstants.TBL_RADIO;
                        break;

                    case "4":
                        tableName = DatabaseConstants.TBL_SELECT;
                        break;
                }

                List<DataArray> dataArrayList = mDatabaseHelper.getDataArrayByFieldId(tableName, fieldsModel.getB_id());
                fieldsModel.setDataArrayList(dataArrayList);
                new AsyncSendEditedField(mContext, fieldsModel, true, false).execute();
            }
        }
        return true;
    }

    private void updateGroupTable(final GroupsModel groupsModel) {
        Call<ResponseBody> updateGroups = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .addGroup(Constants.API_WEB_TOKEN,
                        Constants.CMD_EVENT_POST_GROUP,
                        groupsModel.getCampaignCode(),
                        groupsModel.getBrandCode(),
                        groupsModel.getEventCode(),
                        groupsModel.getTitle(),
                        groupsModel.getName(),
                        groupsModel.getDescription(),
                        groupsModel.getIsActive(),
                        groupsModel.getDateTime());
        updateGroups.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    try {
                        String result = response.body().string();
                        JSONObject jsonObject = new JSONObject(result);
                        String status = jsonObject.getString("status");
                        switch (status) {
                            case "success":
                                String id = jsonObject.getString("id");
                                String oldGroupId = groupsModel.getId();
                                groupsModel.setId(id);
                                groupsModel.setStatus("0");
                                Log.e(TAG, "onResponse: " + oldGroupId + " " + result);
                                mDatabaseHelper.updateExistingGroup(groupsModel, oldGroupId, brandCode, campaignCode, eventCode);
                                mDatabaseHelper.updateFieldsGroup(oldGroupId, groupsModel.getId());
                                Toast.makeText(mContext, "Groups updated", Toast.LENGTH_SHORT).show();
                                break;

                            case "failed":
                                String message = jsonObject.getString("msg");
                                Log.e(TAG, "onResponse: " + message);
                                Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                                break;

                            default:
                                Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show();
                                Log.e(TAG, "onResponse: " + result);
                                break;
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "updateGroupTable-onFailure: " + t.getMessage());
            }
        });
    }

    private void postFieldToDbmForOffline(final FieldsModel fieldsModel) {
        Call<DbmAddFieldsModel> dbmAddFieldsModelCall = MaintenanceApiProvider.getMaintenanceDbmApiRoutes()
                .addDbmField(Constants.API_DBM_TOKEN,
                        Constants.CMD_EVENT_DBM_POST_FIELD,
                        fieldsModel.getCampaignCode(),
                        fieldsModel.getBrandCode(),
                        fieldsModel.getEventCode(),
                        fieldsModel.getTypeId(),
                        fieldsModel.getTitle(),
                        fieldsModel.getName(),
                        fieldsModel.getDescription(),
                        fieldsModel.getGroupId(),
                        fieldsModel.getIsRequired(),
                        fieldsModel.getA_isActive(),
                        Utils.getDateTime(),
                        fieldsModel.getCategory(),
                        Constants.DBM_HOSTNAME);
        dbmAddFieldsModelCall.enqueue(new Callback<DbmAddFieldsModel>() {
            @Override
            public void onResponse(Call<DbmAddFieldsModel> call, Response<DbmAddFieldsModel> response) {

                if (response.body() != null) {
                    DbmAddFieldsModel dbmAddFieldsModel = response.body();
                    Log.e(TAG, "postFieldToDbmForOffline-onResponse: " + dbmAddFieldsModel.getMessage());
                    switch (dbmAddFieldsModel.getMessage()) {
                        case "Field is successfully added!":
                            fieldsModel.setDbmStatus("0");
                            break;

                        case "Field is already exists!":
                            fieldsModel.setDbmStatus("2");
                            break;

                        default:
                            fieldsModel.setDbmStatus("1");
                            break;
                    }

                    mDatabaseHelper.updateDbmStatusByFieldId(fieldsModel);
                }
            }

            @Override
            public void onFailure(Call<DbmAddFieldsModel> call, Throwable t) {
                Log.e(TAG, "onFailure: postFieldToDbmForOffline: " + t.getMessage());
            }
        });
    }

    public void showSuccessPromptDialog(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View v = LayoutInflater.from(context).inflate(R.layout.dialog_information, null);
        builder.setView(v);
        builder.setCancelable(false);

        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        Button btnOk = (Button) v.findViewById(R.id.btnOk);

        txtTitle.setText(title);
        txtMessage.setText(message);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertSuccessPromptDialog.dismiss();
                mMaintenancePresenter.synchronizeForm(true);
            }
        });


        mAlertSuccessPromptDialog = builder.create();
        if (mAlertSuccessPromptDialog != null) {
            if (mAlertSuccessPromptDialog.isShowing()) {
                mAlertSuccessPromptDialog.dismiss();
            }
        }
        mAlertSuccessPromptDialog.show();
    }

}
