package com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.CampaignEventModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;

import java.util.List;

public class CampaignEventArrayAdapter extends ArrayAdapter<CampaignEventModel> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private List<CampaignEventModel> mCampaignEventModelList;
    private MaintenancePresenter mMaintenancePresenter;
    private boolean isInitial = true;

    public CampaignEventArrayAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<CampaignEventModel> campaignEventModelList) {
        super(context, resource, campaignEventModelList);
        SharedPrefHelper.init(context);

        mContext = context;
        mCampaignEventModelList = campaignEventModelList;
        mMaintenancePresenter = (MaintenancePresenter) context;
    }

    @Override
    public int getCount() {
        return mCampaignEventModelList.size();
    }

    @Nullable
    @Override
    public CampaignEventModel getItem(int position) {
        return mCampaignEventModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final CampaignEventModel campaignEventModel = mCampaignEventModelList.get(position);
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_spinner_default, parent, false);
        TextView label = (TextView) v.findViewById(R.id.txtLabel);
        label.setText(campaignEventModel.getName());

        if (!isInitial) {
            Log.e(TAG, "getView: CampaignName: " + campaignEventModel.getName() + " CampaignCode: " + campaignEventModel.getCode());
            if (!campaignEventModel.getName().equals("--Please select your Initiative--")) {
                mMaintenancePresenter.saveConfiguredSettings(campaignEventModel.getCode(), campaignEventModel.getName());
            }
            isInitial = true;
        }

        return v;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final CampaignEventModel campaignEventModel = mCampaignEventModelList.get(position);
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_spinner_dropdown, parent, false);
        TextView label = (TextView) v.findViewById(R.id.txtLabel);
        label.setText(campaignEventModel.getName());

        String eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();
        if (eventCode.equals(campaignEventModel.getCode())) {
            label.setBackgroundColor(mContext.getResources().getColor(R.color.light_blue));
        }

//        if (position != 0) {
        label.setText(campaignEventModel.getName());
//        } else {
//            label.setVisibility(View.GONE);
//        }
        isInitial = false;
        return v;
    }

    @Override
    public boolean isEnabled(int position) {
        if (position == 0) {
            return false;
        } else {
            return true;
        }
    }
}
