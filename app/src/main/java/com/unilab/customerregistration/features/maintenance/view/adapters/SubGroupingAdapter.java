package com.unilab.customerregistration.features.maintenance.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.util.List;

public class SubGroupingAdapter extends RecyclerView.Adapter<SubGroupingAdapter.ViewHolder> {

    private final String TAG = this.getClass().getSimpleName();
    Context mContext;
    private List<GroupsModel> mGroupsModelList;
    private MaintenancePresenter mMaintenancePresenter;
    private SubGroupingAdapter mSubGroupingAdapter;
    private List<GroupsModel> mGroupNameList;
    private ArrayAdapter<GroupsModel> mGroupNameAdapter;

    public SubGroupingAdapter(Context context, List<GroupsModel> groupsModelList,
                              List<GroupsModel> groupNameList, ArrayAdapter<GroupsModel> groupNameAdapter) {
        mContext = context;
        mGroupsModelList = groupsModelList;
        mMaintenancePresenter = (MaintenancePresenter) mContext;
        mSubGroupingAdapter = this;
        mGroupNameList = groupNameList;
        mGroupNameAdapter = groupNameAdapter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_sub_group_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final GroupsModel groupsModel = mGroupsModelList.get(position);

        if (groupsModel.getId().equals("1") || groupsModel.getId().equals("2") || groupsModel.getId().equals("3")) {
            holder.txtUpdateSubGroup.setVisibility(View.INVISIBLE);
            holder.txtDeleteSubGroup.setVisibility(View.INVISIBLE);
        } else {
            holder.txtUpdateSubGroup.setVisibility(View.VISIBLE);
            holder.txtDeleteSubGroup.setVisibility(View.VISIBLE);
        }

        holder.txtSubGroupName.setText(groupsModel.getTitle());

        holder.imgCloseEditTextGroupName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isUpdatingGroupName = holder.txtUpdateSubGroup.getText().equals("Save");
                updateGroupNameViewToggle(isUpdatingGroupName, holder, groupsModel);
            }
        });

        holder.txtUpdateSubGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "onClick: " + "Updating group");
                boolean isUpdatingGroupName = holder.txtUpdateSubGroup.getText().equals("Save");
                updateGroupNameViewToggle(isUpdatingGroupName, holder, groupsModel);
                if (isUpdatingGroupName) {
                    String groupTitle = holder.edtGroupName.getText().toString();
                    String groupName = groupTitle.toLowerCase().replace(" ", "_");
                    mMaintenancePresenter.updateGroup(groupTitle, groupName,
                            mGroupNameList, mGroupNameAdapter,
                            groupsModel, mGroupsModelList,
                            mSubGroupingAdapter, position);
                }
            }
        });

        holder.txtDeleteSubGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "onClick: " + "Deleting group");
                mMaintenancePresenter.deleteGroup(groupsModel, mGroupsModelList,
                        mGroupNameList, mGroupNameAdapter,
                        mSubGroupingAdapter, position);
            }
        });
    }

    private void updateGroupNameViewToggle(boolean isEditingGroupName, ViewHolder holder, GroupsModel groupsModel) {
        if (isEditingGroupName) {
            holder.txtUpdateSubGroup.setText("Edit");
            holder.edtGroupName.setVisibility(View.GONE);
            holder.imgCloseEditTextGroupName.setVisibility(View.GONE);
        } else {
            holder.txtUpdateSubGroup.setText("Save");
            holder.edtGroupName.setVisibility(View.VISIBLE);
            holder.imgCloseEditTextGroupName.setVisibility(View.VISIBLE);
            holder.edtGroupName.setText(groupsModel.getTitle());
        }
        Utils.hideKeyboard(mContext);
    }

    @Override
    public int getItemCount() {
        return mGroupsModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtSubGroupName, txtUpdateSubGroup, txtDeleteSubGroup;
        EditText edtGroupName;
        ImageView imgCloseEditTextGroupName;

        public ViewHolder(View v) {
            super(v);
//            setIsRecyclable(false);
            txtSubGroupName = (TextView) v.findViewById(R.id.txtSubGroupName);
            txtUpdateSubGroup = (TextView) v.findViewById(R.id.txtUpdateSubGroup);
            txtDeleteSubGroup = (TextView) v.findViewById(R.id.txtDeleteSubGroup);
            edtGroupName = (EditText) v.findViewById(R.id.edtGroupName);
            imgCloseEditTextGroupName = (ImageView) v.findViewById(R.id.imgCloseEditTextGroupName);
        }
    }
}
