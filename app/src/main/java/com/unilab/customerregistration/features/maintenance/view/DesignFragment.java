package com.unilab.customerregistration.features.maintenance.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.BrandModel;
import com.unilab.customerregistration.features.maintenance.model.CampaignEventModel;
import com.unilab.customerregistration.features.maintenance.model.CampaignModel;
import com.unilab.customerregistration.features.maintenance.model.DefaultSettingModel;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.maintenance.model.asynctask.AsyncSaveSettingUpdates;
import com.unilab.customerregistration.features.maintenance.model.web.api.MaintenanceApiProvider;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.BrandArrayAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.CampaignArrayAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.CampaignEventArrayAdapter;
import com.unilab.customerregistration.utilities.checkers.CheckerUtils;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.converters.ConverterUtil;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.network.NetworkUtils;
import com.unilab.customerregistration.utilities.network.RestManager;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DesignFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {

    private final String TAG = this.getClass().getSimpleName();
    private View mView;
    Button btnDefaultForegroundColor, btnDefaultBackgroundColor, btnDefaultHeaderColor,
            btnDefaultFooterColor;
    Button btnSetForegroundColor, btnSetBackgroundColor,
            btnSetHeaderColor, btnSetFooterColor, btnUploadWebLogo,
            btnSetWebLogoAlignment, btnSetMobileLogoAlignment,
            btnUploadMobileLogo, btnSetSubmitButton, btnSubmitBackgroundColor,
            btnSave, btnSetFont, btnReset;
    ImageView imgWebLogo, imgMobileLogo, imgSynchronize;
    private EditText edtFooter;
    private AlertDialog mAlertSetColorDialog, mAlertSetSubmitButtonDialog,
            mAlertSetLogoAlignmentDialog, mAlertSelectFontDialog;
    private DatabaseHelper mDatabaseHelper;
    private String email, brandCode, campaignCode, eventCode, formattedEventName;
    RelativeLayout rlRootView;
    private String colorBlack = "#000000", colorWhite = "#FFFFFF";
    private SettingsModel mSettingModel;
    private boolean hasSelectWebLogo = false, hasSelectedMobileLogo = false, isCopyingSettings = false;
    private AlertDialog mAlertSavePromptDialog, mAlertSuccessPromptDialog;
    CheckBox chCopyDesign;
    Button btnApplyDesign;
    private ProgressBar mProgressBarWebLogo, mProgressBarMobLogo;

    private BrandArrayAdapter mBrandArrayAdapter;
    private CampaignArrayAdapter mCampaignArrayAdapter;
    private CampaignEventArrayAdapter mCampaignEventArrayAdapter;
    private ArrayList<BrandModel> mBrandModelList;
    private ArrayList<CampaignModel> mCampaignModelList;
    private ArrayList<CampaignEventModel> mCampaignEventModelList;
    private String mSelectedBrandCode, mSelectedCampaignCode, mSelectedCampaignEventCode;
    String mSelectedBrandName, mSelectedCampaignName, mSelectedCampaignEventName;
    Spinner spnBrands, spnCampaigns, spnEvent;
    String defaultFooterText;
    ImageView imgDeleteWebLogo, imgDeleteMobLogo;

    private MaintenancePresenter mMaintenancePresenter;
    private boolean mHasChanges = false;
    private RelativeLayout rlDesignInactiveLayout, rlCopyDesignInactiveLayout;
    View vCopyDesignSeparator;
    private DefaultSettingModel mDefaultSettingModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_design, container, false);
        setRetainInstance(true);
        mDatabaseHelper = new DatabaseHelper(getActivity());
        SharedPrefHelper.init(getActivity());
        RestManager.init();
        DialogHelper.init(getActivity());

        email = SharedPrefHelper.getInstance().getEmailFromShared();
        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();
        formattedEventName = SharedPrefHelper.getInstance().getFormattedCampaignNameFromShared();

        mMaintenancePresenter = (MaintenancePresenter) getActivity();

        Log.e(TAG, "onCreateView: BrandCode: " + brandCode + " CampaignCode: " + campaignCode + " EventCode: " + eventCode);

        setupViews();
        Log.e(TAG, "onCreateView: " + (savedInstanceState == null));
        if (savedInstanceState == null) {
            isCopyingDesign(chCopyDesign.isChecked());
            configureSettings(false);
            callBrandAPI();
        }

        /**Initialize default settings*/
        mDefaultSettingModel = mDatabaseHelper.getDefaultSettingsByBrand(brandCode);

        return mView;
    }

    private void configureSettings(boolean isSaving) {
        if (!brandCode.isEmpty() && !campaignCode.isEmpty() && !eventCode.isEmpty()) {
            Log.e(TAG, "configureSettings: SettingIsExist? " +
                    mDatabaseHelper.checkSettingsIfExist(brandCode, campaignCode, eventCode, formattedEventName));

            if (!mDatabaseHelper.checkSettingsIfExist(brandCode, campaignCode, eventCode, formattedEventName)) {
                Log.e(TAG, "configureSettings: " + "DB Setting not exist");
                createInitialSettings();
            } else {
                callGetSettingAPI(isSaving);
            }

            mSettingModel = mDatabaseHelper.getDbSetting(brandCode, campaignCode, eventCode, formattedEventName);
            Log.e(TAG, "configureSettings: " + mSettingModel.toString());
        }
    }

    private void synchronize() {
        isCopyingSettings = false;
        hasSelectWebLogo = false;
        hasSelectedMobileLogo = false;
        mHasChanges = false;

        mSelectedBrandCode = null;
        mSelectedCampaignCode = null;
        mSelectedCampaignEventCode = null;

        isCopyingDesign(false);
        chCopyDesign.setChecked(false);

        mBrandModelList.clear();
        setupInitialListPromptsOnResponse(Constants.BRAND_LIST_PROMPT);
        mBrandArrayAdapter.notifyDataSetChanged();

        mCampaignModelList.clear();
        setupInitialListPromptsOnResponse(Constants.CAMPAIGN_LIST_PROMPT);
        mCampaignArrayAdapter.notifyDataSetChanged();

        mCampaignEventModelList.clear();
        setupInitialListPromptsOnResponse(Constants.EVENT_LIST_PROMPT);
        mCampaignEventArrayAdapter.notifyDataSetChanged();

        if (NetworkUtils.isNetworkConnected(getActivity())) {
            if (!brandCode.isEmpty() && !campaignCode.isEmpty() && !eventCode.isEmpty()) {
                callGetSettingAPI(false);
                callBrandAPI();
            }
        } else {
            Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void createInitialSettings() {
        Log.e(TAG, "createInitialSettings: ");
        mDatabaseHelper.createDbSetting(defaultSettings());
    }

    private void callGetSettingAPI(final boolean isSaving) {
        Log.e(TAG, "callGetSettingAPI: ");
        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Synchronize settings");
        Call<List<SettingsModel>> getSettingCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .getSetting(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_GET_SETTING, campaignCode,
                        brandCode, eventCode, formattedEventName);
        getSettingCall.enqueue(new Callback<List<SettingsModel>>() {
            @Override
            public void onResponse(Call<List<SettingsModel>> call, Response<List<SettingsModel>> response) {
                int responseCode = response.code();
                Log.e(TAG, "onResponse: " + responseCode);
                switch (responseCode) {
                    case Constants.API_RESPONSE_OK:
                        List<SettingsModel> settingsModelList = response.body();
                        Log.e(TAG, "onResponse: " + settingsModelList.size());
                        if (settingsModelList.size() > 0) {
                            for (SettingsModel settingsModel : settingsModelList) {
                                if (mDatabaseHelper.checkSettingsIfExist(brandCode, campaignCode, eventCode, formattedEventName)) {
                                    Log.e(TAG, "onResponse: " + "DB Setting exist");
                                    mDatabaseHelper.updateDbSetting(settingsModel);
                                } else {
                                    Log.e(TAG, "onResponse: " + "DB Setting not exist");
                                    mDatabaseHelper.createDbSetting(settingsModel);
                                }
                            }
                            mSettingModel = mDatabaseHelper.getDbSetting(brandCode, campaignCode, eventCode, formattedEventName);

                            if (isSaving) {
                                mMaintenancePresenter.showFormPage();
                            } else {
                                onResume();
                            }
                        }
                        break;
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }

            @Override
            public void onFailure(Call<List<SettingsModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                DialogHelper.getInstance().hideProgressDialog(getActivity());
                mSettingModel = mDatabaseHelper.getDbSetting(brandCode, campaignCode, eventCode, formattedEventName);
                onResume();
            }
        });
    }

    private void setupViews() {
        /**Views*/
        imgWebLogo = (ImageView) mView.findViewById(R.id.imgWebLogo);
        imgMobileLogo = (ImageView) mView.findViewById(R.id.imgMobileLogo);
        imgSynchronize = (ImageView) mView.findViewById(R.id.imgSynchornize);

        btnDefaultForegroundColor = (Button) mView.findViewById(R.id.btnDefaultForeground);
        btnDefaultBackgroundColor = (Button) mView.findViewById(R.id.btnDefaultBackground);
        btnDefaultHeaderColor = (Button) mView.findViewById(R.id.btnDefaultHeader);
        btnDefaultFooterColor = (Button) mView.findViewById(R.id.btnDefaultFooter);

        btnSetForegroundColor = (Button) mView.findViewById(R.id.btnSetForegroundColor);
        btnSetBackgroundColor = (Button) mView.findViewById(R.id.btnSetBackgroundColor);
        btnSetHeaderColor = (Button) mView.findViewById(R.id.btnSetHeaderColor);
        btnSetFooterColor = (Button) mView.findViewById(R.id.btnSetFooterColor);
        btnUploadWebLogo = (Button) mView.findViewById(R.id.btnUploadWebLogo);
        btnSetWebLogoAlignment = (Button) mView.findViewById(R.id.btnSetWebLogo);
        btnUploadMobileLogo = (Button) mView.findViewById(R.id.btnUploadMobileLogo);
        btnSetMobileLogoAlignment = (Button) mView.findViewById(R.id.btnSetMobileLogo);
        btnSetSubmitButton = (Button) mView.findViewById(R.id.btnSetSubmitButton);
        btnSave = (Button) mView.findViewById(R.id.btnSave);
        btnSetFont = (Button) mView.findViewById(R.id.btnSetFonts);
        btnReset = (Button) mView.findViewById(R.id.btnReset);
        edtFooter = (EditText) mView.findViewById(R.id.edtFooter);

        imgDeleteWebLogo = (ImageView) mView.findViewById(R.id.imgDeleteWebLogo);
        imgDeleteMobLogo = (ImageView) mView.findViewById(R.id.imgDeleteMobLogo);
        mProgressBarWebLogo = (ProgressBar) mView.findViewById(R.id.pbWebLogo);
        mProgressBarMobLogo = (ProgressBar) mView.findViewById(R.id.pbMobLogo);

        chCopyDesign = (CheckBox) mView.findViewById(R.id.chCopyDesign);
        btnApplyDesign = (Button) mView.findViewById(R.id.btnApplyDesign);
        spnBrands = (Spinner) mView.findViewById(R.id.spnBrands);
        spnCampaigns = (Spinner) mView.findViewById(R.id.spnCampaigns);
        spnEvent = (Spinner) mView.findViewById(R.id.spnCampaignEvents);

        rlDesignInactiveLayout = (RelativeLayout) mView.findViewById(R.id.rlDesignInActiveLayout);
        rlCopyDesignInactiveLayout = (RelativeLayout) mView.findViewById(R.id.rlCopyDesignInActiveLayout);

        rlRootView = (RelativeLayout) mView.findViewById(R.id.rlRootView);
        vCopyDesignSeparator = mView.findViewById(R.id.copySeparator);

        /**Listeners*/
        btnDefaultForegroundColor.setOnClickListener(this);
        btnDefaultBackgroundColor.setOnClickListener(this);
        btnDefaultHeaderColor.setOnClickListener(this);
        btnDefaultFooterColor.setOnClickListener(this);

        btnSetForegroundColor.setOnClickListener(this);
        btnSetBackgroundColor.setOnClickListener(this);
        btnSetHeaderColor.setOnClickListener(this);
        btnSetFooterColor.setOnClickListener(this);
        btnUploadWebLogo.setOnClickListener(this);
        btnSetWebLogoAlignment.setOnClickListener(this);
        btnUploadMobileLogo.setOnClickListener(this);
        btnSetMobileLogoAlignment.setOnClickListener(this);
        btnSetSubmitButton.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnReset.setOnClickListener(this);
        btnSetFont.setOnClickListener(this);
        imgSynchronize.setOnClickListener(this);
        imgDeleteWebLogo.setOnClickListener(this);
        imgDeleteMobLogo.setOnClickListener(this);

        chCopyDesign.setOnClickListener(this);
        btnApplyDesign.setOnClickListener(this);
        edtFooter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SharedPrefHelper.getInstance().putFooterTextToShared(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        rlRootView.setOnTouchListener(this);

        defaultFooterText = getActivity().getResources().getString(R.string.default_footer_text);

        /**Setting up lists*/
        mBrandModelList = new ArrayList<>();
        setupInitialListPrompt(Constants.BRAND_LIST_PROMPT);
        mBrandArrayAdapter = new BrandArrayAdapter(getActivity(), R.layout.list_spinner_default, mBrandModelList);
        spnBrands.setAdapter(mBrandArrayAdapter);

        mCampaignModelList = new ArrayList<>();
        mCampaignArrayAdapter = new CampaignArrayAdapter(getActivity(), R.layout.list_spinner_default, mCampaignModelList);
        spnCampaigns.setAdapter(mCampaignArrayAdapter);

        mCampaignEventModelList = new ArrayList<>();
        mCampaignEventArrayAdapter = new CampaignEventArrayAdapter(getActivity(), R.layout.list_spinner_default, mCampaignEventModelList);
        spnEvent.setAdapter(mCampaignEventArrayAdapter);

        int copyDesignSeparatorWidth = getActivity().getResources().getDimensionPixelSize(R.dimen.l_padding_left_copy_design_separator);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(copyDesignSeparatorWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
        vCopyDesignSeparator.setLayoutParams(layoutParams);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnDefaultForeground:
                btnSetForegroundColor.setBackgroundColor(ConverterUtil.hexColorToInt("", TAG, Constants.SETTING_TYPE_TEXTCOLOR));
                mSettingModel.setForeground("");
                break;

            case R.id.btnDefaultBackground:
                btnSetBackgroundColor.setBackgroundColor(ConverterUtil.hexColorToInt("", TAG, Constants.SETTING_TYPE_NONE));
                mSettingModel.setBackground("");
                break;

            case R.id.btnDefaultHeader:
                btnSetHeaderColor.setBackgroundColor(ConverterUtil.hexColorToInt("", TAG, Constants.SETTING_TYPE_NONE));
                mSettingModel.setHeader("");
                break;

            case R.id.btnDefaultFooter:
                btnSetFooterColor.setBackgroundColor(ConverterUtil.hexColorToInt("", TAG, Constants.SETTING_TYPE_NONE));
                mSettingModel.setFooter("");
                break;

            case R.id.btnSetForegroundColor:
                mMaintenancePresenter.showColorPickerDialog(Constants.SET_FOREGROUND_COLOR);
                break;

            case R.id.btnSetBackgroundColor:
                mMaintenancePresenter.showColorPickerDialog(Constants.SET_BACKGROUND_COLOR);
                break;

            case R.id.btnSetHeaderColor:
                mMaintenancePresenter.showColorPickerDialog(Constants.SET_HEADER_COLOR);
                break;

            case R.id.btnSetFooterColor:
                mMaintenancePresenter.showColorPickerDialog(Constants.SET_FOOTER_COLOR);
                break;

            case R.id.btnUploadWebLogo:
                selectImageFromGallery(Constants.PICK_WEB_LOGO);
                break;

            case R.id.btnSetWebLogo:
                mMaintenancePresenter.showImageAlignmentDialog(Constants.SET_WEB_LOGO_ALIGNMENT, mSettingModel);
                break;

            case R.id.btnUploadMobileLogo:
                selectImageFromGallery(Constants.PICK_MOBILE_LOGO);
                break;

            case R.id.btnSetMobileLogo:
                mMaintenancePresenter.showImageAlignmentDialog(Constants.SET_MOBILE_LOGO_ALIGNMENT, mSettingModel);
                break;

            case R.id.btnSetFonts:
                mMaintenancePresenter.showDesignFontsDialog(mSettingModel);
                break;

            case R.id.btnSetSubmitButton:
                mMaintenancePresenter.showSubmitButtonDialog(mSettingModel);
                break;

            case R.id.imgDeleteWebLogo:
                if (mSettingModel.getWebLogoImg().isEmpty()) {
                    mMaintenancePresenter
                            .showAlertDialog("Alert",
                                    "Cannot remove default Logo!",
                                    Constants.DIALOG_ACTION_NORMAL);
                } else {
                    mSettingModel.setWebLogoImg("");
                    hasSelectWebLogo = true;
                    Glide.with(getActivity())
                            .load(mDefaultSettingModel.getImage())
                            .into(imgWebLogo);
                    isCopyingSettings = true;
                }
                break;

            case R.id.imgDeleteMobLogo:
                if (mSettingModel.getMobLogoImg().isEmpty()) {
                    mMaintenancePresenter
                            .showAlertDialog("Alert",
                                    "Cannot remove default Logo!",
                                    Constants.DIALOG_ACTION_NORMAL);
                } else {
                    mSettingModel.setMobLogoImg("");
                    hasSelectedMobileLogo = true;
                    Glide.with(getActivity())
                            .load(mDefaultSettingModel.getImage())
                            .into(imgMobileLogo);
                    isCopyingSettings = true;
                }

                break;

            case R.id.imgSynchornize:
                synchronize();
                break;

            case R.id.chCopyDesign:
                isCopyingDesign(chCopyDesign.isChecked());
                break;

            case R.id.btnSave:
                mSettingModel.setFooterText(edtFooter.getText().toString());
                mSettingModel.setDateTime(Utils.getDateTime());

                if (chCopyDesign.isChecked()) {
                    if (mSelectedBrandCode == null || mSelectedBrandCode.isEmpty()) {
                        mMaintenancePresenter.showAlertDialog("Alert", "Please select Brand", Constants.DIALOG_ACTION_NORMAL);

                    } else if (mSelectedCampaignCode == null || mSelectedCampaignCode.isEmpty()) {
                        mMaintenancePresenter.showAlertDialog("Alert", "Please select Campaign", Constants.DIALOG_ACTION_NORMAL);

                    } else if (mSelectedCampaignEventCode == null || mSelectedCampaignEventCode.isEmpty()) {
                        mMaintenancePresenter.showAlertDialog("Alert", "Please select Initiative", Constants.DIALOG_ACTION_NORMAL);
                    } else {
                        mMaintenancePresenter.showDesignFragmentSavePrompt("Confirm", "Are you sure you want to save?",
                                Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT, mSettingModel, hasSelectedMobileLogo, hasSelectWebLogo, isCopyingSettings);
                    }
                } else {
                    if (!hasChanges()) {
                        mMaintenancePresenter.showAlertDialog("Alert", "No changes detected", Constants.DIALOG_ACTION_NO_DESIGN_CHANGES);
                    } else {
                        mMaintenancePresenter.showDesignFragmentSavePrompt("Confirm", "Are you sure you want to save?",
                                Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT, mSettingModel, hasSelectedMobileLogo, hasSelectWebLogo, isCopyingSettings);
                    }
                }
                break;

            case R.id.btnReset:
//                synchronizeForm();
                mMaintenancePresenter.showDesignFragmentSavePrompt("Confirm", "Are you sure you want to reset the current set-up?",
                        Constants.DIALOG_RESET_DESIGN_SETTINGS, mSettingModel, hasSelectedMobileLogo, hasSelectWebLogo, isCopyingSettings);

                break;

            case R.id.btnApplyDesign:
                if (mSelectedBrandCode == null || mSelectedBrandCode.isEmpty()) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please select Brand", Constants.DIALOG_ACTION_NORMAL);

                } else if (mSelectedCampaignCode == null || mSelectedCampaignCode.isEmpty()) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please select Campaign", Constants.DIALOG_ACTION_NORMAL);

                } else if (mSelectedCampaignEventCode == null || mSelectedCampaignEventCode.isEmpty()) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please select Initiative", Constants.DIALOG_ACTION_NORMAL);
                } else {
                    copyDesignFromAPI();
                }
                break;
        }
    }

    public void selectImageFromGallery(int requestCode) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestCode);
    }

    public void setImageAlignment(SettingsModel settingsModel) {
        mSettingModel = settingsModel;
    }

    public void setColors(int type, int color, String hexColor) {
        switch (type) {
            case Constants.SET_FOREGROUND_COLOR:
                btnSetForegroundColor.setBackgroundColor(color);
//                btnSetForegroundColor.setTextColor(ConverterUtil.hexColorToInt(colorBlack));
                mSettingModel.setForeground(hexColor);
                break;

            case Constants.SET_BACKGROUND_COLOR:
                btnSetBackgroundColor.setBackgroundColor(color);
                mSettingModel.setBackground(hexColor);
                break;

            case Constants.SET_HEADER_COLOR:
                btnSetHeaderColor.setBackgroundColor(color);
                mSettingModel.setHeader(hexColor);
                break;

            case Constants.SET_FOOTER_COLOR:
                btnSetFooterColor.setBackgroundColor(color);
                mSettingModel.setFooter(hexColor);
                break;

            case Constants.SET_SUBMIT_BUTTON_COLOR:
                mSettingModel.setSubmitBackground(hexColor);
                mMaintenancePresenter.showSubmitButtonDialog(mSettingModel);
                break;
        }
    }

    public void setFonts(SettingsModel settingsModel) {
        mSettingModel = settingsModel;
    }

    public void saveDesign() {
        isCopyingSettings = false;
        hasSelectWebLogo = false;
        hasSelectedMobileLogo = false;
        mHasChanges = false;

        isCopyingSettings = false;
        configureSettings(true);
    }

    public void isCopyingDesign(boolean status) {
        if (status) {
            rlDesignInactiveLayout.setVisibility(View.VISIBLE);
            btnSetFont.setEnabled(false);
            btnSetSubmitButton.setEnabled(false);

            rlCopyDesignInactiveLayout.setVisibility(View.GONE);
            btnApplyDesign.setEnabled(true);
        } else {
            rlDesignInactiveLayout.setVisibility(View.GONE);
            btnSetFont.setEnabled(true);
            btnSetSubmitButton.setEnabled(true);

            rlCopyDesignInactiveLayout.setVisibility(View.VISIBLE);
            btnApplyDesign.setEnabled(false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && data.getData() != null) {
            Uri uri = data.getData();
            Log.e(TAG, "onActivityResult: " + CheckerUtils.checkMimeType(getActivity(), uri));


            switch (requestCode) {
                case Constants.PICK_WEB_LOGO:
                    if (resultCode == Activity.RESULT_OK) {
                        String mimeType = CheckerUtils.checkMimeType(getActivity(), uri);
                        switch (mimeType) {
                            case "image/jpeg": {
                                Utils.loadImageFromURI(getActivity(), imgWebLogo, uri, mProgressBarWebLogo);
                                mSettingModel.setWebLogoImg(uri.toString());
                                hasSelectWebLogo = true;
                                File file = new File(Utils.getPath(getActivity(), uri));
                                String filename = file.getName();
                                String dateTime = Utils.getDateTime();
                                mSettingModel.setWebLogoImgFileName(dateTime.replace(" ", "").replace(":", "").toString() + "_" + filename);
                                Log.e(TAG, "onActivityResult: " + mSettingModel.getWebLogoImg());
                                Log.e(TAG, "onActivityResult: " + mSettingModel.getWebLogoImgFileName());
                            }
                            break;

                            case "image/png": {
                                Utils.loadImageFromURI(getActivity(), imgWebLogo, uri, mProgressBarWebLogo);
                                mSettingModel.setWebLogoImg(uri.toString());
                                hasSelectWebLogo = true;
                                File file = new File(Utils.getPath(getActivity(), uri));
                                String filename = file.getName();
                                String dateTime = Utils.getDateTime();
                                mSettingModel.setWebLogoImgFileName(dateTime.replace(" ", "").replace(":", "").toString() + "_" + filename);
                                Log.e(TAG, "onActivityResult: " + mSettingModel.getWebLogoImg());
                                Log.e(TAG, "onActivityResult: " + mSettingModel.getWebLogoImgFileName());
                            }
                            break;

                            default:
                                mMaintenancePresenter.showAlertDialog("Alert", "Invalid file format. System accepts .jpg or .png file only.",
                                        Constants.DIALOG_ACTION_NORMAL);
                                break;
                        }

                    }
                    break;

                case Constants.PICK_MOBILE_LOGO:
                    if (resultCode == Activity.RESULT_OK) {
                        String mimeType = CheckerUtils.checkMimeType(getActivity(), uri);
                        switch (mimeType) {
                            case "image/jpeg": {
                                Utils.loadImageFromURI(getActivity(), imgMobileLogo, uri, mProgressBarMobLogo);
                                mSettingModel.setMobLogoImg(uri.toString());
                                hasSelectedMobileLogo = true;

                                File file = new File(Utils.getPath(getActivity(), uri));
                                String filename = file.getName();
                                String dateTime = Utils.getDateTime();
                                mSettingModel.setMobLogoImgFileName(dateTime.replace(" ", "").toString() + "_" + filename);
                                Log.e(TAG, "onActivityResult: " + mSettingModel.getMobLogoImg());
                                Log.e(TAG, "onActivityResult: " + mSettingModel.getMobLogoImgFileName());
                            }
                            break;

                            case "image/png": {
                                Utils.loadImageFromURI(getActivity(), imgMobileLogo, uri, mProgressBarMobLogo);
                                mSettingModel.setMobLogoImg(uri.toString());
                                hasSelectedMobileLogo = true;

                                File file = new File(Utils.getPath(getActivity(), uri));
                                String filename = file.getName();
                                String dateTime = Utils.getDateTime();
                                mSettingModel.setMobLogoImgFileName(dateTime.replace(" ", "").toString() + "_" + filename);
                                Log.e(TAG, "onActivityResult: " + mSettingModel.getMobLogoImg());
                                Log.e(TAG, "onActivityResult: " + mSettingModel.getMobLogoImgFileName());
                            }
                            break;

                            default:
                                mMaintenancePresenter.showAlertDialog("Alert", "Invalid file format. System accepts .jpg or .png file only.",
                                        Constants.DIALOG_ACTION_NORMAL);
                                break;
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.rlRootView:
                Utils.hideKeyboard(getActivity());
                break;
        }
        return false;
    }

    private void applyDBSetting() {
        Log.e(TAG, "applyDBSetting: " + mSettingModel.toString());

//        if (mSettingModel.getForeground().equals("#000000")) {
//            btnSetForegroundColor.setTextColor(ConverterUtil.hexColorToInt(colorWhite));
//        } else {
//            btnSetForegroundColor.setTextColor(ConverterUtil.hexColorToInt(colorBlack));
//        }

        btnSetForegroundColor.setBackgroundColor(ConverterUtil.hexColorToInt(mSettingModel.getForeground(), TAG, Constants.SETTING_TYPE_NONE));
        btnSetBackgroundColor.setBackgroundColor(ConverterUtil.hexColorToInt(mSettingModel.getBackground(), TAG, Constants.SETTING_TYPE_NONE));
        btnSetHeaderColor.setBackgroundColor(ConverterUtil.hexColorToInt(mSettingModel.getHeader(), TAG, Constants.SETTING_TYPE_NONE));
        btnSetFooterColor.setBackgroundColor(ConverterUtil.hexColorToInt(mSettingModel.getFooter(), TAG, Constants.SETTING_TYPE_NONE));
        edtFooter.setText(mSettingModel.getFooterText());

        /**Web Logo*/
        if (mSettingModel.getWebLogoImg() != null && !mSettingModel.getWebLogoImg().isEmpty()) {
            if ((mSettingModel.getWebLogoImg().startsWith("content://media/"))) {
                Glide.with(MaintenanceActivity.mainActivityContext)
                        .load(mSettingModel.getWebLogoImg())
                        .into(imgWebLogo);
            } else {
                Glide.with(MaintenanceActivity.mainActivityContext)
                        .load(Constants.API_WEB_BASE_URL3 + mSettingModel.getWebLogoImg())
                        .into(imgWebLogo);
            }
        } else {
            Glide.with(MaintenanceActivity.mainActivityContext)
                    .load(mDefaultSettingModel.getImage())
                    .into(imgWebLogo);
        }

        /**Mobile Logo*/
        if (mSettingModel.getMobLogoImg() != null && !mSettingModel.getMobLogoImg().isEmpty()) {
            if ((mSettingModel.getMobLogoImg().startsWith("content://media/"))) {
                Glide.with(MaintenanceActivity.mainActivityContext)
                        .load(mSettingModel.getMobLogoImg())
                        .into(imgMobileLogo);
            } else {
                Glide.with(MaintenanceActivity.mainActivityContext)
                        .load(Constants.API_WEB_BASE_URL3 + mSettingModel.getMobLogoImg())
                        .into(imgMobileLogo);
            }
        } else {
            Glide.with(MaintenanceActivity.mainActivityContext)
                    .load(mDefaultSettingModel.getImage())
                    .into(imgMobileLogo);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: ");

        if (mSettingModel != null) {
            applyDBSetting();
        }
    }

    private boolean hasChanges() {

        SettingsModel settingsModel = mDatabaseHelper.getDbSetting(brandCode, campaignCode, eventCode, formattedEventName);
        Log.e(TAG, "hasChanges: SETTINGSMODEL: " + settingsModel.toString());
        Log.e(TAG, "hasChanges: mSETTINGSMODEL: " + mSettingModel.toString());

        /**Colors*/
        if (mSettingModel.getForeground() != null) {
            if (!mSettingModel.getForeground().equals(settingsModel.getForeground())) {
                mHasChanges = true;
            }
        }

        if (mSettingModel.getBackground() != null) {
            if (!mSettingModel.getBackground().equals(settingsModel.getBackground())) {
                mHasChanges = true;
            }
        }

        if (mSettingModel.getHeader() != null) {
            if (!mSettingModel.getHeader().equals(settingsModel.getHeader())) {
                mHasChanges = true;
            }
        }

        if (mSettingModel.getFooter() != null) {
            if (!mSettingModel.getFooter().equals(settingsModel.getFooter())) {
                mHasChanges = true;
            }
        }

        /**Label*/
        if (mSettingModel.getFooterText() != null) {
            if (!mSettingModel.getFooterText().equals(settingsModel.getFooterText())) {
                mHasChanges = true;
            }
        }

        /**Logo*/
        if (mSettingModel.getWebLogoImg() != null) {
            if (!mSettingModel.getWebLogoImg().equals(settingsModel.getWebLogoImg())) {
                mHasChanges = true;
            }
        } /*else {
            mSettingModel.setWebLogoImg(Utils.getDateTime());
            mHasChanges = true;
        }*/

        if (mSettingModel.getWebLogo() != null) {
            if (!mSettingModel.getWebLogo().equals(settingsModel.getWebLogo())) {
                mHasChanges = true;
            }
        }

        if (mSettingModel.getMobLogoImg() != null) {
            if (!mSettingModel.getMobLogoImg().equals(settingsModel.getMobLogoImg())) {
                mHasChanges = true;
            }
        } /*else {
            mSettingModel.setMobLogoImg(Utils.getDateTime());
            mHasChanges = true;
        }*/

        if (mSettingModel.getMobLogo() != null) {
            if (!mSettingModel.getMobLogo().equals(settingsModel.getMobLogo())) {
                mHasChanges = true;
            }
        }

        /**Fonts*/
        if (mSettingModel.getFontHeadfamily() != null) {
            if (!mSettingModel.getFontHeadfamily().equals(settingsModel.getFontHeadfamily())) {
                mHasChanges = true;
            }
        }

        if (mSettingModel.getFontHeadsize() != null) {
            if (!mSettingModel.getFontHeadsize().equals(settingsModel.getFontHeadsize())) {
                mHasChanges = true;
            }
        }

        if (mSettingModel.getFontBasefamily() != null) {
            if (!mSettingModel.getFontBasefamily().equals(settingsModel.getFontBasefamily())) {
                mHasChanges = true;
            }
        }

        if (mSettingModel.getFontBasesize() != null) {
            if (!mSettingModel.getFontBasesize().equals(settingsModel.getFontBasesize())) {
                mHasChanges = true;
            }
        }

        /**Submit Button*/
        if (mSettingModel.getSubmitBackground() != null) {
            if (!mSettingModel.getSubmitBackground().equals(settingsModel.getSubmitBackground())) {
                mHasChanges = true;
            }
        }

        if (mSettingModel.getSubmitBorderRadius() != null) {
            if (!mSettingModel.getSubmitBorderRadius().equals(settingsModel.getSubmitBorderRadius())) {
                mHasChanges = true;
            }
        }

        if (mSettingModel.getSubmitBorderWidth() != null) {
            if (!mSettingModel.getSubmitBorderWidth().equals(settingsModel.getSubmitBorderWidth())) {
                mHasChanges = true;
            }
        }

        if (mSettingModel.getSubmitAlignment() != null) {
            if (!mSettingModel.getSubmitAlignment().equals(settingsModel.getSubmitAlignment())) {
                mHasChanges = true;
            }
        }

        return mHasChanges;
    }

    private void callBrandAPI() {
        Log.e(TAG, "callBrandAPI: ");
        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Loading Brands");
        Call<List<BrandModel>> onGetBrandListsCall = MaintenanceApiProvider.getMaintenanceDbmApiRoutes()
                .onGetBrandLists(Constants.API_DBM_TOKEN, Constants.CMD_EVENT_GET_BRAND, email, Constants.DBM_HOSTNAME);
        onGetBrandListsCall.enqueue(new Callback<List<BrandModel>>() {

            @Override
            public void onResponse(Call<List<BrandModel>> call, Response<List<BrandModel>> response) {
                if (response.body() != null) {
                    List<BrandModel> brandModelList = response.body();

                    mBrandModelList.clear();
                    if (brandModelList.size() > 0) {
                        BrandModel brandModel = new BrandModel();
                        brandModel.setName("--Please select your brand--");
                        brandModel.setBrandCode(null);
                        mBrandModelList.add(brandModel);
                        mBrandModelList.addAll(brandModelList);

                        /**Sorting into ascending order*/
                        Collections.sort(mBrandModelList, new Comparator<BrandModel>() {
                            public int compare(BrandModel obj1, BrandModel obj2) {
                                return obj1.getName().compareToIgnoreCase(obj2.getName());
                            }
                        });

                    } else {
                        BrandModel brandModel = new BrandModel();
                        brandModel.setName("No brand found!");
                        brandModel.setBrandCode(null);
                        mBrandModelList.add(brandModel);
                    }
                    mBrandArrayAdapter.notifyDataSetChanged();
                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }

            @Override
            public void onFailure(Call<List<BrandModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }
        });
    }

    public void callCampaignAPI(final String brandCode, String brandName) {
        Log.e(TAG, "callCampaignAPI: BrandCode: " + brandCode + " BrandName: " + brandName);
        mSelectedBrandCode = brandCode;
        mSelectedBrandName = brandName;

        mCampaignModelList.clear();
        mCampaignArrayAdapter.notifyDataSetChanged();
        mSelectedCampaignCode = null;

        mCampaignEventModelList.clear();
        mCampaignEventArrayAdapter.notifyDataSetChanged();
        mSelectedCampaignEventCode = null;


        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Loading campaigns");
        Call<List<CampaignModel>> onGetCampaignList = MaintenanceApiProvider.getMaintenanceDbmApiRoutes()
                .onGetCampaignLists(Constants.API_DBM_TOKEN, Constants.CMD_EVENT_GET_CAMPAIGN, brandCode, Constants.DBM_HOSTNAME);
        onGetCampaignList.enqueue(new Callback<List<CampaignModel>>() {

            @Override
            public void onResponse(Call<List<CampaignModel>> call, Response<List<CampaignModel>> response) {
                if (response.body() != null) {
                    List<CampaignModel> campaignModelList = response.body();
                    Log.e(TAG, "onResponse: campaignModelListSize: " + campaignModelList.size());

                    mCampaignModelList.clear();
                    if (campaignModelList.size() > 0) {
                        CampaignModel campaignModel = new CampaignModel();
                        campaignModel.setName("--Please select your campaign--");
                        campaignModel.setCode(null);
                        mCampaignModelList.add(campaignModel);
                        mCampaignModelList.addAll(campaignModelList);

                        /**Sorting into ascending order*/
                        Collections.sort(mCampaignModelList, new Comparator<CampaignModel>() {
                            public int compare(CampaignModel obj1, CampaignModel obj2) {
                                return obj1.getName().compareToIgnoreCase(obj2.getName());
                            }
                        });

                    } else {
                        CampaignModel campaignModel = new CampaignModel();
                        campaignModel.setName("No campaign found!");
                        campaignModel.setCode(null);
                        mCampaignModelList.add(campaignModel);
                    }
                    mCampaignArrayAdapter.notifyDataSetChanged();
                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }

            @Override
            public void onFailure(Call<List<CampaignModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }
        });
    }

    public void callCampaignEventAPI(final String campaignCode, String campaignName) {
        mSelectedCampaignCode = campaignCode;
        mSelectedCampaignName = campaignName;

        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Loading Initiative");

        Call<List<CampaignEventModel>> onGetCampaignEventList = MaintenanceApiProvider.getMaintenanceDbmApiRoutes()
                .onGetCampaignEventLists(Constants.API_DBM_TOKEN, Constants.CMD_EVENT_GET_CAMPAIGN_EVENT,
                        mSelectedCampaignCode, Constants.DBM_HOSTNAME);
        onGetCampaignEventList.enqueue(new Callback<List<CampaignEventModel>>() {
            @Override
            public void onResponse(Call<List<CampaignEventModel>> call, Response<List<CampaignEventModel>> response) {
                if (response.body() != null) {
                    List<CampaignEventModel> campaignEventModels = response.body();
                    mCampaignEventModelList.clear();
                    if (campaignEventModels.size() > 0) {
                        CampaignEventModel campaignEventModel = new CampaignEventModel();
                        campaignEventModel.setName("--Please select your Initiative--");
                        campaignEventModel.setCode(null);
                        mCampaignEventModelList.add(campaignEventModel);
                        mCampaignEventModelList.addAll(campaignEventModels);

                        /**Sorting into ascending order*/
                        Collections.sort(mCampaignEventModelList, new Comparator<CampaignEventModel>() {
                            public int compare(CampaignEventModel obj1, CampaignEventModel obj2) {
                                return obj1.getName().compareToIgnoreCase(obj2.getName());
                            }
                        });

                    } else {
                        CampaignEventModel campaignEventModel = new CampaignEventModel();
                        campaignEventModel.setName("No event found!");
                        campaignEventModel.setCode(null);
                        mCampaignEventModelList.add(campaignEventModel);
                    }
                    mCampaignEventArrayAdapter.notifyDataSetChanged();
                    DialogHelper.getInstance().hideProgressDialog(getActivity());
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }

            @Override
            public void onFailure(Call<List<CampaignEventModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(getActivity());
            }
        });
    }

    public void saveConfiguredSettings(String campaignEventCode, String campaignEventName) {
        Log.e(TAG, "saveConfiguredSettings: ");
        mSelectedCampaignEventCode = campaignEventCode;
        mSelectedCampaignEventName = campaignEventName;

        Log.e(TAG, "saveConfiguredSettings: Selected Brand Code: " + mSelectedBrandCode);
        Log.e(TAG, "saveConfiguredSettings: Selected Brand Name: " + mSelectedBrandName);
        Log.e(TAG, "saveConfiguredSettings: Selected Campaign Code: " + mSelectedCampaignCode);
        Log.e(TAG, "saveConfiguredSettings: Selected Campaign Name: " + mSelectedCampaignName);
        Log.e(TAG, "saveConfiguredSettings: Selected Campaign Event Code: " + mSelectedCampaignEventCode);
        Log.e(TAG, "saveConfiguredSettings: Selected Campaign Event Name: " + mSelectedCampaignEventName);

    }

    private void copyDesignFromAPI() {
        Log.e(TAG, "callGetSettingAPI: ");
        DialogHelper.getInstance().hideProgressDialog(getActivity());
        DialogHelper.getInstance().showProgressDialog(getActivity(), "Synchronize settings");
        Call<List<SettingsModel>> getSettingCall = MaintenanceApiProvider.getMaintenanceWebApiRoutes()
                .getSetting(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_GET_SETTING, mSelectedCampaignCode,
                        mSelectedBrandCode, mSelectedCampaignEventCode, formattedEventName);
        getSettingCall.enqueue(new Callback<List<SettingsModel>>() {
            @Override
            public void onResponse(Call<List<SettingsModel>> call, Response<List<SettingsModel>> response) {
                int responseCode = response.code();
                Log.e(TAG, "onResponse: " + responseCode);
                switch (responseCode) {
                    case Constants.API_RESPONSE_OK:
                        List<SettingsModel> settingsModelList = response.body();
                        Log.e(TAG, "onResponse: copyDesignFromAPI: " + settingsModelList.size());
                        hasSelectWebLogo = true;
                        hasSelectedMobileLogo = true;
                        isCopyingSettings = true;
                        mHasChanges = true;

                        if (settingsModelList.size() > 0) {
                            for (SettingsModel settingsModel : settingsModelList) {
                                Log.e(TAG, "onResponse: copyDesignFromAPI: " + settingsModel.toString());
                                mSettingModel = settingsModel;
                            }
                        } else {
                            mSettingModel = defaultSettings();
                        }
                        onResume();
                        break;
                }
                DialogHelper.getInstance().hideProgressDialog(getActivity());
                mMaintenancePresenter.showAlertDialog("Confirm", "Design has been successfully copied.",
                        Constants.DIALOG_ACTION_NORMAL);
            }

            @Override
            public void onFailure(Call<List<SettingsModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                DialogHelper.getInstance().hideProgressDialog(getActivity());
                mSettingModel = mDatabaseHelper.getDbSetting(brandCode, campaignCode, eventCode, formattedEventName);
                onResume();
            }
        });
    }

    private void setupInitialListPromptsOnResponse(int listType) {

        switch (listType) {
            case Constants.BRAND_LIST_PROMPT:
                BrandModel brandModel = new BrandModel();
                if (mSelectedBrandCode != null) {
                    brandModel.setName(mSelectedBrandName);
                    brandModel.setBrandCode(mSelectedBrandCode);

                } else {
                    brandModel.setName("--Please select your brand--");
                    brandModel.setBrandCode(null);
                }
                mBrandModelList.add(brandModel);
                break;

            case Constants.CAMPAIGN_LIST_PROMPT:
                CampaignModel campaignModel = new CampaignModel();
                if (mSelectedCampaignCode != null) {
                    campaignModel.setName(mSelectedCampaignName);
                    campaignModel.setCode(mSelectedCampaignCode);

                } else {
                    campaignModel.setName("--Please select your campaign--");
                    campaignModel.setCode(null);
                }
                mCampaignModelList.add(campaignModel);
                break;

            case Constants.EVENT_LIST_PROMPT:
                CampaignEventModel campaignEventModel = new CampaignEventModel();
                if (mSelectedCampaignEventCode != null) {
                    campaignEventModel.setCode(mSelectedCampaignEventCode);
                    campaignEventModel.setName(mSelectedCampaignEventName);

                } else {
                    campaignEventModel.setName("--Please select your event--");
                    campaignEventModel.setCode(null);
                }

                mCampaignEventModelList.add(campaignEventModel);
                break;
        }
    }

    private void setupInitialListPrompt(int listType) {
        switch (listType) {
            case Constants.BRAND_LIST_PROMPT:
                BrandModel brandModel = new BrandModel();
                brandModel.setName("--Please select your brand--");
                brandModel.setBrandCode(null);
                mBrandModelList.add(brandModel);
                break;

            case Constants.CAMPAIGN_LIST_PROMPT:
                CampaignModel campaignModel = new CampaignModel();
                campaignModel.setName("--Please select your campaign--");
                campaignModel.setCode(null);
                mCampaignModelList.add(campaignModel);
                break;

            case Constants.EVENT_LIST_PROMPT:
                CampaignEventModel campaignEventModel = new CampaignEventModel();
                campaignEventModel.setName("--Please select your event--");
                campaignEventModel.setCode(null);
                mCampaignEventModelList.add(campaignEventModel);
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.e(TAG, "onSaveInstanceState: " + (outState == null));

        outState.putString(Constants.STATE_SELECTED_BRAND_CODE, mSelectedBrandCode);
        outState.putString(Constants.STATE_SELECTED_BRAND_NAME, mSelectedBrandName);
        outState.putString(Constants.STATE_SELECTED_CAMPAIGN_CODE, mSelectedCampaignCode);
        outState.putString(Constants.STATE_SELECTED_CAMPAIGN_NAME, mSelectedCampaignName);
        outState.putString(Constants.STATE_SELECTED_EVENT_CODE, mSelectedCampaignEventCode);
        outState.putString(Constants.STATE_SELECTED_EVENT_NAME, mSelectedCampaignEventName);
        outState.putString(Constants.STATE_DEFAULT_FOOTER_TEXT, defaultFooterText);

        outState.putString(Constants.STATE_IS_COPYING, (isCopyingSettings) ? "1" : "0");
        outState.putString(Constants.STATE_HAS_WEB_LOGO, (hasSelectWebLogo) ? "1" : "0");
        outState.putString(Constants.STATE_HAS_MOBILE_LOGO, (hasSelectedMobileLogo) ? "1" : "0");

        outState.putBoolean(Constants.STATE_COPY_DESIGN_STATUS, chCopyDesign.isChecked());

        outState.putParcelable(Constants.STATE_SETTING_MODEL, mSettingModel);
        outState.putParcelableArrayList(Constants.STATE_BRAND_LIST, mBrandModelList);
        outState.putParcelableArrayList(Constants.STATE_CAMPAIGN_LIST, mCampaignModelList);
        outState.putParcelableArrayList(Constants.STATE_EVENT_LIST, mCampaignEventModelList);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.e(TAG, "onViewStateRestored: " + (savedInstanceState == null));

        if (savedInstanceState != null) {
            boolean copyDesignStatus = savedInstanceState.getBoolean(Constants.STATE_COPY_DESIGN_STATUS);
            chCopyDesign.setChecked(copyDesignStatus);
            isCopyingDesign(copyDesignStatus);

            mSelectedBrandCode = savedInstanceState.getString(Constants.STATE_SELECTED_BRAND_CODE);
            mSelectedBrandName = savedInstanceState.getString(Constants.STATE_SELECTED_BRAND_NAME);
            mSelectedCampaignCode = savedInstanceState.getString(Constants.STATE_SELECTED_CAMPAIGN_CODE);
            mSelectedCampaignName = savedInstanceState.getString(Constants.STATE_SELECTED_CAMPAIGN_NAME);
            mSelectedCampaignEventCode = savedInstanceState.getString(Constants.STATE_SELECTED_EVENT_CODE);
            mSelectedCampaignEventName = savedInstanceState.getString(Constants.STATE_SELECTED_EVENT_NAME);
            defaultFooterText = savedInstanceState.getString(Constants.STATE_DEFAULT_FOOTER_TEXT);

            isCopyingSettings = (savedInstanceState.getString(Constants.STATE_IS_COPYING).equals("1"));
            hasSelectWebLogo = (savedInstanceState.getString(Constants.STATE_HAS_WEB_LOGO).equals("1"));
            hasSelectedMobileLogo = (savedInstanceState.getString(Constants.STATE_HAS_MOBILE_LOGO).equals("1"));

            mSettingModel = savedInstanceState.getParcelable(Constants.STATE_SETTING_MODEL);

            ArrayList<BrandModel> brandListFromParcel = savedInstanceState.getParcelableArrayList(Constants.STATE_BRAND_LIST);
            mBrandModelList.clear();
            mBrandModelList.addAll(brandListFromParcel);
            mBrandArrayAdapter.notifyDataSetChanged();

            ArrayList<CampaignModel> campaignListFromParcel = savedInstanceState.getParcelableArrayList(Constants.STATE_CAMPAIGN_LIST);
            mCampaignModelList.clear();
            mCampaignModelList.addAll(campaignListFromParcel);
            mCampaignArrayAdapter.notifyDataSetChanged();

            ArrayList<CampaignEventModel> campaignEventFromParcel = savedInstanceState.getParcelableArrayList(Constants.STATE_EVENT_LIST);
            mCampaignEventModelList.clear();
            mCampaignEventModelList.addAll(campaignEventFromParcel);
            mCampaignEventArrayAdapter.notifyDataSetChanged();

            Log.e(TAG, "onViewStateRestored: Selected Brand Code: " + mSelectedBrandCode);
            Log.e(TAG, "onViewStateRestored: Selected Brand Name: " + mSelectedBrandName);
            Log.e(TAG, "onViewStateRestored: Selected Campaign Code: " + mSelectedCampaignCode);
            Log.e(TAG, "onViewStateRestored: Selected Campaign Name: " + mSelectedCampaignName);
            Log.e(TAG, "onViewStateRestored: Selected Campaign Event Code: " + mSelectedCampaignEventCode);
            Log.e(TAG, "onViewStateRestored: Selected Campaign Event Name: " + mSelectedCampaignEventName);
            Log.e(TAG, "onViewStateRestored: Is Copying Design: " + isCopyingSettings);
            Log.e(TAG, "onViewStateRestored: Has selected web logo: " + hasSelectWebLogo);
            Log.e(TAG, "onViewStateRestored: Has selected mobile logo: " + hasSelectedMobileLogo);
            Log.e(TAG, "onViewStateRestored: Default Footer Text: " + defaultFooterText);
        }
    }

    private SettingsModel defaultSettings() {
        SettingsModel settingsModel = new SettingsModel();
        settingsModel.setId("1");
        settingsModel.setBrandCode(brandCode);
        settingsModel.setCampaignCode(campaignCode);
        settingsModel.setEventCode(eventCode);
        settingsModel.setFormattedEventName(formattedEventName);
        settingsModel.setForeground("");
        settingsModel.setBackground("");
        settingsModel.setHeader("");
        settingsModel.setFooter("");
        settingsModel.setFooterText(defaultFooterText);
        settingsModel.setWebLogo(Constants.CENTER_ALIGNMENT);
        settingsModel.setWebLogoImg("");
        settingsModel.setMobLogo(Constants.CENTER_ALIGNMENT);
        settingsModel.setMobLogoImg("");
        settingsModel.setFontHeadfamily("Arial");
        settingsModel.setFontHeadsize("15");
        settingsModel.setFontBasefamily("Arial");
        settingsModel.setFontBasesize("15");
        settingsModel.setSubmitBorderWidth("1");
        settingsModel.setSubmitBorderRadius("1");
        settingsModel.setSubmitBackground(colorWhite);
        settingsModel.setSubmitAlignment(Constants.CENTER_ALIGNMENT);
        settingsModel.setIsActive("1");
        settingsModel.setDateTime(Utils.getDateTime());
        settingsModel.setStatus("1");
        settingsModel.setWebLogoImgFileName("");
        settingsModel.setMobLogoImgFileName("");
        return settingsModel;
    }

    public void resetDesign() {
        isCopyingSettings = true;
        hasSelectWebLogo = true;
        hasSelectedMobileLogo = true;
        mHasChanges = true;

        mSelectedBrandCode = null;
        mSelectedCampaignCode = null;
        mSelectedCampaignEventCode = null;

        isCopyingDesign(false);
        chCopyDesign.setChecked(false);

        mBrandModelList.clear();
        setupInitialListPromptsOnResponse(Constants.BRAND_LIST_PROMPT);
        mBrandArrayAdapter.notifyDataSetChanged();

        mCampaignModelList.clear();
        setupInitialListPromptsOnResponse(Constants.CAMPAIGN_LIST_PROMPT);
        mCampaignArrayAdapter.notifyDataSetChanged();

        mCampaignEventModelList.clear();
        setupInitialListPromptsOnResponse(Constants.EVENT_LIST_PROMPT);
        mCampaignEventArrayAdapter.notifyDataSetChanged();

        mSettingModel.setId("1");
        mSettingModel.setBrandCode(brandCode);
        mSettingModel.setCampaignCode(campaignCode);
        mSettingModel.setEventCode(eventCode);
        mSettingModel.setFormattedEventName(formattedEventName);
        mSettingModel.setForeground("");
        mSettingModel.setBackground(colorWhite);
        mSettingModel.setHeader(colorWhite);
        mSettingModel.setFooter(colorWhite);
        mSettingModel.setFooterText(defaultFooterText);
        mSettingModel.setWebLogo(Constants.CENTER_ALIGNMENT);
        mSettingModel.setWebLogoImg("");
        mSettingModel.setMobLogo(Constants.CENTER_ALIGNMENT);
        mSettingModel.setMobLogoImg("");
        mSettingModel.setFontHeadfamily("Arial");
        mSettingModel.setFontHeadsize("15");
        mSettingModel.setFontBasefamily("Arial");
        mSettingModel.setFontBasesize("15");
        mSettingModel.setSubmitBorderWidth("1");
        mSettingModel.setSubmitBorderRadius("1");
        mSettingModel.setSubmitBackground(colorWhite);
        mSettingModel.setSubmitAlignment(Constants.CENTER_ALIGNMENT);
        mSettingModel.setIsActive("1");
        mSettingModel.setDateTime(Utils.getDateTime());
        mSettingModel.setStatus("1");
        mSettingModel.setWebLogoImgFileName("");
        mSettingModel.setMobLogoImgFileName("");


        if (NetworkUtils.isNetworkConnected(getActivity())) {
            new AsyncSaveSettingUpdates(getActivity(), Constants.DIALOG_RESET_DESIGN_SETTINGS,
                    mSettingModel, hasSelectedMobileLogo, hasSelectWebLogo, isCopyingSettings).execute();
            hasSelectWebLogo = false;
            hasSelectedMobileLogo = false;

        } else {
            mDatabaseHelper.updateDbSetting(mSettingModel);
            mMaintenancePresenter.showAlertDialog("Alert", "Changes have been applied successfully!",
                    Constants.DIALOG_RESET_DESIGN_SETTINGS);
        }

    }
}
