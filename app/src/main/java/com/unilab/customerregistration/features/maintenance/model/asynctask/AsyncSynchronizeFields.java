package com.unilab.customerregistration.features.maintenance.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.unilab.customerregistration.features.maintenance.model.AssignFieldModel;
import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.model.InputModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.constants.DatabaseConstants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.util.List;

public class AsyncSynchronizeFields extends AsyncTask<Void, Void, Boolean> {

    private final String TAG = this.getClass().getSimpleName();
    private List<InputModel> mInputModelList;
    private Context mContext;
    private DatabaseHelper mDatabaseHelper;
    private String brandCode, campaignCode, eventCode;
    private MaintenancePresenter mMaintenancePresenter;
    private boolean mIsSynchronizingForm = true, mIsConfiguringDesignForm = true;

    public AsyncSynchronizeFields(Context context, List<InputModel> inputModelList,
                                  boolean isSynchronizingForm, boolean isConfiguringDesignForm) {
        Log.e(TAG, "AsyncSynchronizeFields: ");
        mDatabaseHelper = new DatabaseHelper(context);
        SharedPrefHelper.init(context);

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        mContext = context;
        mInputModelList = inputModelList;
        mMaintenancePresenter = (MaintenancePresenter) context;
        mIsSynchronizingForm = isSynchronizingForm;
        mIsConfiguringDesignForm = isConfiguringDesignForm;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.e(TAG, "onPreExecute: ");
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        return synchronizeFields();
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        Log.e(TAG, "onPostExecute: " + mIsSynchronizingForm);
        if (!mIsConfiguringDesignForm) {
            if (mIsSynchronizingForm) {
                mMaintenancePresenter.refreshFormAdapters();
            } else {
                mMaintenancePresenter.showPreviewPage();
            }
        } else {
            mMaintenancePresenter.showAlertDialog("Alert", "Settings saved successfully!",
                    Constants.DIALOG_ACTION_SAVE_BRAND_FRAGMENT);
        }
        DialogHelper.getInstance().hideProgressDialog(mContext);
    }

    private boolean synchronizeFields() {
        /**Un-assigning all fields*/
        List<FieldsModel> fieldsModelList = mDatabaseHelper.getCustomFieldInAssign(brandCode, campaignCode, eventCode);
        for (FieldsModel fm : fieldsModelList) {
            mDatabaseHelper.deleteAllAssignedCustomFields(fm, brandCode, campaignCode, eventCode);
            mDatabaseHelper.deleteFieldByFieldId(fm);

            switch (fm.getTypeId()) {
                case "3":
                    mDatabaseHelper.deleteDataArrayByInputId(fm.getB_id(), DatabaseConstants.TBL_RADIO);
                    break;

                case "4":
                    mDatabaseHelper.deleteDataArrayByInputId(fm.getB_id(), DatabaseConstants.TBL_SELECT);
                    break;
            }
        }

        for (InputModel inputModel : mInputModelList) {
            Log.e(TAG, "onResponse: fieldname: " + inputModel.getName() + "-" + inputModel.getAssignIsActive());
            Log.e(TAG, "SyncFields: " + inputModel.toString());
            inputModel.setStatus("0");

            /**If it is custom fields*/
            if (inputModel.getCategory().equals("2")) {
                /**Reassigning fields of web to dbm */
                inputModel.setDbmStatus("1");

                /**Reassigning groups into field*/
                if (mDatabaseHelper.checkIfGroupTitleIsExist(brandCode, campaignCode, eventCode, inputModel.getGroupName())
                        && !inputModel.getGroupId().equals("1") && !inputModel.getGroupId().equals("2")
                        && !inputModel.getGroupId().equals("3") && !inputModel.getGroupId().equals("4")) {
                    String groupId = mDatabaseHelper.getGroupIdByBrandCampaignEvent(brandCode, campaignCode,
                            eventCode, inputModel.getGroupName());

                    if (!inputModel.getGroupId().equals(groupId)) {
                        Log.e(TAG, "synchronizeFields: "
                                + " fieldName: " + inputModel.getTitle()
                                + " groupId: " + inputModel.getGroupId()
                                + " groupName: " + inputModel.getGroupName()
                                + " newGroupId: " + groupId);
                        inputModel.setGroupId(groupId);
                        inputModel.setEditStatus("1");
                    }
                }

                if (mDatabaseHelper.checkFieldIdExist(inputModel)) {
                    Log.e(TAG, "onResponse: " + "field id exist " + inputModel.getId() + "-" + inputModel.getName());
                    String fieldId = inputModel.getId();
                    mDatabaseHelper.deleteDataArrayByInputId(fieldId, DatabaseConstants.TBL_TEXTBOX);
                    mDatabaseHelper.deleteDataArrayByInputId(fieldId, DatabaseConstants.TBL_RADIO);
                    mDatabaseHelper.deleteDataArrayByInputId(fieldId, DatabaseConstants.TBL_SELECT);
                    mDatabaseHelper.updateFieldById(inputModel);

                } else {
                    Log.e(TAG, "onResponse: " + "field id and name not exist " + inputModel.getId() + "-" + inputModel.getName());
                    mDatabaseHelper.createField(inputModel);
                }

                /**If it is standard and non-standard fields*/
            } else {
                inputModel.setDbmStatus("0");
                if (mDatabaseHelper.checkFieldIdExist(inputModel)) {
                    Log.e(TAG, "onResponse: " + "field id exist " + inputModel.getId() + "-" + inputModel.getName());
                    String fieldId = inputModel.getId();
                    mDatabaseHelper.deleteDataArrayByInputId(fieldId, DatabaseConstants.TBL_TEXTBOX);
                    mDatabaseHelper.deleteDataArrayByInputId(fieldId, DatabaseConstants.TBL_RADIO);
                    mDatabaseHelper.deleteDataArrayByInputId(fieldId, DatabaseConstants.TBL_SELECT);
                    mDatabaseHelper.updateFieldById(inputModel);

                } else {
                    String fieldId = mDatabaseHelper.getFieldIdByFieldName(inputModel.getName());
                    mDatabaseHelper.deleteDataArrayByInputId(fieldId, DatabaseConstants.TBL_TEXTBOX);
                    mDatabaseHelper.deleteDataArrayByInputId(fieldId, DatabaseConstants.TBL_RADIO);
                    mDatabaseHelper.deleteDataArrayByInputId(fieldId, DatabaseConstants.TBL_SELECT);

                    if (mDatabaseHelper.checkFieldNameExist(inputModel)) {
                        Log.e(TAG, "onResponse: " + "field id not exist but has field name " + inputModel.getId() + "-" + inputModel.getName());
                        mDatabaseHelper.updateFieldByName(inputModel);

                    } else {
                        Log.e(TAG, "onResponse: " + "field id and name not exist " + inputModel.getId() + "-" + inputModel.getName());
                        mDatabaseHelper.createField(inputModel);
                    }
                }
            }

            /**Assigning Form Fields*/
            AssignFieldModel assignFieldModel = new AssignFieldModel();
            assignFieldModel.setCampaignCode(campaignCode);
            assignFieldModel.setBrandCode(brandCode);
            assignFieldModel.setEventCode(eventCode);
            assignFieldModel.setInputId(inputModel.getInputId());
            assignFieldModel.setUserId("");
            if (inputModel.getName().equals("email") ||
                    inputModel.getName().equals("firstname") ||
                    inputModel.getName().equals("lastname") ||
                    inputModel.getName().equals("civil_status") ||
                    inputModel.getName().equals("gender") ||
                    inputModel.getName().equals("dob") ||
                    inputModel.getName().equals("mobile")) {
                assignFieldModel.setIsActive("1");
            } else {
                assignFieldModel.setIsActive(inputModel.getAssignIsActive());
            }
            assignFieldModel.setDateTime(Utils.getDateTime());
            assignFieldModel.setSort(Integer.parseInt(inputModel.getSort()));
            assignFieldModel.setStatus("0");

            if (mDatabaseHelper.checkAssignFieldIdExistInBrandCampaign(inputModel, brandCode, campaignCode, eventCode)) {
                Log.e(TAG, "onResponse: field: " + inputModel.getId() + "-" + inputModel.getName() + " assigned in " + brandCode + "-" + campaignCode + "-" + eventCode);
                mDatabaseHelper.updateAssignByFieldIdBrandCampaign(assignFieldModel);

            } else {
                Log.e(TAG, "onResponse: field: " + inputModel.getId() + "-" + inputModel.getName() + " not assigned in " + brandCode + "-" + campaignCode + "-" + eventCode);
                int maxAssignId = mDatabaseHelper.getMaxInputAssignId();
                maxAssignId++;
                Log.e(TAG, "onResponse: maxAssignId: " + maxAssignId);
                assignFieldModel.setId(String.valueOf(maxAssignId));
                mDatabaseHelper.assignFieldIdBrandCampaign(assignFieldModel);
            }

            if (!inputModel.getName().equals("country_id") && !inputModel.getName().equals("regionid")
                    && !inputModel.getName().equals("provinceid") && !inputModel.getName().equals("cityid")) {
                for (DataArray dataArray : inputModel.getDataArray()) {
                    mDatabaseHelper.createDataArray(dataArray, inputModel.getTypeId());
                }
            }

            /**Synchronize groups*/
            String strGroupName = inputModel.getGroupName();
            String strFormattedGroupName = strGroupName.replace(" ", "_").toLowerCase();

            GroupsModel groupsModel = new GroupsModel();
            groupsModel.setId(inputModel.getGroupId());
            groupsModel.setTitle(strGroupName);
            groupsModel.setName(strFormattedGroupName);
            groupsModel.setDescription("");
            groupsModel.setBrandCode(brandCode);
            groupsModel.setCampaignCode(campaignCode);
            groupsModel.setEventCode(eventCode);
            groupsModel.setIsActive("1");
            groupsModel.setUserId("");
            groupsModel.setStatus("0");

            if (mDatabaseHelper.checkIfGroupExistById(groupsModel)) {
                Log.e(TAG, "synchronizingGroups: GroupID: " + groupsModel.getId()
                        + " GroupName: " + groupsModel.getName()
                        + " is exist");

                mDatabaseHelper.updateGroupById(groupsModel);
            } else {
                Log.e(TAG, "synchronizingGroups: GroupID: " + groupsModel.getId()
                        + " GroupName: " + groupsModel.getName()
                        + " is not exist");
                mDatabaseHelper.createGroup(groupsModel);
            }
        }

        return true;
    }
}
