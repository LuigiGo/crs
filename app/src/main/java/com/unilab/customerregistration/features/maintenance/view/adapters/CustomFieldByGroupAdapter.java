package com.unilab.customerregistration.features.maintenance.view.adapters;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;


import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;

import java.util.Collections;
import java.util.List;

public class CustomFieldByGroupAdapter extends RecyclerView.Adapter<CustomFieldByGroupAdapter.ViewHolder> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private List<FieldsModel> mFieldsModelArrayList;
    private DatabaseHelper mDatabaseHelper;
    private String brandCode, campaignCode, eventCode;
    private LinearLayoutManager mLayoutManager;
    private AlertDialog mEditFieldDialog, mAlertUpdatePrompt;
    private String mGroupId;
    private MaintenancePresenter mMaintenancePresenter;

    public CustomFieldByGroupAdapter(Context context, List<FieldsModel> fieldsModelArrayList, String groupId) {
        mDatabaseHelper = new DatabaseHelper(context);
        SharedPrefHelper.init(context);

        mContext = context;
        mFieldsModelArrayList = fieldsModelArrayList;

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        mGroupId = groupId;

        mMaintenancePresenter = (MaintenancePresenter) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View views = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_additional_fields, parent, false);

        return new ViewHolder(views);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final FieldsModel fieldsModel = mFieldsModelArrayList.get(position);

        Log.e(TAG, "onBindViewHolder: " + fieldsModel.toString());
        holder.txtInputFieldName.setText(fieldsModel.getTitle());
        holder.chActiveStatus.setChecked(fieldsModel.getA_isActive().equals("1"));
        holder.chActiveStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox ch = (CheckBox) v;
                Log.e(TAG, "onClick: " + fieldsModel.getA_id() + " " + ch.isChecked());
                fieldsModel.setA_isActive((ch.isChecked()) ? "1" : "0");
                fieldsModel.setEditStatus("1");
            }
        });

        holder.btnMoveUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    Log.e(TAG, "onClick: " + "first item");

                } else {
                    Log.e(TAG, "onClick: " + "previous item: " + mFieldsModelArrayList.get(position - 1).getSort());
                    Log.e(TAG, "onClick: " + "current item " + fieldsModel.getSort());
                    Log.e(TAG, "onClick: " + "item " + position);

                    int previousItem = mFieldsModelArrayList.get(position - 1).getSort();
                    int currentItem = fieldsModel.getSort();
                    swapOnMoveUp(previousItem, currentItem, position);
                    Collections.swap(mFieldsModelArrayList, position, position - 1);
                    mMaintenancePresenter.stopHorizontalScrollViewScroll();
                    notifyDataSetChanged();
                    mMaintenancePresenter.setHorizontalScrollViewPosition(HorizontalScrollView.FOCUS_RIGHT);
                }
            }
        });

        holder.btnMoveDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int lastItem = getItemCount() - 1;
                if (position == lastItem) {
                    Log.e(TAG, "onClick: " + "last item");
                } else {
                    Log.e(TAG, "onClick: " + "current item " + fieldsModel.getSort());
                    Log.e(TAG, "onClick: " + "next item: " + mFieldsModelArrayList.get(position + 1).getSort());
                    Log.e(TAG, "onClick: " + "item " + position);

                    int nextItem = mFieldsModelArrayList.get(position + 1).getSort();
                    int currentItem = fieldsModel.getSort();
                    swapOnMoveDown(nextItem, currentItem, position);
                    Collections.swap(mFieldsModelArrayList, position, position + 1);
                    mMaintenancePresenter.stopHorizontalScrollViewScroll();
                    notifyDataSetChanged();
                    mMaintenancePresenter.setHorizontalScrollViewPosition(HorizontalScrollView.FOCUS_RIGHT);
                }
            }
        });

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaintenancePresenter.showAddFormFieldAdapterDialog(fieldsModel);
            }
        });

    }


    void swapOnMoveUp(int previousItem, int currentItem, int position) {
        int temp = previousItem;
        previousItem = currentItem;
        currentItem = temp;

        mFieldsModelArrayList.get(position).setSort(currentItem);
        mFieldsModelArrayList.get(position).setEditStatus("1");
        mFieldsModelArrayList.get(position - 1).setSort(previousItem);
        mFieldsModelArrayList.get(position - 1).setEditStatus("1");

    }

    void swapOnMoveDown(int nextItem, int currentItem, int position) {
        int temp = nextItem;
        nextItem = currentItem;
        currentItem = temp;

        mFieldsModelArrayList.get(position).setSort(currentItem);
        mFieldsModelArrayList.get(position).setEditStatus("1");
        mFieldsModelArrayList.get(position + 1).setSort(nextItem);
        mFieldsModelArrayList.get(position + 1).setEditStatus("1");
    }

    public void saveFormFields() {
        for (FieldsModel fieldsModel : mFieldsModelArrayList) {
            Log.e(TAG, "saveFormFields: " + fieldsModel.toString());
            mDatabaseHelper.updateInputAssign(fieldsModel);
            mDatabaseHelper.updateFieldEditStatus(fieldsModel);
        }
    }

    public List<FieldsModel> getFields() {
        return mFieldsModelArrayList;
    }

    @Override
    public int getItemCount() {
        return mFieldsModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtInputFieldName;
        private CheckBox chActiveStatus;
        private Button btnMoveUp, btnMoveDown;
        private ImageView imgEdit;

        public ViewHolder(View v) {
            super(v);
            txtInputFieldName = (TextView) v.findViewById(R.id.txtFieldName);
            chActiveStatus = (CheckBox) v.findViewById(R.id.chActiveStatus);
            btnMoveUp = (Button) v.findViewById(R.id.btnMoveUp);
            btnMoveDown = (Button) v.findViewById(R.id.btnMoveDown);
            imgEdit = (ImageView) v.findViewById(R.id.imgEdit);
        }
    }
}
