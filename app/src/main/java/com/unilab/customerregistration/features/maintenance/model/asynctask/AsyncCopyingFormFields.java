package com.unilab.customerregistration.features.maintenance.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.DbmAddFieldsModel;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.model.web.api.MaintenanceApiProvider;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.constants.DatabaseConstants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsyncCopyingFormFields extends AsyncTask<Void, Void, String> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private List<FieldsModel> mFieldsModelList;
    private List<GroupsModel> mGroupsModelList;
    private String brandCode, campaignCode, eventCode, eventName, formattedEventName;
    private boolean mIsCopy = false, isLastItem = false, mIsConfiguring = false;
    private DatabaseHelper mDatabaseHelper;
    private MaintenancePresenter mMaintenancePresenter;
    private int successfulFieldSave = 0, failedFieldSave = 0;

    public AsyncCopyingFormFields(Context context, List<FieldsModel> fieldsModelList, List<GroupsModel> groupsModelList,
                                  boolean isCopy, boolean isConfiguring) {
        SharedPrefHelper.init(context);
        mDatabaseHelper = new DatabaseHelper(context);
        DialogHelper.init(context);

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();
        eventName = SharedPrefHelper.getInstance().getEventNameFromShared();
        formattedEventName = SharedPrefHelper.getInstance().getFormattedCampaignNameFromShared();

        mMaintenancePresenter = (MaintenancePresenter) context;

        mContext = context;
        mFieldsModelList = fieldsModelList;
        mGroupsModelList = groupsModelList;
        mIsCopy = isCopy;
        mIsConfiguring = isConfiguring;
        Log.e(TAG, "isCopying???: " + mIsCopy);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        DialogHelper.getInstance().showProgressDialog(mContext, "Saving form fields");
    }

    @Override
    protected String doInBackground(Void... params) {

        /**Post copied form fields groups*/
        for (GroupsModel groupsModel : mGroupsModelList) {
            Log.e(TAG, "doInBackground: " + groupsModel.toString());
            if (mDatabaseHelper.checkIfGroupExistById(groupsModel)) {
                Log.e(TAG, "synchronizingGroups: GroupID: " + groupsModel.getId()
                        + " GroupName: " + groupsModel.getName()
                        + " is exist");

                mDatabaseHelper.updateGroupById(groupsModel);
            } else {
                Log.e(TAG, "synchronizingGroups: GroupID: " + groupsModel.getId()
                        + " GroupName: " + groupsModel.getName()
                        + " is not exist");
                mDatabaseHelper.createGroup(groupsModel);
            }
        }

        for (int i = 0; i < mFieldsModelList.size(); i++) {
            FieldsModel fieldsModel = mFieldsModelList.get(i);

            /**Sending field to dbm*/
            if (fieldsModel.getCategory().equals("2")) {
                postCopiedFieldToDbm(fieldsModel);
            }

            /**Getting data array of standard and non-standard field*/
            if (fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_STANDARD_FIELDS)) ||
                    fieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_NON_STANDARD_FIELDS))) {

                switch (fieldsModel.getTypeId()) {
                    case "3":
                        fieldsModel.setDataArrayList(mDatabaseHelper
                                .getDataArrayByFieldId(DatabaseConstants.TBL_RADIO, fieldsModel.getB_id()));
                        break;

                    case "4":
                        fieldsModel.setDataArrayList(mDatabaseHelper
                                .getDataArrayByFieldId(DatabaseConstants.TBL_SELECT, fieldsModel.getB_id()));
                        break;
                }
            }

            for (FieldsModel fieldsModel1 : mFieldsModelList) {
                Log.e(TAG, "doInBackground: Configuring on API: " + fieldsModel1.toString());
            }

            /**Requesting and receiving response from postData*/
            String response = postData(fieldsModel);
            if (response != null) {
                Log.e(TAG, "postData result: FieldName: " + fieldsModel.getName() + " Response: " + response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    String status = jsonResponse.getString("status");
                    switch (status) {
                        case "success":
                            successfulFieldSave++;

                            break;

                        case "failed":
                            failedFieldSave++;
                            break;
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "doInBackground: " + e.getMessage());
                }

            }

            /**Un-assigning all custom fields of the current brand, campaign and event*/
            if (mIsCopy) {
                mIsCopy = false;
                List<FieldsModel> fieldsModelList = mDatabaseHelper.getCustomFieldInAssign(brandCode, campaignCode, eventCode);
                for (FieldsModel fm : fieldsModelList) {
                    mDatabaseHelper.deleteAllAssignedCustomFields(fm, brandCode, campaignCode, eventCode);
                    mDatabaseHelper.deleteFieldByFieldId(fm);

                    switch (fm.getTypeId()) {
                        case "3":
                            mDatabaseHelper.deleteDataArrayByInputId(fieldsModel.getB_id(), DatabaseConstants.TBL_RADIO);
                            break;

                        case "4":
                            mDatabaseHelper.deleteDataArrayByInputId(fieldsModel.getB_id(), DatabaseConstants.TBL_SELECT);
                            break;
                    }
                }
            }
        }
        return null;
    }

    private String postData(FieldsModel mFieldsModel) {
        Log.e(TAG, "postDataFieldModel: " + mFieldsModel.toString());
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Constants.API_WEB_BASE_URL3.concat("api/"));
        try {
            List fieldList = new ArrayList();
            fieldList.add(new BasicNameValuePair("token", Constants.API_WEB_TOKEN));

            if (mFieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_STANDARD_FIELDS)) ||
                    mFieldsModel.getCategory().equals(String.valueOf(Constants.CATEGORY_NON_STANDARD_FIELDS))) {
                fieldList.add(new BasicNameValuePair("cmdEvent", Constants.CMD_EVENT_UPDATE_INPUT));
                fieldList.add(new BasicNameValuePair("input_id", mFieldsModel.getB_id()));
            } else {
                fieldList.add(new BasicNameValuePair("cmdEvent", Constants.CMD_EVENT_POST_INPUT));
            }

            fieldList.add(new BasicNameValuePair("campaign_code", campaignCode));
            fieldList.add(new BasicNameValuePair("brand_code", brandCode));
            fieldList.add(new BasicNameValuePair("campaign_event", eventCode));
            fieldList.add(new BasicNameValuePair("campaign_name", formattedEventName));
            fieldList.add(new BasicNameValuePair("type_id", mFieldsModel.getTypeId()));
            fieldList.add(new BasicNameValuePair("title", mFieldsModel.getTitle()));
            fieldList.add(new BasicNameValuePair("name", mFieldsModel.getName()));
            fieldList.add(new BasicNameValuePair("description", mFieldsModel.getDescription()));
            fieldList.add(new BasicNameValuePair("group_id", mFieldsModel.getGroupId()));
            fieldList.add(new BasicNameValuePair("is_required", mFieldsModel.getIsRequired()));
            fieldList.add(new BasicNameValuePair("is_active", mFieldsModel.getB_isActive()));
            fieldList.add(new BasicNameValuePair("date_time", mFieldsModel.getA_dateTime()));
            fieldList.add(new BasicNameValuePair("category", mFieldsModel.getCategory()));
            fieldList.add(new BasicNameValuePair("sort", String.valueOf(mFieldsModel.getSort())));
            fieldList.add(new BasicNameValuePair("assign_is_active", mFieldsModel.getA_isActive()));
            fieldList.add(new BasicNameValuePair("isCopy", mIsCopy ? "1" : "0"));

            Log.e(TAG, "postData: fieldName: " + mFieldsModel.getName() + " isDataArrayNull: " + (mFieldsModel.getDataArrayList() == null));

            if (mFieldsModel.getDataArrayList() != null && mFieldsModel.getDataArrayList().size() != 0
                    && !mFieldsModel.getTypeId().equals("1")) {
                Log.e(TAG, "postData: DataArraySize: " + mFieldsModel.getDataArrayList().size());
                for (int i = 0; i < mFieldsModel.getDataArrayList().size(); i++) {
                    final DataArray dataArray = mFieldsModel.getDataArrayList().get(i);
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][title]", dataArray.getTitle()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][name]", dataArray.getName()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][value]", dataArray.getValue()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][is_active]", dataArray.getIsActive()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][date_time]", dataArray.getDateTime()));
                    Log.e(TAG, "postDataArray: " + dataArray.toString());
                }
            } else {
                if (!mFieldsModel.getTypeId().equals("1")) {
                    fieldList.add(new BasicNameValuePair("data_array[" + 0 + "][title]", mFieldsModel.getTitle()));
                    fieldList.add(new BasicNameValuePair("data_array[" + 0 + "][name]", mFieldsModel.getName()));
                    fieldList.add(new BasicNameValuePair("data_array[" + 0 + "][value]", ""));
                    fieldList.add(new BasicNameValuePair("data_array[" + 0 + "][is_active]", ""));
                    fieldList.add(new BasicNameValuePair("data_array[" + 0 + "][date_time]", Utils.getDateTime()));
                }
            }

            Log.e(TAG, "postDataFieldList: " + fieldList.toString());

            httppost.setEntity(new UrlEncodedFormEntity(fieldList));
            HttpResponse response = httpclient.execute(httppost);
            return EntityUtils.toString(response.getEntity());
        } catch (ClientProtocolException e) {
            Log.e(TAG, "postData: " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e(TAG, "postData: " + e.getMessage());
            return null;
        }
    }

    @Override

    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        DialogHelper.getInstance().hideProgressDialog(mContext);
        if (mIsConfiguring) {
            mMaintenancePresenter.showAlertDialog("Alert", "Settings saved successfully!",
                    Constants.DIALOG_ACTION_SAVE_BRAND_FRAGMENT);

        } else {
            if (successfulFieldSave == mFieldsModelList.size()) {
                mMaintenancePresenter.showAlertDialog("Alert", "Changes saved successfully!",
                        Constants.DIALOG_ACTION_SAVE_FORM_FRAGMENT);

            } else {
                mMaintenancePresenter.showAlertDialog("Alert", successfulFieldSave + " fields successfully saved out of " + mFieldsModelList.size(),
                        Constants.DIALOG_ACTION_SAVE_FORM_FRAGMENT);
            }
        }
    }

    private void postCopiedFieldToDbm(final FieldsModel fieldsModel) {
        Call<DbmAddFieldsModel> dbmAddFieldsModelCall = MaintenanceApiProvider.getMaintenanceDbmApiRoutes()
                .addDbmField(Constants.API_DBM_TOKEN, Constants.CMD_EVENT_DBM_POST_FIELD, fieldsModel.getCampaignCode(),
                        fieldsModel.getBrandCode(), fieldsModel.getEventCode(), fieldsModel.getTypeId(), fieldsModel.getTitle(),
                        fieldsModel.getName(), fieldsModel.getDescription(), fieldsModel.getGroupId(),
                        fieldsModel.getIsRequired(), fieldsModel.getA_isActive(), Utils.getDateTime(),
                        fieldsModel.getCategory(), Constants.DBM_HOSTNAME);
        dbmAddFieldsModelCall.enqueue(new Callback<DbmAddFieldsModel>() {
            @Override
            public void onResponse(Call<DbmAddFieldsModel> call, Response<DbmAddFieldsModel> response) {

                if (response.body() != null) {
                    DbmAddFieldsModel dbmAddFieldsModel = response.body();
                    Log.e(TAG, "postCopiedFieldToDbm-onResponse: " + dbmAddFieldsModel.getMessage());
                    switch (dbmAddFieldsModel.getMessage()) {
                        case "Field is successfully added!":
                            fieldsModel.setDbmStatus("0");
                            break;

                        case "Field is already exists!":
                            fieldsModel.setDbmStatus("2");
                            break;

                        default:
                            fieldsModel.setDbmStatus("1");
                            break;
                    }

                    mDatabaseHelper.updateDbmStatusByFieldId(fieldsModel);
                }
            }

            @Override
            public void onFailure(Call<DbmAddFieldsModel> call, Throwable t) {
                Log.e(TAG, "onFailure: postFieldToDbmForOffline: " + t.getMessage());
            }
        });
    }

}
