package com.unilab.customerregistration.features.login.model.web.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginApiRoutes {

    @FormUrlEncoded
    @POST("webapp/")
    Call<ResponseBody> onGetLoginStatus(@Field("token") String token,
                                        @Field("cmdEvent") String cmdEvent,
                                        @Field("email") String email,
                                        @Field("password") String password,
                                        @Field("host") String hostname);
}
