package com.unilab.customerregistration.features.dashboard.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.dashboard.presenter.DashboardContract;
import com.unilab.customerregistration.features.dashboard.model.CountryModel;
import com.unilab.customerregistration.features.dashboard.model.RegionModel;
import com.unilab.customerregistration.features.dashboard.model.api.DashboardApiProvider;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsyncFetchCounty extends AsyncTask<Void, Void, Void> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private DatabaseHelper mDatabaseHelper;
    private List<CountryModel> mCountryModelList;
    private DashboardContract mDashboardContract;

    public AsyncFetchCounty(Context context, List<CountryModel> countryModelList) {
        DialogHelper.init(context);
        mDatabaseHelper = new DatabaseHelper(context);

        mContext = context;
        mCountryModelList = countryModelList;

        mDashboardContract = (DashboardContract) context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        DialogHelper.getInstance().showProgressDialog(mContext, "Synchronizing data");

    }

    @Override
    protected Void doInBackground(Void... params) {
        for (CountryModel countryModel : mCountryModelList) {
            Log.e(TAG, "onResponse: " + countryModel.getCountryId());
            if (!mDatabaseHelper.checkCountryIdIfExist(countryModel)) {
                mDatabaseHelper.createCountry(countryModel);
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
//        DialogHelper.getInstance().hideProgressDialog(mContext);
        getRegionAPI();
    }

    private void getRegionAPI() {
        Log.e(TAG, "getRegionAPI: ");
        Call<List<RegionModel>> getRegionCall = DashboardApiProvider.getDashboardApiRoutes()
                .getRegionModel(Constants.API_WEB_TOKEN, Constants.CMD_EVENT_GET_REGION);
        getRegionCall.enqueue(new Callback<List<RegionModel>>() {
            @Override
            public void onResponse(Call<List<RegionModel>> call, Response<List<RegionModel>> response) {
                Log.e(TAG, "onResponse: " + response.code());
                if (response.body() != null) {
                    List<RegionModel> regionModelList = response.body();
//                    for (RegionModel regionModel : regionModelList) {
//                        Log.e(TAG, "onResponse: " + regionModel.getRegionName());
//                        mDatabaseHelper.createRegion(regionModel);
//                    }
                    new AsyncFetchRegion(mContext, regionModelList).execute();
//                    getProvinceAPI();
                } else {
                    DialogHelper.getInstance().hideProgressDialog(mContext);
                }
            }

            @Override
            public void onFailure(Call<List<RegionModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                mDashboardContract.showAlertInfoDialog("Alert", mContext.getResources().getString(R.string.error_with_dbm_or_connectivity),
                        Constants.DIALOG_ACTION_NORMAL);
                DialogHelper.getInstance().hideProgressDialog(mContext);
            }
        });
    }
}
