package com.unilab.customerregistration.features.maintenance.model.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.unilab.customerregistration.features.maintenance.model.AssignFieldModel;
import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.InputModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AsyncAddField extends AsyncTask<Void, Void, String> {

    private final String TAG = this.getClass().getSimpleName();
    private DatabaseHelper mDatabaseHelper;
    private List<DataArray> mDataArrayList;
    private Context mContext;
    private InputModel mInputModel;
    private AssignFieldModel mAssignFieldModel;
    private FieldsModel mFieldsModel;
    private boolean mSynchronization = false;
    private MaintenancePresenter mMaintenancePresenter;
    private String eventName, formattedEventName;

    public AsyncAddField(Context context, List<DataArray> dataArrayList, InputModel inputModel,
                         AssignFieldModel assignFieldModel, FieldsModel fieldsModel, boolean synchronization) {
        Log.e(TAG, "AsyncAddField: ");
        mDatabaseHelper = new DatabaseHelper(context);
        DialogHelper.init(context);

        mDataArrayList = dataArrayList;
        mContext = context;
        mMaintenancePresenter = (MaintenancePresenter) context;

        mInputModel = inputModel;
        mAssignFieldModel = assignFieldModel;
        mFieldsModel = fieldsModel;
        mSynchronization = synchronization;

        eventName = SharedPrefHelper.getInstance().getEventNameFromShared();
        formattedEventName = SharedPrefHelper.getInstance().getFormattedCampaignNameFromShared();

        Log.e(TAG, "AsyncAddField: " + "DataArrayListAddField: " + mDataArrayList.size());
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (!mSynchronization) {
//            DialogHelper.getInstance().hideProgressDialog(mContext);
//            DialogHelper.getInstance().showProgressDialog(mContext, "Adding fields");
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        return postData();
    }

    private String postData() {
        Log.e(TAG, "postData: " + mDataArrayList.size());
        Log.e(TAG, "postData: " + mFieldsModel.toString());
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Constants.API_WEB_BASE_URL3.concat("api/"));
        try {
            List fieldList = new ArrayList();
            fieldList.add(new BasicNameValuePair("token", Constants.API_WEB_TOKEN));
            fieldList.add(new BasicNameValuePair("cmdEvent", Constants.CMD_EVENT_POST_INPUT));
            fieldList.add(new BasicNameValuePair("campaign_code", mFieldsModel.getCampaignCode()));
            fieldList.add(new BasicNameValuePair("brand_code", mFieldsModel.getBrandCode()));
            fieldList.add(new BasicNameValuePair("campaign_event", mFieldsModel.getEventCode()));
            fieldList.add(new BasicNameValuePair("campaign_name", formattedEventName));
            fieldList.add(new BasicNameValuePair("type_id", mFieldsModel.getTypeId()));
            fieldList.add(new BasicNameValuePair("title", mFieldsModel.getTitle()));
            fieldList.add(new BasicNameValuePair("name", mFieldsModel.getName()));
            fieldList.add(new BasicNameValuePair("description", mFieldsModel.getDescription()));
            fieldList.add(new BasicNameValuePair("group_id", mFieldsModel.getGroupId()));
            fieldList.add(new BasicNameValuePair("is_required", mFieldsModel.getIsRequired()));
            fieldList.add(new BasicNameValuePair("is_active", mFieldsModel.getB_isActive()));
            fieldList.add(new BasicNameValuePair("date_time", mFieldsModel.getA_dateTime()));
            fieldList.add(new BasicNameValuePair("category", mFieldsModel.getCategory()));
            fieldList.add(new BasicNameValuePair("sort", String.valueOf(mFieldsModel.getSort())));
            fieldList.add(new BasicNameValuePair("assign_is_active", mFieldsModel.getA_isActive()));
            fieldList.add(new BasicNameValuePair("isCopy", "0"));

            if (mDataArrayList.size() > 0) {
                for (int i = 0; i < mDataArrayList.size(); i++) {
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][title]", mDataArrayList.get(i).getTitle()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][name]", mDataArrayList.get(i).getName()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][value]", mDataArrayList.get(i).getValue()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][is_active]", mDataArrayList.get(i).getIsActive()));
                    fieldList.add(new BasicNameValuePair("data_array[" + i + "][date_time]", mDataArrayList.get(i).getDateTime()));
                }
            } else {
                fieldList.add(new BasicNameValuePair("data_array[]", ""));
            }

            httppost.setEntity(new UrlEncodedFormEntity(fieldList));
            HttpResponse response = httpclient.execute(httppost);
            return EntityUtils.toString(response.getEntity());
        } catch (ClientProtocolException e) {
            Log.e(TAG, "postData: " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e(TAG, "postData: " + e.getMessage());
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Log.e(TAG, "onPostExecute: Result: " + result);
        DialogHelper.getInstance().hideProgressDialog(mContext);
        if (result != null) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String status = jsonObject.getString("status");
                switch (status) {
                    case "success":
                        String id = jsonObject.getString("id");
                        mInputModel.setId(id);
                        mInputModel.setStatus("0");
                        mAssignFieldModel.setStatus("0");
                        mAssignFieldModel.setInputId(id);

                        if (mDataArrayList.size() > 0) {
                            for (DataArray dataArray : mDataArrayList) {
                                int maxDataArrayId = mDatabaseHelper.getMaxDataArrayId(mFieldsModel.getTypeId());
                                maxDataArrayId++;

                                dataArray.setId(String.valueOf(maxDataArrayId));
                                dataArray.setInputId(id);
                                mDatabaseHelper.createDataArray(dataArray, mFieldsModel.getTypeId());

                            }
                        } else {
                            int maxDataArrayId = mDatabaseHelper.getMaxDataArrayId(mFieldsModel.getTypeId());
                            maxDataArrayId++;
                            DataArray dataArray = new DataArray();
                            dataArray.setId(String.valueOf(maxDataArrayId));
                            dataArray.setInputId(id);
                            dataArray.setTitle(mFieldsModel.getTitle());
                            dataArray.setName(mFieldsModel.getName());
                            dataArray.setValue("");
                            dataArray.setIsActive("1");
                            dataArray.setDateTime(Utils.getDateTime());
                            mDatabaseHelper.createDataArray(dataArray, mFieldsModel.getTypeId());
                        }

                        mDatabaseHelper.createField(mInputModel);
                        mDatabaseHelper.assignField(mAssignFieldModel);

                        if (!mSynchronization) {
                            mMaintenancePresenter.showAlertDialog("Alert", "Changes saved successfully!", Constants.DIALOG_ACTION_NORMAL);
                        }
//                        mMaintenancePresenter.refreshFormAdapters();
                        mMaintenancePresenter.refreshCustomFieldsAdapter();
                        break;

                    case "failed":
                        String message = jsonObject.getString("msg");
                        mMaintenancePresenter.showAlertDialog("Alert", message, Constants.DIALOG_ACTION_NORMAL);
                        break;

                    default:
                        mMaintenancePresenter.showAlertDialog("Alert", "Adding field failed", Constants.DIALOG_ACTION_NORMAL);
                        break;
                }
            } catch (JSONException e) {
                Log.e(TAG, "onPostExecute: " + e.getMessage());
                mMaintenancePresenter.showAlertDialog("Alert", e.getMessage(), Constants.DIALOG_ACTION_NORMAL);
            }
        }
    }
}
