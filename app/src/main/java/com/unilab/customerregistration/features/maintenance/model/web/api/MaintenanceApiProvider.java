package com.unilab.customerregistration.features.maintenance.model.web.api;


import com.unilab.customerregistration.features.registration.model.web.api.RegistrationApiRoutes;
import com.unilab.customerregistration.utilities.network.RestManager;

import retrofit2.Retrofit;

public class MaintenanceApiProvider {

    public static MaintenanceApiRoutes getMaintenanceDbmApiRoutes() {
        Retrofit retrofit = RestManager.getInstance().getDBMRetrofitAdapter();
        return retrofit.create(MaintenanceApiRoutes.class);
    }

//    public static MaintenanceApiRoutes getMaintenanceDbmApiRoutes2() {
//        Retrofit retrofit = RestManager.getInstance().getDBMRetrofitAdapter2();
//        return retrofit.create(MaintenanceApiRoutes.class);
//    }

    public static MaintenanceApiRoutes getMaintenanceWebApiRoutes(){
        Retrofit retrofit = RestManager.getInstance().getWebRetrofitAdapter();
        return retrofit.create(MaintenanceApiRoutes.class);
    }
}
