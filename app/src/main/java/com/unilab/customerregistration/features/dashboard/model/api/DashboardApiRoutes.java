package com.unilab.customerregistration.features.dashboard.model.api;

import com.unilab.customerregistration.features.dashboard.model.CityModel;
import com.unilab.customerregistration.features.dashboard.model.CountryModel;
import com.unilab.customerregistration.features.dashboard.model.ProvinceModel;
import com.unilab.customerregistration.features.dashboard.model.RegionModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface DashboardApiRoutes {

    @FormUrlEncoded
    @POST("api/")
    Call<List<CountryModel>> getCountry(@Field("token") String token,
                                        @Field("cmdEvent") String cmdEvent);

    @FormUrlEncoded
    @POST("api/")
    Call<List<ProvinceModel>> getProvinceList(@Field("token") String token,
                                              @Field("cmdEvent") String cmdEvent);

    @FormUrlEncoded
    @POST("api/")
    Call<List<RegionModel>> getRegionModel(@Field("token") String token,
                                           @Field("cmdEvent") String cmdEvent);

    @FormUrlEncoded
    @POST("api/")
    Call<List<CityModel>> getCity(@Field("token") String token,
                                  @Field("cmdEvent") String cmdEvent);

}
