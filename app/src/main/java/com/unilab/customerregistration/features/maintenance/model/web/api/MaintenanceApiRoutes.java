package com.unilab.customerregistration.features.maintenance.model.web.api;


import com.unilab.customerregistration.features.maintenance.model.CampaignEventModel;
import com.unilab.customerregistration.features.maintenance.model.DataSourceModel;
import com.unilab.customerregistration.features.maintenance.model.DbmAddFieldsModel;
import com.unilab.customerregistration.features.maintenance.model.GroupApiResponseModel;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.model.InputModel;
import com.unilab.customerregistration.features.maintenance.model.BrandModel;
import com.unilab.customerregistration.features.maintenance.model.CampaignModel;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.maintenance.model.SettingsResponseModel;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface MaintenanceApiRoutes {

    @FormUrlEncoded
    @POST("webapp/")
    Call<List<BrandModel>> onGetBrandLists(@Field("token") String token,
                                           @Field("cmdEvent") String cmdEvent,
                                           @Field("email") String email,
                                           @Field("host") String hostname);

    @FormUrlEncoded
    @POST("webapp/")
    Call<List<CampaignModel>> onGetCampaignLists(@Field("token") String token,
                                                 @Field("cmdEvent") String cmdEvent,
                                                 @Field("brand_id") String brand_code,
                                                 @Field("host") String hostname);

    @FormUrlEncoded
    @POST("webapp/")
    Call<List<CampaignEventModel>> onGetCampaignEventLists(@Field("token") String token,
                                                           @Field("cmdEvent") String cmdEvent,
                                                           @Field("campaign_id") String campaignId,
                                                           @Field("host") String hostname);

    @FormUrlEncoded
    @POST("webapp/")
    Call<List<DataSourceModel>> onGetDataSource(@Field("token") String token,
                                                @Field("cmdEvent") String cmdEvent,
                                                @Field("initiative_id") String campaignId,
                                                @Field("host") String hostname);

    @FormUrlEncoded
    @POST("api/")
    Call<List<SettingsModel>> getSetting(@Field("token") String token,
                                         @Field("cmdEvent") String cmdEvent,
                                         @Field("campaign_code") String campaignCode,
                                         @Field("brand_code") String brandCode,
                                         @Field("campaign_event") String campaignEventCode,
                                         @Field("campaign_name") String formattedCampaignEventName);

    @FormUrlEncoded
    @POST("api/")
    Call<SettingsResponseModel> updateAllSetting(@Field("token") String token,
                                                 @Field("cmdEvent") String cmdEvent,
                                                 @Field("campaign_code") String campaignCode,
                                                 @Field("brand_code") String brandCode,
                                                 @Field("campaign_event") String campaignEventCode,
                                                 @Field("campaign_name") String formattedCampaignEventName,
                                                 @Field("foreground") String foreground,
                                                 @Field("background") String background,
                                                 @Field("header") String header,
                                                 @Field("footer") String footer,
                                                 @Field("footer_text") String footerText,
                                                 @Field("web_logo_img") String webLogoImg,
                                                 @Field("web_logo") String webLogo,
                                                 @Field("mob_logo_img") String mobLogoImg,
                                                 @Field("mob_logo") String mobLogo,
                                                 @Field("font_headfamily") String fontHeadFamily,
                                                 @Field("font_headsize") String fontHeadSize,
                                                 @Field("font_basefamily") String fontBaseFamily,
                                                 @Field("font_basesize") String fontBaseSize,
                                                 @Field("submit_border_width") String submitBorderWidth,
                                                 @Field("submit_border_radius") String submitBorderRadius,
                                                 @Field("submit_background") String submitBackground,
                                                 @Field("submit_alignment") String submitAlignment,
                                                 @Field("date_time") String dateTime,
                                                 @Field("web_logo_img_fileName") String webLogoImgFileName,
                                                 @Field("mob_logo_img_fileName") String mobLogoImgFileName,
                                                 @Field("isWeb") String isWeb,
                                                 @Field("isMob") String isMob);

    @FormUrlEncoded
    @POST("api/")
    Call<SettingsResponseModel> updateSettingWithMobLogo(@Field("token") String token,
                                                         @Field("cmdEvent") String cmdEvent,
                                                         @Field("campaign_code") String campaignCode,
                                                         @Field("brand_code") String brandCode,
                                                         @Field("campaign_event") String campaignEventCode,
                                                         @Field("campaign_name") String formattedCampaignEventName,
                                                         @Field("foreground") String foreground,
                                                         @Field("background") String background,
                                                         @Field("header") String header,
                                                         @Field("footer") String footer,
                                                         @Field("footer_text") String footerText,
                                                         @Field("web_logo") String webLogo,
                                                         @Field("mob_logo_img") String mobLogoImg,
                                                         @Field("mob_logo") String mobLogo,
                                                         @Field("font_headfamily") String fontHeadFamily,
                                                         @Field("font_headsize") String fontHeadSize,
                                                         @Field("font_basefamily") String fontBaseFamily,
                                                         @Field("font_basesize") String fontBaseSize,
                                                         @Field("submit_border_width") String submitBorderWidth,
                                                         @Field("submit_border_radius") String submitBorderRadius,
                                                         @Field("submit_background") String submitBackground,
                                                         @Field("submit_alignment") String submitAlignment,
                                                         @Field("date_time") String dateTime,
                                                         @Field("mob_logo_img_fileName") String mobLogoImgFileName,
                                                         @Field("isWeb") String isWeb,
                                                         @Field("isMob") String isMob);

    @FormUrlEncoded
    @POST("api/")
    Call<SettingsResponseModel> updateSettingWithWebLogo(@Field("token") String token,
                                                         @Field("cmdEvent") String cmdEvent,
                                                         @Field("campaign_code") String campaignCode,
                                                         @Field("brand_code") String brandCode,
                                                         @Field("campaign_event") String campaignEventCode,
                                                         @Field("campaign_name") String formattedCampaignEventName,
                                                         @Field("foreground") String foreground,
                                                         @Field("background") String background,
                                                         @Field("header") String header,
                                                         @Field("footer") String footer,
                                                         @Field("footer_text") String footerText,
                                                         @Field("web_logo") String webLogo,
                                                         @Field("web_logo_img") String webLogoImg,
                                                         @Field("mob_logo") String mobLogo,
                                                         @Field("font_headfamily") String fontHeadFamily,
                                                         @Field("font_headsize") String fontHeadSize,
                                                         @Field("font_basefamily") String fontBaseFamily,
                                                         @Field("font_basesize") String fontBaseSize,
                                                         @Field("submit_border_width") String submitBorderWidth,
                                                         @Field("submit_border_radius") String submitBorderRadius,
                                                         @Field("submit_background") String submitBackground,
                                                         @Field("submit_alignment") String submitAlignment,
                                                         @Field("date_time") String dateTime,
                                                         @Field("web_logo_img_fileName") String webLogoImgFileName,
                                                         @Field("isWeb") String isWeb,
                                                         @Field("isMob") String isMob);

    @FormUrlEncoded
    @POST("api/")
    Call<SettingsResponseModel> updateSettingWithoutLogo(@Field("token") String token,
                                                         @Field("cmdEvent") String cmdEvent,
                                                         @Field("campaign_code") String campaignCode,
                                                         @Field("brand_code") String brandCode,
                                                         @Field("campaign_event") String campaignEventCode,
                                                         @Field("campaign_name") String formattedCampaignEventName,
                                                         @Field("foreground") String foreground,
                                                         @Field("background") String background,
                                                         @Field("header") String header,
                                                         @Field("footer") String footer,
                                                         @Field("footer_text") String footerText,
                                                         @Field("web_logo") String webLogo,
                                                         @Field("mob_logo") String mobLogo,
                                                         @Field("font_headfamily") String fontHeadFamily,
                                                         @Field("font_headsize") String fontHeadSize,
                                                         @Field("font_basefamily") String fontBaseFamily,
                                                         @Field("font_basesize") String fontBaseSize,
                                                         @Field("submit_border_width") String submitBorderWidth,
                                                         @Field("submit_border_radius") String submitBorderRadius,
                                                         @Field("submit_background") String submitBackground,
                                                         @Field("submit_alignment") String submitAlignment,
                                                         @Field("date_time") String dateTime,
                                                         @Field("isWeb") String isWeb,
                                                         @Field("isMob") String isMob);

    @FormUrlEncoded
    @POST("api/")
    Call<List<InputModel>> getFields(@Field("token") String token,
                                     @Field("cmdEvent") String cmdEvent,
                                     @Field("campaign_code") String campaignCode,
                                     @Field("brand_code") String brandCode,
                                     @Field("campaign_event") String campaign_event);

    @FormUrlEncoded
    @POST("webapp/")
    Call<DbmAddFieldsModel> addDbmField(@Field("token") String token,
                                        @Field("cmdEvent") String cmdEvent,
                                        @Field("campaign_code") String campaignCode,
                                        @Field("brand_code") String brandCode,
                                        @Field("campaign_event") String campaign_event,
                                        @Field("type_id") String typeId,
                                        @Field("title") String title,
                                        @Field("name") String name,
                                        @Field("description") String description,
                                        @Field("group_id") String group_id,
                                        @Field("is_required") String isRequired,
                                        @Field("is_active") String isActive,
                                        @Field("date_time") String dateTime,
                                        @Field("category") String category,
                                        @Field("host") String hostname);

    @FormUrlEncoded
    @POST("api/")
    Call<ResponseBody> addGroup(@Field("token") String token,
                                @Field("cmdEvent") String cmdEvent,
                                @Field("campaign_code") String campaign_code,
                                @Field("brand_code") String brand_code,
                                @Field("campaign_event") String campaign_event,
                                @Field("title") String title,
                                @Field("name") String name,
                                @Field("description") String description,
                                @Field("is_active") String isActive,
                                @Field("date_time") String date_time);

    @FormUrlEncoded
    @POST("api/")
    Call<List<GroupsModel>> getGroups(@Field("token") String token,
                                      @Field("cmdEvent") String cmdEvent,
                                      @Field("campaign_code") String campaign_code,
                                      @Field("brand_code") String brand_code,
                                      @Field("campaign_event") String campaign_event);

    @FormUrlEncoded
    @POST("api/")
    Call<GroupApiResponseModel> updateGroup(@Field("token") String token,
                                            @Field("cmdEvent") String cmdEvent,
                                            @Field("id") String id,
                                            @Field("title") String title,
                                            @Field("is_active") String isActive);

    @FormUrlEncoded
    @POST("api/")
    Call<GroupApiResponseModel> deleteGroup(@Field("token") String token,
                                            @Field("cmdEvent") String cmdEvent,
                                            @Field("id") String id,
                                            @Field("title") String title,
                                            @Field("is_active") String isActive);

}
