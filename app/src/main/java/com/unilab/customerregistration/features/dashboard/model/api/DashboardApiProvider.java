package com.unilab.customerregistration.features.dashboard.model.api;

import com.unilab.customerregistration.utilities.network.RestManager;

import retrofit2.Retrofit;

public class DashboardApiProvider {

    public static DashboardApiRoutes getDashboardApiRoutes() {
        Retrofit mRetrofit = RestManager.getInstance().getWebRetrofitAdapter();
        return mRetrofit.create(DashboardApiRoutes.class);
    }

}
