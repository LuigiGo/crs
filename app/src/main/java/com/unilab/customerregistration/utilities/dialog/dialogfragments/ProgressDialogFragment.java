package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import com.unilab.customerregistration.utilities.constants.Constants;

public class ProgressDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static ProgressDialogFragment frag;
    ProgressDialog mProgressDialog;

    public static ProgressDialogFragment newInstance(String message) {
        frag = new ProgressDialogFragment();
        Bundle args = new Bundle();
        args.putString("message", message);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        String message = getArguments().getString("message");
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(message);

        return mProgressDialog;
    }

    public static void showDialog(Context context, DialogFragment dialogFragment, String message) {
//        ProgressDialogFragment.newInstance(message);
        dialogFragment.show(((Activity) context).getFragmentManager(), Constants.PROGRESS_DIALOG_FRAGMENT);

    }
}
