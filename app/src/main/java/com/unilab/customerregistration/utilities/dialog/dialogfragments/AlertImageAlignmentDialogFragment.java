package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.constants.Constants;

public class AlertImageAlignmentDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static AlertImageAlignmentDialogFragment frag;
    AlertDialog mAlertSetLogoAlignmentDialog;
    private MaintenancePresenter mMaintenancePresenter;

    public static AlertImageAlignmentDialogFragment newInstance(int logoType, SettingsModel settingsModel) {
        frag = new AlertImageAlignmentDialogFragment();
        Bundle args = new Bundle();
        args.putInt("logoType", logoType);
        args.putParcelable("settings_model", settingsModel);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final int logoType = getArguments().getInt("logoType");
        final SettingsModel mSettingsModel = getArguments().getParcelable("settings_model");

        mMaintenancePresenter = (MaintenancePresenter) getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_logo_alignment, null);
        builder.setView(v);

        ImageView imgAlignLeft = (ImageView) v.findViewById(R.id.imgAlignLeft);
        ImageView imgAlignCenter = (ImageView) v.findViewById(R.id.imgAlignCenter);
        ImageView imgAlignRight = (ImageView) v.findViewById(R.id.imgAlignRight);

        Log.e(TAG, "onCreateDialog: WebLogoAlignment: " + mSettingsModel.getWebLogo());
        Log.e(TAG, "onCreateDialog: MobLogoAlignment: " + mSettingsModel.getMobLogo());

        switch (logoType) {
            case Constants.SET_WEB_LOGO_ALIGNMENT:
                switch (mSettingsModel.getWebLogo()){
                    case Constants.LEFT_ALIGNMENT:
                        imgAlignLeft.setImageResource(R.drawable.ic_selected_left);
                        break;
                    case Constants.CENTER_ALIGNMENT:
                        imgAlignCenter.setImageResource(R.drawable.ic_selected_center);
                        break;
                    case Constants.RIGHT_ALIGNMENT:
                        imgAlignRight.setImageResource(R.drawable.ic_selected_right);
                        break;
                }
                break;

            case Constants.SET_MOBILE_LOGO_ALIGNMENT:
                switch (mSettingsModel.getMobLogo()){
                    case Constants.LEFT_ALIGNMENT:
                        imgAlignLeft.setImageResource(R.drawable.ic_selected_left);
                        break;
                    case Constants.CENTER_ALIGNMENT:
                        imgAlignCenter.setImageResource(R.drawable.ic_selected_center);
                        break;
                    case Constants.RIGHT_ALIGNMENT:
                        imgAlignRight.setImageResource(R.drawable.ic_selected_right);
                        break;
                }
                break;
        }


        imgAlignLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (logoType) {
                    case Constants.SET_WEB_LOGO_ALIGNMENT:
                        mSettingsModel.setWebLogo(Constants.LEFT_ALIGNMENT);
                        break;
                    case Constants.SET_MOBILE_LOGO_ALIGNMENT:
                        mSettingsModel.setMobLogo(Constants.LEFT_ALIGNMENT);
                        break;
                }
                mMaintenancePresenter.setImageAlignment(mSettingsModel);
                dismiss();
            }
        });

        imgAlignCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (logoType) {
                    case Constants.SET_WEB_LOGO_ALIGNMENT:
                        mSettingsModel.setWebLogo(Constants.CENTER_ALIGNMENT);
                        break;
                    case Constants.SET_MOBILE_LOGO_ALIGNMENT:
                        mSettingsModel.setMobLogo(Constants.CENTER_ALIGNMENT);
                        break;
                }
                mMaintenancePresenter.setImageAlignment(mSettingsModel);
                dismiss();
            }
        });

        imgAlignRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (logoType) {
                    case Constants.SET_WEB_LOGO_ALIGNMENT:
                        mSettingsModel.setWebLogo(Constants.RIGHT_ALIGNMENT);
                        break;
                    case Constants.SET_MOBILE_LOGO_ALIGNMENT:
                        mSettingsModel.setMobLogo(Constants.RIGHT_ALIGNMENT);
                        break;
                }
                mMaintenancePresenter.setImageAlignment(mSettingsModel);
                dismiss();
            }
        });


        mAlertSetLogoAlignmentDialog = builder.create();
        mAlertSetLogoAlignmentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAlertSetLogoAlignmentDialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        int dialogWidth = getActivity().getResources().getInteger(R.integer.dialog_image_alignment_width);
        Window window = getDialog().getWindow();
        window.setLayout(dialogWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
