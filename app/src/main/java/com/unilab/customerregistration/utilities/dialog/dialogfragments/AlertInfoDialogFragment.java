package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unilab.customerregistration.R;

public class AlertInfoDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static AlertInfoDialogFragment frag;
    AlertDialog mAlertInfoDialog;

    public static AlertInfoDialogFragment newInstance(String title, String message) {
        frag = new AlertInfoDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String message = getArguments().getString("message");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        getActivity().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_information, null);
        builder.setView(v);
        builder.setCancelable(false);

        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        Button btnOk = (Button) v.findViewById(R.id.btnOk);

        txtTitle.setText(title);
        txtMessage.setText(message);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mAlertInfoDialog = builder.create();
        mAlertInfoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAlertInfoDialog;
    }
}
