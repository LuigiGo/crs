package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.features.maintenance.view.adapters.DataArrayAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.SubGroupingAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.AddUpdateFieldSubGroupNameAdapter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.constants.DatabaseConstants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.network.NetworkUtils;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class AddFieldAdapterDialogFragment extends DialogFragment implements View.OnTouchListener {

    private final String TAG = this.getClass().getSimpleName();
    private static AddFieldAdapterDialogFragment frag;
    AlertDialog mEditFieldDialog, mAlertSavePromptDialog;
    private DatabaseHelper mDatabaseHelper;
    private String brandCode, campaignCode, eventCode;
    private List<DataArray> dataArrayList;
    private List<String> groupsNameList;
    private List<GroupsModel> mGroupsModelDropdownList = new ArrayList<>();
    private AddUpdateFieldSubGroupNameAdapter mAddUpdateFieldSubGroupNameAdapter;
    private MaintenancePresenter mMaintenancePresenter;
    private RelativeLayout rlFieldDetailsContainer;

    public static AddFieldAdapterDialogFragment newInstance(FieldsModel fieldsModel) {
        frag = new AddFieldAdapterDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("field_model", fieldsModel);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final FieldsModel fieldsModel = getArguments().getParcelable("field_model");

        /**Initialize variables*/
        mDatabaseHelper = new DatabaseHelper(getActivity());
        SharedPrefHelper.init(getActivity());

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        mMaintenancePresenter = (MaintenancePresenter) getActivity();

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_input_fields, null);
        alertBuilder.setView(v);

        /**Views*/
        final TextView txtHeader = (TextView) v.findViewById(R.id.txtHeader);
        final EditText edtFieldName = (EditText) v.findViewById(R.id.edtNameOfField);
        final EditText edtGroupName = (EditText) v.findViewById(R.id.edtSubGroupName);
        final EditText edtOptionsTitle = (EditText) v.findViewById(R.id.edtOptionsTitle);
        final EditText edtOptionsValue = (EditText) v.findViewById(R.id.edtOptionsValue);
        final EditText edtDescription = (EditText) v.findViewById(R.id.edtDescription);
        final CheckBox chRequired = (CheckBox) v.findViewById(R.id.chRequired);
        final Spinner spnInputType = (Spinner) v.findViewById(R.id.spnInputType);
        final Spinner spnSubGroup = (Spinner) v.findViewById(R.id.spnSubGroup);
        final LinearLayout llInputOptions = (LinearLayout) v.findViewById(R.id.llInputOptions);
        ImageView btnAddOptions = (ImageView) v.findViewById(R.id.imgAddInputOptions);
        ImageView btnAddGroup = (ImageView) v.findViewById(R.id.imgAddSubGroup);
        Button btnSave = (Button) v.findViewById(R.id.btnSave);
        final Button btnCancel = (Button) v.findViewById(R.id.btnCancel);
        final RelativeLayout rlAddSubGroup = (RelativeLayout) v.findViewById(R.id.rlAddSubgroup);
        Button btnAddSubGrouping = (Button) v.findViewById(R.id.btnAddSubGrouping);
        ImageView imgCloseSubGrouping = (ImageView) v.findViewById(R.id.imgCloseSubGrouping);

        LinearLayout llAddFieldRootView = (LinearLayout) v.findViewById(R.id.llAddFieldRootView);
        rlFieldDetailsContainer = (RelativeLayout) v.findViewById(R.id.rlFieldDetailsContainer);

        btnSave.setText("Update");

        RecyclerView rvInputOptions = (RecyclerView) v.findViewById(R.id.rvInputOptions);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        rvInputOptions.setLayoutManager(mLayoutManager);
        rvInputOptions.setItemAnimator(new DefaultItemAnimator());

        RecyclerView rvGroupsList = (RecyclerView) v.findViewById(R.id.rvGroupsList);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity().getApplicationContext());
        rvGroupsList.setLayoutManager(mLayoutManager1);
        rvGroupsList.setItemAnimator(new DefaultItemAnimator());
        rvGroupsList.setHasFixedSize(false);

        List<String> inputTypeList = mDatabaseHelper.getInputType();
        spnInputType.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_list_item, inputTypeList) {
            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                final View v = super.getView(position, convertView, parent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((TextView) v).setTextColor(Color.WHITE);
                    }
                }, 50);
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                final View v = super.getDropDownView(position, convertView, parent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((TextView) v).setTextColor(Color.WHITE);
                        ((TextView) v).setBackgroundColor(getResources().getColor(R.color.darkest_gray));
                    }
                }, 50);
                return v;
            }
        });

        String tableName = null;
        Log.e(TAG, "editFieldDialog: " + fieldsModel.getTypeId());
        switch (fieldsModel.getTypeId()) {
            case "1":
                tableName = DatabaseConstants.TBL_TEXTBOX;
                spnInputType.setSelection(0);
                break;

            case "2":
                tableName = DatabaseConstants.TBL_CHECKBOX;
                break;

            case "3":
                tableName = DatabaseConstants.TBL_RADIO;
                spnInputType.setSelection(1);
                break;

            case "4":
                tableName = DatabaseConstants.TBL_SELECT;
                spnInputType.setSelection(2);
                break;
        }

        /**Adapter*/
        final List<DataArray> dataArrayList = mDatabaseHelper.getDataArrayByFieldId(tableName, fieldsModel.getB_id());
        final DataArrayAdapter dataArrayAdapter = new DataArrayAdapter(getActivity(), dataArrayList);
        rvInputOptions.setAdapter(dataArrayAdapter);

        txtHeader.setText("Update Field");
        edtDescription.setText(fieldsModel.getDescription());
        chRequired.setChecked((fieldsModel.getIsRequired().equals("1")));
        edtFieldName.setText(fieldsModel.getTitle());

        /**Setting up subgroup name dropdown list*/
        mGroupsModelDropdownList.addAll(mDatabaseHelper.getGroupNames(brandCode, campaignCode, eventCode));
        mAddUpdateFieldSubGroupNameAdapter = new AddUpdateFieldSubGroupNameAdapter(getActivity(),
                android.R.layout.simple_list_item_1, mGroupsModelDropdownList);
        spnSubGroup.setAdapter(mAddUpdateFieldSubGroupNameAdapter);
        for (int i = 0; i < mGroupsModelDropdownList.size(); i++) {
            GroupsModel groupsModel = mGroupsModelDropdownList.get(i);
            if (groupsModel.getId().equals(fieldsModel.getGroupId())) {
                spnSubGroup.setSelection(i);
            }
        }

        final List<GroupsModel> groupModelList = new ArrayList<>();
        groupModelList.addAll(mDatabaseHelper.getAllGroupsByBrandCampaignEvent(brandCode, campaignCode, eventCode));
        final SubGroupingAdapter mSubGroupingAdapter = new SubGroupingAdapter(getActivity(), groupModelList, mGroupsModelDropdownList, mAddUpdateFieldSubGroupNameAdapter);
        rvGroupsList.setAdapter(mSubGroupingAdapter);

        /**Listeners*/
        llAddFieldRootView.setOnTouchListener(this);
        rlFieldDetailsContainer.setOnTouchListener(this);
        spnInputType.setOnTouchListener(this);
        spnSubGroup.setOnTouchListener(this);
        btnAddSubGrouping.setOnTouchListener(this);
        btnAddGroup.setOnTouchListener(this);
        imgCloseSubGrouping.setOnTouchListener(this);

        spnSubGroup.setOnItemSelectedListener(spinnerListener);
        spnInputType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> adapterView, View view, final int i, long l) {
                Log.e(TAG, "onItemSelected: DataArrayListSize:  " + dataArrayList.size());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (adapterView.getItemAtPosition(i).equals("Radio Button") ||
                                adapterView.getItemAtPosition(i).equals("Dropdown")) {

                            if (dataArrayList.size() == 0) {
                                DataArray dataArray = new DataArray();
                                dataArray.setTitle("");
                                dataArray.setName("");
                                dataArray.setValue("");
                                dataArray.setIsActive("1");
                                dataArray.setDateTime(Utils.getDateTime());
                                dataArrayList.add(dataArray);
                                dataArrayAdapter.notifyDataSetChanged();
                            }

                            llInputOptions.setVisibility(View.VISIBLE);
                        } else {
                            llInputOptions.setVisibility(View.GONE);
                            dataArrayList.clear();
                            dataArrayAdapter.notifyDataSetChanged();
                        }
                    }
                }, 300);
                Log.e(TAG, "onItemSelected: " + dataArrayList.size());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnAddGroup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final String groupName = edtGroupName.getText().toString();
                final String title = groupName;
                final String name = groupName.replace(" ", "_").toLowerCase();

                if (name.equals("personal_information") ||
                        name.equals("contact_information") ||
                        name.equals("login_information") ||
                        name.equals("other_information")) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Group name exist!", Constants.DIALOG_ACTION_NORMAL);

                } else if (mDatabaseHelper.checkIfGroupIsExist(brandCode, campaignCode, eventCode, name)) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Group name exist!", Constants.DIALOG_ACTION_NORMAL);

                } else {
                    if (!groupName.isEmpty() && !groupName.equals("")) {
                        Log.e(TAG, "onClick: BrandCode: " + brandCode);
                        Log.e(TAG, "onClick: CampaignCode: " + campaignCode);

                        int maxGroupId = mDatabaseHelper.getMaxGroupId();
                        ++maxGroupId;
                        GroupsModel groupsModel = new GroupsModel();
                        groupsModel.setId(String.valueOf(maxGroupId));
                        groupsModel.setTitle(title);
                        groupsModel.setName(name);
                        groupsModel.setDescription("");
                        groupsModel.setBrandCode(brandCode);
                        groupsModel.setCampaignCode(campaignCode);
                        groupsModel.setEventCode(eventCode);
                        groupsModel.setIsActive("1");
                        groupsModel.setUserId("");
                        groupsModel.setDateTime(Utils.getDateTime());
                        groupsModel.setStatus("1");

                        if (NetworkUtils.isNetworkConnected(getActivity())) {
                            Log.e(TAG, "onClick: ADD GROUPS API" + " Connected");
                            mMaintenancePresenter.addFormGroups(groupsModel, edtGroupName, mGroupsModelDropdownList, mAddUpdateFieldSubGroupNameAdapter,
                                    groupModelList, mSubGroupingAdapter);
                        } else {
                            mMaintenancePresenter.showAlertDialog("Alert", "New Sub-group is successfully added.", Constants.DIALOG_ACTION_NORMAL);
                            edtGroupName.setText("");
                            mDatabaseHelper.createGroup(groupsModel);
                            groupsNameList.add(groupName);
                            groupModelList.add(groupsModel);
                        }
                    } else {
                        mMaintenancePresenter.showAlertDialog("Alert", "Please fill-out required field.", Constants.DIALOG_ACTION_NORMAL);
                    }
                }
            }
        });

        btnAddSubGrouping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlAddSubGroup.setVisibility(View.VISIBLE);
            }
        });

        imgCloseSubGrouping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlAddSubGroup.setVisibility(View.GONE);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fieldName = edtFieldName.getText().toString();
                if (fieldName.isEmpty()) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please fill out all required fields.", Constants.DIALOG_ACTION_NORMAL);
                } else {
                    mMaintenancePresenter.showAddFormFieldAdapterSavePrompt("Confirm", "Are you sure you want to Update?",
                            edtFieldName, dataArrayList, spnInputType, spnSubGroup, mAddUpdateFieldSubGroupNameAdapter,
                            edtDescription, fieldsModel, chRequired);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        mEditFieldDialog = alertBuilder.create();
        mEditFieldDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        return mEditFieldDialog;
    }

    AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//            ((TextView) view).setTextColor(getActivity().getResources().getColor(R.color.midnight_blue));
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.e(TAG, "onTouch: " + v.getTag());
        if (v instanceof EditText) {
            Log.e(TAG, "onTouch: " + "View is an Edittext");
        } else {
            mMaintenancePresenter.hideKeyboardInFragment(getActivity(), v.getWindowToken());
        }

//        switch (v.getTag().toString()) {
//            case "llAddFieldRootView":
//                mMaintenancePresenter.hideKeyboardInFragment(getActivity(), v.getWindowToken());
//                break;
//
//            case "rlFieldDetailsContainer":
//                mMaintenancePresenter.hideKeyboardInFragment(getActivity(), v.getWindowToken());
//                break;
//        }
        return false;
    }
}
