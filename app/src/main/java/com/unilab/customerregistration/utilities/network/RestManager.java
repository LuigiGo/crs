package com.unilab.customerregistration.utilities.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.unilab.customerregistration.utilities.constants.Constants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestManager {

    private static RestManager sRestManager;
    private Retrofit mDBMRetrofitAdapter, mDBMRetrofitAdapter2, mWebRetrofitAdapter;
    private static OkHttpClient sOkHttpClient;

    public static void init() {
        if (sRestManager == null) {
            sRestManager = new RestManager();

            // Define the interceptor, add authentication headers
            Interceptor interceptor = new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    okhttp3.Request newRequest = chain.request()
                            .newBuilder()
                            .build();
                    return chain.proceed(newRequest);
                }
            };

            // Add the interceptor to OkHttpClient
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.interceptors().add(interceptor);
            sOkHttpClient = builder
                    .connectTimeout(20, TimeUnit.MINUTES)
                    .readTimeout(30, TimeUnit.MINUTES)
                    .build();
        }
    }

    public static RestManager getInstance() {
        if (sRestManager != null) {
            return sRestManager;
        }
        throw new IllegalStateException("Call RestManager init() method");
    }


    public Retrofit getDBMRetrofitAdapter() {
        if (mDBMRetrofitAdapter == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            mDBMRetrofitAdapter = new Retrofit.Builder()
                    .baseUrl(Constants.API_DBM_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(sOkHttpClient)
                    .build();
        }
        return mDBMRetrofitAdapter;
    }

//    public Retrofit getDBMRetrofitAdapter2() {
//        if (mDBMRetrofitAdapter2 == null) {
//            Gson gson = new GsonBuilder()
//                    .setLenient()
//                    .create();
//
//            mDBMRetrofitAdapter2 = new Retrofit.Builder()
//                    .baseUrl(Constants.API_DBM_BASE_URL2)
//                    .addConverterFactory(GsonConverterFactory.create(gson))
//                    .client(sOkHttpClient)
//                    .build();
//        }
//        return mDBMRetrofitAdapter2;
//    }

    public Retrofit getWebRetrofitAdapter() {
        if (mWebRetrofitAdapter == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            mWebRetrofitAdapter = new Retrofit.Builder()
                    .baseUrl(Constants.API_WEB_BASE_URL3)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(sOkHttpClient)
                    .build();
        }
        return mWebRetrofitAdapter;
    }
}
