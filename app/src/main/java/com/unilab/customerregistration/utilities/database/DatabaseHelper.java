package com.unilab.customerregistration.utilities.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.unilab.customerregistration.features.dashboard.model.CityModel;
import com.unilab.customerregistration.features.dashboard.model.CountryModel;
import com.unilab.customerregistration.features.dashboard.model.ProvinceModel;
import com.unilab.customerregistration.features.dashboard.model.RegionModel;
import com.unilab.customerregistration.features.maintenance.model.AssignFieldModel;
import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.DefaultSettingModel;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.model.InputModel;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.registration.model.RegistrationModel;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.constants.DatabaseConstants;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private final String TAG = this.getClass().getSimpleName();
    private SQLiteDatabase db;
    private Context mContext;

    public DatabaseHelper(Context context) {
        super(context, DatabaseConstants.DATABASE_NAME, null, DatabaseConstants.DATABASE_VERSION);
        mContext = context;
        Log.e(TAG, "DatabaseHelper: Initialize DatabaseHelper");
        Log.e(TAG, "DatabaseHelper: databaseExist: " + doesDatabaseExist(mContext));

        if (!doesDatabaseExist(mContext)) {
            copyDataBase();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e(TAG, "onCreate: ");
//        db.execSQL(DatabaseConstants.CREATE_INPUT_TABLE);
//        db.execSQL(DatabaseConstants.CREATE_INPUT_ASSIGN_TABLE);
//        db.execSQL(DatabaseConstants.CREATE_INPUT_TYPE_TABLE);
//        db.execSQL(DatabaseConstants.CREATE_GROUP_TABLE);
//        db.execSQL(DatabaseConstants.CREATE_SETTING_TABLE);
//        db.execSQL(DatabaseConstants.CREATE_REGISTRATION_TABLE);
//
//        db.execSQL(DatabaseConstants.CREATE_TEXTBOX_TABLE);
//        db.execSQL(DatabaseConstants.CREATE_CHECKBOX_TABLE);
//        db.execSQL(DatabaseConstants.CREATE_RADIO_TABLE);
//        db.execSQL(DatabaseConstants.CREATE_SELECT_TABLE);
//
//        db.execSQL(DatabaseConstants.CREATE_DEFAULT_FIELDS_TABLE);
//        db.execSQL(DatabaseConstants.CREATE_DEFAULT_VIEWS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e(TAG, "onUpgrade: ");

    }

    private void copyDataBase() {
        Log.e(TAG, "copyDataBase: ");

        File projDir = new File(DatabaseConstants.DATABASE_DEVICE_PATH);
        if (!projDir.exists()) {
            projDir.mkdirs();
        }

        String outFileName = DatabaseConstants.DATABASE_DEVICE_PATH
                + DatabaseConstants.DATABASE_NAME;

        try {
            OutputStream myOutput = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int length;

            InputStream myInput = mContext.getAssets()
                    .open(DatabaseConstants.DATABASE_ASSETS_PATH);

            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }

            myInput.close();
            myOutput.flush();
            myOutput.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "copyDataBase: " + e.getMessage());
        }
    }

    private static boolean doesDatabaseExist(Context context) {
        File dbFile = context.getDatabasePath(DatabaseConstants.DATABASE_NAME);
        return dbFile.exists();
    }

    public int getMaxInputAssignId() {
        db = getReadableDatabase();
        String query = "select max(_id) from tbl_input_assign";

        Cursor c = db.rawQuery(query, null);

        if (c != null && c.moveToFirst()) {
            return c.getInt(0);
        }

        return 0;
    }

    public String getGroupNameById(int groupId) {
        db = getReadableDatabase();
        String[] column = {DatabaseConstants.KEY_TITLE};
        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {String.valueOf(groupId)};
        Cursor c = db.query(DatabaseConstants.TBL_GROUP, column, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            return c.getString(0);
        }
        db.close();
        return "";
    }

    public SettingsModel getSettingsConfig(String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        SettingsModel settingsModel = null;
        String where = DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=?";
        String[] whereArgs = {brandCode, campaignCode, eventCode};

        Cursor c = db.query(DatabaseConstants.TBL_SETTING, null, where, whereArgs,
                null, null, null, null);

        if (c != null && c.moveToFirst()) {
            settingsModel = new SettingsModel();
            settingsModel.setId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_ID)));
            settingsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
            settingsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
            settingsModel.setEventCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EVENT_CODE)));
            settingsModel.setForeground(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FOREGROUND_COLOR)));
            settingsModel.setBackground(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BACKGROUND_COLOR)));
            settingsModel.setHeader(c.getString(c.getColumnIndex(DatabaseConstants.KEY_HEADER_COLOR)));
            settingsModel.setFooter(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FOOTER_COLOR)));
            settingsModel.setFooterText(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FOOTER_TEXT)));
            settingsModel.setWebLogo(c.getString(c.getColumnIndex(DatabaseConstants.KEY_WEB_LOGO)));
            settingsModel.setWebLogoImg(c.getString(c.getColumnIndex(DatabaseConstants.KEY_WEB_LOGO_IMG)));
            settingsModel.setMobLogo(c.getString(c.getColumnIndex(DatabaseConstants.KEY_MOBILE_LOGO)));
            settingsModel.setMobLogoImg(c.getString(c.getColumnIndex(DatabaseConstants.KEY_MOBILE_LOGO_IMG)));
            settingsModel.setFontHeadfamily(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FONT_HEADFAMILY)));
            settingsModel.setFontHeadsize(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FONT_HEADSIZE)));
            settingsModel.setFontBasefamily(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FONT_BASEFAMILY)));
            settingsModel.setFontBasesize(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FONT_BASESIZE)));
            settingsModel.setSubmitBorderWidth(c.getString(c.getColumnIndex(DatabaseConstants.KEY_SUBMIT_BORDER_WIDTH)));
            settingsModel.setSubmitBorderRadius(c.getString(c.getColumnIndex(DatabaseConstants.KEY_SUBMIT_BORDER_RADIUS)));
            settingsModel.setSubmitBackground(c.getString(c.getColumnIndex(DatabaseConstants.KEY_SUBMIT_BACKGROUND)));
            settingsModel.setSubmitAlignment(c.getString(c.getColumnIndex(DatabaseConstants.KEY_SUBMIT_ALIGNMENT)));
            settingsModel.setIsActive(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_ACTIVE)));
            settingsModel.setDateTime(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DATE_TIME)));
            settingsModel.setWebLogoImgFileName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_WEB_LOGO_IMG_FILENAME)));
            settingsModel.setMobLogoImgFileName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_MOB_LOGO_IMG_FILENAME)));
            c.close();
            db.close();
        }

        return settingsModel;
    }

    /**
     * Latest Database Calls
     */
    public boolean checkSettingsIfExist(String brandCode, String campaignCode, String eventCode, String formattedCampaignEventName) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_NAME + "=?";
        String[] whereArgs = {brandCode, campaignCode, eventCode, formattedCampaignEventName};

        Cursor c = db.query(DatabaseConstants.TBL_SETTING, null, where, whereArgs,
                null, null, null, null);
        if (c != null) {
            if (c.moveToFirst()) {
                c.close();
                return true;
            }
        }
        db.close();
        return false;

    }

    public void createDbSetting(SettingsModel settingsModel) {
        Log.e(TAG, "createDbSetting: ");
        db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_ID, settingsModel.getId());
        cv.put(DatabaseConstants.KEY_CAMPAIGN_CODE, settingsModel.getCampaignCode());
        cv.put(DatabaseConstants.KEY_BRAND_CODE, settingsModel.getBrandCode());
        cv.put(DatabaseConstants.KEY_EVENT_CODE, settingsModel.getEventCode());
        cv.put(DatabaseConstants.KEY_CAMPAIGN_NAME, settingsModel.getFormattedEventName());
        cv.put(DatabaseConstants.KEY_FOREGROUND_COLOR, settingsModel.getForeground());
        cv.put(DatabaseConstants.KEY_BACKGROUND_COLOR, settingsModel.getBackground());
        cv.put(DatabaseConstants.KEY_HEADER_COLOR, settingsModel.getHeader());
        cv.put(DatabaseConstants.KEY_FOOTER_COLOR, settingsModel.getFooter());
        cv.put(DatabaseConstants.KEY_FOOTER_TEXT, settingsModel.getFooterText());
        cv.put(DatabaseConstants.KEY_WEB_LOGO, settingsModel.getWebLogo());
        cv.put(DatabaseConstants.KEY_WEB_LOGO_IMG, settingsModel.getWebLogoImg());
        cv.put(DatabaseConstants.KEY_MOBILE_LOGO, settingsModel.getMobLogo());
        cv.put(DatabaseConstants.KEY_MOBILE_LOGO_IMG, settingsModel.getMobLogoImg());
        cv.put(DatabaseConstants.KEY_FONT_HEADFAMILY, settingsModel.getFontHeadfamily());
        cv.put(DatabaseConstants.KEY_FONT_HEADSIZE, settingsModel.getFontHeadsize());
        cv.put(DatabaseConstants.KEY_FONT_BASEFAMILY, settingsModel.getFontBasefamily());
        cv.put(DatabaseConstants.KEY_FONT_BASESIZE, settingsModel.getFontBasesize());
        cv.put(DatabaseConstants.KEY_SUBMIT_BORDER_WIDTH, settingsModel.getSubmitBorderWidth());
        cv.put(DatabaseConstants.KEY_SUBMIT_BORDER_RADIUS, settingsModel.getSubmitBorderRadius());
        cv.put(DatabaseConstants.KEY_SUBMIT_BACKGROUND, settingsModel.getSubmitBackground());
        cv.put(DatabaseConstants.KEY_SUBMIT_ALIGNMENT, settingsModel.getSubmitAlignment());
        cv.put(DatabaseConstants.KEY_IS_ACTIVE, settingsModel.getIsActive());
        cv.put(DatabaseConstants.KEY_DATE_TIME, settingsModel.getDateTime());
        cv.put(DatabaseConstants.KEY_WEB_LOGO_IMG_FILENAME, settingsModel.getWebLogoImgFileName());
        cv.put(DatabaseConstants.KEY_MOB_LOGO_IMG_FILENAME, settingsModel.getMobLogoImgFileName());


        if (settingsModel.getStatus() == null) {
            cv.put(DatabaseConstants.KEY_STATUS, "0");
        } else {
            cv.put(DatabaseConstants.KEY_STATUS, settingsModel.getStatus());
        }
        db.insert(DatabaseConstants.TBL_SETTING, null, cv);
        db.close();
    }

    public void updateDbSetting(SettingsModel settingsModel) {
        Log.e(TAG, "updateDbSetting: " + settingsModel.toString());
        db = getWritableDatabase();
        String where = DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_NAME + "=?";

        String[] whereArgs = {settingsModel.getBrandCode(), settingsModel.getCampaignCode(),
                settingsModel.getEventCode(), settingsModel.getFormattedEventName()};

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_FOREGROUND_COLOR, settingsModel.getForeground());
        cv.put(DatabaseConstants.KEY_BACKGROUND_COLOR, settingsModel.getBackground());
        cv.put(DatabaseConstants.KEY_HEADER_COLOR, settingsModel.getHeader());
        cv.put(DatabaseConstants.KEY_FOOTER_COLOR, settingsModel.getFooter());
        cv.put(DatabaseConstants.KEY_FOOTER_TEXT, settingsModel.getFooterText());
        cv.put(DatabaseConstants.KEY_WEB_LOGO, settingsModel.getWebLogo());
        cv.put(DatabaseConstants.KEY_WEB_LOGO_IMG, settingsModel.getWebLogoImg());
        cv.put(DatabaseConstants.KEY_MOBILE_LOGO, settingsModel.getMobLogo());
        cv.put(DatabaseConstants.KEY_MOBILE_LOGO_IMG, settingsModel.getMobLogoImg());
        cv.put(DatabaseConstants.KEY_FONT_HEADFAMILY, settingsModel.getFontHeadfamily());
        cv.put(DatabaseConstants.KEY_FONT_HEADSIZE, settingsModel.getFontHeadsize());
        cv.put(DatabaseConstants.KEY_FONT_BASEFAMILY, settingsModel.getFontBasefamily());
        cv.put(DatabaseConstants.KEY_FONT_BASESIZE, settingsModel.getFontBasesize());
        cv.put(DatabaseConstants.KEY_SUBMIT_BORDER_WIDTH, settingsModel.getSubmitBorderWidth());
        cv.put(DatabaseConstants.KEY_SUBMIT_BORDER_RADIUS, settingsModel.getSubmitBorderRadius());
        cv.put(DatabaseConstants.KEY_SUBMIT_BACKGROUND, settingsModel.getSubmitBackground());
        cv.put(DatabaseConstants.KEY_SUBMIT_ALIGNMENT, settingsModel.getSubmitAlignment());
        cv.put(DatabaseConstants.KEY_IS_ACTIVE, settingsModel.getIsActive());
        cv.put(DatabaseConstants.KEY_DATE_TIME, settingsModel.getDateTime());
        cv.put(DatabaseConstants.KEY_STATUS, "1");
        db.update(DatabaseConstants.TBL_SETTING, cv, where, whereArgs);
        db.close();
    }

    public SettingsModel getDbSetting(String brandCode, String campaignCode, String eventCode, String formattedEventName) {
        db = getReadableDatabase();
        SettingsModel settingsModel = null;
        String where = DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_NAME + "=?";
        String[] whereArgs = {brandCode, campaignCode, eventCode, formattedEventName};

        Cursor c = db.query(DatabaseConstants.TBL_SETTING, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            do {
                settingsModel = new SettingsModel();
                settingsModel.setId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_ID)));
                settingsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                settingsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                settingsModel.setEventCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EVENT_CODE)));
                settingsModel.setFormattedEventName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_NAME)));
                settingsModel.setForeground(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FOREGROUND_COLOR)));
                settingsModel.setBackground(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BACKGROUND_COLOR)));
                settingsModel.setHeader(c.getString(c.getColumnIndex(DatabaseConstants.KEY_HEADER_COLOR)));
                settingsModel.setFooter(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FOOTER_COLOR)));
                settingsModel.setFooterText(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FOOTER_TEXT)));
                settingsModel.setWebLogo(c.getString(c.getColumnIndex(DatabaseConstants.KEY_WEB_LOGO)));
                settingsModel.setWebLogoImg(c.getString(c.getColumnIndex(DatabaseConstants.KEY_WEB_LOGO_IMG)));
                settingsModel.setMobLogo(c.getString(c.getColumnIndex(DatabaseConstants.KEY_MOBILE_LOGO)));
                settingsModel.setMobLogoImg(c.getString(c.getColumnIndex(DatabaseConstants.KEY_MOBILE_LOGO_IMG)));
                settingsModel.setFontHeadfamily(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FONT_HEADFAMILY)));
                settingsModel.setFontHeadsize(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FONT_HEADSIZE)));
                settingsModel.setFontBasefamily(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FONT_BASEFAMILY)));
                settingsModel.setFontBasesize(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FONT_BASESIZE)));
                settingsModel.setSubmitBorderWidth(c.getString(c.getColumnIndex(DatabaseConstants.KEY_SUBMIT_BORDER_WIDTH)));
                settingsModel.setSubmitBorderRadius(c.getString(c.getColumnIndex(DatabaseConstants.KEY_SUBMIT_BORDER_RADIUS)));
                settingsModel.setSubmitBackground(c.getString(c.getColumnIndex(DatabaseConstants.KEY_SUBMIT_BACKGROUND)));
                settingsModel.setSubmitAlignment(c.getString(c.getColumnIndex(DatabaseConstants.KEY_SUBMIT_ALIGNMENT)));
                settingsModel.setIsActive(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_ACTIVE)));
                settingsModel.setDateTime(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DATE_TIME)));
                settingsModel.setStatus(c.getString(c.getColumnIndex(DatabaseConstants.KEY_STATUS)));
            } while (c.moveToNext());
            c.close();
        }
        db.close();

        return settingsModel;
    }

    public boolean checkDefaultFieldIfAssigned(String brandCode, String campaignCode, String eventCode, String inputId) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=? AND " +
                DatabaseConstants.KEY_INPUT_ID + "=?";
        String[] whereArgs = {brandCode, campaignCode, eventCode, inputId};
        Cursor c = db.query(DatabaseConstants.TBL_INPUT_ASSIGN, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            c.close();
            return true;
        }
        db.close();
        return false;
    }

    public List<InputModel> getDefaultFields() {
        db = getReadableDatabase();
        ArrayList<InputModel> arrayList = new ArrayList<>();
        String where = DatabaseConstants.KEY_CATEGORY + " IN(0, 1)";

        Cursor c = db.query(DatabaseConstants.TBL_INPUT, null, where, null,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            do {
                InputModel inputModel = new InputModel();
                inputModel.setId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_ID)));
                inputModel.setTypeId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TYPE_ID)));
                inputModel.setTitle(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TITLE)));
                inputModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                inputModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                inputModel.setGroupId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_GROUP_ID)));
                inputModel.setIsRequired(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_REQUIRED)));
                inputModel.setIsActive(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_ACTIVE)));
                inputModel.setDateTime(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DATE_TIME)));
                inputModel.setCategory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CATEGORY)));
                inputModel.setStatus(c.getString(c.getColumnIndex(DatabaseConstants.KEY_STATUS)));
                inputModel.setEditStatus(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EDIT_STATUS)));
                inputModel.setIsMandatory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_MANDATORY)));
                arrayList.add(inputModel);
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return arrayList;
    }

    public void assignField(AssignFieldModel assignFieldModel) {
        db = getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_ID, assignFieldModel.getId());
        cv.put(DatabaseConstants.KEY_CAMPAIGN_CODE, assignFieldModel.getCampaignCode());
        cv.put(DatabaseConstants.KEY_BRAND_CODE, assignFieldModel.getBrandCode());
        cv.put(DatabaseConstants.KEY_INPUT_ID, assignFieldModel.getInputId());
        cv.put(DatabaseConstants.KEY_USER_ID, assignFieldModel.getUserId());
        cv.put(DatabaseConstants.KEY_IS_ACTIVE, assignFieldModel.getIsActive());
        cv.put(DatabaseConstants.KEY_DATE_TIME, assignFieldModel.getDateTime());
        cv.put(DatabaseConstants.KEY_SORT, assignFieldModel.getSort());
        cv.put(DatabaseConstants.KEY_STATUS, assignFieldModel.getStatus());
        cv.put(DatabaseConstants.KEY_EVENT_CODE, assignFieldModel.getEventCode());
        db.insert(DatabaseConstants.TBL_INPUT_ASSIGN, null, cv);
        db.close();

    }

    public int getMaxAssignedFieldId() {
        db = getReadableDatabase();
        String query = "SELECT MAX(_id) FROM tbl_input_assign";
        Cursor c = db.rawQuery(query, null);
        if (c != null & c.moveToFirst()) {
            return c.getInt(0);
        }
        c.close();
        db.close();
        return 0;
    }

    public void createField(InputModel inputModel) {
        db = getWritableDatabase();
        Log.e(TAG, "createField: " + inputModel.toString());

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_ID, inputModel.getId());
        cv.put(DatabaseConstants.KEY_TYPE_ID, inputModel.getTypeId());
        cv.put(DatabaseConstants.KEY_TITLE, inputModel.getTitle());
        cv.put(DatabaseConstants.KEY_NAME, inputModel.getName());
        cv.put(DatabaseConstants.KEY_DESCRIPTION, inputModel.getDescription());
        cv.put(DatabaseConstants.KEY_GROUP_ID, inputModel.getGroupId());
        cv.put(DatabaseConstants.KEY_IS_REQUIRED, inputModel.getIsRequired());
        cv.put(DatabaseConstants.KEY_IS_ACTIVE, inputModel.getIsActive());
        cv.put(DatabaseConstants.KEY_DATE_TIME, Utils.getDateTime());
        cv.put(DatabaseConstants.KEY_CATEGORY, inputModel.getCategory());
        cv.put(DatabaseConstants.KEY_STATUS, inputModel.getStatus());
        cv.put(DatabaseConstants.KEY_IS_MANDATORY, inputModel.getIsMandatory());
        cv.put(DatabaseConstants.KEY_DBM_STATUS, inputModel.getDbmStatus());
        db.insert(DatabaseConstants.TBL_INPUT, null, cv);
        db.close();


    }

    public void createDataArray(DataArray dataArray, String typeId) {
        db = getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_ID, dataArray.getId());
        cv.put(DatabaseConstants.KEY_INPUT_ID, dataArray.getInputId());
        cv.put(DatabaseConstants.KEY_TITLE, dataArray.getTitle());
        cv.put(DatabaseConstants.KEY_NAME, dataArray.getName());
        cv.put(DatabaseConstants.KEY_VALUE, dataArray.getValue());
        cv.put(DatabaseConstants.KEY_IS_ACTIVE, dataArray.getIsActive());
        cv.put(DatabaseConstants.KEY_DATE_TIME, Utils.getDateTime());

        switch (typeId) {
            case "1":
                db.insert(DatabaseConstants.TBL_TEXTBOX, null, cv);
                break;

            case "2":
                db.insert(DatabaseConstants.TBL_CHECKBOX, null, cv);
                break;

            case "3":
                db.insert(DatabaseConstants.TBL_RADIO, null, cv);
                break;

            case "4":
                db.insert(DatabaseConstants.TBL_SELECT, null, cv);
                break;
        }
        db.close();
    }

    public ArrayList<String> getInputType() {
        db = getReadableDatabase();
        ArrayList<String> arrayList = new ArrayList<>();
        String[] column = {DatabaseConstants.KEY_TITLE};
        String where = DatabaseConstants.KEY_ID + " NOT IN(2,5,6)";
        Cursor c = db.query(DatabaseConstants.TBL_INPUT_TYPE, column, where, null,
                null, null, null, null);

        if (c != null && c.moveToFirst()) {
            do {
                arrayList.add(c.getString(0));
            } while (c.moveToNext());
            c.close();
            db.close();
        }
        return arrayList;
    }

    public ArrayList<GroupsModel> getGroupNames(String brandCode, String campaigncode, String eventCode) {
        db = getReadableDatabase();
        ArrayList<GroupsModel> arrayList = new ArrayList<>();
        String[] whereArgs = {brandCode, campaigncode, eventCode};
        String query = "select * from tbl_group " +
                "where brand_code =? " +
                "and campaign_code =? " +
                "and event_code=? " +
                "union all select * from tbl_group " +
                "where brand_code is null " +
                "and campaign_code is null " +
                "and event_code is null " +
                "order by title COLLATE NOCASE";

        Cursor c = db.rawQuery(query, whereArgs);
        if (c != null && c.moveToFirst()) {
            do {
                GroupsModel groupsModel = new GroupsModel();
                groupsModel.setId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_ID)));
                groupsModel.setTitle(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TITLE)));
                groupsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                groupsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                groupsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                groupsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                groupsModel.setIsActive(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_ACTIVE)));
                groupsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                groupsModel.setDateTime(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DATE_TIME)));
                groupsModel.setStatus(c.getString(c.getColumnIndex(DatabaseConstants.KEY_STATUS)));
                groupsModel.setEventCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EVENT_CODE)));
                arrayList.add(groupsModel);
            } while (c.moveToNext());
            c.close();
            db.close();
        }
        return arrayList;
    }

    public void createGroup(GroupsModel groupsModel) {
        db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_ID, groupsModel.getId());
        cv.put(DatabaseConstants.KEY_TITLE, groupsModel.getTitle());
        cv.put(DatabaseConstants.KEY_NAME, groupsModel.getName());
        cv.put(DatabaseConstants.KEY_BRAND_CODE, groupsModel.getBrandCode());
        cv.put(DatabaseConstants.KEY_CAMPAIGN_CODE, groupsModel.getCampaignCode());
        cv.put(DatabaseConstants.KEY_EVENT_CODE, groupsModel.getEventCode());
        cv.put(DatabaseConstants.KEY_IS_ACTIVE, groupsModel.getIsActive());
        cv.put(DatabaseConstants.KEY_USER_ID, groupsModel.getUserId());
        cv.put(DatabaseConstants.KEY_DATE_TIME, groupsModel.getDateTime());
        cv.put(DatabaseConstants.KEY_STATUS, groupsModel.getStatus());
        db.insert(DatabaseConstants.TBL_GROUP, null, cv);
        db.close();
    }

    public boolean checkIfGroupIsExist(String brandCode, String campaignCode, String eventCode, String name) {
        db = getReadableDatabase();
        String query = "select * from tbl_group " +
                "where brand_code =? " +
                "and campaign_code =? " +
                "and event_code =? " +
                "and name =? " +
                "union all select * from tbl_group " +
                "where brand_code is null " +
                "and campaign_code is null " +
                "and event_code is null";

        String[] whereArgs = {brandCode, campaignCode, eventCode, name};
        Cursor c = db.rawQuery(query, whereArgs);
        if (c != null && c.moveToFirst()) {
            Log.e(TAG, "checkIfGroupIsExist: " + c.getCount());
            do {
                if (name.equals(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)))) {
                    return true;
                }
            } while (c.moveToNext());
            c.close();
        }
        db.close();

        return false;
    }

    public boolean checkIfGroupTitleIsExist(String brandCode, String campaignCode, String eventCode, String title) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=? AND " +
                DatabaseConstants.KEY_TITLE + "=?";
        String[] whereArgs = {brandCode, campaignCode, eventCode, title};
        Cursor c = db.query(DatabaseConstants.TBL_GROUP, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            c.close();
            return true;
        }
        db.close();

        return false;
    }

    public int getMaxGroupId() {
        db = getReadableDatabase();
        String query = "SELECT MAX(_id) FROM tbl_group";
        Cursor c = db.rawQuery(query, null);

        if (c != null && c.moveToFirst()) {
            return c.getInt(0);
        }
        c.close();
        db.close();

        return 0;
    }

    public String getGroupId(String title) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_TITLE + "=?";
        String[] whereArgs = {title};
        Cursor c = db.query(DatabaseConstants.TBL_GROUP, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            return c.getString(1);
        }

        c.close();
        db.close();
        return "0";
    }

    public String getGroupIdByBrandCampaignEvent(String brandCode, String campaignCode, String eventCode, String title) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=? AND " +
                DatabaseConstants.KEY_TITLE + "=?";
        String[] whereArgs = {brandCode, campaignCode, eventCode, title};
        Cursor c = db.query(DatabaseConstants.TBL_GROUP, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            return c.getString(1);
        }

        c.close();
        db.close();
        return "0";
    }

    public int getTypeId(String title) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_TITLE + "=?";
        String[] whereArgs = {title};
        Cursor c = db.query(DatabaseConstants.TBL_INPUT_TYPE, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            return c.getInt(0);
        }
        c.close();
        db.close();

        return 0;
    }

    public int getMaxFieldId() {
        db = getReadableDatabase();
        String query = "SELECT MAX(_id) FROM tbl_input";
        Cursor c = db.rawQuery(query, null);
        if (c != null && c.moveToFirst()) {
            return c.getInt(0);
        }
        c.close();
        db.close();

        return 0;
    }

    public int getMaxDataArrayId(String typeId) {
        db = getReadableDatabase();
        String query = null;
        switch (typeId) {
            case "1":
                query = "SELECT MAX(_id) FROM tbl_textbox";
                break;

            case "2":
                query = "SELECT MAX(_id) FROM tbl_checkbox";
                break;

            case "3":
                query = "SELECT MAX(_id) FROM tbl_radio";
                break;

            case "4":
                query = "SELECT MAX(_id) FROM tbl_select";
                break;

            default:
                query = "SELECT MAX(_id) FROM tbl_textbox";
                break;
        }

        Cursor c = db.rawQuery(query, null);

        if (c != null && c.moveToFirst()) {
            return c.getInt(0);
        }
        c.close();
        db.close();
        return 0;
    }

    public List<DataArray> getDataArrayByFieldId(String tableName, String fieldId) {
        db = getReadableDatabase();
        List<DataArray> dataArrayList = new ArrayList<>();
        String where = DatabaseConstants.KEY_INPUT_ID + "=?";
        String[] whereArgs = {fieldId};

        Cursor c = db.query(tableName, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            do {
                DataArray dataArray = new DataArray();
                dataArray.setInputId(c.getString(1));
                dataArray.setTitle(c.getString(2));
                dataArray.setName(c.getString(3));
                dataArray.setValue(c.getString(4));
                dataArray.setIsActive(c.getString(5));
                dataArray.setDateTime(c.getString(6));
                dataArray.setId(c.getString(7));
                dataArrayList.add(dataArray);
            } while (c.moveToNext());
            c.close();
        }
        db.close();

        return dataArrayList;
    }

    public void clearExistingDataArray(String tableName, String inputId) {
        db = getWritableDatabase();
        String where = DatabaseConstants.KEY_INPUT_ID + "=?";
        String[] whereArgs = {inputId};
        db.delete(tableName, where, whereArgs);
        db.close();
    }

    public List<GroupsModel> checkGroupForUpdate(String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        List<GroupsModel> groupsModelList = new ArrayList<>();
        String where = DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=? AND " +
                DatabaseConstants.KEY_STATUS + "=1";
        String[] whereArgs = {brandCode, campaignCode, eventCode};

        Cursor c = db.query(DatabaseConstants.TBL_GROUP, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            do {
                GroupsModel groupsModel = new GroupsModel();
                groupsModel.setId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_ID)));
                groupsModel.setTitle(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TITLE)));
                groupsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                groupsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                groupsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                groupsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                groupsModel.setEventCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EVENT_CODE)));
                groupsModel.setIsActive(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_ACTIVE)));
                groupsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                groupsModel.setDateTime(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DATE_TIME)));
                groupsModel.setStatus(c.getString(c.getColumnIndex(DatabaseConstants.KEY_STATUS)));
                groupsModelList.add(groupsModel);

            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return groupsModelList;
    }

    public void updateExistingGroup(GroupsModel groupsModel, String oldGroupId, String brandCode, String campaignCode, String eventCode) {
        db = getWritableDatabase();
        String where = DatabaseConstants.KEY_ID + "=? AND " +
                DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=?";

        String[] whereArgs = {oldGroupId, brandCode, campaignCode, eventCode};
        if (groupsModel.getId().equals("1") ||
                groupsModel.getId().equals("2") ||
                groupsModel.getId().equals("3") ||
                groupsModel.getId().equals("4")) {
        } else {
            ContentValues cv = new ContentValues();
            cv.put(DatabaseConstants.KEY_ID, groupsModel.getId());
            cv.put(DatabaseConstants.KEY_TITLE, groupsModel.getTitle());
            cv.put(DatabaseConstants.KEY_NAME, groupsModel.getName());
            cv.put(DatabaseConstants.KEY_BRAND_CODE, groupsModel.getBrandCode());
            cv.put(DatabaseConstants.KEY_CAMPAIGN_CODE, groupsModel.getCampaignCode());
            cv.put(DatabaseConstants.KEY_EVENT_CODE, groupsModel.getEventCode());
            cv.put(DatabaseConstants.KEY_IS_ACTIVE, groupsModel.getIsActive());
            cv.put(DatabaseConstants.KEY_USER_ID, groupsModel.getUserId());
            cv.put(DatabaseConstants.KEY_DATE_TIME, groupsModel.getDateTime());
            cv.put(DatabaseConstants.KEY_STATUS, groupsModel.getStatus());
            db.update(DatabaseConstants.TBL_GROUP, cv, where, whereArgs);
        }
        db.close();
    }

    public void updateFieldsGroup(String oldGroupId, String newGroupId) {
        db = getWritableDatabase();
        String where = DatabaseConstants.KEY_GROUP_ID + "=?";
        String[] whereArgs = {oldGroupId};

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_GROUP_ID, newGroupId);
        db.update(DatabaseConstants.TBL_INPUT, cv, where, whereArgs);

        db.close();

    }

    public void createCountry(CountryModel countryModel) {
        db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_ID, countryModel.getId());
        cv.put(DatabaseConstants.KEY_COUNTRY_ID, countryModel.getCountryId());
        cv.put(DatabaseConstants.KEY_ISO2_CODE, countryModel.getIso2Code());
        db.insert(DatabaseConstants.TBL_COUNTRY, null, cv);
        db.close();
    }

    public void createRegion(RegionModel regionModel) {
        db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_ID, regionModel.getId());
        cv.put(DatabaseConstants.KEY_REGION_ID, regionModel.getRegionId());
        cv.put(DatabaseConstants.KEY_REGION_NAME, Utils.convertingSpecialCharacters(regionModel.getRegionName()));
        cv.put(DatabaseConstants.KEY_COUNTRY_ID, regionModel.getCountryId());
        db.insert(DatabaseConstants.TBL_REGION, null, cv);
        db.close();
    }

    public void createProvince(ProvinceModel provinceModel) {
        db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_ID, provinceModel.getId());
        cv.put(DatabaseConstants.KEY_PROVINCE_ID, provinceModel.getProvinceId());
        cv.put(DatabaseConstants.KEY_COUNTRY_ID, provinceModel.getCountryId());
        cv.put(DatabaseConstants.KEY_PROVINCE_CODE, provinceModel.getProvinceCode());
        cv.put(DatabaseConstants.KEY_PROVINCE_NAME, Utils.convertingSpecialCharacters(provinceModel.getProvinceName()));
        cv.put(DatabaseConstants.KEY_REGION_ID, provinceModel.getRegionId());
        db.insert(DatabaseConstants.TBL_PROVINCE, null, cv);
        db.close();
    }

    public void createCity(CityModel cityModel) {
        db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_ID, cityModel.getId());
        cv.put(DatabaseConstants.KEY_CITY_ID, cityModel.getCityId());
        cv.put(DatabaseConstants.KEY_PROVINCE_ID, cityModel.getProvinceId());
        cv.put(DatabaseConstants.KEY_CITY_NAME, Utils.convertingSpecialCharacters(cityModel.getCityName()));
        db.insert(DatabaseConstants.TBL_CITY, null, cv);
        db.close();
    }

    public void deletePreloadedData(String tableName) {
        db = getWritableDatabase();
        db.delete(tableName, null, null);
        db.close();
    }

    public List<FieldsModel> getAllActiveFields(String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        List<FieldsModel> fieldsModelList = new ArrayList<>();
        String query = "select a.*, b.*, c.title " +
                "from tbl_input_assign as a " +
                "left join tbl_input as b " +
                "on a.input_id = b.id " +
                "left join tbl_group as c " +
                "on b.group_id = c.id " +
                "where a.is_active = 1 " +
                "and a.brand_code =? " +
                "and a.campaign_code =? " +
                "and a.event_code =? " +
                "order by b.group_id asc, b.category, a.sort";
        String[] whereArgs = {brandCode, campaignCode, eventCode};
        Cursor c = db.rawQuery(query, whereArgs);
        if (c != null && c.moveToFirst()) {
            do {
                FieldsModel fieldsModel = new FieldsModel();
                fieldsModel.setA_id(c.getString(1));
                fieldsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                fieldsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                fieldsModel.setInputId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_INPUT_ID)));
                fieldsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                fieldsModel.setA_isActive(c.getString(6));
                fieldsModel.setA_dateTime(c.getString(7));
                fieldsModel.setSort(c.getInt(c.getColumnIndex(DatabaseConstants.KEY_SORT)));
                fieldsModel.setA_status(c.getString(9));
                fieldsModel.setB_id(c.getString(12));
                fieldsModel.setTypeId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TYPE_ID)));
                fieldsModel.setTitle(c.getString(14));
                fieldsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                fieldsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                fieldsModel.setGroupId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_GROUP_ID)));
                fieldsModel.setIsRequired(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_REQUIRED)));
                fieldsModel.setB_isActive(c.getString(19));
                fieldsModel.setB_dateTime(c.getString(20));
                fieldsModel.setCategory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CATEGORY)));
                fieldsModel.setB_status(c.getString(22));
                fieldsModel.setC_title(c.getString(23));
                fieldsModelList.add(fieldsModel);
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return fieldsModelList;
    }

    public List<RegionModel> getRegion() {
        db = getReadableDatabase();
        List<RegionModel> regionModelList = new ArrayList<>();
        Cursor c = db.query(DatabaseConstants.TBL_REGION, null, null, null,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            do {
                RegionModel regionModel = new RegionModel();
                regionModel.setId(c.getString(1));
                regionModel.setRegionId(c.getString(2));
                regionModel.setRegionName(c.getString(3));
                regionModel.setCountryId(c.getString(4));
                regionModelList.add(regionModel);
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return regionModelList;
    }

    public List<FieldsModel> checkFieldUpdatesForWeb(String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        List<FieldsModel> fieldsModelList = new ArrayList<>();
        String query = "SELECT a.*,b.*" +
                "FROM tbl_input_assign as a " +
                "LEFT JOIN tbl_input as b " +
                "ON a.input_id = b.id " +
                "WHERE a.brand_code =? " +
                "AND a.campaign_code=? " +
                "AND a.event_code=? " +
                "AND b.status=1";
        String[] whereArgs = {brandCode, campaignCode, eventCode};
        Cursor c = db.rawQuery(query, whereArgs);
        if (c != null && c.moveToFirst()) {
            do {
                FieldsModel fieldsModel = new FieldsModel();
                fieldsModel.setA_id(c.getString(1));
                fieldsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                fieldsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                fieldsModel.setInputId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_INPUT_ID)));
                fieldsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                fieldsModel.setA_isActive(c.getString(6));
                fieldsModel.setA_dateTime(c.getString(7));
                fieldsModel.setSort(c.getInt(c.getColumnIndex(DatabaseConstants.KEY_SORT)));
                fieldsModel.setA_status(c.getString(9));
                fieldsModel.setB_id(c.getString(12));
                fieldsModel.setTypeId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TYPE_ID)));
                fieldsModel.setTitle(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TITLE)));
                fieldsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                fieldsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                fieldsModel.setGroupId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_GROUP_ID)));
                fieldsModel.setIsRequired(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_REQUIRED)));
                fieldsModel.setB_isActive(c.getString(19));
                fieldsModel.setB_dateTime(c.getString(20));
                fieldsModel.setCategory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CATEGORY)));
                fieldsModel.setB_status(c.getString(22));
                fieldsModel.setEventCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EVENT_CODE)));
                fieldsModel.setIsMandatory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_MANDATORY)));
                fieldsModel.setDbmStatus(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DBM_STATUS)));
                fieldsModelList.add(fieldsModel);
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return fieldsModelList;
    }

    public List<FieldsModel> checkFieldUpdatesForDbm(String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        List<FieldsModel> fieldsModelList = new ArrayList<>();
        String query = "SELECT a.*,b.*" +
                "FROM tbl_input_assign as a " +
                "LEFT JOIN tbl_input as b " +
                "ON a.input_id = b.id " +
                "WHERE a.brand_code =? " +
                "AND a.campaign_code=? " +
                "AND a.event_code=? " +
                "AND b.dbm_status=1";
        String[] whereArgs = {brandCode, campaignCode, eventCode};
        Cursor c = db.rawQuery(query, whereArgs);
        if (c != null && c.moveToFirst()) {
            do {
                FieldsModel fieldsModel = new FieldsModel();
                fieldsModel.setA_id(c.getString(1));
                fieldsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                fieldsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                fieldsModel.setInputId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_INPUT_ID)));
                fieldsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                fieldsModel.setA_isActive(c.getString(6));
                fieldsModel.setA_dateTime(c.getString(7));
                fieldsModel.setSort(c.getInt(c.getColumnIndex(DatabaseConstants.KEY_SORT)));
                fieldsModel.setA_status(c.getString(9));
                fieldsModel.setB_id(c.getString(12));
                fieldsModel.setTypeId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TYPE_ID)));
                fieldsModel.setTitle(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TITLE)));
                fieldsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                fieldsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                fieldsModel.setGroupId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_GROUP_ID)));
                fieldsModel.setIsRequired(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_REQUIRED)));
                fieldsModel.setB_isActive(c.getString(19));
                fieldsModel.setB_dateTime(c.getString(20));
                fieldsModel.setCategory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CATEGORY)));
                fieldsModel.setB_status(c.getString(22));
                fieldsModel.setEventCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EVENT_CODE)));
                fieldsModel.setIsMandatory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_MANDATORY)));
                fieldsModel.setDbmStatus(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DBM_STATUS)));
                fieldsModelList.add(fieldsModel);
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return fieldsModelList;
    }

    public void updateExistingFieldId(FieldsModel fieldsModel, String oldFieldId) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {oldFieldId};

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_ID, fieldsModel.getB_id());
        cv.put(DatabaseConstants.KEY_STATUS, fieldsModel.getB_status());
        db.update(DatabaseConstants.TBL_INPUT, cv, where, whereArgs);
        db.close();

    }

    public void updateDbmStatusByFieldId(FieldsModel fieldsModel) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {fieldsModel.getB_id()};

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_DBM_STATUS, fieldsModel.getDbmStatus());
        db.update(DatabaseConstants.TBL_INPUT, cv, where, whereArgs);
        db.close();

    }

    public void updateExistingAssignFieldId(FieldsModel fieldsModel, String oldFieldId,
                                            String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_INPUT_ID + "=? AND " +
                DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=?";
        String[] whereArgs = {oldFieldId, brandCode, campaignCode, eventCode};

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_INPUT_ID, fieldsModel.getB_id());
        cv.put(DatabaseConstants.KEY_STATUS, fieldsModel.getA_status());
        db.update(DatabaseConstants.TBL_INPUT_ASSIGN, cv, where, whereArgs);
        db.close();
    }

    public void updateExistingDataArrayFieldId(FieldsModel fieldsModel, String oldFieldId) {
        db = getReadableDatabase();
        String tableName = null;
        String where = DatabaseConstants.KEY_INPUT_ID + "=?";
        String[] whereArgs = {oldFieldId};

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_INPUT_ID, fieldsModel.getB_id());

        switch (fieldsModel.getTypeId()) {
            case "1":
                tableName = DatabaseConstants.TBL_TEXTBOX;
                break;

            case "2":
                tableName = DatabaseConstants.TBL_CHECKBOX;
                break;

            case "3":
                tableName = DatabaseConstants.TBL_RADIO;
                break;

            case "4":
                tableName = DatabaseConstants.TBL_SELECT;
                break;
        }
        db.update(tableName, cv, where, whereArgs);
        db.close();

    }

    public List<String> getProvinceByRegionId(String regionId) {
        db = getReadableDatabase();
        List<String> provinceModelList = new ArrayList<>();
        String where = DatabaseConstants.KEY_REGION_ID + "=?";
        String[] whereArgs = {regionId};
        Cursor c = db.query(DatabaseConstants.TBL_PROVINCE, null, where, whereArgs,
                null, null, null, null);

        if (c != null && c.moveToFirst()) {
            do {
                provinceModelList.add(c.getString(5));
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return provinceModelList;
    }

    public String getRegionIdByName(String regionName) {
        db = getReadableDatabase();
        String[] column = {DatabaseConstants.KEY_REGION_ID};
        String where = DatabaseConstants.KEY_REGION_NAME + "=?";
        String[] whereArgs = {regionName};

        Cursor c = db.query(DatabaseConstants.TBL_REGION, column, where, whereArgs,
                null, null, null, null);

        if (c != null && c.moveToFirst()) {
            return c.getString(0);
        }
        db.close();
        return null;

    }

    public String getProvinceIdByName(String provinceName) {
        db = getReadableDatabase();
        String[] column = {DatabaseConstants.KEY_PROVINCE_ID};
        String where = DatabaseConstants.KEY_PROVINCE_NAME + "=?";
        String[] whereArgs = {provinceName};

        Cursor c = db.query(DatabaseConstants.TBL_PROVINCE, column, where, whereArgs,
                null, null, null, null);

        if (c != null && c.moveToFirst()) {
            return c.getString(0);
        }
        db.close();
        return null;
    }

    public List<String> getCityByProvinceId(String provinceId) {
        db = getReadableDatabase();
        List<String> cityList = new ArrayList<>();
        String where = DatabaseConstants.KEY_PROVINCE_ID + "=?";
        String[] whereArgs = {provinceId};
        Cursor c = db.query(DatabaseConstants.TBL_CITY, null, where, whereArgs,
                null, null, null, null);

        if (c != null && c.moveToFirst()) {
            do {
                cityList.add(c.getString(4));
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return cityList;
    }

    public boolean checkFieldNameIfExist(String fieldName, String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        String query = "Select a.*, b.* " +
                "from tbl_input_assign as a " +
                "left join tbl_input as b " +
                "on a.input_id = b.id " +
                "where a.brand_code =? " +
                "and a.campaign_code =? " +
                "and a.event_code =? " +
                "and b.name=?";
        String[] whereArgs = {brandCode, campaignCode, eventCode, fieldName};
        Cursor c = db.rawQuery(query, whereArgs);

        if (c != null && c.moveToFirst()) {
            return true;
        }
        c.close();
        db.close();
        return false;
    }

    public String getFieldIdByFieldName(String fieldName) {
        db = getReadableDatabase();
        String[] column = {DatabaseConstants.KEY_ID};
        String where = DatabaseConstants.KEY_NAME + "=?";
        String[] whereArgs = {fieldName};
        Cursor c = db.query(DatabaseConstants.TBL_INPUT, column, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            return c.getString(0);
        }
        c.close();
        db.close();
        return null;

    }

    public void deleteDataArrayByInputId(String inputId, String tableName) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_INPUT_ID + "=?";
        String[] whereArgs = {inputId};
        db.delete(tableName, where, whereArgs);
        db.close();
    }

    public List<FieldsModel> checkEditedFieldsForUpdate(String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        List<FieldsModel> fieldsModelList = new ArrayList<>();
        String query = "SELECT a.*,b.*" +
                "FROM tbl_input_assign as a " +
                "LEFT JOIN tbl_input as b " +
                "ON a.input_id = b.id " +
                "WHERE a.brand_code =? " +
                "AND a.campaign_code=? " +
                "AND a.event_code=? " +
                "AND b.edit_status =1";
        String[] whereArgs = {brandCode, campaignCode, eventCode};
        Cursor c = db.rawQuery(query, whereArgs);
        if (c != null && c.moveToFirst()) {
            do {
                FieldsModel fieldsModel = new FieldsModel();
                fieldsModel.setA_id(c.getString(1));
                fieldsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                fieldsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                fieldsModel.setInputId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_INPUT_ID)));
                fieldsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                fieldsModel.setA_isActive(c.getString(6));
                fieldsModel.setA_dateTime(c.getString(7));
                fieldsModel.setSort(c.getInt(c.getColumnIndex(DatabaseConstants.KEY_SORT)));
                fieldsModel.setA_status(c.getString(9));
                fieldsModel.setB_id(c.getString(12));
                fieldsModel.setTypeId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TYPE_ID)));
                fieldsModel.setTitle(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TITLE)));
                fieldsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                fieldsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                fieldsModel.setGroupId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_GROUP_ID)));
                fieldsModel.setIsRequired(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_REQUIRED)));
                fieldsModel.setB_isActive(c.getString(19));
                fieldsModel.setB_dateTime(c.getString(20));
                fieldsModel.setCategory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CATEGORY)));
                fieldsModel.setB_status(c.getString(22));
                fieldsModel.setEventCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EVENT_CODE)));
                fieldsModel.setIsMandatory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_MANDATORY)));
                fieldsModelList.add(fieldsModel);
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return fieldsModelList;
    }

    public List<String> getDataArray(FieldsModel fieldsModel) {
        db = getReadableDatabase();
        List<String> list = new ArrayList<>();
        String[] column = {DatabaseConstants.KEY_TITLE};
        String where = DatabaseConstants.KEY_INPUT_ID + "=?";
        String[] whereArgs = {fieldsModel.getB_id()};

        String tableName = null;
        switch (fieldsModel.getTypeId()) {
            case "1":
                tableName = DatabaseConstants.TBL_TEXTBOX;
                break;

            case "3":
                tableName = DatabaseConstants.TBL_RADIO;
                break;

            case "4":
                tableName = DatabaseConstants.TBL_SELECT;
                break;
        }

        Cursor c = db.query(tableName, column, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            do {
                list.add(c.getString(0));
            } while (c.moveToNext());
            c.close();
        }
        db.close();

        return list;

    }

    public List<String> getRegionList() {
        db = getReadableDatabase();
        List<String> list = new ArrayList<>();
        String[] column = {DatabaseConstants.KEY_REGION_NAME};
        Cursor c = db.query(DatabaseConstants.TBL_REGION, column, null, null,
                null, null, DatabaseConstants.KEY_REGION_NAME, null);
        if (c != null && c.moveToFirst()) {
            do {
                list.add(c.getString(0).toUpperCase());
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return list;
    }

    public String getRegionNameByRegionId(String regionId) {
        db = getReadableDatabase();
        String regionName = "";

        String[] column = {DatabaseConstants.KEY_REGION_NAME};
        String where = DatabaseConstants.KEY_REGION_ID + "=?";
        String[] whereArgs = {regionId};

        Cursor c = db.query(DatabaseConstants.TBL_REGION, column, where, whereArgs,
                null, null, DatabaseConstants.KEY_REGION_NAME, null);
        if (c != null && c.moveToFirst()) {
            regionName = c.getString(0);
            c.close();
        }
        db.close();
        return regionName;
    }


    public String getRegionIdByRegionName(String regionName) {
        db = getReadableDatabase();
        String[] column = {DatabaseConstants.KEY_REGION_ID};
        String where = DatabaseConstants.KEY_REGION_NAME + "=?";
        String[] whereArgs = {regionName};
        Cursor c = db.query(DatabaseConstants.TBL_REGION, column, where, whereArgs,
                null, null, null, null);

        if (c != null && c.moveToFirst()) {
            return c.getString(0);
        }
        return null;
    }

    public List<String> getProvinceNameByRegionId(String regionId) {
        db = getReadableDatabase();
        List<String> list = new ArrayList<>();
        String[] column = {DatabaseConstants.KEY_PROVINCE_NAME};
        String where = DatabaseConstants.KEY_REGION_ID + "=?";
        String[] whereArgs = {regionId};

        Cursor c = db.query(DatabaseConstants.TBL_PROVINCE, column, where, whereArgs,
                null, null, DatabaseConstants.KEY_PROVINCE_NAME, null);
        if (c != null && c.moveToFirst()) {
            do {
                list.add(c.getString(0).toUpperCase());
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return list;
    }

    public String getProvinceNameByProvinceId(String provinceId) {
        db = getReadableDatabase();
        String provinceName = "";

        String[] column = {DatabaseConstants.KEY_PROVINCE_NAME};
        String where = DatabaseConstants.KEY_PROVINCE_ID + "=?";
        String[] whereArgs = {provinceId};

        Cursor c = db.query(DatabaseConstants.TBL_PROVINCE, column, where, whereArgs,
                null, null, DatabaseConstants.KEY_PROVINCE_NAME, null);
        if (c != null && c.moveToFirst()) {
            provinceName = c.getString(0);
            c.close();
        }
        db.close();
        return provinceName;
    }

    public String getProvinceIdByProvinceName(String provinceName) {
        db = getReadableDatabase();
        String[] column = {DatabaseConstants.KEY_PROVINCE_ID};
        String where = DatabaseConstants.KEY_PROVINCE_NAME + "=?";
        String[] whereArgs = {provinceName};
        Cursor c = db.query(DatabaseConstants.TBL_PROVINCE, column, where, whereArgs,
                null, null, null, null);

        if (c != null && c.moveToFirst()) {
            return c.getString(0);
        }
        return null;
    }

    public List<String> getCityNameByProvinceId(String provinceId) {
        db = getReadableDatabase();
        List<String> list = new ArrayList<>();
        String[] column = {DatabaseConstants.KEY_CITY_NAME};
        String where = DatabaseConstants.KEY_PROVINCE_ID + "=?";
        String[] whereArgs = {provinceId};

        Cursor c = db.query(DatabaseConstants.TBL_CITY, column, where, whereArgs,
                null, null, DatabaseConstants.KEY_CITY_NAME, null);
        if (c != null && c.moveToFirst()) {
            do {
                list.add(c.getString(0).toUpperCase());
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return list;
    }

    public String getCityNameByCityId(String cityId) {
        db = getReadableDatabase();
        String cityName = "";

        String[] column = {DatabaseConstants.KEY_CITY_NAME};
        String where = DatabaseConstants.KEY_CITY_ID + "=?";
        String[] whereArgs = {cityId};

        Cursor c = db.query(DatabaseConstants.TBL_CITY, column, where, whereArgs,
                null, null, DatabaseConstants.KEY_CITY_NAME, null);
        if (c != null && c.moveToFirst()) {
            cityName = c.getString(0);
            c.close();
        }
        db.close();
        return cityName;
    }

    public String getCityIdByCityName(String cityName) {
        db = getReadableDatabase();
        String[] column = {DatabaseConstants.KEY_CITY_ID};
        String where = DatabaseConstants.KEY_CITY_NAME + "=?";
        String[] whereArgs = {cityName};
        Cursor c = db.query(DatabaseConstants.TBL_CITY, column, where, whereArgs,
                null, null, null, null);

        if (c != null && c.moveToFirst()) {
            return c.getString(0);
        }
        return null;
    }

    public String getGroupTitleByGroupId(String groupId) {
        db = getReadableDatabase();
        String[] column = {DatabaseConstants.KEY_TITLE};
        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {groupId};

        Cursor c = db.query(DatabaseConstants.TBL_GROUP, column, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            return c.getString(0);
        }
        c.close();
        db.close();

        return null;
    }

    public void createRegistration(RegistrationModel registrationModel) {
        db = getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_ID, registrationModel.getId());
        cv.put(DatabaseConstants.KEY_EMAIL, registrationModel.getEmail());
        cv.put(DatabaseConstants.KEY_CAMPAIGN_CODE, registrationModel.getCampaignCode());
        cv.put(DatabaseConstants.KEY_BRAND_CODE, registrationModel.getBrandCode());
        cv.put(DatabaseConstants.KEY_EVENT_CODE, registrationModel.getEventCode());
        cv.put(DatabaseConstants.KEY_DATA, registrationModel.getData());
        cv.put(DatabaseConstants.KEY_STATUS, registrationModel.getStatus());
        cv.put(DatabaseConstants.KEY_DATE_TIME, registrationModel.getDateTime());
        cv.put(DatabaseConstants.KEY_ACTION_TYPE, registrationModel.getActionType());
        cv.put(DatabaseConstants.KEY_EVENT_NAME, registrationModel.getEventName());
        db.insert(DatabaseConstants.TBL_REGISTRATION, null, cv);
        db.close();

    }

    public int getMaxRegistrationId() {
        db = getReadableDatabase();
        String query = "SELECT MAX(_id) FROM tbl_registration";
        Cursor c = db.rawQuery(query, null);
        if (c != null && c.moveToFirst()) {
            return c.getInt(0);
        }
        c.close();
        db.close();
        return 0;
    }

    public void updateRegistration(RegistrationModel registrationModel, String brandCode, String campaignCode, String eventCode, String status) {
        db = getWritableDatabase();
        Log.e(TAG, "updateRegistration: " + registrationModel.getEmail() + "-" + brandCode + "-" + campaignCode + "-" + eventCode);
        String where = DatabaseConstants.KEY_EMAIL + "=? " +
                "AND " + DatabaseConstants.KEY_BRAND_CODE + "=? " +
                "AND " + DatabaseConstants.KEY_CAMPAIGN_CODE + "=? " +
                "AND " + DatabaseConstants.KEY_EVENT_CODE + "=?";

        String[] whereArgs = {registrationModel.getEmail(), brandCode, campaignCode, eventCode};
        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_DATA, registrationModel.getData());
        cv.put(DatabaseConstants.KEY_STATUS, status);
        cv.put(DatabaseConstants.KEY_DATE_TIME, registrationModel.getDateTime());
        cv.put(DatabaseConstants.KEY_ACTION_TYPE, registrationModel.getActionType());
        db.update(DatabaseConstants.TBL_REGISTRATION, cv, where, whereArgs);
        db.close();
    }

    public void deleteFieldByFieldId(FieldsModel fieldsModel) {
        db = getWritableDatabase();
        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {fieldsModel.getB_id()};

        db.delete(DatabaseConstants.TBL_INPUT, where, whereArgs);
        db.close();
    }

    public void deleteAssignByFieldId(FieldsModel fieldsModel) {
        db = getWritableDatabase();
        String where = DatabaseConstants.KEY_INPUT_ID + "=?";
        String[] whereArgs = {fieldsModel.getA_id()};

        db.delete(DatabaseConstants.TBL_INPUT_ASSIGN, where, whereArgs);
        db.close();
    }

    public boolean checkFieldIdExist(InputModel inputModel) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {inputModel.getId()};

        Cursor c = db.query(DatabaseConstants.TBL_INPUT, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            c.close();
            db.close();
            return true;
        }

        return false;
    }

    public boolean checkFieldNameExist(InputModel inputModel) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_NAME + "=?";
        String[] whereArgs = {inputModel.getName()};

        Cursor c = db.query(DatabaseConstants.TBL_INPUT, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            c.close();
            db.close();
            return true;
        }

        return false;
    }

    public void updateFieldById(InputModel inputModel) {
        db = getWritableDatabase();
        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {inputModel.getId()};

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_TYPE_ID, inputModel.getTypeId());
        cv.put(DatabaseConstants.KEY_TITLE, inputModel.getTitle());
        cv.put(DatabaseConstants.KEY_NAME, inputModel.getName());
        cv.put(DatabaseConstants.KEY_DESCRIPTION, inputModel.getDescription());
        cv.put(DatabaseConstants.KEY_GROUP_ID, inputModel.getGroupId());
        cv.put(DatabaseConstants.KEY_IS_REQUIRED, inputModel.getIsRequired());
        cv.put(DatabaseConstants.KEY_IS_ACTIVE, inputModel.getIsActive());
        cv.put(DatabaseConstants.KEY_DATE_TIME, Utils.getDateTime());
        cv.put(DatabaseConstants.KEY_CATEGORY, inputModel.getCategory());
        cv.put(DatabaseConstants.KEY_STATUS, inputModel.getStatus());
        cv.put(DatabaseConstants.KEY_EDIT_STATUS, inputModel.getEditStatus());
//        cv.put(DatabaseConstants.KEY_IS_MANDATORY, inputModel.getIsMandatory());
        cv.put(DatabaseConstants.KEY_DBM_STATUS, inputModel.getDbmStatus());
        db.update(DatabaseConstants.TBL_INPUT, cv, where, whereArgs);
        db.close();
    }

    public void updateFieldByName(InputModel inputModel) {
        db = getWritableDatabase();
        String where = DatabaseConstants.KEY_NAME + "=?";
        String[] whereArgs = {inputModel.getName()};

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_ID, inputModel.getId());
        cv.put(DatabaseConstants.KEY_TYPE_ID, inputModel.getTypeId());
        cv.put(DatabaseConstants.KEY_TITLE, inputModel.getTitle());
        cv.put(DatabaseConstants.KEY_NAME, inputModel.getName());
        cv.put(DatabaseConstants.KEY_DESCRIPTION, inputModel.getDescription());
        cv.put(DatabaseConstants.KEY_GROUP_ID, inputModel.getGroupId());
        cv.put(DatabaseConstants.KEY_IS_REQUIRED, inputModel.getIsRequired());
        cv.put(DatabaseConstants.KEY_IS_ACTIVE, inputModel.getIsActive());
        cv.put(DatabaseConstants.KEY_DATE_TIME, Utils.getDateTime());
        cv.put(DatabaseConstants.KEY_CATEGORY, inputModel.getCategory());
        cv.put(DatabaseConstants.KEY_STATUS, inputModel.getStatus());
        cv.put(DatabaseConstants.KEY_DBM_STATUS, inputModel.getDbmStatus());
        db.update(DatabaseConstants.TBL_INPUT, cv, where, whereArgs);
        db.close();
    }

    public boolean checkAssignFieldIdExistInBrandCampaign(InputModel inputModel, String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_INPUT_ID + "=? AND " +
                DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=?";
        String[] whereArgs = {inputModel.getId(), brandCode, campaignCode, eventCode};
        Cursor c = db.query(DatabaseConstants.TBL_INPUT_ASSIGN, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            c.close();
            db.close();
            return true;
        }
        return false;
    }

    public void assignFieldIdBrandCampaign(AssignFieldModel assignFieldModel) {
        db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_ID, assignFieldModel.getId());
        cv.put(DatabaseConstants.KEY_CAMPAIGN_CODE, assignFieldModel.getCampaignCode());
        cv.put(DatabaseConstants.KEY_BRAND_CODE, assignFieldModel.getBrandCode());
        cv.put(DatabaseConstants.KEY_EVENT_CODE, assignFieldModel.getEventCode());
        cv.put(DatabaseConstants.KEY_INPUT_ID, assignFieldModel.getInputId());
        cv.put(DatabaseConstants.KEY_USER_ID, assignFieldModel.getUserId());
        cv.put(DatabaseConstants.KEY_IS_ACTIVE, assignFieldModel.getIsActive());
        cv.put(DatabaseConstants.KEY_DATE_TIME, assignFieldModel.getDateTime());
        cv.put(DatabaseConstants.KEY_SORT, assignFieldModel.getSort());
        cv.put(DatabaseConstants.KEY_STATUS, assignFieldModel.getStatus());
        db.insert(DatabaseConstants.TBL_INPUT_ASSIGN, null, cv);
        db.close();

    }

    public void updateAssignByFieldIdBrandCampaign(AssignFieldModel assignFieldModel) {
        db = getWritableDatabase();
        String where = DatabaseConstants.KEY_INPUT_ID + "=? AND " +
                DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=?";
        String[] whereArgs = {assignFieldModel.getInputId(), assignFieldModel.getBrandCode(),
                assignFieldModel.getCampaignCode(), assignFieldModel.getEventCode()};

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_CAMPAIGN_CODE, assignFieldModel.getCampaignCode());
        cv.put(DatabaseConstants.KEY_BRAND_CODE, assignFieldModel.getBrandCode());
        cv.put(DatabaseConstants.KEY_EVENT_CODE, assignFieldModel.getEventCode());
        cv.put(DatabaseConstants.KEY_INPUT_ID, assignFieldModel.getInputId());
        cv.put(DatabaseConstants.KEY_USER_ID, assignFieldModel.getUserId());
        cv.put(DatabaseConstants.KEY_IS_ACTIVE, assignFieldModel.getIsActive());
        cv.put(DatabaseConstants.KEY_DATE_TIME, assignFieldModel.getDateTime());
        cv.put(DatabaseConstants.KEY_SORT, assignFieldModel.getSort());
        cv.put(DatabaseConstants.KEY_STATUS, assignFieldModel.getStatus());
        db.update(DatabaseConstants.TBL_INPUT_ASSIGN, cv, where, whereArgs);
        db.close();
    }

    public ArrayList<GroupsModel> getStandardGroupNames(String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();

        ArrayList<GroupsModel> arrayList = new ArrayList<>();
        String query = "SELECT c.title, c.id FROM tbl_input_assign as a " +
                "LEFT JOIN tbl_input as b ON a.input_id = b.id " +
                "LEFT JOIN tbl_group as c ON b.group_id = c.id " +
                "WHERE b.category =0 " +
                "AND a.brand_code =? " +
                "AND a.campaign_code =? " +
                "AND a.event_code=? " +
                "GROUP BY b.group_id";

        String[] whereArgs = {brandCode, campaignCode, eventCode};
        Cursor c = db.rawQuery(query, whereArgs);

        if (c != null && c.moveToFirst()) {
            do {
                GroupsModel groupsModel = new GroupsModel();
                groupsModel.setTitle(c.getString(0));
                groupsModel.setId((c.getString(1)));
                arrayList.add(groupsModel);

            } while (c.moveToNext());
            c.close();
            db.close();
        }
        return arrayList;
    }

    public ArrayList<GroupsModel> getNonStandardGroupNames(String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();

        ArrayList<GroupsModel> arrayList = new ArrayList<>();
        String query = "SELECT c.title, c.id FROM tbl_input_assign as a " +
                "LEFT JOIN tbl_input as b ON a.input_id = b.id " +
                "LEFT JOIN tbl_group as c ON b.group_id = c.id " +
                "WHERE b.category =1 " +
                "AND a.brand_code =? " +
                "AND a.campaign_code =? " +
                "AND a.event_code=? " +
                "GROUP BY b.group_id";
        String[] whereArgs = {brandCode, campaignCode, eventCode};
        Cursor c = db.rawQuery(query, whereArgs);

        if (c != null && c.moveToFirst()) {
            do {
                GroupsModel groupsModel = new GroupsModel();
                groupsModel.setTitle(c.getString(0));
                groupsModel.setId((c.getString(1)));
                arrayList.add(groupsModel);

            } while (c.moveToNext());
            c.close();
            db.close();
        }
        return arrayList;
    }

    public ArrayList<GroupsModel> getCustomGroupNames(String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();

        ArrayList<GroupsModel> arrayList = new ArrayList<>();
        String query = "SELECT c.title, c.id FROM tbl_input_assign as a " +
                "LEFT JOIN tbl_input as b ON a.input_id = b.id " +
                "LEFT JOIN tbl_group as c ON b.group_id = c.id " +
                "WHERE b.category = 2 " +
                "AND a.brand_code =? " +
                "AND a.campaign_code =? " +
                "AND a.event_code =? " +
                "GROUP BY b.group_id";
        String[] whereArgs = {brandCode, campaignCode, eventCode};
        Cursor c = db.rawQuery(query, whereArgs);

        if (c != null && c.moveToFirst()) {
            do {
                GroupsModel groupsModel = new GroupsModel();
                groupsModel.setTitle(c.getString(0));
                groupsModel.setId((c.getString(1)));
                arrayList.add(groupsModel);

            } while (c.moveToNext());
            c.close();
            db.close();
        }
        return arrayList;
    }

    public List<FieldsModel> getStandardFieldsByGroupId(String brandCode, String campaignCode, String eventCode, String groupId) {
        db = getReadableDatabase();
        List<FieldsModel> fieldsModelList = new ArrayList<>();
        String query = "SELECT a.*, b.*" +
                "FROM tbl_input_assign a " +
                "LEFT JOIN tbl_input b " +
                "ON a.input_id = b.id " +
                "WHERE a.brand_code =? " +
                "AND a.campaign_code =? " +
                "AND a.event_code =? " +
                "AND b.category =0 " +
                "AND b.group_id =?" +
                "ORDER BY a.sort";
        String[] whereArgs = {brandCode, campaignCode, eventCode, groupId};

        Cursor c = db.rawQuery(query, whereArgs);
        if (c != null && c.moveToFirst()) {
            do {
                FieldsModel fieldsModel = new FieldsModel();
                fieldsModel.setA_id(c.getString(1));
                fieldsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                fieldsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                fieldsModel.setInputId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_INPUT_ID)));
                fieldsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                fieldsModel.setA_isActive(c.getString(6));
                fieldsModel.setA_dateTime(c.getString(7));
                fieldsModel.setSort(c.getInt(c.getColumnIndex(DatabaseConstants.KEY_SORT)));
                fieldsModel.setA_status(c.getString(9));
                fieldsModel.setB_id(c.getString(12));
                fieldsModel.setTypeId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TYPE_ID)));
                fieldsModel.setTitle(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TITLE)));
                fieldsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                fieldsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                fieldsModel.setGroupId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_GROUP_ID)));
                fieldsModel.setIsRequired(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_REQUIRED)));
                fieldsModel.setB_isActive(c.getString(19));
                fieldsModel.setB_dateTime(c.getString(20));
                fieldsModel.setCategory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CATEGORY)));
                fieldsModel.setB_status(c.getString(22));
                fieldsModel.setIsMandatory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_MANDATORY)));
                fieldsModelList.add(fieldsModel);
            } while (c.moveToNext());
            c.close();
        }
        db.close();

        return fieldsModelList;
    }

    public List<FieldsModel> getNonStandardFieldsByGroupId(String brandCode, String campaignCode, String eventCode, String groupId) {
        db = getReadableDatabase();
        List<FieldsModel> fieldsModelList = new ArrayList<>();
        String query = "SELECT a.*, b.*" +
                "FROM tbl_input_assign a " +
                "LEFT JOIN tbl_input b " +
                "ON a.input_id = b.id " +
                "WHERE a.brand_code =? " +
                "AND a.campaign_code =?" +
                "AND a.event_code =? " +
                "AND b.category =1 " +
                "AND b.group_id =?" +
                "ORDER BY a.sort";
        String[] whereArgs = {brandCode, campaignCode, eventCode, groupId};

        Cursor c = db.rawQuery(query, whereArgs);
        if (c != null && c.moveToFirst()) {
            do {
                FieldsModel fieldsModel = new FieldsModel();
                fieldsModel.setA_id(c.getString(1));
                fieldsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                fieldsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                fieldsModel.setInputId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_INPUT_ID)));
                fieldsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                fieldsModel.setA_isActive(c.getString(6));
                fieldsModel.setA_dateTime(c.getString(7));
                fieldsModel.setSort(c.getInt(c.getColumnIndex(DatabaseConstants.KEY_SORT)));
                fieldsModel.setA_status(c.getString(9));
                fieldsModel.setB_id(c.getString(12));
                fieldsModel.setTypeId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TYPE_ID)));
                fieldsModel.setTitle(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TITLE)));
                fieldsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                fieldsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                fieldsModel.setGroupId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_GROUP_ID)));
                fieldsModel.setIsRequired(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_REQUIRED)));
                fieldsModel.setB_isActive(c.getString(19));
                fieldsModel.setB_dateTime(c.getString(20));
                fieldsModel.setCategory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CATEGORY)));
                fieldsModel.setB_status(c.getString(22));
                fieldsModelList.add(fieldsModel);
            } while (c.moveToNext());
            c.close();
        }
        db.close();

        return fieldsModelList;
    }

    public List<FieldsModel> getCustomFieldsByGroupId(String brandCode, String campaignCode, String eventCode, String groupId) {
        Log.e(TAG, "getCustomFieldsByGroupId: " + groupId);
        db = getReadableDatabase();
        List<FieldsModel> fieldsModelList = new ArrayList<>();
        String query = "SELECT a.*, b.*" +
                "FROM tbl_input_assign a " +
                "LEFT JOIN tbl_input b " +
                "ON a.input_id = b.id " +
                "WHERE a.brand_code =? " +
                "AND a.campaign_code =? " +
                "AND a.event_code =?" +
                "AND b.category =2 " +
                "AND b.group_id =?" +
                "ORDER BY a.sort";
        String[] whereArgs = {brandCode, campaignCode, eventCode, groupId};

        Cursor c = db.rawQuery(query, whereArgs);
        if (c != null && c.moveToFirst()) {
            do {
                FieldsModel fieldsModel = new FieldsModel();
                fieldsModel.setA_id(c.getString(1));
                fieldsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                fieldsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                fieldsModel.setEventCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EVENT_CODE)));
                fieldsModel.setInputId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_INPUT_ID)));
                fieldsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                fieldsModel.setA_isActive(c.getString(6));
                fieldsModel.setA_dateTime(c.getString(7));
                fieldsModel.setSort(c.getInt(c.getColumnIndex(DatabaseConstants.KEY_SORT)));
                fieldsModel.setA_status(c.getString(9));
                fieldsModel.setB_id(c.getString(12));
                fieldsModel.setTypeId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TYPE_ID)));
                fieldsModel.setTitle(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TITLE)));
                fieldsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                fieldsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                fieldsModel.setGroupId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_GROUP_ID)));
                fieldsModel.setIsRequired(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_REQUIRED)));
                fieldsModel.setB_isActive(c.getString(19));
                fieldsModel.setB_dateTime(c.getString(20));
                fieldsModel.setCategory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CATEGORY)));
                fieldsModel.setB_status(c.getString(22));
                fieldsModelList.add(fieldsModel);
            } while (c.moveToNext());
            c.close();
        }
        db.close();

        return fieldsModelList;
    }

    public boolean checkIfRegistrationEmailExist(String email, String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        String where = DatabaseConstants.KEY_EMAIL + "=? AND " +
                DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=?";
        String[] whereArgs = {email, brandCode, campaignCode, eventCode};
        Cursor c = db.query(DatabaseConstants.TBL_REGISTRATION, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            c.close();
            return true;
        }
        db.close();
        return false;
    }

    public List<RegistrationModel> getRegistrationUpdates(String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        List<RegistrationModel> registrationModelList = new ArrayList<>();
        String where = DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=? AND " +
                DatabaseConstants.KEY_STATUS + "=1";
        String[] whereArgs = {brandCode, campaignCode, eventCode};
        Cursor c = db.query(DatabaseConstants.TBL_REGISTRATION, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            do {
                RegistrationModel registrationModel = new RegistrationModel();
                registrationModel.setId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_ID)));
                registrationModel.setEmail(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EMAIL)));
                registrationModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                registrationModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                registrationModel.setEventCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EVENT_CODE)));
                registrationModel.setData(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DATA)));
                registrationModel.setStatus(c.getString(c.getColumnIndex(DatabaseConstants.KEY_STATUS)));
                registrationModel.setDateTime(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DATE_TIME)));
                registrationModel.setActionType(c.getString(c.getColumnIndex(DatabaseConstants.KEY_ACTION_TYPE)));
                registrationModel.setEventName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EVENT_NAME)));
                registrationModelList.add(registrationModel);
            } while (c.moveToNext());
        }
        return registrationModelList;
    }

    public void updateInputAssign(FieldsModel fieldsModel) {
        db = getWritableDatabase();
        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {fieldsModel.getA_id()};

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_SORT, fieldsModel.getSort());
        cv.put(DatabaseConstants.KEY_IS_ACTIVE, fieldsModel.getA_isActive());
        db.update(DatabaseConstants.TBL_INPUT_ASSIGN, cv, where, whereArgs);
        db.close();
    }

    public void updateFieldEditStatus(FieldsModel fieldsModel) {
        db = getWritableDatabase();
        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {fieldsModel.getB_id()};

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.KEY_EDIT_STATUS, fieldsModel.getEditStatus());

        db.update(DatabaseConstants.TBL_INPUT, cv, where, whereArgs);
        db.close();
    }

    public int getCustomFieldsCount(String brandCode, String campaignCode) {
        db = getReadableDatabase();
        String query = "select a.*, b.*" +
                "from tbl_input_assign as a " +
                "left join tbl_input as b " +
                "on a.input_id = b.id " +
                "where a.brand_code =? " +
                "and a.campaign_code =? " +
                "and b.category = 2";
        String[] whereArgs = {brandCode, campaignCode};
        Cursor c = db.rawQuery(query, whereArgs);

        if (c != null && c.moveToFirst()) {
            return c.getCount();
        }
        return 0;
    }

    public List<RegistrationModel> getRegistrationDetails(String email, String brandCode, String campaignCode) {
        db = getReadableDatabase();
        List<RegistrationModel> registrationModelList = new ArrayList<>();
        String where = DatabaseConstants.KEY_EMAIL + "=? AND " +
                DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + "=?";
        String[] whereArgs = {email, brandCode, campaignCode};
        Cursor c = db.query(DatabaseConstants.TBL_REGISTRATION, null, where, whereArgs,
                null, null, null, null);
        if (c != null && c.moveToFirst()) {
            do {
                RegistrationModel registrationModel = new RegistrationModel();
                registrationModel.setId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_ID)));
                registrationModel.setEmail(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EMAIL)));
                registrationModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                registrationModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                registrationModel.setData(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DATA)));
                registrationModel.setStatus(c.getString(c.getColumnIndex(DatabaseConstants.KEY_STATUS)));
                registrationModel.setDateTime(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DATE_TIME)));
                registrationModel.setEventCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EVENT_CODE)));
                registrationModelList.add(registrationModel);
            } while (c.moveToNext());
        }
        return registrationModelList;
    }

    public List<FieldsModel> getAllDefaultFields(String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        List<FieldsModel> fieldsModelList = new ArrayList<>();
        String query = "select a.*, b.* " +
                "from tbl_input_assign as a " +
                "left join tbl_input as b " +
                "on a.input_id = b.id " +
                "where a.brand_code =? " +
                "and a.campaign_code =? " +
                "and a.event_code =? " +
                "and b.category IN (0, 1)" +
                "order by b.group_id asc, b.category, a.sort";

        String[] whereArgs = {brandCode, campaignCode, eventCode};
        Cursor c = db.rawQuery(query, whereArgs);
        if (c != null && c.moveToFirst()) {
            do {
                FieldsModel fieldsModel = new FieldsModel();
                fieldsModel.setA_id(c.getString(1));
                fieldsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                fieldsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                fieldsModel.setInputId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_INPUT_ID)));
                fieldsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                fieldsModel.setA_isActive(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_MANDATORY))
                        .equals("1") ? "1" : "0");
                fieldsModel.setA_dateTime(c.getString(7));
                fieldsModel.setSort(c.getInt(c.getColumnIndex(DatabaseConstants.KEY_SORT)));
                fieldsModel.setA_status(c.getString(9));
                fieldsModel.setB_id(c.getString(12));
                fieldsModel.setTypeId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TYPE_ID)));
                fieldsModel.setTitle(c.getString(14));
                fieldsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                fieldsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                fieldsModel.setGroupId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_GROUP_ID)));
                fieldsModel.setIsRequired(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_REQUIRED)));
                fieldsModel.setB_isActive(c.getString(19));
                fieldsModel.setB_dateTime(c.getString(20));
                fieldsModel.setCategory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CATEGORY)));
                fieldsModel.setB_status(c.getString(22));
                fieldsModel.setIsMandatory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_MANDATORY)));
                fieldsModelList.add(fieldsModel);
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return fieldsModelList;
    }

    public List<GroupsModel> getDefaultGroups() {
        db = getWritableDatabase();
        List<GroupsModel> groupsModelList = new ArrayList<>();
        String query = "select * " +
                "from tbl_group " +
                "where brand_code is null " +
                "and campaign_code is null " +
                "and event_code is null " +
                "order by id";

        Cursor c = db.rawQuery(query, null);
        if (c != null && c.moveToFirst()) {
            do {
                GroupsModel groupsModel = new GroupsModel();
                groupsModel.setId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_ID)));
                groupsModel.setTitle(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TITLE)));
                groupsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                groupsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                groupsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                groupsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                groupsModel.setIsActive(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_ACTIVE)));
                groupsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                groupsModel.setDateTime(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DATE_TIME)));
                groupsModel.setStatus(c.getString(c.getColumnIndex(DatabaseConstants.KEY_STATUS)));
                groupsModel.setEventCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EVENT_CODE)));
                groupsModelList.add(groupsModel);
            } while (c.moveToNext());
        }
        return groupsModelList;
    }

    public List<FieldsModel> getCustomFieldInAssign(String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        List<FieldsModel> fieldsModelList = new ArrayList<>();

        String query = "select a.*, b.* " +
                "from tbl_input_assign as a " +
                "left join tbl_input as b " +
                "on a.input_id = b.id " +
                "where a.brand_code =? " +
                "and a.campaign_code =? " +
                "and a.event_code =? " +
                "and b.category =2";
        String[] whereArgs = {brandCode, campaignCode, eventCode};

        Cursor c = db.rawQuery(query, whereArgs);
        if (c != null && c.moveToFirst()) {
            do {
                FieldsModel fieldsModel = new FieldsModel();
                fieldsModel.setA_id(c.getString(1));
                fieldsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                fieldsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                fieldsModel.setInputId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_INPUT_ID)));
                fieldsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                fieldsModel.setA_isActive(c.getString(6));
                fieldsModel.setA_dateTime(c.getString(7));
                fieldsModel.setSort(c.getInt(c.getColumnIndex(DatabaseConstants.KEY_SORT)));
                fieldsModel.setA_status(c.getString(9));
                fieldsModel.setB_id(c.getString(12));
                fieldsModel.setTypeId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TYPE_ID)));
                fieldsModel.setTitle(c.getString(14));
                fieldsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                fieldsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                fieldsModel.setGroupId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_GROUP_ID)));
                fieldsModel.setIsRequired(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_REQUIRED)));
                fieldsModel.setB_isActive(c.getString(19));
                fieldsModel.setB_dateTime(c.getString(20));
                fieldsModel.setCategory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CATEGORY)));
                fieldsModel.setB_status(c.getString(22));
                fieldsModelList.add(fieldsModel);
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return fieldsModelList;
    }

    public void deleteAllAssignedCustomFields(FieldsModel fieldsModel, String brandCode, String campaignCode, String eventCode) {
        db = getWritableDatabase();
        String where = DatabaseConstants.KEY_ID + "=? AND " +
                DatabaseConstants.KEY_BRAND_CODE + "=? AND " +
                DatabaseConstants.KEY_CAMPAIGN_CODE + " =? AND " +
                DatabaseConstants.KEY_EVENT_CODE + "=?";

        String[] whereArgs = {fieldsModel.getA_id(), brandCode, campaignCode, eventCode};
        db.delete(DatabaseConstants.TBL_INPUT_ASSIGN, where, whereArgs);
        db.close();
    }

    public boolean checkIfGroupExistById(GroupsModel groupsModel) {
        db = getReadableDatabase();

        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {groupsModel.getId()};

        Cursor c = db.query(DatabaseConstants.TBL_GROUP, null, where, whereArgs,
                null, null, null, null);
        if (c != null) {
            if (c.moveToFirst()) {
                c.close();
                db.close();
                return true;
            }
        }
        return false;
    }

    public void updateGroupById(GroupsModel groupsModel) {
        db = getWritableDatabase();

        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {groupsModel.getId()};
        if (groupsModel.getId().equals("1") ||
                groupsModel.getId().equals("2") ||
                groupsModel.getId().equals("3") ||
                groupsModel.getId().equals("4")) {

        } else {
            ContentValues cv = new ContentValues();
            cv.put(DatabaseConstants.KEY_ID, groupsModel.getId());
            cv.put(DatabaseConstants.KEY_TITLE, groupsModel.getTitle());
            cv.put(DatabaseConstants.KEY_NAME, groupsModel.getName());
            cv.put(DatabaseConstants.KEY_DESCRIPTION, groupsModel.getDescription());
            cv.put(DatabaseConstants.KEY_BRAND_CODE, groupsModel.getBrandCode());
            cv.put(DatabaseConstants.KEY_CAMPAIGN_CODE, groupsModel.getCampaignCode());
            cv.put(DatabaseConstants.KEY_EVENT_CODE, groupsModel.getEventCode());
            cv.put(DatabaseConstants.KEY_IS_ACTIVE, groupsModel.getIsActive());
            cv.put(DatabaseConstants.KEY_USER_ID, groupsModel.getUserId());
            cv.put(DatabaseConstants.KEY_DATE_TIME, groupsModel.getDateTime());
            cv.put(DatabaseConstants.KEY_STATUS, groupsModel.getStatus());
            db.update(DatabaseConstants.TBL_GROUP, cv, where, whereArgs);
        }

        db.close();
    }

    public boolean checkCountryIdIfExist(CountryModel countryModel) {
        db = getReadableDatabase();

        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {countryModel.getId()};

        Cursor c = db.query(DatabaseConstants.TBL_COUNTRY, null, where, whereArgs,
                null, null, null, null);
        if (c != null) {
            if (c.moveToFirst()) {
                return true;
            }
            c.close();
            db.close();
        }
        return false;

    }

    public boolean checkRegionIdIfExist(RegionModel regionModel) {
        db = getReadableDatabase();

        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {regionModel.getId()};

        Cursor c = db.query(DatabaseConstants.TBL_REGION, null, where, whereArgs,
                null, null, null, null);
        if (c != null) {
            if (c.moveToFirst()) {
                return true;
            }
            c.close();
            db.close();
        }


        return false;

    }

    public boolean checkProvinceIdIfExist(ProvinceModel provinceModel) {
        db = getReadableDatabase();

        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {provinceModel.getId()};

        Cursor c = db.query(DatabaseConstants.TBL_PROVINCE, null, where, whereArgs,
                null, null, null, null);
        if (c != null) {
            if (c.moveToFirst()) {
                return true;
            }
            c.close();
            db.close();
        }
        return false;

    }

    public boolean checkCityIdIfExist(CityModel cityModel) {
        db = getReadableDatabase();

        String where = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {cityModel.getId()};

        Cursor c = db.query(DatabaseConstants.TBL_CITY, null, where, whereArgs,
                null, null, null, null);
        if (c != null) {
            if (c.moveToFirst()) {
                return true;
            }
            c.close();
            db.close();
        }
        return false;

    }

    public List<FieldsModel> getAllFormFields(String brandCode, String campaignCode, String eventCode) {
        db = getReadableDatabase();
        List<FieldsModel> fieldsModelList = new ArrayList<>();
        String query = "select a.*, b.* " +
                "from tbl_input_assign as a " +
                "left join tbl_input as b " +
                "on a.input_id = b.id " +
                "where a.brand_code =? " +
                "and a.campaign_code =? " +
                "and a.event_code =? " +
                "order by b.group_id asc, b.category, a.sort";

        String[] whereArgs = {brandCode, campaignCode, eventCode};
        Cursor c = db.rawQuery(query, whereArgs);
        if (c != null && c.moveToFirst()) {
            do {
                FieldsModel fieldsModel = new FieldsModel();
                fieldsModel.setA_id(c.getString(1));
                fieldsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                fieldsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                fieldsModel.setInputId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_INPUT_ID)));
                fieldsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                fieldsModel.setA_isActive(c.getString(6));
                fieldsModel.setA_dateTime(c.getString(7));
                fieldsModel.setSort(c.getInt(c.getColumnIndex(DatabaseConstants.KEY_SORT)));
                fieldsModel.setA_status(c.getString(9));
                fieldsModel.setB_id(c.getString(12));
                fieldsModel.setTypeId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TYPE_ID)));
                fieldsModel.setTitle(c.getString(14));
                fieldsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                fieldsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                fieldsModel.setGroupId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_GROUP_ID)));
                fieldsModel.setIsRequired(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_REQUIRED)));
                fieldsModel.setB_isActive(c.getString(19));
                fieldsModel.setB_dateTime(c.getString(20));
                fieldsModel.setCategory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CATEGORY)));
                fieldsModel.setB_status(c.getString(22));
                fieldsModel.setIsMandatory(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_MANDATORY)));
                fieldsModelList.add(fieldsModel);
            } while (c.moveToNext());
            c.close();
        }
        db.close();
        return fieldsModelList;
    }

    public List<GroupsModel> getAllGroupsByBrandCampaignEvent(String brandCode, String campaignCode, String eventCode) {
        List<GroupsModel> groupsModelList = new ArrayList<>();
        db = getReadableDatabase();

        String[] whereArgs = {brandCode, campaignCode, eventCode};
        String query = "select * from tbl_group " +
                "where brand_code =? " +
                "and campaign_code =? " +
                "and event_code=? " +
                "union all select * from tbl_group " +
                "where brand_code is null " +
                "and campaign_code is null " +
                "and event_code is null " +
                "order by title COLLATE NOCASE";

        Cursor c = db.rawQuery(query, whereArgs);
        if (c != null && c.moveToFirst()) {
            do {
                GroupsModel groupsModel = new GroupsModel();
                groupsModel.setId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_ID)));
                groupsModel.setTitle(c.getString(c.getColumnIndex(DatabaseConstants.KEY_TITLE)));
                groupsModel.setName(c.getString(c.getColumnIndex(DatabaseConstants.KEY_NAME)));
                groupsModel.setDescription(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DESCRIPTION)));
                groupsModel.setBrandCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                groupsModel.setCampaignCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_CAMPAIGN_CODE)));
                groupsModel.setIsActive(c.getString(c.getColumnIndex(DatabaseConstants.KEY_IS_ACTIVE)));
                groupsModel.setUserId(c.getString(c.getColumnIndex(DatabaseConstants.KEY_USER_ID)));
                groupsModel.setDateTime(c.getString(c.getColumnIndex(DatabaseConstants.KEY_DATE_TIME)));
                groupsModel.setStatus(c.getString(c.getColumnIndex(DatabaseConstants.KEY_STATUS)));
                groupsModel.setEventCode(c.getString(c.getColumnIndex(DatabaseConstants.KEY_EVENT_CODE)));
                groupsModelList.add(groupsModel);
            } while (c.moveToNext());
            c.close();
            db.close();
        }

        return groupsModelList;
    }

    public void deleteGroupById(GroupsModel groupsModel) {
        db = getWritableDatabase();
        String whereClause = DatabaseConstants.KEY_ID + "=?";
        String[] whereArgs = {groupsModel.getId()};
        db.delete(DatabaseConstants.TBL_GROUP, whereClause, whereArgs);
        db.close();

    }

    public List<DefaultSettingModel> getAllDefaultSettings() {
        db = getReadableDatabase();
        List<DefaultSettingModel> defaultSettingModelList = new ArrayList<>();
        String query = "SELECT * FROM " + DatabaseConstants.TBL_DEFAULT_SETTINGS;
        Cursor c = db.rawQuery(query, null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    DefaultSettingModel defaultSettingModel = new DefaultSettingModel();
                    defaultSettingModel.setBrand_code(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                    defaultSettingModel.setBrand_name(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_NAME)));
                    defaultSettingModel.setFilename(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FILENAME)));
                    defaultSettingModel.setImage(c.getBlob(c.getColumnIndex(DatabaseConstants.KEY_IMAGE)));
                    defaultSettingModelList.add(defaultSettingModel);
                } while (c.moveToNext());
                c.close();
            }
            db.close();
        }
        return defaultSettingModelList;
    }

    public DefaultSettingModel getDefaultSettingsByBrand(String brandCode) {
        db = getReadableDatabase();
        DefaultSettingModel defaultSettingModel = new DefaultSettingModel();
        String where = DatabaseConstants.KEY_BRAND_CODE + "=?";
        String[] whereArgs = {brandCode};

        Cursor c = db.query(DatabaseConstants.TBL_DEFAULT_SETTINGS, null, where, whereArgs,
                null, null, null, null);
        if (c != null) {
            if (c.moveToFirst()) {
                defaultSettingModel.setBrand_code(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_CODE)));
                defaultSettingModel.setBrand_name(c.getString(c.getColumnIndex(DatabaseConstants.KEY_BRAND_NAME)));
                defaultSettingModel.setFilename(c.getString(c.getColumnIndex(DatabaseConstants.KEY_FILENAME)));
                defaultSettingModel.setImage(c.getBlob(c.getColumnIndex(DatabaseConstants.KEY_IMAGE)));
            }
            c.close();
            db.close();
        }
        return defaultSettingModel;
    }

}