package com.unilab.customerregistration.utilities.applicationcontroller;

import android.app.Application;
import android.content.Intent;

import com.crashlytics.android.Crashlytics;
import com.tsengvn.typekit.Typekit;
import com.unilab.customerregistration.utilities.checkers.CheckerUtils;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;

import io.fabric.sdk.android.Fabric;

public class ApplicationController extends Application {

    private final String TAG = this.getClass().getSimpleName();
    private static ApplicationController mApplicationController = null;
    private DatabaseHelper mDatabaseHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mApplicationController = this;

        mDatabaseHelper = new DatabaseHelper(this);

        /**Setting up default font for the application*/
        Typekit.getInstance()
                .addNormal(Typekit.createFromAsset(this, "fonts/Arial.ttf"))
                .addBoldItalic(Typekit.createFromAsset(this, "fonts/arial_bold_italic.TTF"));

        /**Support registration data synchronization for Android N*/
        if (CheckerUtils.checkVersionCodeIfNougatOrAbove()) {
            sendBroadcast(new Intent(Constants.RECEIVER_SERVICE_WAS_KILLED));
        }
    }

    public static ApplicationController getInstance() {
        return mApplicationController;
    }

    public DatabaseHelper getDatabaseInstance() {
        return mDatabaseHelper;
    }

}
