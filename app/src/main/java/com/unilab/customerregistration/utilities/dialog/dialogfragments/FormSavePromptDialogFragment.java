package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.constants.Constants;

public class FormSavePromptDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static FormSavePromptDialogFragment frag;
    AlertDialog mAlertSavePromptDialog;
    private MaintenancePresenter mMaintenancePresenter;

    public static FormSavePromptDialogFragment
    newInstance(String title, String message, String actionType) {

        frag = new FormSavePromptDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        args.putString("action_type", actionType);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        String title = getArguments().getString("title");
        String message = getArguments().getString("message");
        final String actionType = getArguments().getString("action_type");

        mMaintenancePresenter = (MaintenancePresenter) getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_save_prompt, null);
        builder.setView(v);

        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        Button btnYes = (Button) v.findViewById(R.id.btnYes);
        Button btnNo = (Button) v.findViewById(R.id.btnNo);

        txtTitle.setText(title);
        txtMessage.setText(message);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (actionType) {
                    case Constants.DIALOG_ACTION_SAVE_FORM_FRAGMENT:
                        mMaintenancePresenter.saveForm();
                        break;

                    case Constants.DIALOG_RESET_FORM_PAGE:
                        mMaintenancePresenter.resetForm();
                        break;
                }
                dismiss();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        mAlertSavePromptDialog = builder.create();
        mAlertSavePromptDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAlertSavePromptDialog;
    }
}
