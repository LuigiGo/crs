package com.unilab.customerregistration.utilities.converters;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.util.Base64;

import com.unilab.customerregistration.utilities.constants.Constants;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class ConverterUtil {

    public static int hexColorToInt(String hexColor, String activityName, int settingType) {
        /**Note: F - white, 0 - black*/
        int color;

        try {
            switch (activityName) {
                case "DesignFragment":
                    if (hexColor == null || hexColor.isEmpty()) {
                        color = Color.parseColor("#FFFFFF");

                    } else if (hexColor.equals("transparent")) {
                        color = Color.parseColor("#00FFFFFF");

                    } else {
                        color = Color.parseColor(hexColor);
                    }
                    break;

                default:
                    if (hexColor == null || hexColor.isEmpty()) {
                        if (settingType == Constants.SETTING_TYPE_TEXTCOLOR) {
                            color = Color.parseColor("#000000");
                        } else {
                            color = Color.parseColor("#FFFFFF");
                        }

                    } else if (hexColor.equals("transparent")) {
                        color = Color.parseColor("#00FFFFFF");

                    } else {
                        color = Color.parseColor(hexColor);
                    }
                    break;
            }
            return color;
        } catch (StringIndexOutOfBoundsException e) {
            return Color.parseColor("#FFFFFF");
        }
    }

    public static String convertUriToBase64(Context context, String stringUri) throws IOException {
        return uriToBase64Convertion(context, stringUri);
    }

    private static String uriToBase64Convertion(Context context, String stringUri) {
        ContentResolver cr = ((Activity) context).getBaseContext().getContentResolver();
        InputStream inputStream = null;
        try {
            inputStream = cr.openInputStream(Uri.parse(stringUri));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        byte[] bytes;
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        bytes = output.toByteArray();
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }
}
