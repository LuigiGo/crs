package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.AssignFieldModel;
import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.InputModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.network.NetworkUtils;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class AddFormFieldSavePromptDialog extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static AddFormFieldSavePromptDialog frag;
    AlertDialog mAlertSavePromptDialog;
    private DatabaseHelper mDatabaseHelper;
    private MaintenancePresenter mMaintenancePresenter;

    public static AddFormFieldSavePromptDialog
    newInstance(String title, String message, final List<DataArray> dataArrayList, final InputModel inputModel,
                final AssignFieldModel assignFieldModel, final FieldsModel fieldsModel) {

        frag = new AddFormFieldSavePromptDialog();

        ArrayList<DataArray> dataArrayArrayList = new ArrayList<>();
        dataArrayArrayList.addAll(dataArrayList);

        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        args.putParcelableArrayList("data_array_list", dataArrayArrayList);
        args.putParcelable("input_model", inputModel);
        args.putParcelable("assign_field_model", assignFieldModel);
        args.putParcelable("fields_model", fieldsModel);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        String title = getArguments().getString("title");
        String message = getArguments().getString("message");
        final ArrayList<DataArray> dataArrayList = getArguments().getParcelableArrayList("data_array_list");
        final InputModel inputModel = getArguments().getParcelable("input_model");
        final AssignFieldModel assignFieldModel = getArguments().getParcelable("assign_field_model");
        final FieldsModel fieldsModel = getArguments().getParcelable("fields_model");

        mDatabaseHelper = new DatabaseHelper(getActivity());
        mMaintenancePresenter = (MaintenancePresenter) getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_save_prompt, null);
        builder.setView(v);

        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        Button btnYes = (Button) v.findViewById(R.id.btnYes);
        Button btnNo = (Button) v.findViewById(R.id.btnNo);

        txtTitle.setText(title);
        txtMessage.setText(message);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e(TAG, "onClick: " + "Field name not exist!");
                if (NetworkUtils.isNetworkConnected(getActivity())) {
                    mMaintenancePresenter.addFormField(fieldsModel, dataArrayList, inputModel,
                            assignFieldModel, Constants.DIALOG_ACTION_ADD_FORM_FIELD);
                    dismiss();

                } else {
                    if (dataArrayList.size() > 0) {
                        for (DataArray dataArray : dataArrayList) {
                            int maxDataArrayId = mDatabaseHelper.getMaxDataArrayId(inputModel.getTypeId());
                            maxDataArrayId++;
                            Log.e(TAG, "onClick: " + "InputOptions InputId: " + dataArray.getInputId());
                            Log.e(TAG, "onClick: " + "InputOptions Title: " + dataArray.getTitle());
                            Log.e(TAG, "onClick: " + "InputOptions Name: " + dataArray.getName());
                            Log.e(TAG, "onClick: " + "InputOptions Value: " + dataArray.getValue());
                            Log.e(TAG, "onClick: " + "InputOptions ActiveStatus: " + dataArray.getIsActive());

                            dataArray.setId(String.valueOf(maxDataArrayId));
                            dataArray.setInputId(String.valueOf(inputModel.getId()));
                            mDatabaseHelper.createDataArray(dataArray, inputModel.getTypeId());

                        }
                    } else {
                        int maxDataArrayId = mDatabaseHelper.getMaxDataArrayId(inputModel.getTypeId());
                        maxDataArrayId++;
                        DataArray dataArray = new DataArray();
                        dataArray.setId(String.valueOf(maxDataArrayId));
                        dataArray.setInputId(String.valueOf(inputModel.getId()));
                        dataArray.setTitle(inputModel.getTitle());
                        dataArray.setName(inputModel.getName());
                        dataArray.setValue("");
                        dataArray.setIsActive("1");
                        dataArray.setDateTime(Utils.getDateTime());
                        mDatabaseHelper.createDataArray(dataArray, inputModel.getTypeId());
                    }

                    mDatabaseHelper.createField(inputModel);
                    mDatabaseHelper.assignField(assignFieldModel);
//                    mMaintenancePresenter.refreshFormAdapters();
                    mMaintenancePresenter.refreshCustomFieldsAdapter();
                }
                dismiss();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        mAlertSavePromptDialog = builder.create();
        mAlertSavePromptDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAlertSavePromptDialog;
    }
}
