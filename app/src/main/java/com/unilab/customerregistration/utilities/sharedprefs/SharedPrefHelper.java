package com.unilab.customerregistration.utilities.sharedprefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.unilab.customerregistration.utilities.constants.Constants;


public class SharedPrefHelper {

    private static SharedPrefHelper sSharedPrefHelper;
    private static SharedPreferences mSharedPreferences;

    public static void init(Context context) {
        if (sSharedPrefHelper == null) {
            sSharedPrefHelper = new SharedPrefHelper();
            mSharedPreferences = context.getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        }
    }

    public static SharedPrefHelper getInstance() {
        if (sSharedPrefHelper != null) {
            return sSharedPrefHelper;
        }

        throw new IllegalStateException("Call SharedPrefHelper init()");
    }

    public void putEmailToShared(String email) {
        mSharedPreferences.edit().putString(Constants.PREFKEY_EMAIL, email).apply();
    }

    public String getEmailFromShared() {
        return mSharedPreferences.getString(Constants.PREFKEY_EMAIL, "");
    }

    public void putPasswordToShared(String password) {
        mSharedPreferences.edit().putString(Constants.PREFKEY_PASSWORD, password).apply();
    }

    public String getPasswordFromShared() {
        return mSharedPreferences.getString(Constants.PREFKEY_PASSWORD, "");
    }

    public void putBrandNameToShared(String brandName) {
        mSharedPreferences.edit().putString(Constants.PREFKEY_BRANDNAME, brandName).apply();
    }

    public String getBrandNameFromShared() {
        return mSharedPreferences.getString(Constants.PREFKEY_BRANDNAME, "");
    }

    public void putCampaignNameToShared(String campaignName) {
        mSharedPreferences.edit().putString(Constants.PREFKEY_CAMPAIGNNAME, campaignName).apply();
    }

    public String getCampaignNameFromShared() {
        return mSharedPreferences.getString(Constants.PREFKEY_CAMPAIGNNAME, "");
    }

    public void putFormattedCampaignNameToShared(String formattedCampaignName) {
        mSharedPreferences.edit().putString(Constants.PREFKEY_FORMATTED_CAMPAIGN_EVENT_NAME, formattedCampaignName).apply();
    }

    public String getFormattedCampaignNameFromShared() {
        return mSharedPreferences.getString(Constants.PREFKEY_FORMATTED_CAMPAIGN_EVENT_NAME, "");
    }

    public void putEventNameToShared(String eventName) {
        mSharedPreferences.edit().putString(Constants.PREFKEY_EVENTNAME, eventName).apply();
    }

    public String getEventNameFromShared() {
        return mSharedPreferences.getString(Constants.PREFKEY_EVENTNAME, "");
    }

    public void putBrandCodeToShared(String brandCode) {
        mSharedPreferences.edit().putString(Constants.PREFKEY_BRANDCODE, brandCode).apply();
    }

    public String getBrandCodeFromShared() {
        return mSharedPreferences.getString(Constants.PREFKEY_BRANDCODE, "");
    }

    public void putCampaignCodeToShared(String campaignCode) {
        mSharedPreferences.edit().putString(Constants.PREFKEY_CAMPAIGNCODE, campaignCode).apply();
    }

    public String getCampaignCodeFromShared() {
        return mSharedPreferences.getString(Constants.PREFKEY_CAMPAIGNCODE, "");
    }

    public void putEventCodeToShared(String eventCode) {
        mSharedPreferences.edit().putString(Constants.PREFKEY_EVENTCODE, eventCode).apply();
    }

    public String getEventCodeFromShared() {
        return mSharedPreferences.getString(Constants.PREFKEY_EVENTCODE, "");
    }


    public void putFooterTextToShared(String footerText) {
        mSharedPreferences.edit().putString(Constants.PREFKEY_FOOTER_TEXT, footerText).apply();
    }

    public void putPreLoadedDataStatusToShared(boolean status) {
        mSharedPreferences.edit().putBoolean(Constants.PREFKEY_PRELOADED_DATA_STATUS, status).apply();
    }

    public boolean getPreLoadedDataStatusFromShared() {
        return mSharedPreferences.getBoolean(Constants.PREFKEY_PRELOADED_DATA_STATUS, false);
    }

    public void putGeneratedUrlToShared(String generatedUrl) {
        mSharedPreferences.edit().putString(Constants.PREFKEY_GENERATED_URL, generatedUrl).apply();
    }

    public String getGeneratedUrlFromShared() {
        return mSharedPreferences.getString(Constants.PREFKEY_GENERATED_URL, "");
    }

}
