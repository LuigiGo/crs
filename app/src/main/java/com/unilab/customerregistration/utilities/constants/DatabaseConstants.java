package com.unilab.customerregistration.utilities.constants;

public class DatabaseConstants {

    /**
     * DATABASE TAGS
     */
    public static final String DATABASE_NAME = "db_ocr.sqlite";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_ASSETS_PATH = "db_ocr.sqlite";
    public static final String DATABASE_DEVICE_PATH = "/data/data/com.unilab.customerregistration/databases/";

    public static final String TBL_INPUT = "tbl_input";
    public static final String TBL_INPUT_TYPE = "tbl_input_type";
    public static final String TBL_INPUT_ASSIGN = "tbl_input_assign";
    public static final String TBL_GROUP = "tbl_group";
    public static final String TBL_SETTING = "tbl_settings";
    public static final String TBL_REGISTRATION = "tbl_registration";

    public static final String TBL_TEXTBOX = "tbl_textbox";
    public static final String TBL_CHECKBOX = "tbl_checkbox";
    public static final String TBL_RADIO = "tbl_radio";
    public static final String TBL_SELECT = "tbl_select";
    public static final String TBL_BUTTON = "tbl_button";

    public static final String TBL_DESIGN = "tbl_design";
    public static final String TBL_COUNTRY = "tbl_country";
    public static final String TBL_REGION = "tbl_region";
    public static final String TBL_PROVINCE = "tbl_province";
    public static final String TBL_CITY = "tbl_city";
    public static final String TBL_DEFAULT_SETTINGS = "tbl_default_settings";


    public static final String KEY_ID = "id";
    public static final String KEY_TYPE_ID = "type_id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_NAME = "name";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_GROUP_ID = "group_id";
    public static final String KEY_IS_REQUIRED = "is_required";
    public static final String KEY_IS_ACTIVE = "is_active";
    public static final String KEY_DATE_TIME = "date_time";
    public static final String KEY_CATEGORY = "category";
    public static final String KEY_INPUT_ID = "input_id";
    public static final String KEY_VALUE = "value";
    public static final String KEY_ICON = "icon";
    public static final String KEY_CAMPAIGN_CODE = "campaign_code";
    public static final String KEY_BRAND_CODE = "brand_code";
    public static final String KEY_BACKGROUND = "background";
    public static final String KEY_LOGO = "logo";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_STATUS = "status";
    public static final String KEY_SORT = "sort";
    public static final String KEY_DATA = "data";
    public static final String KEY_WEB_LOGO_IMG_FILENAME = "web_logo_img_fileName";
    public static final String KEY_MOB_LOGO_IMG_FILENAME = "mob_logo_img_fileName";
    public static final String KEY_EDIT_STATUS = "edit_status";
    public static final String KEY_EVENT_CODE = "event_code";
    public static final String KEY_CAMPAIGN_NAME = "campaign_name";
    public static final String KEY_IS_MANDATORY = "is_mandatory";
    public static final String KEY_DBM_STATUS = "dbm_status";
    public static final String KEY_ACTION_TYPE = "action_type";
    public static final String KEY_EVENT_NAME = "event_name";

    public static final String KEY_FOREGROUND_COLOR = "foreground";
    public static final String KEY_BACKGROUND_COLOR = "background";
    public static final String KEY_HEADER_COLOR = "header";
    public static final String KEY_FOOTER_COLOR = "footer";
    public static final String KEY_FOOTER_TEXT = "footer_text";
    public static final String KEY_WEB_LOGO_IMG = "web_logo_img";
    public static final String KEY_WEB_LOGO = "web_logo";
    public static final String KEY_MOBILE_LOGO_IMG = "mob_logo_img";
    public static final String KEY_MOBILE_LOGO = "mob_logo";
    public static final String KEY_FONT_HEADFAMILY = "font_headfamily";
    public static final String KEY_FONT_BASEFAMILY = "font_basefamily";
    public static final String KEY_SUBMIT_BACKGROUND = "submit_background";
    public static final String KEY_SUBMIT_BORDER_RADIUS = "submit_border_radius";
    public static final String KEY_SUBMIT_BORDER_WIDTH = "submit_border_width";
    public static final String KEY_SUBMIT_ALIGNMENT = "submit_alignment";
    public static final String KEY_FONT_HEADSIZE = "font_headsize";
    public static final String KEY_FONT_BASESIZE = "font_basesize";

    public static final String KEY_COUNTRY_ID = "country_id";
    public static final String KEY_ISO2_CODE = "iso2_code";
    public static final String KEY_REGION_ID = "region_id";
    public static final String KEY_REGION_NAME = "region_name";
    public static final String KEY_PROVINCE_ID = "province_id";
    public static final String KEY_PROVINCE_CODE = "province_code";
    public static final String KEY_PROVINCE_NAME = "province_name";
    public static final String KEY_CITY_ID = "city_id";
    public static final String KEY_CITY_NAME = "city_name";
    public static final String KEY_BRAND_NAME = "brand_name";
    public static final String KEY_FILENAME = "filename";
    public static final String KEY_IMAGE = "image";

}
