package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.registration.presenter.RegistrationContract;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.converters.ConverterUtil;
import com.unilab.customerregistration.utilities.utils.Utils;

public class AlertRegistrationDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static AlertRegistrationDialogFragment frag;
    AlertDialog mAlertInfoDialog;
    private RegistrationContract mRegistrationContract;
    private RegistrationContract.DialogListener mRegistrationDialogListener;
    private Typeface tfBase;

    public static AlertRegistrationDialogFragment newInstance(String title, String message, String actionType, String result, SettingsModel settingsModel) {
        frag = new AlertRegistrationDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        args.putString("actionType", actionType);
        args.putString("result", result);
        args.putParcelable("settingsModel", settingsModel);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String message = getArguments().getString("message");
        final String actionType = getArguments().getString("actionType");
        final String registrationResult = getArguments().getString("result");
        SettingsModel mSettingsModel = getArguments().getParcelable("settingsModel");

        mRegistrationContract = (RegistrationContract) getActivity();
        mRegistrationDialogListener = (RegistrationContract.DialogListener) getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        getActivity().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_registration_information, null);
        builder.setView(v);
        builder.setCancelable(false);

        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        Button btnOk = (Button) v.findViewById(R.id.btnOk);

        txtTitle.setText(title);
        txtMessage.setText(message);

        /**Setting up dialog style*/
        if (mSettingsModel != null) {
            /**Text color*/
            tfBase = Typeface.createFromAsset(getActivity().getAssets(),
                    Utils.getDefaultTypeface(mSettingsModel.getFontBasefamily()));

            txtTitle.setTypeface(tfBase);
            txtMessage.setTypeface(tfBase);
            btnOk.setTypeface(tfBase);

            txtTitle.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtMessage.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            btnOk.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
        }

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (actionType) {
                    case Constants.DIALOG_ACTION_SAVE_REGISTRATION_DETAILS:
                        if (registrationResult.equals("1")) {
                            mRegistrationContract.refreshRegistrationForm();
                        }
                        break;

                    case Constants.DIALOG_ACTION_UPDATE_REGISTRATION_DETAILS:
                        if (registrationResult.equals("1")) {
                            mRegistrationContract.refreshRegistrationForm();
                        }
                        break;

                    case Constants.DIALOG_ACTION_UPDATE_REGISTRATION_DETAILS_LOCALLY:
                        mRegistrationContract.refreshRegistrationForm();
                        break;

                    case Constants.DIALOG_ACTION_SAVE_REGISTRATION_DETAILS_LOCALLY:
                        mRegistrationContract.refreshRegistrationForm();
                        break;

                    default:

                        break;
                }

                dismiss();
            }
        });

        mAlertInfoDialog = builder.create();
        mAlertInfoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAlertInfoDialog;
    }
}
