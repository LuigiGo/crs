package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unilab.customerregistration.R;

import org.w3c.dom.Text;

public class AlertShowRegUpdatesDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static AlertShowRegUpdatesDialogFragment frag;
    AlertDialog mAlertInfoDialog;

    public static AlertShowRegUpdatesDialogFragment newInstance(Intent intent) {
        frag = new AlertShowRegUpdatesDialogFragment();

        String message = intent.getStringExtra("message");
        String totalRecords = intent.getStringExtra("totalRecords");
        String newRegistered = intent.getStringExtra("newRegistered");
        String duplicateRecords = intent.getStringExtra("duplicateRecords");

        Bundle args = new Bundle();
        args.putString("message", message);
        args.putString("totalRecords", totalRecords);
        args.putString("newRegistered", newRegistered);
        args.putString("duplicateRecords", duplicateRecords);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String message = getArguments().getString("message");
        String totalRecords = getArguments().getString("totalRecords");
        String newRegistered = getArguments().getString("newRegistered");
        String duplicateRecords = getArguments().getString("duplicateRecords");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        getActivity().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_registration_updates, null);
        builder.setView(v);
        builder.setCancelable(false);

        TextView txtTitle, txtMessage, txtTotalRecords, txtNewRegistered, txtDuplicates;
        txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        txtTotalRecords = (TextView) v.findViewById(R.id.txtTotalRows);
        txtNewRegistered = (TextView) v.findViewById(R.id.txtNewRegistered);
        txtDuplicates = (TextView) v.findViewById(R.id.txtDuplicates);
        Button btnOk = (Button) v.findViewById(R.id.btnOk);

        txtTitle.setText("Registration Updates");
        Log.e(TAG, "onCreateDialog: " + message);
        txtMessage.setText(message);
        txtTotalRecords.setText(totalRecords);
        txtNewRegistered.setText(newRegistered);
        txtDuplicates.setText(duplicateRecords);


        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mAlertInfoDialog = builder.create();
        mAlertInfoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAlertInfoDialog;
    }
}
