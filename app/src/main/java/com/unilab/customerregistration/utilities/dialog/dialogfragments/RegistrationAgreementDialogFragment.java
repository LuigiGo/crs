package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.registration.presenter.RegistrationContract;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.converters.ConverterUtil;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

public class RegistrationAgreementDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static RegistrationAgreementDialogFragment frag;
    AlertDialog mAlertAgreementPromptDialog;
    private Typeface tfBase;
    private RegistrationContract.DialogListener mRegistrationDialogListener;

    public static RegistrationAgreementDialogFragment newInstance(SettingsModel settingsModel) {
        frag = new RegistrationAgreementDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("settings_model", settingsModel);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Dialog);
        SharedPrefHelper.init(getActivity());
        String brandName = SharedPrefHelper.getInstance().getBrandNameFromShared();
        SettingsModel mSettingsModel = getArguments().getParcelable("settings_model");

        /**Note: Setting default text for brand name Health+*/
        if (brandName.equals("Health+")) {
            brandName = "United Laboratories, Inc.";
        }
        mRegistrationDialogListener = (RegistrationContract.DialogListener) getActivity();

        if (mSettingsModel != null) {
            /**Text color*/
            tfBase = Typeface.createFromAsset(getActivity().getAssets(),
                    Utils.getDefaultTypeface(mSettingsModel.getFontBasefamily()));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_agreement_prompt, null);
        builder.setView(v);
        builder.setCancelable(false);

        final TextView lblBullet1 = (TextView) v.findViewById(R.id.lblBullet1);
        final TextView lblBullet2 = (TextView) v.findViewById(R.id.lblBullet2);
        final TextView lblBullet3 = (TextView) v.findViewById(R.id.lblBullet3);
        final TextView lblBullet4 = (TextView) v.findViewById(R.id.lblBullet4);
        final TextView lblNumerical1 = (TextView) v.findViewById(R.id.lblNumerical1);
        final TextView lblNumerical2 = (TextView) v.findViewById(R.id.lblNumerical2);
        final TextView lblNumerical3 = (TextView) v.findViewById(R.id.lblNumerical3);

        final TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        final TextView txtContent1 = (TextView) v.findViewById(R.id.txtContent1);
        final TextView txtList1 = (TextView) v.findViewById(R.id.txtList1);
        final TextView txtList2 = (TextView) v.findViewById(R.id.txtList2);
        final TextView txtList3 = (TextView) v.findViewById(R.id.txtList3);
        final TextView txtList4 = (TextView) v.findViewById(R.id.txtList4);
        final TextView txtContent2 = (TextView) v.findViewById(R.id.txtContent2);
        final TextView txtListNumerical1 = (TextView) v.findViewById(R.id.txtListNumerical1);
        final TextView txtListNumerical2 = (TextView) v.findViewById(R.id.txtListNumerical2);
        final TextView txtListNumerical3 = (TextView) v.findViewById(R.id.txtListNumerical3);
        final TextView txtContent3 = (TextView) v.findViewById(R.id.txtContent3);
        final Button btnAgree = (Button) v.findViewById(R.id.btnAgree);
        final Button btnDisagree = (Button) v.findViewById(R.id.btnDisagree);

        final String strContent1 = "<html>" +
                "<head><title></title></head>" +
                "<body>" +
                "<strong>" + brandName.toUpperCase() + "&apos;S ONLINE PRIVACY STATEMENT</strong>" +
                "<br><br>" +
                "Thank you for visiting our website or interacting with us via email and/or our Facebook Page." +
                "<br><br>" +
                "At " + brandName + ", we understand that protecting the privacy of visitors to our website is very important and that information about your health is particularly sensitive. That&apos;s why we have taken the necessary steps to meet worldwide data privacy requirements. We treat your personal data according to the applicable laws which regulate the storage, process, access and transfer of personal data." +
                "<br><br>" +
                brandName + " websites that display this privacy policy (&quot;policy&quot;) and that ask for any information from you are committed to collecting, maintaining, and securing personal information about you in accordance with this policy, as well as applicable laws, rules and regulations. This policy applies to personal information (as defined below) collected from " + brandName + " online resources and communications (such as web pages, email and other online tools) that display a link to this policy. This policy does not apply to personal information collected from offline resources and communications, except in cases where such personal information is consolidated with personal information collected by " + brandName + " online. This policy also does not apply to third-party online resources to which " + brandName + "&apos;s websites may link, where " + brandName + " does not control the content or the privacy practices of such resources." +
                "<br><br>" +
                "We only collect personally identifiable information about you if you choose to give it to us. We do not share any of your personally identifiable information with third parties for their own marketing use unless you explicitly give us permission to do so. Please review this Privacy Statement to learn more about how we collect, use, share and protect information online." +
                "<br><br>" +
                "<strong>Information Collected</strong>" +
                "<br><br>" +
                "<strong>There are two general methods that " + brandName + " uses to collect personal information online:</strong>" +
                "</body>" +
                "</html>";

        final String strContent2 = "<html>" +
                "<head><title></title></head>" +
                "<body>" +
                "We automatically receive certain types of information whenever you interact with us on our sites and in some emails we may send each other. Automatic technologies we use may include, for example, web server logs/IP addresses, cookies and web beacons." +
                "<br><br>" +
                "<u>Web Server Logs/IP Addresses.</u> An IP address is a number assigned to your computer whenever you access the Internet. All computer identification on the Internet is conducted with IP addresses, which allow computers and servers to recognize and communicate with each other. " + brandName + " collects IP addresses to conduct system administration and report aggregate information to affiliates, business partners and/or vendors to conduct site analysis and website performance review." +
                "<br><br>" +
                "<u>Cookies.</u> A cookie is a piece of information that is placed automatically on your computer&apos;s hard drive when you access certain websites. The cookie uniquely identifies your browser to the server. Cookies allow us to store information on the server to help make the web experience better for you and to conduct site analysis and website performance review. Most web browsers are set up to accept cookies, although you can reset your browser to refuse all cookies or to indicate when a cookie is being sent. Note, however, that some portions of our sites may not work properly if you refuse cookies." +
                "<br><br>" +
                "<u>Web Beacons.</u> On certain web pages or emails, " + brandName + " may utilize a common Internet technology called a &quot;web beacon&quot; (also known as an &quot;action tag&quot; or &quot;clear GIF technology&quot;). Web beacons help analyze the effectiveness of websites by measuring, for example, the number of visitors to a site or how many visitors clicked on key elements of a site." +
                "<br><br>" +
                "Web beacons, cookies and other tracking technologies do not automatically obtain personally identifiable information about you. Only if you voluntarily submit personally identifiable information, such as by registering or sending emails, can these automatic tracking technologies be used to provide further information about your use of the websites and/or interactive emails to improve their usefulness to you." +
                "<br><br>" +
                "<strong>Your Choices</strong>" +
                "<br><br>" +
                "You have several choices regarding your use of our websites. You could decide not to submit any personally identifiable information at all by not entering it into any forms or data fields on our sites and not using any available personalized services. If you choose to submit personal data, you have the right to see and correct your data at any time by accessing the application. Certain sites may ask for your permission for certain uses of your information and you can agree to or decline those uses. If you opt-in for particular services or communications, such as an e-newsletter, you will be able to unsubscribe at any time by following the instructions included in each communication. If you decide to unsubscribe from a service or communication, we will work to remove your information promptly, although we may require additional information before we can process your request." +
                "<br><br>" +
                "As described above, if you wish to prevent cookies from tracking you anonymously as you navigate our sites, you can reset your browser to refuse all cookies or to indicate when a cookie is being sent." +
                "<br><br>" +
                "<strong>Security</strong>" +
                "<br><br>" +
                brandName + "uses technology and security precautions, rules and other procedures to protect your personal data from unauthorized access, improper use, disclosure, loss or destruction. To ensure the confidentiality of your data, " + brandName + " uses also industry standard firewalls and password protection. It is, however, your personal responsibility to ensure that the computer you are using is adequately secured and protected against malicious software, such as trojans, computer viruses and worm programs. You are aware of the fact that without adequate security measures (e.g., secure web browser configuration, up-to-date antivirus software, personal firewall software, no usage of software from dubious sources), there is a risk that the data and passwords you use to protect access to your data could be disclosed to unauthorized third parties." +
                "<br><br>" +
                "<strong>Use of Data</strong>" +
                "<br><br>" +
                brandName + ", including the subsidiaries, divisions and groups worldwide and/or the companies we hire to perform services on our behalf will use any personally identifiable information you choose to give us to comply with your requests. We will retain control of and responsibility for the use of this information. Some of these data may be stored or processed at computers located in other jurisdictions, such as the United States, whose data protection laws may differ from the jurisdiction in which you live. In such cases, we will ensure that appropriate protections are in place to require the data processor in that country to maintain protections on the data that are equivalent to those that apply in the country in which you live." +
                "<br><br>" +
                "The information, which is also used for different HR purposes (performance management, succession decisions or development actions), will be helpful for us to better understand your needs and how we can improve our products and services. It helps us also to personalize certain communications with you about services and promotions that you might find interesting. For example, we may analyze the gender or age of visitors to sites about a particular medication or disease state, and we may use that analysis of aggregate data internally or share it with others." +
                "<br><br>" +
                "<strong>Data Sharing and Transfer</strong>" +
                "<br><br>" +
                brandName + " shares personally identifiable data about you with various outside companies or agents working on our behalf to help fulfill business transactions, such as providing customer services, sending marketing communications about our products, services and offers, and doing technological maintenance. We may also share personally identifiable data with our company&apos;s subsidiaries and affiliates. All these companies and agents are required to comply with the terms of our privacy policies." +
                "<br><br>" +
                "We may also disclose personally identifiable information for these purposes:" +
                "</body>" +
                "</html>";

        final String strList1 = "<html>" +
                "<head><title></title></head>" +
                "<body>" +
                "<strong>Information We Get</strong>" +
                "</body>" +
                "</html>";

        final String strList2 = "<html>" +
                "<head><title></title></head>" +
                "<body>" +
                "<strong>Personal information:</strong>You can visit our websites without providing any personal information. We may collect your personally identifiable information (such as name, address, telephone number, email or other identifying information) only when you choose to submit it to us. We may also collect health information about you that you provide by responding to our questions or surveys." +
                "</body>" +
                "</html>";

        final String strList3 = "<html>" +
                "<head><title></title></head>" +
                "<body>" +
                "<strong>Aggregate information:</strong>In some cases, we also remove personal identifiers from data you provide to us and maintain it in aggregate form. We may combine this data with other information to produce anonymous, aggregated statistical information (e.g., number of visitors, originating domain name of the Internet Service Provider), helpful to us in improving our products and services." +
                "</body>" +
                "</html>";

        final String strList4 = "<html>" +
                "<head><title></title></head>" +
                "<body>" +
                "<strong>Automatically Collected Information</strong>" +
                "</body>" +
                "</html>";

        final String strListNumerical1 = "<html>" +
                "<head><title></title></head>" +
                "<body>" +
                "(a) in connection with the sale, assignment or other transfer of the business of the site to which the data relates;" +
                "</body>" +
                "</html>";

        final String strListNumerical2 = "<html>" +
                "<head><title></title></head>" +
                "<body>" +
                "(b) to respond to appropriate requests of legitimate government agencies or where required by applicable laws, court orders, or government regulations; or" +
                "</body>" +
                "</html>";

        final String strListNumerical3 = "<html>" +
                "<head><title></title></head>" +
                "<body>" +
                "(c) where needed for corporate audits or to investigate or respond to a complaint or security threat." +
                "</body>" +
                "</html>";

        final String strContent3 = "<html>" +
                "<head><title> </title></head>" +
                "<body>" +
                "<u>No Third-Party Direct Marketing Use.</u> We will not sell or otherwise transfer the personally identifiable information you provide to us at our websites to any third parties for their own direct marketing use unless we provide clear notice to you and obtain your explicit consent for your data to be shared in this manner." +
                "<br><br>" +
                "<u>Email a Friend or Colleague.</u> On some " + brandName + " sites, you can choose to send a link or a message to a friend or colleague referring them to a " + brandName + " website. Email addresses you may provide for a friend will be used to send your friend information on your behalf and will not be collected or used by " + brandName + " or other third parties for additional purposes." +
                "<br><br>" +
                "<strong>Links to Other Sites</strong>" +
                "<br><br>" +
                "Our sites contain links to a number of websites that may offer useful information to our visitors. This Privacy Statement does not apply to those sites, and we recommend communicating to them directly for information on their privacy policies." +
                "<br><br>" +
                "<strong>Privacy Statement for Children</strong>" +
                "<br><br>" +
                "Our websites are directed at an adult audience. We do not collect any personally identifiable information from anyone we know to be under the age of 13 without the prior, verifiable consent of his or her legal representative. Such legal representative has the right, upon request, to view the information provided by the child and/or to require that it be deleted." +
                "<br><br>" +
                "<strong>Additional Information on Websites</strong>" +
                "<br><br>" +
                "If a website has particular provisions relating to privacy that differ from those stated here, those provisions will be disclosed to you on the page on which personally identifiable information is collected." +
                "<br><br>" +
                "<strong>Note to Users of Business or Professional Websites</strong>" +
                "<br><br>" +
                "Information you submit on our sites, including sites intended specifically for business and professional users, to fulfill your requests and develop our business relationship with you and the entities you represent. We may also share such information with third parties acting on our behalf." +
                "<br><br>" +
                "<strong>Updates to Privacy Statement</strong>" +
                "<br><br>" +
                "From time to time, " + brandName + " may revise this online Privacy Statement. Any such changes to this Privacy Statement will be promptly communicated on this page. Continued use of our sites after receiving notice of a change in our Privacy Statement indicates your consent to the use of newly submitted information in accordance with the amended " + brandName + " Privacy Statement. The effective date of this Privacy Statement is May 15, 2016." +
                "<br><br>" +
                "<strong>How to Contact " + brandName + "</strong>" +
                "<br><br>" +
                "For questions, please call Unilab Consumer Care Center at (632) 864-5221 (Monday to Saturday, 8am-5pm)." +
                "<br><br>" +
                "<strong>I have read the Privacy Policy and the terms and conditions for this promotion.</strong>" +
                "<br><br>" +
                "<strong>I want to receive exclusive offers and updates from " + brandName + " and give consent for United Laboratories, Inc. to use and share with its affiliates, subsidiaries including Unilab, Inc. and agents.</strong>" +
                "</body>" +
                "</html>";


        /**Note: This is setting static text to dialog agreement due to incompatibility of setting custom font to webview*/
        txtTitle.setText(brandName);
        txtContent1.setText(Html.fromHtml(strContent1));
        txtList1.setText(Html.fromHtml(strList1));
        txtList2.setText(Html.fromHtml(strList2));
        txtList3.setText(Html.fromHtml(strList3));
        txtList4.setText(Html.fromHtml(strList4));
        txtContent2.setText(Html.fromHtml(strContent2));
        txtListNumerical1.setText(Html.fromHtml(strListNumerical1));
        txtListNumerical2.setText(Html.fromHtml(strListNumerical2));
        txtListNumerical3.setText(Html.fromHtml(strListNumerical3));

        txtContent3.setText(Html.fromHtml(strContent3));

        /**Setting selected typeface to views*/
        if (mSettingsModel != null && tfBase != null) {
            txtTitle.setTypeface(tfBase);
            txtContent1.setTypeface(tfBase);
            txtList1.setTypeface(tfBase);
            txtList2.setTypeface(tfBase);
            txtList3.setTypeface(tfBase);
            txtList4.setTypeface(tfBase);
            txtContent2.setTypeface(tfBase);
            txtListNumerical1.setTypeface(tfBase);
            txtListNumerical2.setTypeface(tfBase);
            txtListNumerical3.setTypeface(tfBase);
            txtContent3.setTypeface(tfBase);
            btnAgree.setTypeface(tfBase);
            btnDisagree.setTypeface(tfBase);

            txtTitle.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtContent1.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtList1.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtList2.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtList3.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtList4.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtContent2.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtListNumerical1.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtListNumerical2.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtListNumerical3.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtContent3.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));

            /**Bullets*/
            lblBullet1.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            lblBullet2.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            lblBullet3.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            lblBullet4.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            lblNumerical1.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            lblNumerical2.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            lblNumerical3.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));

            lblNumerical1.setTypeface(tfBase);
            lblNumerical2.setTypeface(tfBase);
            lblNumerical3.setTypeface(tfBase);
        }


        btnAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                mRegistrationDialogListener.setSubscriptionStatus(true);

            }
        });

        btnDisagree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                mRegistrationDialogListener.setSubscriptionStatus(false);

            }
        });

        mAlertAgreementPromptDialog = builder.create();
        mAlertAgreementPromptDialog.setCancelable(false);
        mAlertAgreementPromptDialog.getWindow().setBackgroundDrawable(null);
        mAlertAgreementPromptDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        return mAlertAgreementPromptDialog;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
