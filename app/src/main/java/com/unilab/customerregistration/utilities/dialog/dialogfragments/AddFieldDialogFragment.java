package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.AssignFieldModel;
import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.GroupsModel;
import com.unilab.customerregistration.features.maintenance.model.InputModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.features.maintenance.view.adapters.DataArrayAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.SubGroupingAdapter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.AddUpdateFieldSubGroupNameAdapter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.network.NetworkUtils;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class AddFieldDialogFragment extends DialogFragment implements View.OnTouchListener {

    private final String TAG = this.getClass().getSimpleName();
    private static AddFieldDialogFragment frag;
    AlertDialog mAddFieldDialog;
    private DatabaseHelper mDatabaseHelper;
    private String brandCode, campaignCode, eventCode;
    private List<DataArray> dataArrayList;
    //    private List<String> groupsNameList;
    private List<GroupsModel> mGroupsModelDropdownList = new ArrayList<>();
    private MaintenancePresenter mMaintenancePresenter;
    private RelativeLayout rlAddSubGroup;
    private RelativeLayout rlFieldDetailsContainer;
    private AddUpdateFieldSubGroupNameAdapter mAddUpdateFieldSubGroupNameAdapter;

    public static AddFieldDialogFragment newInstance() {
        frag = new AddFieldDialogFragment();
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        /**Initialize variables*/
        mDatabaseHelper = new DatabaseHelper(getActivity());
        SharedPrefHelper.init(getActivity());

        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        mMaintenancePresenter = (MaintenancePresenter) getActivity();

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_input_fields, null);
        alertBuilder.setView(v);

        /**Views*/
        final TextView txtHeader = (TextView) v.findViewById(R.id.txtHeader);
        final EditText edtFieldName = (EditText) v.findViewById(R.id.edtNameOfField);
        final EditText edtGroupName = (EditText) v.findViewById(R.id.edtSubGroupName);
        final EditText edtOptionsTitle = (EditText) v.findViewById(R.id.edtOptionsTitle);
        final EditText edtOptionsValue = (EditText) v.findViewById(R.id.edtOptionsValue);
        final EditText edtDescription = (EditText) v.findViewById(R.id.edtDescription);
        final CheckBox chIsRequired = (CheckBox) v.findViewById(R.id.chRequired);
        final Spinner spnInputType = (Spinner) v.findViewById(R.id.spnInputType);
        final Spinner spnSubGroup = (Spinner) v.findViewById(R.id.spnSubGroup);
        final LinearLayout llInputOptions = (LinearLayout) v.findViewById(R.id.llInputOptions);
        LinearLayout llAddFieldRootView = (LinearLayout) v.findViewById(R.id.llAddFieldRootView);
        rlFieldDetailsContainer = (RelativeLayout) v.findViewById(R.id.rlFieldDetailsContainer);

        ImageView btnAddOptions = (ImageView) v.findViewById(R.id.imgAddInputOptions);
        ImageView imgAddGroup = (ImageView) v.findViewById(R.id.imgAddSubGroup);
        final Button btnCancel = (Button) v.findViewById(R.id.btnCancel);
        Button btnSave = (Button) v.findViewById(R.id.btnSave);
        rlAddSubGroup = (RelativeLayout) v.findViewById(R.id.rlAddSubgroup);
        Button btnAddSubGrouping = (Button) v.findViewById(R.id.btnAddSubGrouping);
        ImageView imgCloseSubGrouping = (ImageView) v.findViewById(R.id.imgCloseSubGrouping);

        RecyclerView rvInputOptions = (RecyclerView) v.findViewById(R.id.rvInputOptions);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        rvInputOptions.setLayoutManager(mLayoutManager);
        rvInputOptions.setItemAnimator(new DefaultItemAnimator());

        RecyclerView rvGroupsList = (RecyclerView) v.findViewById(R.id.rvGroupsList);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity().getApplicationContext());
        rvGroupsList.setLayoutManager(mLayoutManager1);
        rvGroupsList.setItemAnimator(new DefaultItemAnimator());

        txtHeader.setText("Add Fields");

        /**Adapter*/
        dataArrayList = new ArrayList<>();
        final DataArrayAdapter dataArrayAdapter = new DataArrayAdapter(getActivity(), dataArrayList);
        rvInputOptions.setAdapter(dataArrayAdapter);

        List<String> inputTypeList = new ArrayList<>();
        inputTypeList.add("-- SELECT INPUT TYPE --");
        inputTypeList.addAll(mDatabaseHelper.getInputType());
        spnInputType.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_list_item, inputTypeList) {
            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                final View v = super.getView(position, convertView, parent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((TextView) v).setTextColor(Color.WHITE);
                    }
                }, 50);
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                final View v = super.getDropDownView(position, convertView, parent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((TextView) v).setTextColor(Color.WHITE);
                        ((TextView) v).setBackgroundColor(getResources().getColor(R.color.darkest_gray));
                    }
                }, 50);
                return v;
            }
        });

        /**Setting up group name spinner list*/
        mGroupsModelDropdownList.addAll(mDatabaseHelper.getGroupNames(brandCode, campaignCode, eventCode));
        mAddUpdateFieldSubGroupNameAdapter = new AddUpdateFieldSubGroupNameAdapter(getActivity(),
                android.R.layout.simple_list_item_1, mGroupsModelDropdownList);
        spnSubGroup.setAdapter(mAddUpdateFieldSubGroupNameAdapter);

        final List<GroupsModel> groupModelList = new ArrayList<>();
        groupModelList.addAll(mDatabaseHelper.getAllGroupsByBrandCampaignEvent(brandCode, campaignCode, eventCode));
        final SubGroupingAdapter mSubGroupingAdapter = new SubGroupingAdapter(getActivity(), groupModelList, mGroupsModelDropdownList, mAddUpdateFieldSubGroupNameAdapter);
        rvGroupsList.setAdapter(mSubGroupingAdapter);

        /**Listeners*/
        llAddFieldRootView.setOnTouchListener(this);
        rlFieldDetailsContainer.setOnTouchListener(this);
        spnInputType.setOnTouchListener(this);
        spnSubGroup.setOnTouchListener(this);
        btnAddSubGrouping.setOnTouchListener(this);
        imgAddGroup.setOnTouchListener(this);
        imgCloseSubGrouping.setOnTouchListener(this);

        spnSubGroup.setOnItemSelectedListener(spinnerListener);
        spnInputType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> adapterView, View view,
                                       final int i, long l) {
//                ((TextView) view).setTextColor(getActivity().getResources().getColor(R.color.midnight_blue));
                Log.e(TAG, "run: DataArrayListSize: " + dataArrayList.size());

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (adapterView.getItemAtPosition(i).equals("Radio Button") ||
                                adapterView.getItemAtPosition(i).equals("Dropdown")) {

                            if (dataArrayList.size() == 0) {
                                DataArray dataArray = new DataArray();
                                dataArray.setTitle("");
                                dataArray.setName("");
                                dataArray.setValue("");
                                dataArray.setIsActive("1");
                                dataArray.setDateTime(Utils.getDateTime());
                                dataArrayList.add(dataArray);
                                dataArrayAdapter.notifyDataSetChanged();
                            }
                            llInputOptions.setVisibility(View.VISIBLE);
                        } else {
                            llInputOptions.setVisibility(View.GONE);
                            dataArrayList.clear();
                            dataArrayAdapter.notifyDataSetChanged();

                        }
                    }
                }, 300);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        imgAddGroup.setOnClickListener(new View.OnClickListener()

        {

            @Override
            public void onClick(View v) {
                final String groupName = edtGroupName.getText().toString();
                final String title = groupName;
                final String name = groupName.replace(" ", "_").toLowerCase();

                if (name.equals("personal_information") ||
                        name.equals("contact_information") ||
                        name.equals("login_information") ||
                        name.equals("other_information")) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Group name exist!", Constants.DIALOG_ACTION_NORMAL);

                } else if (mDatabaseHelper.checkIfGroupIsExist(brandCode, campaignCode, eventCode, name)) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Group name exist!", Constants.DIALOG_ACTION_NORMAL);

                } else {
                    if (!groupName.isEmpty() && !groupName.equals("")) {
                        Log.e(TAG, "onClick: BrandCode: " + brandCode);
                        Log.e(TAG, "onClick: CampaignCode: " + campaignCode);

                        int maxGroupId = mDatabaseHelper.getMaxGroupId();
                        ++maxGroupId;
                        GroupsModel groupsModel = new GroupsModel();
                        groupsModel.setId(String.valueOf(maxGroupId));
                        groupsModel.setTitle(title);
                        groupsModel.setName(name);
                        groupsModel.setDescription("");
                        groupsModel.setBrandCode(brandCode);
                        groupsModel.setCampaignCode(campaignCode);
                        groupsModel.setEventCode(eventCode);
                        groupsModel.setIsActive("1");
                        groupsModel.setUserId("");
                        groupsModel.setDateTime(Utils.getDateTime());
                        groupsModel.setStatus("1");

                        if (NetworkUtils.isNetworkConnected(getActivity())) {
                            Log.e(TAG, "onClick: ADD GROUPS API" + " Connected");
                            mMaintenancePresenter.addFormGroups(groupsModel, edtGroupName, mGroupsModelDropdownList, mAddUpdateFieldSubGroupNameAdapter,
                                    groupModelList, mSubGroupingAdapter);
                        } else {
                            mMaintenancePresenter.showAlertDialog("Confirm", "New Sub-group is successfully added.", Constants.DIALOG_ACTION_NORMAL);
                            edtGroupName.setText("");
                            mDatabaseHelper.createGroup(groupsModel);
                            mGroupsModelDropdownList.add(groupsModel);
                            mGroupsModelDropdownList.add(groupsModel);
                            groupModelList.add(groupsModel);
                            mSubGroupingAdapter.notifyDataSetChanged();
                        }
                    } else {
                        mMaintenancePresenter.showAlertDialog("Alert", "Please fill-out required field.", Constants.DIALOG_ACTION_NORMAL);
                    }
                }
            }
        });

        btnAddSubGrouping.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                rlAddSubGroup.setVisibility(View.VISIBLE);
            }
        });

        imgCloseSubGrouping.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                rlAddSubGroup.setVisibility(View.GONE);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                String fieldTitle = edtFieldName.getText().toString();
                String fieldName = fieldTitle.replace(" ", "_").toLowerCase();

                Log.e(TAG, "onClick: " + dataArrayList.size());
                if (spnInputType.getSelectedItemPosition() == 0) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please fill out all required fields.", Constants.DIALOG_ACTION_NORMAL);

                } else if (fieldName.isEmpty() && fieldTitle.isEmpty()) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please fill out all required fields.", Constants.DIALOG_ACTION_NORMAL);

                } else if (dataArrayList.size() == 0 && spnInputType.getSelectedItemPosition() != 1) {
                    Log.e(TAG, "onClick: " + dataArrayList.size() + " " + spnInputType.getSelectedItemPosition());
                    mMaintenancePresenter.showAlertDialog("Alert", spnInputType.getSelectedItem().toString() + " requires title and value", Constants.DIALOG_ACTION_NORMAL);

                } else if (mDatabaseHelper.getCustomFieldsCount(brandCode, campaignCode) >= 20) {
                    mMaintenancePresenter.setAddFieldButtonVisibility(View.GONE);
                    mMaintenancePresenter.showAlertDialog("Alert", "Custom field exceeded the maximum limit.", Constants.DIALOG_ACTION_NORMAL);
                } else {
                    String description = edtDescription.getText().toString();
                    String typeId = String.valueOf(mDatabaseHelper.getTypeId(spnInputType.getSelectedItem().toString()));
//                    String groupId = mDatabaseHelper.getGroupId(spnSubGroup.getSelectedItem().toString());
                    String groupId = mAddUpdateFieldSubGroupNameAdapter.getItem(spnSubGroup.getSelectedItemPosition()).getId();
                    int maxInputId = mDatabaseHelper.getMaxFieldId();
                    maxInputId++;
                    int maxInputAssignedId = mDatabaseHelper.getMaxAssignedFieldId();
                    maxInputAssignedId++;

                    InputModel inputModel = new InputModel();
                    inputModel.setId(String.valueOf(maxInputId));
                    inputModel.setTypeId(typeId);
                    inputModel.setTitle(fieldTitle);
                    inputModel.setName(fieldName);
                    inputModel.setDescription(description);
                    inputModel.setGroupId(groupId);
                    inputModel.setIsRequired((chIsRequired.isChecked()) ? "1" : "0");
                    inputModel.setIsActive("1");
                    inputModel.setDateTime(Utils.getDateTime());
                    inputModel.setCategory("2");
                    inputModel.setStatus("1");
                    inputModel.setIsMandatory("0");
                    inputModel.setDbmStatus("1");

                    AssignFieldModel assignFieldModel = new AssignFieldModel();
                    assignFieldModel.setId(String.valueOf(maxInputAssignedId));
                    assignFieldModel.setCampaignCode(campaignCode);
                    assignFieldModel.setBrandCode(brandCode);
                    assignFieldModel.setEventCode(eventCode);
                    assignFieldModel.setInputId(String.valueOf(maxInputId));
                    assignFieldModel.setUserId("");
                    assignFieldModel.setIsActive("1");
                    assignFieldModel.setDateTime(Utils.getDateTime());
                    assignFieldModel.setSort(maxInputAssignedId);
                    assignFieldModel.setStatus("1");

                    FieldsModel fieldsModel = new FieldsModel();
                    fieldsModel.setA_id(assignFieldModel.getId());
                    fieldsModel.setCampaignCode(assignFieldModel.getCampaignCode());
                    fieldsModel.setBrandCode(assignFieldModel.getBrandCode());
                    fieldsModel.setEventCode(assignFieldModel.getEventCode());
                    fieldsModel.setInputId(assignFieldModel.getInputId());
                    fieldsModel.setUserId(assignFieldModel.getUserId());
                    fieldsModel.setA_isActive(assignFieldModel.getIsActive());
                    fieldsModel.setA_dateTime(assignFieldModel.getDateTime());
                    fieldsModel.setSort(assignFieldModel.getSort());
                    fieldsModel.setA_status(assignFieldModel.getStatus());
                    fieldsModel.setB_id(inputModel.getId());
                    fieldsModel.setTypeId(inputModel.getTypeId());
                    fieldsModel.setTitle(inputModel.getTitle());
                    fieldsModel.setName(inputModel.getName());
                    fieldsModel.setDescription(inputModel.getDescription());
                    fieldsModel.setGroupId(inputModel.getGroupId());
                    fieldsModel.setIsRequired(inputModel.getIsRequired());
                    fieldsModel.setB_isActive(inputModel.getIsActive());
                    fieldsModel.setB_dateTime(inputModel.getDateTime());
                    fieldsModel.setCategory(inputModel.getCategory());
                    fieldsModel.setB_status(inputModel.getStatus());
                    fieldsModel.setIsMandatory(inputModel.getIsMandatory());
                    fieldsModel.setDbmStatus(inputModel.getDbmStatus());

                    Log.e(TAG, "onClick: Input field title: " + fieldTitle);
                    Log.e(TAG, "onClick: Input field name: " + fieldName);
                    Log.e(TAG, "onClick: Input type id: " + typeId + " type title: " + spnInputType.getSelectedItem().toString());
                    Log.e(TAG, "onClick: Group id: " + groupId + " group title: " + spnSubGroup.getSelectedItem().toString());

                    if (mDatabaseHelper.checkFieldNameIfExist(fieldName, brandCode, campaignCode, eventCode)) {
                        Log.e(TAG, "onClick: " + "Field name exist!");
                        mMaintenancePresenter.showAlertDialog("Alert", "Field already exist", Constants.DIALOG_ACTION_NORMAL);
                    } else {

                        mMaintenancePresenter.showAddFormFieldSavePrompt("Confirm", "Are you sure you want to Save?",
                                dataArrayList, inputModel, assignFieldModel, fieldsModel);
                    }
                }
            }
        });

        mAddFieldDialog = alertBuilder.create();
        mAddFieldDialog.getWindow().

                setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAddFieldDialog;
    }

    AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//            ((TextView) view).setTextColor(getActivity().getResources().getColor(R.color.midnight_blue));
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.e(TAG, "onSaveInstanceState: " + rlAddSubGroup.getVisibility());
        outState.putBoolean(Constants.STATE_DIALOG_SUB_GROUP_VISIBILITY, (rlAddSubGroup.getVisibility() == View.VISIBLE));
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.e(TAG, "onViewStateRestored: " + savedInstanceState.getBoolean(Constants.STATE_DIALOG_SUB_GROUP_VISIBILITY));
        if (savedInstanceState != null) {
            boolean subGroupLayoutVisibility = savedInstanceState.getBoolean(Constants.STATE_DIALOG_SUB_GROUP_VISIBILITY);
            if (subGroupLayoutVisibility) {
                rlAddSubGroup.setVisibility(View.VISIBLE);
            } else {
                rlAddSubGroup.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v instanceof EditText) {
            Log.e(TAG, "onTouch: " + "View is an Edittext");
        } else {
            mMaintenancePresenter.hideKeyboardInFragment(getActivity(), v.getWindowToken());
        }
        return false;
    }
}
