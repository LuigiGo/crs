package com.unilab.customerregistration.utilities.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.network.NetworkUtils;
import com.unilab.customerregistration.utilities.service.MainService;

public class MainReceiver extends BroadcastReceiver {

    public final String TAG = this.getClass().getSimpleName();
    public static boolean isFirstConnected = false;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive: " + intent.getAction());

        switch (intent.getAction()) {
            case ConnectivityManager.CONNECTIVITY_ACTION:
                boolean isConnected = NetworkUtils.isNetworkConnected(context);
                if (isConnected) {
                    if (!isFirstConnected) {
                        isFirstConnected = true;
                        context.startService(new Intent(context, MainService.class)
                                .setAction(Constants.SERVICE_CHECK_REG_UPDATES));
                    }
                } else {
                    isFirstConnected = false;
                }

                Log.e(TAG, "onReceive: " + isFirstConnected);
                break;

            case Constants.RECEIVER_SERVICE_WAS_KILLED:
                Log.e(TAG, "JobOnReceive: " + Constants.RECEIVER_SERVICE_WAS_KILLED);
                context.startService(new Intent(context, MainService.class)
                        .setAction(Constants.SERVICE_CHECK_REG_UPDATES_LOLLIPOP));
                break;
        }
    }

}
