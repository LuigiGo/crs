package com.unilab.customerregistration.utilities.service;

import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.unilab.customerregistration.features.registration.model.RegistrationModel;
import com.unilab.customerregistration.features.registration.model.asynctask.AsyncSynchronizeRegistrationData;
import com.unilab.customerregistration.utilities.checkers.CheckerUtils;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;

import java.util.List;

public class MainService extends Service {

    public final String TAG = this.getClass().getSimpleName();
    private DatabaseHelper mDatabaseHelper;
    String brandCode, campaignCode, eventCode;
    private JobScheduler mJobScheduler;
    private JobInfo.Builder mJobInfoBuilder;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public MainService() {
        Log.e(TAG, "MainService: ");

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mDatabaseHelper = new DatabaseHelper(getApplicationContext());
        SharedPrefHelper.init(getApplicationContext());

        switch (intent.getAction()) {
            case Constants.SERVICE_CHECK_REG_UPDATES:
                syncRegistrationData();
                break;

            case Constants.SERVICE_CHECK_REG_UPDATES_LOLLIPOP:
                Log.e(TAG, "onStartCommand: " + "LOLLIPOP");
                if (CheckerUtils.checkVersionCodeIfNougatOrAbove()) {
                    startingJobScheduler();
                }
                break;

        }

        return START_NOT_STICKY;
    }

    private synchronized void syncRegistrationData() {
        Log.e(TAG, "syncRegistrationData: ");
        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        if (!brandCode.isEmpty() && !campaignCode.isEmpty() && !eventCode.isEmpty()) {
            List<RegistrationModel> registrationModelList = mDatabaseHelper.getRegistrationUpdates(brandCode, campaignCode, eventCode);
            new AsyncSynchronizeRegistrationData(this, registrationModelList).execute();

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void startingJobScheduler() {
        Log.e(TAG, "startingJobScheduler: ");
        mJobScheduler = (JobScheduler)
                getSystemService(Context.JOB_SCHEDULER_SERVICE);

        mJobInfoBuilder = new JobInfo.Builder(1,
                new ComponentName(getPackageName(),
                        JobSchedulerService.class.getName()));
        mJobInfoBuilder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        mJobInfoBuilder.setPersisted(true);
        mJobInfoBuilder.setMinimumLatency(3000);
        mJobInfoBuilder.setOverrideDeadline(3000);

        if (mJobScheduler.schedule(mJobInfoBuilder.build()) <= 0) {
            Log.e(TAG, "settingUpJobScheduler: " + "Job scheduler is failed to restart");
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.e(TAG, "onTaskRemoved: ");
        getApplicationContext().sendBroadcast(new Intent(Constants.RECEIVER_SERVICE_WAS_KILLED));
    }
}
