package com.unilab.customerregistration.utilities.service;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.unilab.customerregistration.features.registration.model.RegistrationModel;
import com.unilab.customerregistration.features.registration.model.asynctask.AsyncSynchronizeRegistrationData;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;

import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class JobSchedulerService extends JobService {

    private final String TAG = this.getClass().getSimpleName();
    JobScheduler mJobScheduler;
    JobInfo.Builder mJobInfoBuilder;

    String brandCode, campaignCode, eventCode;
    private DatabaseHelper mDatabaseHelper;

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.e(TAG, "onStartJob: ");
        SharedPrefHelper.init(getApplicationContext());
        mDatabaseHelper = new DatabaseHelper(getApplicationContext());

        startingJobScheduler();
        syncRegistrationData();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.e(TAG, "onStopJob: ");
        return false;
    }

    private void startingJobScheduler() {
        Log.e(TAG, "startingJobScheduler: ");
        mJobScheduler = (JobScheduler)
                getSystemService(Context.JOB_SCHEDULER_SERVICE);

        mJobInfoBuilder = new JobInfo.Builder(1,
                new ComponentName(getPackageName(),
                        JobSchedulerService.class.getName()));
        mJobInfoBuilder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        mJobInfoBuilder.setPersisted(true);
        mJobInfoBuilder.setMinimumLatency(3000);
        mJobInfoBuilder.setOverrideDeadline(3000);

        if (mJobScheduler.schedule(mJobInfoBuilder.build()) <= 0) {
            Log.e(TAG, "settingUpJobScheduler: " + "Job scheduler is failed to restart");
        }
    }

    private synchronized void syncRegistrationData() {
        Log.e(TAG, "syncRegistrationData: ");
        brandCode = SharedPrefHelper.getInstance().getBrandCodeFromShared();
        campaignCode = SharedPrefHelper.getInstance().getCampaignCodeFromShared();
        eventCode = SharedPrefHelper.getInstance().getEventCodeFromShared();

        if (!brandCode.isEmpty() && !campaignCode.isEmpty() && !eventCode.isEmpty()) {
            List<RegistrationModel> registrationModelList = mDatabaseHelper.getRegistrationUpdates(brandCode, campaignCode, eventCode);
            if (registrationModelList.size() > 0) {
                new AsyncSynchronizeRegistrationData(this, registrationModelList).execute();
            }
        }
    }
}
