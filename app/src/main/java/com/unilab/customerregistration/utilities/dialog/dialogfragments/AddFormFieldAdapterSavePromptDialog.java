package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.DataArray;
import com.unilab.customerregistration.features.maintenance.model.FieldsModel;
import com.unilab.customerregistration.features.maintenance.model.InputModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.features.maintenance.view.adapters.arrayadapter.AddUpdateFieldSubGroupNameAdapter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.constants.DatabaseConstants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.network.NetworkUtils;
import com.unilab.customerregistration.utilities.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class AddFormFieldAdapterSavePromptDialog extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static AddFormFieldAdapterSavePromptDialog frag;
    AlertDialog mAlertSavePromptDialog;
    private DatabaseHelper mDatabaseHelper;
    private MaintenancePresenter mMaintenancePresenter;

    public static AddFormFieldAdapterSavePromptDialog
    newInstance(String title, String message, final EditText edtFieldName,
                final List<DataArray> dataArrayList, final Spinner spnInputType, final Spinner spnSubGroup,
                AddUpdateFieldSubGroupNameAdapter addUpdateFieldSubGroupNameAdapter,
                final EditText edtDescription, final FieldsModel fieldsModel, final CheckBox chRequired) {

        frag = new AddFormFieldAdapterSavePromptDialog();

        String fieldTitle = edtFieldName.getText().toString();
        ArrayList<DataArray> dataArrayArrayList = new ArrayList<>();
        dataArrayArrayList.addAll(dataArrayList);
        int inputTypePosition = spnInputType.getSelectedItemPosition();
        String inputTypeName = spnInputType.getSelectedItem().toString();
        String description = edtDescription.getText().toString();
        String groupTitle = spnSubGroup.getSelectedItem().toString();
        String groupId = addUpdateFieldSubGroupNameAdapter.getItem(spnSubGroup.getSelectedItemPosition()).getId();

        boolean requiredStatus = chRequired.isChecked();

        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        args.putString("field_title", fieldTitle);
        args.putParcelableArrayList("data_array_list", dataArrayArrayList);
        args.putString("description", description);
        args.putInt("input_type_position", inputTypePosition);
        args.putString("input_type_name", inputTypeName);
        args.putString("group_title", groupTitle);
        args.putString("group_id", groupId);
        args.putBoolean("required_status", requiredStatus);
        args.putParcelable("fields_model", fieldsModel);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mDatabaseHelper = new DatabaseHelper(getActivity());

        String title = getArguments().getString("title");
        String message = getArguments().getString("message");
        final String fieldTitle = getArguments().getString("field_title");
        final ArrayList<DataArray> dataArrayList = getArguments().getParcelableArrayList("data_array_list");
        final String description = getArguments().getString("description");
        final int inputTypePosition = getArguments().getInt("input_type_position");
        final String inputTypeName = getArguments().getString("input_type_name");
//        final String groupTitle = getArguments().getString("group_title");
        final String groupId = getArguments().getString("group_id");
        final boolean requiredStatus = getArguments().getBoolean("required_status");
        final FieldsModel fieldsModel = getArguments().getParcelable("fields_model");

        mMaintenancePresenter = (MaintenancePresenter) getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_save_prompt, null);
        builder.setView(v);

        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        Button btnYes = (Button) v.findViewById(R.id.btnYes);
        Button btnNo = (Button) v.findViewById(R.id.btnNo);

        txtTitle.setText(title);
        txtMessage.setText(message);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fieldName = fieldTitle.replace(" ", "_").toLowerCase();

                Log.e(TAG, "onClick: DataArrayListSize: " + dataArrayList.size());
                Log.e(TAG, "onClick: SpinnerItemPosition: " + inputTypePosition);
                Log.e(TAG, "onClick: SpinnerItemName: " + inputTypeName);

                if (fieldName.isEmpty() && fieldTitle.isEmpty()) {
                    mMaintenancePresenter.showAlertDialog("Alert", "Please fill up required fields.", Constants.DIALOG_ACTION_NORMAL);

                } else if (dataArrayList.size() == 0 && inputTypePosition != 0) {
                    mMaintenancePresenter.showAlertDialog("Alert", inputTypeName + " requires title and value", Constants.DIALOG_ACTION_NORMAL);

                } else {
                    String typeId = String.valueOf(mDatabaseHelper.getTypeId(inputTypeName));
//                    String groupId = mDatabaseHelper.getGroupId(groupTitle);
                    Log.e(TAG, "onClick: GG:  " + groupId);

                    InputModel inputModel = new InputModel();
                    inputModel.setId(String.valueOf(fieldsModel.getB_id()));
                    inputModel.setTypeId(typeId);
                    inputModel.setTitle(fieldTitle);
                    inputModel.setName(fieldName);
                    inputModel.setDescription(description);
                    inputModel.setGroupId(groupId);
                    inputModel.setIsRequired((requiredStatus) ? "1" : "0");
                    inputModel.setIsActive(fieldsModel.getB_isActive());
                    inputModel.setDateTime(Utils.getDateTime());
                    inputModel.setCategory(fieldsModel.getCategory());
                    inputModel.setStatus(fieldsModel.getA_status());
                    inputModel.setEditStatus("1");

                    fieldsModel.setTitle(fieldTitle);
                    fieldsModel.setName(fieldName);
                    fieldsModel.setTypeId(typeId);
                    fieldsModel.setDescription(description);
                    fieldsModel.setGroupId(groupId);
                    fieldsModel.setIsRequired((requiredStatus) ? "1" : "0");
                    fieldsModel.setA_dateTime(Utils.getDateTime());
                    fieldsModel.setB_dateTime(Utils.getDateTime());
                    fieldsModel.setA_status(fieldsModel.getA_status());
                    fieldsModel.setDataArrayList(dataArrayList);

                    mDatabaseHelper.clearExistingDataArray(DatabaseConstants.TBL_TEXTBOX, fieldsModel.getB_id());
                    mDatabaseHelper.clearExistingDataArray(DatabaseConstants.TBL_CHECKBOX, fieldsModel.getB_id());
                    mDatabaseHelper.clearExistingDataArray(DatabaseConstants.TBL_RADIO, fieldsModel.getB_id());
                    mDatabaseHelper.clearExistingDataArray(DatabaseConstants.TBL_SELECT, fieldsModel.getB_id());

                    if (NetworkUtils.isNetworkConnected(getActivity())) {
                        Log.e(TAG, "onClick: " + "Connected");
                        mMaintenancePresenter.addFormField(fieldsModel, null, null, null, Constants.DIALOG_ACTION_UPDATE_FORM_FIELD);
                        dismiss();

                    } else {
                        Log.e(TAG, "onClick: " + "Not connected");
                        if (dataArrayList.size() > 0) {
                            for (DataArray dataArray : dataArrayList) {
                                int maxDataArrayId = mDatabaseHelper.getMaxDataArrayId(typeId);
                                maxDataArrayId++;
                                dataArray.setId(String.valueOf(maxDataArrayId));
                                dataArray.setInputId(fieldsModel.getB_id());
                                mDatabaseHelper.createDataArray(dataArray, typeId);

                            }
                        } else {
                            int maxDataArrayId = mDatabaseHelper.getMaxDataArrayId(typeId);
                            maxDataArrayId++;
                            DataArray dataArray = new DataArray();
                            dataArray.setId(String.valueOf(maxDataArrayId));
                            dataArray.setInputId(fieldsModel.getB_id());
                            dataArray.setTitle(fieldTitle);
                            dataArray.setName(fieldName);
                            dataArray.setValue("");
                            dataArray.setIsActive("1");
                            dataArray.setDateTime(Utils.getDateTime());
                            mDatabaseHelper.createDataArray(dataArray, typeId);
                        }

                        mDatabaseHelper.updateFieldById(inputModel);
//                        mMaintenancePresenter.refreshFormAdapters();
                        mMaintenancePresenter.refreshCustomFieldsAdapter();
                        dismiss();
                    }
                }
                dismiss();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        mAlertSavePromptDialog = builder.create();
        mAlertSavePromptDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAlertSavePromptDialog;
    }
}
