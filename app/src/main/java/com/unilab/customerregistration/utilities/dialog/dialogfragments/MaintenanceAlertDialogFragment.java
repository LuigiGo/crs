package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.constants.Constants;

public class MaintenanceAlertDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static MaintenanceAlertDialogFragment frag;
    AlertDialog mAlertInfoDialog;
    private MaintenancePresenter mMaintenancePresenter;

    public static MaintenanceAlertDialogFragment newInstance(String title, String message, String actionType) {
        frag = new MaintenanceAlertDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        args.putString("actionType", actionType);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String message = getArguments().getString("message");
        final String actionType = getArguments().getString("actionType");

        mMaintenancePresenter = (MaintenancePresenter) getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        getActivity().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_information, null);
        builder.setView(v);
        builder.setCancelable(false);

        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        Button btnOk = (Button) v.findViewById(R.id.btnOk);

        txtTitle.setText(title);
        txtMessage.setText(message);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                switch (actionType) {
                    case Constants.DIALOG_ACTION_SAVE_BRAND_FRAGMENT:
                        mMaintenancePresenter.refreshFormAdapters();
                        mMaintenancePresenter.showDesignPage();
                        break;

                    case Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT:
                        mMaintenancePresenter.saveDesign();
                        break;

                    case Constants.DIALOG_ACTION_SAVE_FORM_FRAGMENT:
                        mMaintenancePresenter.synchronizeForm(false);
                        break;

                    case Constants.DIALOG_RESET_DESIGN_SETTINGS:
                        mMaintenancePresenter.refreshDesign();
                        break;

                    case Constants.DIALOG_ACTION_NO_DESIGN_CHANGES:
                        mMaintenancePresenter.showFormPage();
                        break;

                    case Constants.DIALOG_ACTION_NO_FORM_CHANGES:
                        mMaintenancePresenter.showPreviewPage();
                        break;

                    default:

                        break;
                }
            }
        });

        mAlertInfoDialog = builder.create();
        mAlertInfoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAlertInfoDialog;
    }
}
