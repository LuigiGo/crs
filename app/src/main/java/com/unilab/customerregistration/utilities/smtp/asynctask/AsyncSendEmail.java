package com.unilab.customerregistration.utilities.smtp.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.dialog.DialogHelper;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;
import com.unilab.customerregistration.utilities.smtp.GMailSender;

public class AsyncSendEmail extends AsyncTask<Void, Void, Boolean> {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private String mRecipient;
    private String mGeneratedUrl;
    private MaintenancePresenter mMaintenancePresenter;

    public AsyncSendEmail(Context context, String recipient, String generatedUrl) {
        Log.e(TAG, "AsyncSendEmail: Recipient: " + recipient);
        DialogHelper.init(context);
        SharedPrefHelper.init(context);

        mContext = context;
        mRecipient = recipient;

        mMaintenancePresenter = (MaintenancePresenter) context;

//        mGeneratedUrl = SharedPrefHelper.getInstance().getGeneratedUrlFromShared();
        mGeneratedUrl = generatedUrl;
        Log.e(TAG, "AsyncSendEmail: GeneratedURL: " + mGeneratedUrl);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        DialogHelper.getInstance().hideProgressDialog(mContext);
        DialogHelper.getInstance().showProgressDialog(mContext, "Sending URL to email");
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        return sendEmail();
    }

    @Override
    protected void onPostExecute(Boolean sent) {
        super.onPostExecute(sent);
        DialogHelper.getInstance().hideProgressDialog(mContext);
        if (sent) {
            mMaintenancePresenter.showAlertDialog("Confirm", "Email successfully sent to " +
                    mRecipient, Constants.DIALOG_ACTION_NORMAL);

        } else {
            mMaintenancePresenter.showAlertDialog("Confirm", "Email not sent to " +
                    mRecipient, Constants.DIALOG_ACTION_NORMAL);
        }
    }

    private boolean sendEmail() {
        try {

            String emailSubject = mContext.getResources().getString(R.string.email_sending_subject);
            String emailBody = "Dear Admin,\n" +
                    "\n" +
                    "To access your newly created registration form, please click on the link below. \n" +
                    "\n" +
                    "Link: " + mGeneratedUrl + "\n" +
                    "Thank You.\n" +
                    "Note: This is a system-generated email from Customer Registration Management System.";

            GMailSender sender = new GMailSender(Constants.SMTP_EMAIL_ADDRESS, Constants.SMTP_PASSWORD);
            sender.sendMail(emailSubject, emailBody, Constants.SMTP_EMAIL_ADDRESS, mRecipient);

        } catch (Exception e) {
            Log.e(TAG, "sendEmail-Exception: " + e.getMessage());
            mMaintenancePresenter.showAlertDialog("Alert",
                    (e.getMessage() == null) ? "Please check your internet connection." : e.getMessage(),
                    Constants.DIALOG_ACTION_NORMAL);
            DialogHelper.getInstance().hideProgressDialog(mContext);
            return false;
        }

        return true;
    }
}
