package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorChangedListener;
import com.flask.colorpicker.OnColorSelectedListener;
import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;

import de.hdodenhof.circleimageview.CircleImageView;

public class ColorPickerAlertDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static ColorPickerAlertDialogFragment frag;
    AlertDialog mAlertColorPicker;
    private MaintenancePresenter mMaintenancePresenter;

    public static ColorPickerAlertDialogFragment newInstance(int type) {
        frag = new ColorPickerAlertDialogFragment();
        Bundle args = new Bundle();
        args.putInt("type", type);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final int type = getArguments().getInt("type");

        mMaintenancePresenter = (MaintenancePresenter) getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity().getApplicationContext())
                .inflate(R.layout.dialog_color_picker, null);
        builder.setView(v);

        CircleImageView imgColorTransparent = (CircleImageView) v.findViewById(R.id.imgColorTransparent);
        CircleImageView imgColorWhite = (CircleImageView) v.findViewById(R.id.imgColorWhite);
        CircleImageView imgColorBlack = (CircleImageView) v.findViewById(R.id.imgColorBlack);

        final EditText edtInputColor = (EditText) v.findViewById(R.id.edtInputColor);
        edtInputColor.setSelection(edtInputColor.getText().toString().length());
        Button btnSetColor = (Button) v.findViewById(R.id.btnSetColor);

        imgColorTransparent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String hexColor = "transparent";
                int color = Color.parseColor("#00FFFFFF");
                mMaintenancePresenter.setDesignColors(type, color, hexColor);
                dismiss();
            }
        });

        imgColorWhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String hexColor = "#FFFFFF";
                int color = Color.parseColor(hexColor);
                mMaintenancePresenter.setDesignColors(type, color, hexColor);
                dismiss();
            }
        });

        imgColorBlack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String hexColor = "#000000";
                int color = Color.parseColor(hexColor);
                mMaintenancePresenter.setDesignColors(type, color, hexColor);
                dismiss();
            }
        });

        btnSetColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String hexColor = edtInputColor.getText().toString().substring(0, 7);
                Log.e(TAG, "onClick: " + hexColor);
                try {
                    int color = Color.parseColor(hexColor);
                    mMaintenancePresenter.setDesignColors(type, color, hexColor);
                    dismiss();
                } catch (Exception e) {
                    Log.e(TAG, "onClick: " + e.getMessage());
                }


            }
        });

        ColorPickerView colorPickerView = (ColorPickerView) v.findViewById(R.id.colorPickerView);
        colorPickerView.addOnColorChangedListener(new OnColorChangedListener() {
            @Override
            public void onColorChanged(int selectedColor) {
                String selectedHexColor = Integer.toHexString(selectedColor);
                String hexColor = "#" + selectedHexColor.substring(2);
                edtInputColor.setText(hexColor);
            }
        });

        colorPickerView.addOnColorSelectedListener(new OnColorSelectedListener() {
            @Override
            public void onColorSelected(int selectedColor) {
                String selectedHexColor = Integer.toHexString(selectedColor);
                String hexColor = "#" + selectedHexColor.substring(2);
                Log.e(TAG, "onColorSelected: " + hexColor);
                int color = Color.parseColor(hexColor);
                mMaintenancePresenter.setDesignColors(type, color, hexColor);
                dismiss();
            }
        });

        edtInputColor.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    edtInputColor.setText("#");
                    edtInputColor.setSelection(1);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mAlertColorPicker = builder.create();
        mAlertColorPicker.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAlertColorPicker;
    }

    @Override
    public void onResume() {
        super.onResume();
        int orientation = getActivity().getResources().getConfiguration().orientation;
        switch (orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                int width = getResources().getDimensionPixelSize(R.dimen.w_dialog_color_picker);
                int height = getResources().getDimensionPixelSize(R.dimen.h_dialog_fonts);
                getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
                break;
        }
    }
}
