package com.unilab.customerregistration.utilities.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.unilab.customerregistration.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class NotificationHelper {

    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private NotificationCompat.Builder mNotificationBuilder;
    private NotificationManager mNotificationManager;
    private RemoteViews normalView;
    Notification mNotification;

    public NotificationHelper(Context context) {
        mContext = context;
        mNotificationBuilder = new NotificationCompat.Builder(context);
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        normalView = new RemoteViews(context.getPackageName(), R.layout.notification_status_bar);
    }

    public void showNotification(String message) {
        mNotificationBuilder.setSmallIcon(R.drawable.ic_crs_logo);

        String time = new SimpleDateFormat("MM-dd-yyyy HH:mm").format(Calendar.getInstance().getTime());


        normalView.setTextViewText(R.id.txtTitle, "Registration updates");
        normalView.setTextViewText(R.id.txtTimeDate, time);
        normalView.setTextViewText(R.id.txtContent, message);

        mNotification = mNotificationBuilder.build();
        mNotification.contentView = normalView;

        mNotificationManager.notify((int) System.currentTimeMillis(), mNotification);
    }
}
