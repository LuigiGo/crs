package com.unilab.customerregistration.utilities.checkers;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckerUtils {

    private static Pattern pattern;
    private static Matcher matcher;

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static boolean validateEmail(final String hex) {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(hex);
        return matcher.matches();

    }

    public static String checkMimeType(Context context, Uri uri) {
        String mimeType;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
            Log.e("CheckerUtils", "checkMimeType: 0: " + mimeType);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());

            Log.e("CheckerUtils", "checkMimeType: 1: " + mimeType + "-" + fileExtension);
        }
        return mimeType;
    }

    public static boolean checkVersionCodeIfNougatOrAbove() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N);
    }
}
