package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.maintenance.model.asynctask.AsyncSaveSettingUpdates;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.database.DatabaseHelper;
import com.unilab.customerregistration.utilities.network.NetworkUtils;

public class DesignSavePromptDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static DesignSavePromptDialogFragment frag;
    AlertDialog mAlertSavePromptDialog;
    private DatabaseHelper mDatabaseHelper;
    private MaintenancePresenter mMaintenancePresenter;

    public static DesignSavePromptDialogFragment
    newInstance(String title, String message, String actionType, SettingsModel settingsModel,
                boolean hasSelectedMobileLogo, boolean hasSelectedWebLogo,
                boolean isCopyingSettings) {

        frag = new DesignSavePromptDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        args.putString("action_type", actionType);
        args.putParcelable("settings_model", settingsModel);
        args.putBoolean("hasSelectedMobileLogo", hasSelectedMobileLogo);
        args.putBoolean("hasSelectedWebLogo", hasSelectedWebLogo);
        args.putBoolean("isCopyingSettings", isCopyingSettings);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String message = getArguments().getString("message");
        final String actionType = getArguments().getString("action_type");
        final SettingsModel mSettingModel = getArguments().getParcelable("settings_model");
        final boolean[] hasSelectedMobileLogo = {getArguments().getBoolean("hasSelectedMobileLogo")};
        final boolean[] hasSelectedWebLogo = {getArguments().getBoolean("hasSelectedWebLogo")};
        final boolean isCopyingSettings = getArguments().getBoolean("isCopyingSettings");

        mMaintenancePresenter = (MaintenancePresenter) getActivity();
        mDatabaseHelper = new DatabaseHelper(getActivity());

        final AlertDialog.Builder[] builder = {new AlertDialog.Builder(getActivity())};
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_save_prompt, null);
        builder[0].setView(v);
        builder[0].setCancelable(false);

        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        Button btnYes = (Button) v.findViewById(R.id.btnYes);
        Button btnNo = (Button) v.findViewById(R.id.btnNo);

        txtTitle.setText(title);
        txtMessage.setText(message);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();

                switch (actionType) {
                    case Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT:
                        if (NetworkUtils.isNetworkConnected(getActivity())) {
                            Log.e(TAG, "onClick: " + "Connected : " + hasSelectedWebLogo[0] + " - " + hasSelectedMobileLogo[0]);
                            new AsyncSaveSettingUpdates(getActivity(), Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT,
                                    mSettingModel, hasSelectedMobileLogo[0], hasSelectedWebLogo[0], isCopyingSettings).execute();
                            hasSelectedWebLogo[0] = false;
                            hasSelectedMobileLogo[0] = false;

                        } else {
                            Log.e(TAG, "onClick: " + "Not Connected");
                            mDatabaseHelper.updateDbSetting(mSettingModel);
                            mMaintenancePresenter.showAlertDialog("Alert", "Changes have been applied successfully!",
                                    Constants.DIALOG_ACTION_SAVE_DESIGN_FRAGMENT);
                        }
                        break;

                    case Constants.DIALOG_RESET_DESIGN_SETTINGS:
                        mMaintenancePresenter.resetDesign();
                        break;
                }

            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        mAlertSavePromptDialog = builder[0].create();
        mAlertSavePromptDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAlertSavePromptDialog;
    }
}
