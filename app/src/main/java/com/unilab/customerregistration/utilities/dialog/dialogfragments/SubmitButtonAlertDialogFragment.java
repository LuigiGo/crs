package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.converters.ConverterUtil;

public class SubmitButtonAlertDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static SubmitButtonAlertDialogFragment frag;
    AlertDialog mAlertSetSubmitButtonDialog;
    private MaintenancePresenter mMaintenancePresenter;

    public static SubmitButtonAlertDialogFragment newInstance(SettingsModel settingsModel) {
        frag = new SubmitButtonAlertDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("settings_model", settingsModel);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final SettingsModel mSettingModel = getArguments().getParcelable("settings_model");

        mMaintenancePresenter = (MaintenancePresenter) getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),  R.style.CustomDialogFragmentAttributes);
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_config_submit_button, null);
        builder.setView(v);

        /**Views*/
        Button btnSubmitBackgroundColor = (Button) v.findViewById(R.id.btnBackground);
        final Spinner spnBorderRadius = (Spinner) v.findViewById(R.id.spnBorderRadius);
        final Spinner spnBorderWidth = (Spinner) v.findViewById(R.id.spnBorderWidth);
        Button btnContinue = (Button) v.findViewById(R.id.btnSave);

        final ImageView imgAlignLeft = (ImageView) v.findViewById(R.id.imgAlignLeft);
        final ImageView imgAlignCenter = (ImageView) v.findViewById(R.id.imgAlignCenter);
        final ImageView imgAlignRight = (ImageView) v.findViewById(R.id.imgAlignRight);

        switch (mSettingModel.getSubmitAlignment()) {
            case Constants.LEFT_ALIGNMENT:
                imgAlignLeft.setImageResource(R.drawable.ic_selected_left);
                break;
            case Constants.CENTER_ALIGNMENT:
                imgAlignCenter.setImageResource(R.drawable.ic_selected_center);
                break;
            case Constants.RIGHT_ALIGNMENT:
                imgAlignRight.setImageResource(R.drawable.ic_selected_right);
                break;
        }

        btnSubmitBackgroundColor.setBackgroundColor(ConverterUtil.hexColorToInt(mSettingModel.getSubmitBackground(), TAG, Constants.SETTING_TYPE_NONE));
        if (mSettingModel.getSubmitBorderRadius() != null) {
            spnBorderRadius.setSelection(Integer.parseInt(mSettingModel.getSubmitBorderRadius()) - 1);
        }
        if (mSettingModel.getSubmitBorderWidth() != null) {
            spnBorderWidth.setSelection(Integer.parseInt(mSettingModel.getSubmitBorderWidth()) - 1);
        }

        /**Listeners*/
        spnBorderRadius.setOnItemSelectedListener(spinnerListener);
        spnBorderWidth.setOnItemSelectedListener(spinnerListener);
        btnSubmitBackgroundColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMaintenancePresenter.showColorPickerDialog(Constants.SET_SUBMIT_BUTTON_COLOR);
                dismiss();
            }
        });

        imgAlignLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgAlignLeft.setImageResource(R.drawable.ic_selected_left);
                imgAlignCenter.setImageResource(R.drawable.ic_unselected_center);
                imgAlignRight.setImageResource(R.drawable.ic_unselected_right);
                mSettingModel.setSubmitAlignment("left");

            }
        });

        imgAlignCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgAlignLeft.setImageResource(R.drawable.ic_unselected_left);
                imgAlignCenter.setImageResource(R.drawable.ic_selected_center);
                imgAlignRight.setImageResource(R.drawable.ic_unselected_right);
                mSettingModel.setSubmitAlignment("center");
            }
        });

        imgAlignRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgAlignLeft.setImageResource(R.drawable.ic_unselected_left);
                imgAlignCenter.setImageResource(R.drawable.ic_unselected_center);
                imgAlignRight.setImageResource(R.drawable.ic_selected_right);
                mSettingModel.setSubmitAlignment("right");
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertSetSubmitButtonDialog.dismiss();
                String radius = spnBorderRadius.getSelectedItem().toString();
                String width = spnBorderWidth.getSelectedItem().toString();
                mSettingModel.setSubmitBorderRadius(radius);
                mSettingModel.setSubmitBorderWidth(width);
                dismiss();
            }
        });

        mAlertSetSubmitButtonDialog = builder.create();
        mAlertSetSubmitButtonDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAlertSetSubmitButtonDialog;
    }

    AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.w_dialog_submit_button);
        int height = getResources().getDimensionPixelSize(R.dimen.h_dialog_submit_button);
        getDialog().getWindow().setLayout(width, height);
    }
}
