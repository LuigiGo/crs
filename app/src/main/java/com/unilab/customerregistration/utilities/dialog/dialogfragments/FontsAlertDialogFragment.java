package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.maintenance.presenter.MaintenancePresenter;

public class FontsAlertDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static FontsAlertDialogFragment frag;
    AlertDialog mAlertSelectFontDialog;
    private MaintenancePresenter mMaintenancePresenter;

    public static FontsAlertDialogFragment newInstance(SettingsModel settingsModel) {
        frag = new FontsAlertDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("settings_model", settingsModel);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final SettingsModel mSettingModel = getArguments().getParcelable("settings_model");

        mMaintenancePresenter = (MaintenancePresenter) getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomDialogFragmentAttributes);
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_set_fonts, null);
        builder.setView(v);

        final Spinner spnHeadFont = (Spinner) v.findViewById(R.id.spnHeadFont);
        final Spinner spnHeadFontSize = (Spinner) v.findViewById(R.id.spnHeadFontSize);
        final Spinner spnBaseFont = (Spinner) v.findViewById(R.id.spnBaseFont);
        final Spinner spnBaseFontSize = (Spinner) v.findViewById(R.id.spnBaseFontSize);
        Button btnContinue = (Button) v.findViewById(R.id.btnSave);

        /**Listener*/
        spnHeadFont.setOnItemSelectedListener(spinnerListener);
        spnHeadFontSize.setOnItemSelectedListener(spinnerListener);
        spnBaseFont.setOnItemSelectedListener(spinnerListener);
        spnBaseFontSize.setOnItemSelectedListener(spinnerListener);

        /**Setting up selected item*/
        if (mSettingModel.getFontHeadfamily() != null) {
            int selectedPosition = 0;
            Adapter spnHeadFontAdapter = spnHeadFont.getAdapter();
            int spnHeadFontItemCount = spnHeadFontAdapter.getCount();
            for (int i = 0; i < spnHeadFontItemCount; i++) {
                if (spnHeadFontAdapter.getItem(i).equals(mSettingModel.getFontHeadfamily())) {
                    selectedPosition = i;
                }
            }
            spnHeadFont.setSelection(selectedPosition);
        }

        if (mSettingModel.getFontBasefamily() != null) {
            int selectedPosition = 0;
            Adapter spnBaseFontAdapter = spnBaseFont.getAdapter();
            int spnBaseFontItemCount = spnBaseFontAdapter.getCount();
            for (int i = 0; i < spnBaseFontItemCount; i++) {
                if (spnBaseFontAdapter.getItem(i).equals(mSettingModel.getFontBasefamily())) {
                    selectedPosition = i;
                }
            }
            spnBaseFont.setSelection(selectedPosition);
        }

        if (mSettingModel.getFontHeadsize() != null) {
            spnHeadFontSize.setSelection(Integer.parseInt(mSettingModel.getFontHeadsize()) - 1);
        }
        if (mSettingModel.getFontBasesize() != null) {
            spnBaseFontSize.setSelection(Integer.parseInt(mSettingModel.getFontBasesize()) - 1);
        }

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSettingModel.setFontHeadfamily(spnHeadFont.getSelectedItem().toString());
                mSettingModel.setFontHeadsize(spnHeadFontSize.getSelectedItem().toString());
                mSettingModel.setFontBasefamily(spnBaseFont.getSelectedItem().toString());
                mSettingModel.setFontBasesize(spnBaseFontSize.getSelectedItem().toString());

                mMaintenancePresenter.setDesignFonts(mSettingModel);
                dismiss();
            }
        });

        mAlertSelectFontDialog = builder.create();
        mAlertSelectFontDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAlertSelectFontDialog;
    }

    AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.w_dialog_fonts);
        int height = getResources().getDimensionPixelSize(R.dimen.h_dialog_fonts);
        getDialog().getWindow().setLayout(width, height);
    }
}
