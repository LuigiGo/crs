package com.unilab.customerregistration.utilities.dialog;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.view.MaintenanceActivity;
import com.unilab.customerregistration.utilities.sharedprefs.SharedPrefHelper;


public class DialogHelper {

    private final String TAG = this.getClass().getSimpleName();
    private static DialogHelper sDialogHelper;
    private AlertDialog mAlertDialog, mAlertInfoDialog;
    private ProgressDialog mProgressDialog;


    public static void init(Context context) {
        if (sDialogHelper == null) {
            sDialogHelper = new DialogHelper();
            SharedPrefHelper.init(context);
        }
    }

    public static DialogHelper getInstance() {
        if (sDialogHelper != null) {
            return sDialogHelper;
        }
        throw new IllegalStateException("Call DialogHelper init() method");
    }

    public void showProgressDialog(Context context, String message) {
        int currentOrientation = context.getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else {
            ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
        if (context != null && !((Activity) context).isFinishing()) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setProgressStyle(R.style.CustomProgressDialogTheme);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.custom_progress_dialog_loader);

        }

    }

    public void hideProgressDialog(Context context) {
        try {
            ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            if (mProgressDialog != null)
                mProgressDialog.dismiss();
        } catch (Exception e) {
            Log.e(TAG, "hideProgressDialog: " + e.getMessage());
            mProgressDialog.dismiss();
        }

    }
}
