package com.unilab.customerregistration.utilities.dialog.dialogfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unilab.customerregistration.R;
import com.unilab.customerregistration.features.maintenance.model.SettingsModel;
import com.unilab.customerregistration.features.registration.model.RegistrationModel;
import com.unilab.customerregistration.features.registration.presenter.RegistrationContract;
import com.unilab.customerregistration.utilities.constants.Constants;
import com.unilab.customerregistration.utilities.converters.ConverterUtil;
import com.unilab.customerregistration.utilities.utils.Utils;

public class RegistrationUpdateAndSavePromptDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    private static RegistrationUpdateAndSavePromptDialogFragment frag;
    AlertDialog mAlertSavePromptDialog;
    private RegistrationContract mRegistrationContract;
    private RegistrationContract.DialogListener mRegistrationContractDialogListener;
    private Typeface tfBase;

    public static RegistrationUpdateAndSavePromptDialogFragment
    newInstance(String title, String message, String actionType, SettingsModel settingsModel, RegistrationModel registrationModel) {

        frag = new RegistrationUpdateAndSavePromptDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        args.putString("action_type", actionType);
        args.putParcelable("settings_model", settingsModel);
        args.putParcelable("registration_model", registrationModel);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String message = getArguments().getString("message");
        final String actionType = getArguments().getString("action_type");
        final SettingsModel mSettingsModel = getArguments().getParcelable("settings_model");
        final RegistrationModel mRegistrationModel = getArguments().getParcelable("registration_model");

        mRegistrationContract = (RegistrationContract) getActivity();
        mRegistrationContractDialogListener = (RegistrationContract.DialogListener) getActivity();

        final AlertDialog.Builder[] builder = {new AlertDialog.Builder(getActivity())};
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.bg_dialog_registration_save_prompt, null);
        builder[0].setView(v);
        builder[0].setCancelable(false);

        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        Button btnYes = (Button) v.findViewById(R.id.btnYes);
        Button btnNo = (Button) v.findViewById(R.id.btnNo);

        txtTitle.setText(title);
        txtMessage.setText(message);

        /**Setting up dialog style*/
        if (mSettingsModel != null) {
            tfBase = Typeface.createFromAsset(getActivity().getAssets(),
                    Utils.getDefaultTypeface(mSettingsModel.getFontBasefamily()));
            txtTitle.setTypeface(tfBase);
            txtMessage.setTypeface(tfBase);
            btnYes.setTypeface(tfBase);
            btnNo.setTypeface(tfBase);

            txtTitle.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            txtMessage.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
            btnNo.setTextColor(ConverterUtil.hexColorToInt(mSettingsModel.getForeground(), TAG, Constants.SETTING_TYPE_TEXTCOLOR));
        }

        Log.e(TAG, "onCreateDialog: " + mRegistrationModel.toString());

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "onClick: " + actionType);
                switch (actionType) {
                    case Constants.PROMPT_EXISTING_REG_DETAILS:
                        mRegistrationContractDialogListener.promptExistingRegistrationDataDetected(mRegistrationModel);
                        break;

                    case Constants.UPDATE_REG_DETAILS:
                        mRegistrationContractDialogListener.updateExistingRegistrationData(mRegistrationModel);
                        break;

                    case Constants.CREATE_REG_DETAILS:
                        mRegistrationContractDialogListener.saveRegistrationDetails(mRegistrationModel);
                        break;
                }

                dismiss();

            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (actionType) {
                    case Constants.PROMPT_EXISTING_REG_DETAILS:
//                        mRegistrationContract.refreshRegistrationForm();

                        dismiss();
                        break;
                }
                dismiss();
            }
        });


        mAlertSavePromptDialog = builder[0].create();
        mAlertSavePromptDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return mAlertSavePromptDialog;
    }
}
