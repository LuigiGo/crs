package com.unilab.customerregistration.utilities.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.unilab.customerregistration.utilities.constants.Constants;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class Utils {

    public static void hideKeyboard(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(((Activity) context).getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }

    public static void hideKeyboardInFragment(Context context, IBinder windowToken) {
        InputMethodManager mgr = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken, 0);
    }

    public static String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getDefaultTypeface(String typeface) {
        String font;
        /**Setting typeface*/
        switch (typeface) {
            case "Arial":
                font = Constants.FONT_ARIAL;
                break;

            case "Calibri":
                font = Constants.FONT_CALIBRI;
                break;

            case "Comic Sans":
                font = Constants.FONT_COMICSANS;
                break;

            case "Consolas":
                font = Constants.FONT_CONSOLAS;
                break;

            case "Helvetica":
                font = Constants.FONT_HELVETICA;
                break;

            case "Tahoma":
                font = Constants.FONT_TAHOMA;
                break;

            case "Garamond":
                font = Constants.FONT_GARAMOND;
                break;

            case "Freestyle Script":
                font = Constants.FONT_FREESTYLESCRIPT;
                break;

            case "Lucida Console":
                font = Constants.FONT_LUCIDACONSOLE;
                break;

            case "Myriad":
                font = Constants.FONT_MYRIAD;
                break;

            default:
                font = Constants.FONT_ARIAL;
                break;
        }
        return font;
    }


    public static int getDefaultFontHeadSize(String size) {
        if (size != null || !size.isEmpty()) {
            if (Integer.parseInt(size) <= 16) {
                return 16;
            } else {
                return Integer.parseInt(size);
            }
        } else {
            return 16;
        }
    }

    public static int getDefaultFontBaseSize(String size) {
        if (size != null || !size.isEmpty()) {
            if (Integer.parseInt(size) <= 15) {
                return 15;
            } else {
                return Integer.parseInt(size);
            }
        } else {
            return 15;
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        //check here to KITKAT or new version
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static void loadImageFromDrawable(Context context, ImageView imageView, int drawable) {
        Glide.with(context).load(drawable)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .into(imageView);
    }

    public static void loadImageFromURI(Context context, ImageView imageView, Uri uri, final ProgressBar progressBar) {
        Glide.with(context).load(uri)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .into(imageView);
    }

    public static String getLastWordInString(String path) {
        String TAG = "Utils";

        String[] parts;
        try {
            parts = path.split("/");
            return parts[parts.length - 1];
        } catch (Exception e) {
            Log.e(TAG, "getLastWordInString: ErrMesage: " + e.getMessage() + " ImagePath: " + path);
            return getDateTime();
        }
    }

    public static String convertingSpecialCharacters(String name) {
        byte[] convertingSpecialCharacters;
        try {
            convertingSpecialCharacters = name.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            Log.e("Utils", "convertingSpecialCharacters: ");
            return name.toLowerCase();
        }
        return new String(convertingSpecialCharacters).toLowerCase();
    }
}
